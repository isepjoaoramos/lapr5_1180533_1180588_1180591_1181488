import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../config";

import { Result } from '../../../src/core/logic/Result';

import INoDTO from '../../../src/dto/INoDTO';
import NoController from '../../../src/controllers/NoController';
import INoService from '../../../src/services/IServices/INoService';

describe('no controller', function () {
    beforeEach(function () {
    });

    it('createNo: returns json with values', async function () {

        let body = {
            "nome": "Aliados",
            "latitude": 41.148929,
            "longitude": -8.610876,
            "abreviatura": "ALIAD",
            "isEstacaoRecolha": false,
            "isPontoRendicao": false,
            "tempoMaxParagem": 120,
            "modelo": "esfera"
        };

        let req: Partial<Request> = {};
        req.body = body;

        let res: Partial<Response> = {
            status: sinon.stub(),
            json: sinon.stub()
        };

        (res.status as sinon.SinonStub).returnsThis();

        let next: Partial<NextFunction> = () => { };

        let noServiceClass = require('../../../src/services/NoService').default;

        let noServiceInstance = Container.get(noServiceClass)

        Container.set(config.services.no.name, noServiceInstance);

        noServiceInstance = Container.get(config.services.no.name);

        sinon.stub(noServiceInstance, "createNo").returns(Result.ok<INoDTO>({
            "nome": req.body.nome,
            "latitude": req.body.latitude, "longitude": req.body.longitude, "abreviatura": req.body.abreviatura, "isEstacaoRecolha": req.body.isEstacaoRecolha, "isPontoRendicao": req.body.isPontoRendicao,
            "capacidade": req.body.capacidade, "tempoMaxParagem": req.body.tempoMaxParagem, "modelo": "esfera"
        }));

        const ctrl = new NoController(noServiceInstance as INoService);

        await ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);

        sinon.assert.calledWithMatch(res.status as sinon.SinonStub, 201);
        sinon.assert.calledWith(res.json as sinon.SinonStub, {
            "nome": req.body.nome, "latitude": req.body.latitude, "longitude": req.body.longitude, "abreviatura": req.body.abreviatura, "isEstacaoRecolha": req.body.isEstacaoRecolha, "isPontoRendicao": req.body.isPontoRendicao,
            "capacidade": req.body.capacidade, "tempoMaxParagem": req.body.tempoMaxParagem, "modelo": req.body.modelo
        });
    });
});



// describe('get nos', function () {
//     beforeEach(function () {
//     });

//     it('getNosAbrevNome: returns json with path atributes ', async function () {
//         let body = {
//             "nome": "",
//             "abreviatura": "R",
//             "opcao": "1"
//         };
//         let req: Partial<Request> = {};
//         req.body = body;

//         let res: Partial<Response> = {
//             status: sinon.stub(),
//             json: sinon.stub()
//         };
//         (res.status as sinon.SinonStub).returnsThis();
//         let next: Partial<NextFunction> = () => { };

//         let noServiceClass = require('../../../src/services/NoService').default;
//         let noServiceInstance = Container.get(noServiceClass)
//         Container.set(config.services.no.name, noServiceInstance);

//         noServiceInstance = Container.get(config.services.no.name);

//         sinon.stub(noServiceInstance, "getNosAbrevNome").returns(Result.ok<INoDTO>({
//             "nome": req.body.nome, "latitude": req.body.latitude, "longitude": req.body.longitude,
//             "abreviatura": req.body.abreviatura, "isEstacaoRecolha": req.body.isEstacaoRecolha,
//             "isPontoRendicao": req.body.isPontoRendicao, "capacidade": req.body.capacidade,
//             "tempoMaxParagem": req.body.tempoMaxParagem
//         }));

//         const ctrl = new NoController(noServiceInstance as INoService);

//         await ctrl.getNosByAbrevNome(<Request>req, <Response>res, <NextFunction>next);

//         sinon.assert.calledWithMatch(res.status as sinon.SinonStub, 201);
//         sinon.assert.calledWith(res.json as sinon.SinonStub, {
//             "nome": req.body.nome, "latitude": req.body.latitude, "longitude": req.body.longitude,
//             "abreviatura": req.body.abreviatura, "isEstacaoRecolha": req.body.isEstacaoRecolha,
//             "isPontoRendicao": req.body.isPontoRendicao, "capacidade": req.body.capacidade,
//             "tempoMaxParagem": req.body.tempoMaxParagem
//         });
//     });
//});