import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../config";

import { Result } from '../../../src/core/logic/Result';

import IPercursoService from "../../../src/services/IServices/IPercursoService";
import PercursoController from "../../../src/controllers/percursoController";
import IPercursoDTO from '../../../src/dto/IPercursoDTO';
import { SingleAssociationAccessors } from 'sequelize/types';

describe('percurso controller', function () {
	beforeEach(function() {
    });

    it('createPercurso: returns json with path atributes ', async function () {
        let body = { "orientacao":'ida', "descricao":'Bolhão-Campanhã', "linha":'azul',
        "segmentos":[{"no1":'3',"no2":'4',"distancia":'2',"duracao":'3',"ordem":'1'},{"no1":'4',"no2":'18',"distancia":'2',
        "duracao":'3',"ordem":'2'}]};
        let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
            status: sinon.stub(),
            json: sinon.stub()
        };
        (res.status as sinon.SinonStub).returnsThis();
		let next: Partial<NextFunction> = () => {};

		let percursoServiceClass = require('../../../src/services/percursoService').default;
		let percursoServiceInstance = Container.get(percursoServiceClass)
		Container.set(config.services.percurso.name, percursoServiceInstance);

        percursoServiceInstance = Container.get(config.services.percurso.name);
        var distanciaTotal=0;
        for(var i = 0; i < req.body.segmentos.length; i++){
        distanciaTotal += parseFloat(req.body.segmentos[i].distancia);
        }
        
        var duracaoTotal=0;
        for(var i = 0; i < req.body.segmentos.length; i++){
        duracaoTotal += parseFloat(req.body.segmentos[i].duracao);
        }

        req.body.distancia=distanciaTotal;
        req.body.duracao=duracaoTotal;
        req.body.noInicio=req.body.segmentos[0].no1;
        req.body.noFim=req.body.segmentos[req.body.segmentos.length-1].no2;

        sinon.stub(percursoServiceInstance, "createPercurso").returns( Result.ok<IPercursoDTO>( {"orientacao": req.body.orientacao, 
        "distancia": req.body.distancia, "descricao": req.body.descricao, "duracao": req.body.duracao, "linha": req.body.linha, 
        "noInicio": req.body.noInicio, "noFim": req.body.noFim, "segmentos": req.body.segmentos} ));

		const ctrl = new PercursoController(percursoServiceInstance as IPercursoService);

		await ctrl.createPercurso(<Request>req, <Response>res, <NextFunction>next);

		sinon.assert.calledWithMatch(res.status as sinon.SinonStub, 201);
		sinon.assert.calledWith(res.json as sinon.SinonStub,{"orientacao": req.body.orientacao, 
        "distancia": req.body.distancia, "descricao": req.body.descricao, "duracao": req.body.duracao, "linha": req.body.linha, 
        "noInicio": req.body.noInicio, "noFim": req.body.noFim, "segmentos": req.body.segmentos});
	});
});


/*
describe('get percursos', function () {
	beforeEach(function() {
    });

    it('getPercursos: returns json with path atributes ', async function () {
        let body = { "linha":'L1'};
        let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
            status: sinon.stub(),
            json: sinon.stub()
        };
        (res.status as sinon.SinonStub).returnsThis();
		let next: Partial<NextFunction> = () => {};

		let percursoServiceClass = require('../../../src/services/percursoService').default;
		let percursoServiceInstance = Container.get(percursoServiceClass)
		Container.set(config.services.percurso.name, percursoServiceInstance);

        percursoServiceInstance = Container.get(config.services.percurso.name);

        sinon.stub(percursoServiceInstance, "getPercursos").returns( Result.ok<IPercursoDTO>( {"orientacao": req.body.orientacao, 
        "distancia": req.body.distancia, "descricao": req.body.descricao, "duracao": req.body.duracao, "linha": req.body.linha, 
        "noInicio": req.body.noInicio, "noFim": req.body.noFim, "segmentos": req.body.segmentos} ));

		const ctrl = new PercursoController(percursoServiceInstance as IPercursoService);

		await ctrl.getPercursos(<Request>req, <Response>res, <NextFunction>next);

		sinon.assert.calledWithMatch(res.status as sinon.SinonStub, 201);
		sinon.assert.calledWith(res.json as sinon.SinonStub);
	});
});*/