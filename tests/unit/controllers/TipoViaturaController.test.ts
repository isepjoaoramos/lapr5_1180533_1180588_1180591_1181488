import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../config";

import { Result } from '../../../src/core/logic/Result';

import ITipoViaturaDTO from '../../../src/dto/ITipoViaturaDTO';
import TipoViaturaController from '../../../src/controllers/TipoViaturaController';
import ITipoViaturaService from '../../../src/services/IServices/ITipoViaturaService';

describe('tipoViatura controller', function () {
	beforeEach(function() {
    });

    it('createTipoViatura: returns json with values', async function () {
        
        let body = {"codTipoViatura": "12345678912345678912",
                    "descricao": "viatura1",
                    "velocidade_media": 60,
                    "tipocombustivel": 1,
                    "consumo_medio": 25,
                    "custo_km": 10,
                    "autonomia": 150,
                    "emissoes": 50 };
        
                    let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
            status: sinon.stub(),
			json: sinon.stub()
        };

        (res.status as sinon.SinonStub).returnsThis();

		let next: Partial<NextFunction> = () => {};

		let tipoViaturaServiceClass = require('../../../src/services/TipoViaturaService').default;
        
        let tipoViaturaServiceInstance = Container.get(tipoViaturaServiceClass)
        
        Container.set(config.services.tipoViatura.name, tipoViaturaServiceInstance);

		tipoViaturaServiceInstance = Container.get(config.services.tipoViatura.name);
        
        sinon.stub(tipoViaturaServiceInstance, "createTipoViatura").returns( Result.ok<ITipoViaturaDTO>( {"codTipoViatura": req.body.codTipoViatura, 
        "descricao": req.body.descricao, "velocidade_media": req.body.velocidade_media, "tipocombustivel": req.body.tipocombustivel, "autonomia": req.body.autonomia, 
        "custo_km": req.body.custo_km, "consumo_medio": req.body.consumo_medio, "emissoes": req.body.emissoes} ));

		const ctrl = new TipoViaturaController(tipoViaturaServiceInstance as ITipoViaturaService);

		await ctrl.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);

        sinon.assert.calledWithMatch(res.status as sinon.SinonStub, 201);
		sinon.assert.calledWith(res.json as sinon.SinonStub, { "codTipoViatura": req.body.codTipoViatura, 
        "descricao": req.body.descricao, "velocidade_media": req.body.velocidade_media, "tipocombustivel": req.body.tipocombustivel, "autonomia": req.body.autonomia, 
        "custo_km": req.body.custo_km, "consumo_medio": req.body.consumo_medio, "emissoes": req.body.emissoes});
	});
});