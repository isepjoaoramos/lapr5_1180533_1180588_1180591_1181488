import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../config";

import { Result } from '../../../src/core/logic/Result';

import ITipoTripulanteDTO from '../../../src/dto/ITipoTripulanteDTO';
import TipoTripulanteController from '../../../src/controllers/TipoTripulanteController';
import ITipoTripulanteService from '../../../src/services/IServices/ITipoTripulanteService';

describe('tipoTripulante controller', function () {
	beforeEach(function() {
    });

    it('createTipoTripulante: returns json with values', async function () {
        
        let body = {"codigo": "T1",
                    "descricao": "tripulante noturna",
                    };
        
                    let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
            status: sinon.stub(),
			json: sinon.stub()
        };

        (res.status as sinon.SinonStub).returnsThis();

		let next: Partial<NextFunction> = () => {};

		let tipoTripulanteServiceClass = require('../../../src/services/TipoTripulanteService').default;
        
        let tipoTripulanteServiceInstance = Container.get(tipoTripulanteServiceClass)
        
        Container.set(config.services.tipoTripulante.name, tipoTripulanteServiceInstance);

		tipoTripulanteServiceInstance = Container.get(config.services.tipoTripulante.name);
        
        sinon.stub(tipoTripulanteServiceInstance, "createTipoTripulante").returns( Result.ok<ITipoTripulanteDTO>( {"codigo": req.body.cod, 
        "descricao": req.body.descricao} ));

		const ctrl = new TipoTripulanteController(tipoTripulanteServiceInstance as ITipoTripulanteService);

		await ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

        sinon.assert.calledWithMatch(res.status as sinon.SinonStub, 201);
		sinon.assert.calledWith(res.json as sinon.SinonStub, {"codigo": req.body.cod, 
        "descricao": req.body.descricao});
	});
});