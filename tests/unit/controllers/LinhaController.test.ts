import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../config";

import { Result } from '../../../src/core/logic/Result';

import ILinhaDTO from '../../../src/dto/ILinhaDTO';
import LinhaController from '../../../src/controllers/LinhaController';
import ILinhaService from '../../../src/services/IServices/ILinhaService';

describe('Linha Controller', function () {
	beforeEach(function() {
    });

    it('create Linha: returns json with values', async function () {
        
        let body = {"codLinha": "AB",
                    "nome": "linha1Porto_Gaia",
                    "vermelho": 60,
                    "verde": 20,
                    "azul": 25,
                    "noInicio": "No1",
                    "noFim": "No2",
                    "restricoes": "omissao" };
        
                    let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
            status: sinon.stub(),
			json: sinon.stub()
        };

        (res.status as sinon.SinonStub).returnsThis();

		let next: Partial<NextFunction> = () => {};

		let LinhaServiceClass = require('../../../src/services/LinhaService').default;
        
        let LinhaServiceInstance = Container.get(LinhaServiceClass)
        
        Container.set(config.services.linha.name, LinhaServiceInstance);

		LinhaServiceInstance = Container.get(config.services.linha.name);
        
        sinon.stub(LinhaServiceInstance, "createLinha").returns( Result.ok<ILinhaDTO>( {"codLinha": req.body.codLinha, 
        "nome": req.body.nome, "vermelho": req.body.vermelho, "verde": req.body.verde, "azul": req.body.azul, 
        "noInicio": req.body.noInicio, "noFim": req.body.noFim, "restricoes": req.body.restricoes} ));

		const ctrl = new LinhaController(LinhaServiceInstance as ILinhaService);

		await ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);

        sinon.assert.calledWithMatch(res.status as sinon.SinonStub, 201);
		sinon.assert.calledWith(res.json as sinon.SinonStub, {"codLinha": req.body.codLinha, 
        "nome": req.body.nome, "vermelho": req.body.vermelho, "verde": req.body.verde, "azul": req.body.azul, 
        "noInicio": req.body.noInicio, "noFim": req.body.noFim, "restricoes": req.body.restricoes});
	});
});