import { Segmento } from "../../../src/domain/segmento";
import ISegmentoDTO from "../../../src/dto/ISegmentoDTO"
import { Percurso } from "../../../src/domain/percurso"
import { orientacao } from "../../../src/domain/Orientacao"

let chai = require('chai');

var assert = chai.assert;

let seg1 = ({
    no1: "AGUIA",
    no2: "PARED",
    distancia: 90,
    duracao: 120,
    ordem: 1
})
let seg2 = ({
    no1: "PARED",
    no2: "BALTR",
    distancia: 50,
    duracao: 90,
    ordem: 2
})

let segs: Array<ISegmentoDTO> = []
segs.push(seg1);
segs.push(seg2);

let seg3 = ({
    no1: "AGUIA",
    no2: "PARED",
    distancia: 90,
    duracao: 120,
    ordem: 2
})
let seg4 = ({
    no1: "PARED",
    no2: "BALTR",
    distancia: 50,
    duracao: 90,
    ordem: 2
})

let segs2: Array<ISegmentoDTO> = []
segs2.push(seg3);
segs2.push(seg4);
var ori = orientacao.ida;

describe("Domínio do Percurso - Testes", () => {

    it("Create - válido", () => {
        assert.equal(
            Percurso.create({
                orientacao: ori,
                distancia: 140,
                descricao: "AGUIA>PARED>BALTR",
                duracao: 210,
                linha: "Paredes_Aguiar",
                noInicio: "AGUIA",
                noFim: "BALTR",
                segmentos: segs
            }).isFailure,false
        )
    });

    it("Create - inválido", () => {
        assert.equal(
            Percurso.create({
                orientacao: ori,
                distancia: -1,
                descricao: "AGUIA>PARED>BALTR",
                duracao: 210,
                linha: "Paredes_Aguiar",
                noInicio: "PARED",
                noFim: "BALTR",
                segmentos: segs2
            }).isFailure,true
        )
    });

})

