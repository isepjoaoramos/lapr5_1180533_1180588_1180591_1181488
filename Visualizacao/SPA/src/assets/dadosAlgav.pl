% Base de Conhecimentos
% no(nome_paragem, abrev_paragem, flag_ponto_rendição, flag_estação_recolha,longitude, latitude)

% linha(nome_linha, número_linha, lista_abrev_paragens, tempo_minutos, distância_metros)

% viagem(id_viagem, número_linha, id_horario)

% horario(número_linha, lista_segundos).
horario(1, [36000,36540,36840,37140,37620]).
horario(1, [37800,38340,38640,38940,39420]).
horario(1, [39600,40140,40440,40740,41220]).
horario(1, [41400,41940,42240,42540,43020]).

horario(3, [73800,74280,74580,74880,75420]).
horario(3, [72900,73380,73680,73980,75420]).
horario(3, [72000,72480,72780,73080,73620]).
horario(3, [71100,71580,71880,72180,72720]).

horario(5, [30360,30960,31200,31440,31920]).
horario(5, [33960,34560,34800,35040,35520]).
horario(5, [37560,38160,38400,38640,39120]).
horario(5, [41160,41760,42000,42240,42720]).

horario(8, [28860,29340,29580,29820,30360]).
horario(8, [32400,32880,33120,33360,33960]).
horario(8, [36000,36480,36720,36960,37560]).
horario(8, [39600,40080,40320,40560,41160]).

horario(9, [30180,30480,30720,30960,31320,31560]).
horario(9, [31380,31680,31920,32160,32520,32760]).
horario(9, [32580,32880,33120,33360,33720,33960]).
horario(9, [33780,34080,34320,34560,34920,35160]).

horario(11, [36000,36240,36600,36840,37080,37380]).
horario(11, [37200,37440,37800,38040,38280,38580]).
horario(11, [38400,38640,39000,39240,39480,39780]).
horario(11, [39600,39840,40200,40440,40680,40980]).

horario(24, [71580,71880,72120,72360,72600,72900]).
horario(24, [70260,70560,70800,71040,71280,71580]).
horario(24, [68940,69240,69480,69720,69960,70260]).
horario(24, [67620,67920,68160,68400,68640,68940]).

horario(26, [33600,33900,34140,34380,34620,34920]).
horario(26, [32280,32580,32820,33060,33300,33600]).
horario(26, [30960,31260,31500,31740,31980,32280]).
horario(26, [29640,29940,30180,30420,30660,30960]).

horario(22, [28980,29280,29520,29760,30120,30360]).
horario(22, [30180,30480,30720,30960,31320,31560]).
horario(22, [31380,31680,31920,32160,32520,32760]).
horario(22, [32580,32880,33120,33360,33720,33960]).

horario(20, [34800,35040,35400,35640,35880,36180]).
horario(20, [33600,33840,34200,34440,34680,34980]).
horario(20, [32400,32640,33000,33240,33480,33780]).
horario(20, [31200,31440,31800,32040,32280,32580]).

horario(34, [26580,26700]).
horario(34, [27900,28020]).
horario(34, [29220,29340]).

horario(35, [73200,73320]).
horario(35, [74520,74640]).
horario(35, [75840,75960]).

horario(36, [27300,27600]).
horario(36, [28500,28800]).
horario(36, [29700,30000]).

horario(37, [77160,77460]).
horario(37, [74760,75060]).
horario(37, [75960,76260]).

horario(38, [27300,27420]).
horario(38, [28200,28320]).
horario(38, [28740,28860]).
horario(38, [29100,29220]).

horario(39, [76320,76440]).
horario(39, [89820,89940]).
horario(39, [82320,82440]).
horario(39, [75900,76020]).

% Bibliotecas
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_server)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_open)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)).
:- use_module(library(uri)).
:- use_module(library(http/http_cors)).
:- use_module(library(http/http_json)).
:- use_module(library(http/http_session)).
:- use_module(library(http/json_convert)).

% Cors
:- set_setting(http:cors, [*]).

% Predicado a ser executado quando e feito um pedido no url /mudanca_motoristas
:- http_handler(root(mudanca_motoristas), plan_mud_mot_http, []).
:- http_handler(root(algoritmo_genetico), algoritmo_genetico_http, []).

% Inicializacao do servidor
server(Port) :- http_server(http_dispatch, [port(Port)]).

% Base conhecimento dinâmica
:- dynamic liga/3.
:- dynamic melhor_sol_ntrocas/2.
:- dynamic edge/4.
:- dynamic no/6.
:- dynamic linhaAtlas/2.
:- dynamic percurso/4.
:- dynamic linha/5.
:- dynamic doadorCodigoLinha/1.

gera_edges(HInicio):-retractall(edge(_,_,_,_,_)),
    findall(_,
            ((no(_,Act,@(true),@(false),_,_);no(_,Act,@(false),@(true),_,_)),
            linha(_,NLinha,LNos,_,_),  
            member(Act,LNos),
            nth1(Pos,LNos,Act),
            length(LNos,X),
            cria_edges(Act,Pos,LNos,X,NLinha,HInicio))
        ,_).

cria_edges(_,X,_,X,_,_):- !.

cria_edges(Act,Pos,LNos,X,NLinha,HInicio):-
    Pos1 is Pos+1,
    nth1(Pos1,LNos,Vizinho),
    ((no(_,Vizinho,@(true),@(false),_,_);no(_,Vizinho,@(false),@(true),_,_)),!,
        cria_edge(Act,Vizinho,LNos,NLinha,HInicio)),
    cria_edges(Act,Pos1,LNos,X,NLinha,HInicio).

cria_edge(Act,Vizinho,LNos,NLinha,HInicio):-
    % Vai buscar a posicao dos dois nos
    nth1(Pos1,LNos,Act),
    nth1(Pos2,LNos,Vizinho),

    % Vai buscar os horarios da linha NLinha
    horario(NLinha,LHorarios),
    !,
    % Descobre os horarios no qual passa nos dois nos e calcula o custo
    nth1(Pos1,LHorarios,HorarioNo1),
    nth1(Pos2,LHorarios,HorarioNo2),
    HorarioNo1>HInicio,

    % Cria edge
    assertz(edge(Act,Vizinho,NLinha,HorarioNo1,HorarioNo2)).

caminho2(No,No,Horario,Lusadas,Lfinal,Horario):-!,reverse(Lusadas,Lfinal).

caminho2(No1,Nof,Horario,Lusadas,Lfinal,HorarioChegada):-
    edge(No1,No2,N,H1,H2),
    H1 > Horario,
    \+member((_,_,N),Lusadas),
    \+member((No2,_,_),Lusadas),
    \+member((_,No2,_),Lusadas),
    caminho2(No2,Nof,H2,[(No1,No2,N)|Lusadas],Lfinal,HorarioChegada).

plan_mud_mot_horarios(Noi,Nof,HInicio,Caminho):-
    format('Content-type: text/plain~n'),
    get_time(Ti),
    %apagaBaseConhecimento(),
    %adicionaNos(),
    %adicionaLinhas(),
    gera_edges(HInicio),
    assert(nSol(0)),
    (melhor_caminho_horarios(Noi,Nof,HInicio);true),
    retract(melhor_sol_ntrocas(Caminho,C)),
    retract(nSol(NSolucoes)),
    get_time(Tf),
    TSol is Tf-Ti,
    write('Numero de solucoes: '),write(NSolucoes),nl,
    write('Tempo de geracao da solucao:'),write(TSol),nl,
    write('Hora Chegada: '),write(C),nl.

melhor_caminho_horarios(Noi,Nof,Horario):-
            asserta(melhor_sol_ntrocas(_,99999999)),
            caminho2(Noi,Nof,Horario,[],LCaminho,HorarioChegada),
            atualiza_melhor_horarios(LCaminho,HorarioChegada),
            fail.

atualiza_melhor_horarios(LCaminho,Custo):-
            retract(nSol(NSol)),
            NSol1 is NSol+1,
            assert(nSol(NSol1)),
            melhor_sol_ntrocas(_,MenorCusto),
            Custo<MenorCusto,retract(melhor_sol_ntrocas(_,_)),
            asserta(melhor_sol_ntrocas(LCaminho,Custo)).

apagaBaseConhecimento():-
    retractall(no(_,_,_,_,_,_)),
    retractall(linha(_,_,_,_,_)),
    retractall(edge(_,_,_,_)),
    retractall(melhor_sol_ntrocas(_,_)).

adicionaNos():-
    % Envia pedido GET (nos)
    http_get("http://localhost:3000/api/no",Json,[request_header('Accept'='application/json')]), 
    format('Content-type: text/plain~n~n'),
    json_to_prolog(Json, Nos),    
    assertaNos(Nos).

assertaNos([]) :- !.

assertaNos([H|Nos]):-
    arg(1,H,No),
    asserta(no(No.nome,No.abreviatura,No.isPontoRendicao,No.isEstacaoRecolha,No.longitude,No.latitude)),
    assertaNos(Nos).

adicionaLinhas() :-
    retractall(doadorCodigoLinha(_)),
    assert(doadorCodigoLinha(0)),

    % Envia pedido GET (percursos)  
    http_get("http://localhost:3000/api/percursos",Json1,[request_header('Accept'='application/json')]),
    json_to_prolog(Json1, Percursos),
    assertaPercursos(Percursos).

assertaPercursos([]):- !,
    format('Content-type: text/plain~n'),
    write('Fiz assert das linhas.'),nl.

assertaPercursos([H|[PropsJson|Percursos]]):-
    % Ignorar variaveis PropsX. Usadas para percorrer
    % o documento json e guardar as variaveis de forma independente
    arg(1,PropsJson,Props),
    arg(2,Props,Props1),
    arg(2,Props1,Props2),
    arg(2,Props2,Props3),
    arg(2,Props3,Props4),
    arg(2,Props4,Props5),
    arg(1,Props5,Props6),
    arg(2,Props6,NomeLinha),
    
    % Ignorar variaveis PropsX.
    arg(2,Props5,Props7),
    arg(2,Props7,Props8),
    arg(2,Props8,Props9),
    
    arg(1,Props9,Props10),
    % Lista segmentos Json
    arg(2,Props10,ListaSegmentosJson),

    % Lista de nos pronta a fazer assert
    processaSegmentos(ListaSegmentosJson,ListaNos, 0, ListaFinal, Tempo, Distancia),

    retract(doadorCodigoLinha(UltimoCodigo)),
    Codigo is UltimoCodigo+1,
    
    assert(linha(NomeLinha,Codigo,ListaFinal,Tempo,Distancia)),
    assert(doadorCodigoLinha(Codigo)),

    % Passa para o proximo percurso
    assertaPercursos(Percursos).

processaSegmentos([], ListaNos, ListaFinal, Tempo, Distancia) :- reverse(ListaNos,ListaFinal),!. 

processaSegmentos(ListaSegmentos, ListaNos, ListaFinal, Tempo, Distancia):-
    arg(1,ListaSegmentos,SegmentoHeader),
    arg(2,ListaSegmentos,RestoLista),
    arg(1,SegmentoHeader,Props1),
    arg(2,Props1,Props2),
    arg(2,Props2,Props3),
    arg(1,Props3,Props4),
    arg(2,Props4,No2),
    
    processaSegmentos(RestoLista, [No2|ListaNos], ListaFinal, Tempo, Distancia).

processaSegmentos([],_,0,ListaFinal, Tempo, Distancia) :- !.

processaSegmentos(ListaSegmentos, ListaNos, 0, ListaFinal, Tempo, Distancia):-
    arg(1,ListaSegmentos,SegmentoHeader),
    arg(2,ListaSegmentos,RestoLista),
    arg(1,SegmentoHeader,Props1),
    arg(2,Props1,Props2),
    arg(1,Props2,Props3),

    % Guarda no1 do segmento
    arg(2,Props3,No1),

    arg(2,Props2,Props4),
    arg(1,Props4,Props5),

    % Guarda No2 do segmento
    arg(2,Props5,No2),

    arg(2,Props4,Props6),
    arg(1,Props6,Props7),
    arg(2,Props6,Props8),
    arg(1,Props8,Props9),
    
    % Duracao do segmento
    arg(2,Props9,Tempo),

    % Distancia do segmento
    arg(2,Props7,Distancia),

    processaSegmentos(RestoLista, [No2,No1|ListaNos], ListaFinal, Tempo, Distancia).
    

plan_mud_mot_http(Request):-

    apagaBaseConhecimento(),
    adicionaNos(),
    adicionaLinhas(),

    % Interpreta parametros (embebidos no url) 
    http_parameters(Request,
                    [ no_inicio(NoI, []),
                      no_fim(NoF, []),
                      horario(HInicio, [between(0,100000)])
                    ]),
    plan_mud_mot_horarios(NoI,NoF,HInicio,Caminho),
	format('Caminho: ~w~n',[Caminho]).

algoritmo_genetico_http(Request):-

    % Interpreta parametros embebidos no url
    http_parameters(Request,[
        dimensaoPop(DP,[]),
        numNovasGeracoes(NG,[]),
        probCruzamento(PC_Percent, []),
        probMutacao(PM_Percent, []),
        tempoLimite(TempoLimite, []),
        menorAvaliacao(MenorAvaliacao, []),
        fatorEstabilizacao(NumeroEstabilizacao, [])
    ]),
    format('Hello world'),
    % executa algoritmo
    format('DP: ~w~n | NG: ~w~n | PC_Percent: ~w~n | PM_Percent: ~w~n | Tempo: ~w~n | MenorAvaliacao : ~w~n | Estabilizacao: ~w~n', [DP, NG, PC_Percent, PM_Percent, TempoLimite, MenorAvaliacao, NumeroEstabilizacao]).