import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import jwt_decode from 'jwt-decode';
import { FormBuilder, Validators } from '@angular/forms';
import { RegistoService } from '../services/registo.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  //createHomeForm;
  username: string;
  constructor(
    //private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private registoService: RegistoService
  ) {
    /* this.createHomeForm = this.formBuilder.group({
      Username: [userName],
      Email: ''
    }); */
    //document.querySelector('.results').innerHTML = userName;
  }
  ngOnInit(): void {
    this.apresenta();
  }

  apresenta() {
    var decoded = jwt_decode(this.authenticationService.userInfo.token);
    this.username = (decoded as any).user;
    console.log((decoded as any).user);

    document.getElementById('UsernameDiv').innerHTML = this.username;
  }

  apagarUser() {
    this.registoService.removeUser(this.username);
  }
}
