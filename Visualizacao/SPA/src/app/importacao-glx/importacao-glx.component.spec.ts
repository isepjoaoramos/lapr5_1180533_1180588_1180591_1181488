import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportacaoGlxComponent } from './importacao-glx.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('LinhaComponent', () => {
  let component: ImportacaoGlxComponent;
  let fixture: ComponentFixture<ImportacaoGlxComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportacaoGlxComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ ImportacaoGlxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportacaoGlxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});