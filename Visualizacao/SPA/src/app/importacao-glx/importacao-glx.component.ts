import { Component, OnInit } from '@angular/core';
import { ImportacaoGlxService } from '../services/importacao-glx.service';

@Component({
  selector: 'app-importacao-glx',
  templateUrl: './importacao-glx.component.html',
  styleUrls: ['./importacao-glx.component.css']
})
export class ImportacaoGlxComponent implements OnInit {
  // Variable to store shortLink from api response 
  shortLink: string = ""; 
  loading: boolean = false; // Flag variable 
  file: File = null as any; // Variable to store file

  constructor(private importacaoGlxService : ImportacaoGlxService) { }

  ngOnInit(): void { 
  }

  // On file Select 
  onChange(event : any) { 
    this.file = event.target.files[0];
    console.log("file! "+this.file.name); 
    var btn = document.getElementById("btn-readyToUpload");
    if (btn != null) { 
      btn.style.visibility="visible";
    }
  }

  // On file Select 
  onChangeMdv(event : any) { 
    this.file = event.target.files[0];
    console.log("file! "+this.file.name); 
    var btn = document.getElementById("btn-readyToUploadMdv");
    if (btn != null) { 
      btn.style.visibility="visible";
    }
  }

  // OnClick of button Upload 
  onUploadMdr() { 
    this.loading = !this.loading; 
    console.log(this.file); 
    this.importacaoGlxService.uploadMdr(this.file).subscribe( 
        (event: any) => { 
            if (typeof (event) === 'object') { 

                // Short link via api response 
                this.shortLink = event.link; 

                this.loading = false; // Flag variable  
            } 
        } 
    ); 
  }
  
  onUploadMdv() { 
    this.loading = !this.loading; 
    console.log(this.file); 
    this.importacaoGlxService.uploadMdv(this.file).subscribe( 
        (event: any) => { 
            if (typeof (event) === 'object') { 

                // Short link via api response 
                this.shortLink = event.link; 

                this.loading = false; // Flag variable  
            } 
        } 
    ); 
  }
}
