import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class DataAdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate() {
    if (this.authenticationService.userInfo) {

      if (this.authenticationService.userInfo.data_admin) {

        return true;
      }else{
        this.router.navigate(['/login', { u: 'no' }]);
        return false;
      }
    }
    return false;
    
  }
}
