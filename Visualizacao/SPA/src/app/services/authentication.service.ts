import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import Login from '../model/Login';
import Token from '../model/Token';
import { environment } from '../../environments/environment';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { Subject } from 'rxjs';
import jwt_decode from 'jwt-decode';
import * as jwt from 'jsonwebtoken';
import { User } from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private authUrl = environment.baseUrl + '/login';

  public userInfo: User;
  authentication: Subject<User> = new Subject<User>();

  constructor(private http: HttpClient) {
    this.userInfo = localStorage.userInfo;
  }

  doLogin(login: Login): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.http.post<Token>(this.authUrl, login).subscribe(
        data => {
          if (data.token) {
            console.log('token : ' + data.token);

            const tokenDecoded = jwt_decode<User>(data.token);
            //var tokenDecoded = jwt_decode(data.token);
            if (tokenDecoded != null) {
              this.userInfo = {
                token: data.token,
                tokenExp: tokenDecoded.tokenExp,
                cliente: tokenDecoded.cliente,
                data_admin: tokenDecoded.data_admin,
                gestor: tokenDecoded.gestor
              };
              console.log(this.userInfo);
            }
            localStorage.setItem('currentUser', JSON.stringify(this.userInfo));
            //localStorage.userInfo = this.userInfo;
            console.log(localStorage);
            this.authentication.next(this.userInfo);
            observer.next(true);
          } else {
            this.authentication.next(this.userInfo);
            observer.next(false);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('Client-side error occured.');
          } else {
            console.log('Server-side error occured.');
          }

          console.log(err);

          this.authentication.next(this.userInfo);
          observer.next(false);
        }
      );
    });
  }

  logout() {
    this.userInfo = undefined;
    localStorage.removeItem('userInfo');
    this.authentication.next(this.userInfo);
  }

  private getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
}
