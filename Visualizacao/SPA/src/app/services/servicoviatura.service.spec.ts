import { TestBed } from '@angular/core/testing';

import { ServicoviaturaService } from './servicoviatura.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'
import ServicoViatura from '../model/ServicoViatura'

import { Viagem } from '../model/Viagem';

import { environment } from '../../environments/environment';

describe('ViaturaService', () => {
  let service: ServicoviaturaService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ServicoviaturaService]
    });
    service = TestBed.inject(ServicoviaturaService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('addServicoViatura test with HTTP mock', () => {

    //Arrange
    //Set Up Data 

    const viagens = [{
      viagemId: 1,
      linha: "linha1",
      horaSaida: 100,
      percurso: "percurso1"
    }];

    const servicoViatura_toCreate: ServicoViatura = { idServicoViatura: 1, codServicoViatura: "123", descricao: "ServicoViatura2", viagens: viagens as Viagem[] }

    const expected_ServicoViatura: ServicoViatura = { idServicoViatura: 1, codServicoViatura: "123", descricao: "ServicoViatura2", viagens: viagens as Viagem[] }

    //Act

    service.addServicoViatura(servicoViatura_toCreate).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expected_ServicoViatura)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/servicoviatura');

    //Assert -3
    httpMock.verify();

  });



  it('getByCodServicoViatura test with HTTP mock', () => {


    const viagens = [{
      viagemId: 1,
      linha: "linha1",
      horaSaida: 100,
      percurso: "percurso1"
    }];

    //Arrange
    //Set Up Data 
    const servicoViatura_expected: ServicoViatura = { idServicoViatura: 1, codServicoViatura: "123", descricao: "ServicoViatura2", viagens: viagens as Viagem[] }
    const codServicoViatura = "123"
    //Act

    service.getByCodServicoViatura(codServicoViatura).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(servicoViatura_expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/servicoviatura' + '/codservicoviatura=123');

    //Assert -3
    httpMock.verify();

  });


  it('getAllViaturas test with HTTP mock', () => {

    const viagens = [{
      viagemId: 1,
      linha: "linha1",
      horaSaida: 100,
      percurso: "percurso1"
    }];
    

    //Arrange
    //Set Up Data 
    const viaturas_expected: ServicoViatura[] = [{idServicoViatura: 1, codServicoViatura: "123", descricao: "ServicoViatura2", viagens: viagens as Viagem[]},
    { idServicoViatura: 2, codServicoViatura: "345", descricao: "ServicoViatura3", viagens: viagens as Viagem[] }]

    //Act

    service.getAllServicosViatura().subscribe((res) => {
      //Assert-1
      expect(res).toEqual(viaturas_expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/servicoviatura');

    //Assert -3
    httpMock.verify();

  });
});
