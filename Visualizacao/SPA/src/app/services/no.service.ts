import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient ,HttpHeaders} from '@angular/common/http';

import No from '../model/No'
import {AuthenticationService} from '../services/authentication.service'

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NoService {

  private noURL = environment.baseUrl + '/no'

  constructor(private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  addNo(no: No): Observable<No> {
    return this.http.post<No>(this.noURL,no,this.getHeaders());
  }


  getNos():Observable<No[]>{
    return this.http.get<No[]>(this.noURL,this.getHeaders());
  }

  getNoByNome(nomeNo: string):Observable<No>{
    const url= `${this.noURL}?nome=${nomeNo}`;
    return this.http.get<No>(url,this.getHeaders());
  }

  getNoByAbreviatura(abreviatura: string): Observable<No>{
    const url = `${this.noURL}?abreviatura=${abreviatura}`;
    return this.http.get<No>(url,this.getHeaders()); 
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }

}
