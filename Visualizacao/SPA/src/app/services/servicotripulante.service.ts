import { Injectable } from '@angular/core';

import {HttpClient ,HttpHeaders} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import ServicoTripulante from '../model/ServicoTripulante';

import { environment } from '../../environments/environment';
import { List } from 'lodash';
import {AuthenticationService} from '../services/authentication.service'


@Injectable({
  providedIn: 'root'
})
export class ServicoTripulanteService {

  constructor(private http: HttpClient,private authenticationService: AuthenticationService) { }


  private servicoTripulanteURL = environment.mdv_url + '/servicotripulante'


  addServicoTripulante(servicoTripulante: ServicoTripulante): Observable<ServicoTripulante> {
    return this.http.post<ServicoTripulante>(this.servicoTripulanteURL, servicoTripulante,this.getHeaders())
  }

  getByCodServicoTripulante(codServicoTripulante: string): Observable<ServicoTripulante>{
    const url = `${this.servicoTripulanteURL}/codServicoTripulante=${codServicoTripulante}`;
    return this.http.get<ServicoTripulante>(url,this.getHeaders()); 
  }

  getAllServicosTripulante(): Observable<ServicoTripulante[]>{
    return this.http.get<ServicoTripulante[]>(this.servicoTripulanteURL,this.getHeaders());
  }


  getHeaders(){
    let headers = new HttpHeaders({'auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }
}
