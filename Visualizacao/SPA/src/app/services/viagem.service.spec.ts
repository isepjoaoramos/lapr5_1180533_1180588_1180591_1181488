import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { Viagem } from '../model/Viagem';

import { environment } from '../../environments/environment';

import { ViagemService } from './viagem.service';

describe('ViagemService', () => {
  let service: ViagemService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ViagemService]
    });
    service = TestBed.inject(ViagemService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });
  

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addViagem test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const viagem: Viagem = {viagemId:100, linha: "Paredes_Aguiar", horaSaida: 1234, percurso: "PARED>AGUIA" }
    const expectedViagem: Viagem = {viagemId:100, linha: "Paredes_Aguiar", horaSaida: 1234, percurso: "PARED>AGUIA" }

    //Act
    service.addViagem(viagem).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expectedViagem)
    });

    //Assert -2
    httpMock.expectOne(environment.mdv_url + '/viagem');

    //Assert -3
    httpMock.verify();
  });


});
