import { TestBed } from '@angular/core/testing';

import { TipoTripulanteService } from './tipotripulante.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import TipoTripulante from '../model/TipoTripulante';
import { environment } from '../../environments/environment';


describe('TipoTripulanteService', () => {
  let service: TipoTripulanteService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TipoTripulanteService]
    });
    service = TestBed.inject(TipoTripulanteService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addTipoTripulante test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const tipoTripulante_toCreate: TipoTripulante = {tipoTripulanteId:1, codigo: "codigoTeste", descricao: "tipo_teste" }
    const expected_TipoTripulante: TipoTripulante = {tipoTripulanteId:1, codigo: "codigoTeste", descricao: "tipo_teste" }
    //Act

    service.addTipoTripulante(tipoTripulante_toCreate).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expected_TipoTripulante)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/tipoTripulante');

    //Assert -3
    httpMock.verify();

  });

  it('getTipoTripulanteByCod test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const tipoTripulante_Expected: TipoTripulante = {tipoTripulanteId:1, codigo: "codigoTeste", descricao: "tipo_teste" }
    const codigo = "codigoTeste"
    //Act

    service.getTipoTripulanteByCod(codigo).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(tipoTripulante_Expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/tipoTripulante'+'?codigo=codigoTeste');

    //Assert -3
    httpMock.verify();

  });

});
