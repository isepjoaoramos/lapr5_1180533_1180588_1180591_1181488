import { Injectable } from '@angular/core';

import {HttpClient ,HttpHeaders} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import TipoTripulante from '../model/TipoTripulante';
import {AuthenticationService} from '../services/authentication.service'


import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TipoTripulanteService {

  private tipoTripulanteURL = environment.mdv_url + '/tipoTripulante'

  constructor(private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  addTipoTripulante(tipoTripulante: TipoTripulante): Observable<TipoTripulante> {
    console.log("fez pedido");
    return this.http.post<TipoTripulante>(this.tipoTripulanteURL, tipoTripulante)
  }

  getTipoTripulanteByCod(codigo: string): Observable<TipoTripulante>{
    const url = `${this.tipoTripulanteURL}/codigo=${codigo}`;
    return this.http.get<TipoTripulante>(url,this.getHeaders()); 
  }

  getTipoTripulanteByDescricao(descricao: string): Observable<TipoTripulante>{
    console.log(`${this.tipoTripulanteURL}/descricao=${descricao}`);
    const url = `${this.tipoTripulanteURL}/descricao=${descricao}`;
    return this.http.get<TipoTripulante>(url,this.getHeaders()); 
  }

  getAllTipos():Observable<TipoTripulante[]>{
    return this.http.get<TipoTripulante[]>(this.tipoTripulanteURL,this.getHeaders());
  }

  
  getHeaders(){
    let headers = new HttpHeaders({'auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }

}
