import { TestBed } from '@angular/core/testing';

import { NoService } from './no.service';
import No from '../model/No';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'

import { environment } from '../../environments/environment';


describe('NoService', () => {
  let service: NoService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NoService]
    });
    service = TestBed.inject(NoService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addNo test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const no_toCreate: No = { nome: "nomeNo", latitude: 5, longitude: 20, abreviatura: "abrev", isEstacaoRecolha: false, isPontoRendicao: false, capacidade: 2, tempoMaxParagem: 4, modelo: "esfera" }
    const expected_No: No = { nome: "nomeNo", latitude: 5, longitude: 20, abreviatura: "abrev", isEstacaoRecolha: false, isPontoRendicao: false, capacidade: 2, tempoMaxParagem: 4, modelo: "esfera" }
    //Act

    service.addNo(no_toCreate).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expected_No)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/no');

    //Assert -3
    httpMock.verify();

  });

  it('getNoByNome test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const no_Expected: No = { nome: "nomeNo", latitude: 5, longitude: 20, abreviatura: "abrev", isEstacaoRecolha: false, isPontoRendicao: false, capacidade: 2, tempoMaxParagem: 4, modelo: "esfera" }
    const nome = "nomeNo"
    //Act

    service.getNoByNome(nome).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(no_Expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/no' + '?nome=nomeNo');

    //Assert -3
    httpMock.verify();

  });

  /* it('getNos test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const no_Expected: No = { nome: "nomeNo", latitude: 5, longitude: 20, abreviatura: "abrev", isEstacaoRecolha: false, isPontoRendicao: false, capacidade: 2, tempoMaxParagem: 4 }

    //Act

    service.getNos().subscribe((res) => {
      //Assert-1
      expect(res).toEqual(no_Expected)
    });

    //Assert -2
    const req = httpMock.expectOne('http://localhost:3000/api/no');

    //Assert -3
    httpMock.verify();

  }); */

  // it('getNoByAbreviatura test with HTTP mock', () => {

  //   Arrange
  //   Set Up Data 
  //   const no_Expected: No = { nome: "nomeNo", latitude: 5, longitude: 20, abreviatura: "abrev", isEstacaoRecolha: false, isPontoRendicao: false, capacidade: 2, tempoMaxParagem: 4 }
  //   const abreviatura = "abrev"
  //   Act

  //   service.getNoByAbreviatura(abreviatura).subscribe((res) => {
  //     Assert-1
  //     expect(res).toEqual(no_Expected)
  //   });

  //   Assert -2
  //   const req = httpMock.expectOne(Config.NOS_URL + '/api/no?abreviatura=abrev');

  //   Assert -3
  //   httpMock.verify();

  // });



});
