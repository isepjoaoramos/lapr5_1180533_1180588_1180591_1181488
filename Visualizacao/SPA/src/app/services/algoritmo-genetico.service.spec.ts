import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AlgoritmoGeneticoService } from './algoritmo-genetico.service';

describe('AlgoritmoGeneticoService', () => {
  let service: AlgoritmoGeneticoService;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule],});
    service = TestBed.inject(AlgoritmoGeneticoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
