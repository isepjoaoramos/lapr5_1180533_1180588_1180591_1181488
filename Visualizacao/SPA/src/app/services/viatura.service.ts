import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import Viatura from '../model/Viatura';

import { environment } from '../../environments/environment';
import { List } from 'lodash';
import {AuthenticationService} from '../services/authentication.service'


@Injectable({
  providedIn: 'root'
})
export class ViaturaService {

  constructor(private http: HttpClient,private authenticationService: AuthenticationService) { }


  private viaturaURL = environment.mdv_url + '/viatura'

  addViatura(viatura: Viatura): Observable<Viatura> {
    return this.http.post<Viatura>(this.viaturaURL, viatura)
  }

  getViaturaByMatricula(matricula: string): Observable<Viatura>{
    const url = `${this.viaturaURL}/matricula=${matricula}`;
    return this.http.get<Viatura>(url); 
  }

  
  getViaturaByVin(vin: string): Observable<Viatura>{
    const url = `${this.viaturaURL}/vin=${vin}`;
    return this.http.get<Viatura>(url); 
  }


  getAllViaturas(): Observable<Viatura[]>{
    return this.http.get<Viatura[]>(this.viaturaURL,this.getHeaders());
  }

  getHeaders(){
    let headers = new HttpHeaders({'x-access-token':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }

}
