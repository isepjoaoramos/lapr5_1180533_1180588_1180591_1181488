import { TestBed } from '@angular/core/testing';

import { RegistoService } from './registo.service';

import {Registo} from '../model/Registo';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'

import { environment } from '../../environments/environment';


describe('UserService', () => {
  let service: RegistoService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RegistoService]
    });
    service = TestBed.inject(RegistoService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addUser test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const user_toCreate: Registo = { email: "email@email.com", username: "username", password:"pass"}
    const expected_User: Registo = { email: "email@email.com", username: "username", password:"pass"}
    //Act

    service.addUser(user_toCreate).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expected_User)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/register');

    //Assert -3
    httpMock.verify();

  });

});
