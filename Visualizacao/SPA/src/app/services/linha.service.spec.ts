import { TestBed } from '@angular/core/testing';

import { LinhaService } from './linha.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'
import Linha from '../model/Linha'

import { environment } from '../../environments/environment';


describe('LinhaService', () => {
  let service: LinhaService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LinhaService]
    });
    service = TestBed.inject(LinhaService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('getCodigosLinhaById test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const linha: Linha = { codLinha: "L1", nome: "linha teste1", vermelho: 20, verde: 20, azul: 25, noInicio: "PARAD", noFim: "PARED", restricoes: "falar ingles" }
    const linha_id = "L1"
    //Act

    service.getCodigosLinhaById(linha_id).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(linha)
    });

    //Assert -2
    const req = httpMock.expectOne( environment.baseUrl + '/linha' + '?codLinha=L1');

    //Assert -3
    httpMock.verify();

  });



  it('addLinha test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const linha: Linha = { codLinha: "L1", nome: "linha teste1", vermelho: 20, verde: 20, azul: 25, noInicio: "PARAD", noFim: "PARED", restricoes: "falar ingles" }
    const expectedlinha: Linha = { codLinha: "L1", nome: "linha teste1", vermelho: 20, verde: 20, azul: 25, noInicio: "PARAD", noFim: "PARED", restricoes: "falar ingles" }

    //Act
    service.addLinha(linha).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expectedlinha)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/linha');

    //Assert -3
    httpMock.verify();
  });


  it('getLinhas test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const linha: Linha[] = [{ codLinha: "L1", nome: "linha teste1", vermelho: 20, verde: 20, azul: 25, noInicio: "PARAD", noFim: "PARED", restricoes: "falar ingles" },
                          { codLinha: "L2", nome: "linha teste2", vermelho: 40, verde: 40, azul: 65, noInicio: "PARAD", noFim: "PARED", restricoes: "falar ingles" }]

    //Act
    service.getLinhas().subscribe((res) => {
      //Assert-1
      expect(res).toEqual(linha)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/linha');

    //Assert -3
    httpMock.verify();
  });

});
