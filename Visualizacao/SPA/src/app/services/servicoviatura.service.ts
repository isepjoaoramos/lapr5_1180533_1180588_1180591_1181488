import { Injectable } from '@angular/core';

import {HttpClient ,HttpHeaders} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import ServicoViatura from '../model/ServicoViatura';

import { environment } from '../../environments/environment';
import { List } from 'lodash';
import {AuthenticationService} from '../services/authentication.service'


@Injectable({
  providedIn: 'root'
})
export class ServicoviaturaService {

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }


  private servicoViaturaURL = environment.mdv_url + '/servicoviatura'


  addServicoViatura(servicoViatura: ServicoViatura): Observable<ServicoViatura> {
    return this.http.post<ServicoViatura>(this.servicoViaturaURL, servicoViatura,this.getHeaders())
  }

  getByCodServicoViatura(codServicoViatura: string): Observable<ServicoViatura>{
    const url = `${this.servicoViaturaURL}/codservicoviatura=${codServicoViatura}`;
    return this.http.get<ServicoViatura>(url,this.getHeaders()); 
  }

  getAllServicosViatura(): Observable<ServicoViatura[]>{
    return this.http.get<ServicoViatura[]>(this.servicoViaturaURL,this.getHeaders());
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }

}
