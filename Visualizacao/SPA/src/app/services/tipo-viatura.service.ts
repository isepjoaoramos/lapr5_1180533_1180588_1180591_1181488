import { Injectable } from '@angular/core';

import {HttpClient ,HttpHeaders} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import TipoViatura from '../model/TipoViatura';

import { environment } from '../../environments/environment';
import { List } from 'lodash';
import {AuthenticationService} from '../services/authentication.service'


@Injectable({
  providedIn: 'root'
})
export class TipoViaturaService {

  constructor(private http: HttpClient,private authenticationService: AuthenticationService) { }


  private tipoViaturaURL = environment.baseUrl + '/tipoviatura'

  addTipoViatura(tipoViatura: TipoViatura): Observable<TipoViatura> {
    return this.http.post<TipoViatura>(this.tipoViaturaURL, tipoViatura,this.getHeaders())
  }

  getTipoViaturaByCod(codTipoViaturaID: string): Observable<TipoViatura>{
    const url = `${this.tipoViaturaURL}?codTipoViatura=${codTipoViaturaID}`;
    return this.http.get<TipoViatura>(url,this.getHeaders()); 
  }

  getAllTiposViatura(): Observable<TipoViatura[]>{
    return this.http.get<TipoViatura[]>(this.tipoViaturaURL,this.getHeaders());
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }
}
