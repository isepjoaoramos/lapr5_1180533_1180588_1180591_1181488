import { TestBed } from '@angular/core/testing';

import { TipoViaturaService } from './tipo-viatura.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'
import TipoViatura from '../model/TipoViatura'

import { environment } from '../../environments/environment';

describe('TipoViaturaService', () => {
  let service: TipoViaturaService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TipoViaturaService]
    });
    service = TestBed.inject(TipoViaturaService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('addTipoViatura test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const tipoViatura_toCreate: TipoViatura = { codTipoViatura: "123456789123456789PT", descricao: "viatura_teste", velocidade_media: 20, tipocombustivel: 75, consumo_medio: 25, custo_km: 10, autonomia: 200, emissoes: 400}
    const expected_TipoViatura: TipoViatura = { codTipoViatura: "123456789123456789PT", descricao: "viatura_teste", velocidade_media: 20, tipocombustivel: 75, consumo_medio: 25, custo_km: 10, autonomia: 200, emissoes: 400}
    //Act

    service.addTipoViatura(tipoViatura_toCreate).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expected_TipoViatura)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/tipoviatura');

    //Assert -3
    httpMock.verify();

  });



  it('getTipoViaturaByCod test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const tipoViatura_Expected: TipoViatura = { codTipoViatura: "123456789123456789PT", descricao: "viatura_teste", velocidade_media: 20, tipocombustivel: 75, consumo_medio: 25, custo_km: 10, autonomia: 200, emissoes: 400}
    const codTipoViatura = "123456789123456789PT"
    //Act

    service.getTipoViaturaByCod(codTipoViatura).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(tipoViatura_Expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/tipoviatura' + '?codTipoViatura=123456789123456789PT');

    //Assert -3
    httpMock.verify();

  });
});
