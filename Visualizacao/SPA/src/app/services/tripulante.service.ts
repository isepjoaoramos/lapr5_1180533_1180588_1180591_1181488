import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import Tripulante from '../model/Tripulante';
import {AuthenticationService} from '../services/authentication.service'


@Injectable({
  providedIn: 'root'
})
export class TripulanteService {
  private tripulanteURL = environment.mdv_url + '/tripulante';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient,private authenticationService: AuthenticationService) {}

  addTripulante(tripulante: Tripulante): Observable<Tripulante> {
    console.log(tripulante);
    return this.http.post<Tripulante>(
      this.tripulanteURL,
      tripulante
    );
  }

  getByNumMecanografico(num: string): Observable<Tripulante> {
    console.log(`${this.tripulanteURL}/numMecanografico=${num}`);
    const url = `${this.tripulanteURL}/numMecanografico=${num}`;
    return this.http.get<Tripulante>(url, this.httpOptions);
  }

  getHeaders(){
    let headers = new HttpHeaders({'x-access-token':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }
}
