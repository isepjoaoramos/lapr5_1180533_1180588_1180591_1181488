import { TestBed } from '@angular/core/testing';

import { VisualizacaoMapa3dService } from './visualizacao-mapa-3d.service';

describe('VisualizacaoMapa3dService', () => {
  let service: VisualizacaoMapa3dService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VisualizacaoMapa3dService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
