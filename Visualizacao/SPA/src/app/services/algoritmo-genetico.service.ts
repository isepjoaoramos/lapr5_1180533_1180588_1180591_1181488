import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlgoritmoGeneticoService {

  constructor(private http:HttpClient) { }

  sendPost(url : string) {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    var x = this.http.get(url).subscribe(
      data => {
        console.log("Data!");
        console.log(data); 
      },
      error => {
        console.log("Error!");
        console.log(error);
      }
    );
    return x;
  }
}
