import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MudancaMotoristasService {

  constructor(private http:HttpClient) { }

  httpOptionsUrlEncoded = {
    headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
  };

  httpOptionsJson = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  /*sendGet(url : string) {
    // url = http://localhost:3000/api/no
    var resposta = this.http.get(url, this.httpOptionsJson).subscribe(
      data => {
        console.log("Data!");
        console.log(data); 
      },
      error => {
        console.log("Error!");
        console.log(error);
      }
    );
  }*/

  sendPost(url : string) {
    var x = this.http.get(url).subscribe(
      data => {
        console.log("Data!");
        console.log(data); 
      },
      error => {
        console.log("Error!");
        console.log(error);
      }
    );
    return x;
  }
}
