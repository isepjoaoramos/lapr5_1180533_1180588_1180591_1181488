import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Viagem } from '../model/Viagem';
import { PercursoService } from './percurso.service';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ViagemService {
  private viagensUrl = environment.mdv_url + '/viagem'; // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private percursoService: PercursoService,
    private authenticationService: AuthenticationService
  ) {}

  addViagem(viagem: Viagem): Observable<Viagem> {
    //  viagem.horariosNos = this.percursoService.getHorarios(viagem.percurso,viagem.horaSaida);
    //console.log(viagem.horaSaida);
    return this.http.post<Viagem>(this.viagensUrl, viagem, this.httpOptions);
  }

  getViagens(): Observable<Viagem[]> {
    return this.http.get<Viagem[]>(this.viagensUrl,this.getHeaders());
  }

  getViagensByLinha(linha: string): Observable<Viagem[]> {
    const url = `${this.viagensUrl}/linha=${linha}`;
    return this.http.get<Viagem[]>(url);
  }

  getHeaders(){
    let headers = new HttpHeaders({'x-access-token':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }
}
