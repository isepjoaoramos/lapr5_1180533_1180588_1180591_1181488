import { TestBed } from '@angular/core/testing';

import { ViaturaService } from './viatura.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'
import Viatura from '../model/Viatura'

import { environment } from '../../environments/environment';

describe('ViaturaService', () => {
  let service: ViaturaService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ViaturaService]
    });
    service = TestBed.inject(ViaturaService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('addViatura test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const viatura_toCreate: Viatura = { matricula: "XQ-33-XP", vin: "3FTBE673ETR678457",  cod_tipo_viatura: "123456789123456789PL", data_entrada_servico: new Date("2020-11-06")}
    const expected_viatura: Viatura = { matricula: "XQ-33-XP", vin: "3FTBE673ETR678457",  cod_tipo_viatura: "123456789123456789PL", data_entrada_servico: new Date("2020-11-06")}
    //Act

    service.addViatura(viatura_toCreate).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expected_viatura)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/viatura');

    //Assert -3
    httpMock.verify();

  });



  it('getViaturaByMatricula test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const viatura_expected: Viatura = { matricula: "XQ-33-XP", vin: "3FTBE673ETR678457",  cod_tipo_viatura: "123456789123456789PL", data_entrada_servico: new Date("2020-11-06")}    
    const matricula = "XQ-33-XP"
    //Act

    service.getViaturaByMatricula(matricula).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(viatura_expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/viatura' + '/matricula=XQ-33-XP');

    //Assert -3
    httpMock.verify();

  });


  it('getViaturaByVin test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const viatura_expected: Viatura = { matricula: "XQ-33-XP", vin: "3FTBE673ETR678457",  cod_tipo_viatura: "123456789123456789PL", data_entrada_servico: new Date("2020-11-06")}    
    const vin = "3FTBE673ETR678457"
    //Act

    service.getViaturaByVin(vin).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(viatura_expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/viatura' + '/vin=3FTBE673ETR678457');

    //Assert -3
    httpMock.verify();

  });


  it('getAllViaturas test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const viaturas_expected: Viatura[] = [{matricula: "XQ-33-XP", vin: "3FTBE673ETR678457",  cod_tipo_viatura: "123456789123456789PL", data_entrada_servico: new Date("2020-11-06")} ,
    {matricula: "XQ-33-GP", vin: "3FTBE673ETR678467",  cod_tipo_viatura: "9123456789123456789LK", data_entrada_servico: new Date("2020-11-06")}]

    //Act

    service.getAllViaturas().subscribe((res) => {
      //Assert-1
      expect(res).toEqual(viaturas_expected)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/viatura');

    //Assert -3
    httpMock.verify();

  });
});
