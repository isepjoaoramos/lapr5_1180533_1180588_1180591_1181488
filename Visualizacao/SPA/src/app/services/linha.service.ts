import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Linha from '../model/Linha';
import { environment } from '../../environments/environment';

import {AuthenticationService} from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LinhaService {

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

  private linhaURL = environment.baseUrl + '/linha'

  addLinha(linha: Linha): Observable<Linha>{
    return this.http.post<Linha>(this.linhaURL, linha,this.getHeaders());
  }


  getCodigosLinhaById(linhaId: string): Observable<Linha>{
    const url = `${this.linhaURL}?codLinha=${linhaId}`;
    return this.http.get<Linha>(url,this.getHeaders()); 
  }

  getLinhas():Observable<Linha[]>{
    return this.http.get<Linha[]>(this.linhaURL, this.getHeaders());
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }

}
