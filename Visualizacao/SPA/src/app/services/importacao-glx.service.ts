import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import {AuthenticationService} from '../services/authentication.service'
import {HttpClient ,HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ImportacaoGlxService {
  // API url 
  baseApiUrl = environment.baseUrl + '/importar_glx'
  mdvApiUrl = environment.mdv_url + '/importacao'

    
  constructor(private http:HttpClient,private authenticationService: AuthenticationService) { }

  // Returns an observable 
  uploadMdr(file : File):Observable<any> { 
  
      // Create form data 
      const formData = new FormData();  
        
      // Store form name as "file" with file data 
      formData.append("glx", file, file.name); 
        
      // Make http post request over api 
      // with formData as req 
      return this.http.post(this.baseApiUrl, formData,this.getHeaders()) 
  }
  
  uploadMdv(file : File):Observable<any> { 
  
    // Create form data 
    const formData = new FormData();  
      
    // Store form name as "file" with file data 
    formData.append("glx", file, file.name); 
      
    // Make http post request over api 
    // with formData as req 
    return this.http.post(this.mdvApiUrl, formData,this.getHeaders()) 
}

  getHeaders(){
    let headers = new HttpHeaders({'auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }
}
