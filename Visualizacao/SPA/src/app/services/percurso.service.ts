import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Percurso } from '../model/percurso';

import { environment } from '../../environments/environment';

import {AuthenticationService} from '../services/authentication.service';



@Injectable({ providedIn: 'root' })
export class PercursoService {

  private percursosUrl = environment.baseUrl + '/percursos'  // URL to web api

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

  /** GET percursos from the server */
  getPercursos(): Observable<Percurso[]> {
    return this.http.get<Percurso[]>(this.percursosUrl);
  }

  getPercursosByLinha(linha: string):Observable<Percurso[]>{
    const url= `${this.percursosUrl}?linha=${linha}`;
    return this.http.get<Percurso[]>(url);
  }

  getPercursoByDescricao(descricao: string):Observable<Percurso[]>{
    const url= `${this.percursosUrl}?descricao=${descricao}`;
    return this.http.get<Percurso[]>(url);
  }

  addPercurso(percurso: Percurso): Observable<Percurso> {
    return this.http.post<Percurso>(this.percursosUrl, percurso, this.getHeaders());
  }

  getHorarios(percurso: string, horaSaida: number): number[] {
    var percursos1;
    var perc;
    var horariosNos: number[] = [];
    horariosNos[0];
    this.getPercursoByDescricao(percurso).subscribe(res => {
      percursos1 = res;
      perc = percursos1[0];
      horariosNos[0] = horaSaida;
      for(var k=1; k<=perc.segmentos.length; k++){
        horariosNos[k] = horariosNos[k-1] + perc.segmentos[k-1].duracao;
      }
      return horariosNos;
    });
    return horariosNos;
  }

  getHeaders(){
    let headers = new HttpHeaders({ 'Content-Type': 'application/json','auth':
    this.authenticationService.userInfo.token});
    
    let httpOptions = {
      headers:headers
    };
    
    return httpOptions;
  }

}