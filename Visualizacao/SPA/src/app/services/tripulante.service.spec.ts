import { TestBed } from '@angular/core/testing';

import { TripulanteService } from './tripulante.service';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'
import Tripulante from '../model/Tripulante';

import { environment } from '../../environments/environment';
import TipoTripulante from '../model/TipoTripulante';
import { TipoTripulantePage } from 'e2e/src/tipoTripulante.po';


describe('TripulanteService', () => {
  let service: TripulanteService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TripulanteService]
    });
    service = TestBed.inject(TripulanteService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addTripulante test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const datanascimento= new Date(1970,10,10);
    const dataemissao= new Date(1989,10,10);
    var trip1: TipoTripulante = {tipoTripulanteId: 1,codigo: "65467",descricao: "Camionista"}
    var tiposTripulantes: TipoTripulante[] = []
    tiposTripulantes.push(trip1)
    const data2= new Date(1995,10,10)
    const data3= new Date(2025,10,10)
    const tripulante_toCreate: Tripulante = {  numMecanografico:"123456780",
    nome:"John Doe",
    dataNascimento: datanascimento,
    numCartaoCidadao: 123456789,
    nif: 123456789,
    numCartaConducao: "123546",
    dataEmissaoCartaConducao: dataemissao,
    tiposTripulante : tiposTripulantes,
    dataEntradaEmpresa : data2,
    dataSaidaEmpresa : data3 }

    const expected_tripulante: Tripulante = {numMecanografico:"123456780",
    nome:"John Doe",
    dataNascimento: datanascimento,
    numCartaoCidadao: 123456789,
    nif: 123456789,
    numCartaConducao: "123546",
    dataEmissaoCartaConducao: dataemissao,
    tiposTripulante : tiposTripulantes,
    dataEntradaEmpresa : data2,
    dataSaidaEmpresa : data3 }
    
    //Act....

    service.addTripulante(tripulante_toCreate).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expected_tripulante)
    });

    //Assert -2
    const req = httpMock.expectOne(environment.mdv_url + '/tripulante');

    //Assert -3
    httpMock.verify();

  });

  /* 
  it('getByNumMecanografico test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const tripulante_Expected: Tripulante = {  numMecanografico:"123456780",
    nome:"John Doe",
    dataNascimento: new Date(1990,08,15),
    numCartaoCidadao: 1234567,
    nif: 123456789,
    numCartaConducao: "123546",
    dataEmissaoCartaConducao: new Date(1995,09,10),
    tipoTripulante : "Camionista Sénior",
    dataEntradaEmpresa : new Date(1999,10,12),
    dataSaidaEmpresa : new Date(2021,11,12) }

    const num ="123456780";
    
    //Act....

    service.getByNumMecanografico(num).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(tripulante_Expected);
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/tripulante'+ '?numMecanografico=123456780');

    //Assert -3
    httpMock.verify();

  }); */

});
