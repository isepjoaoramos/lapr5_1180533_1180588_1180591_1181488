import {HttpClient ,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { List } from 'lodash';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {BlocoTrabalho} from '../model/BlocoTrabalho';
import { CriacaoBlocosTrabalho } from '../model/CriacaoBlocosTrabalho';
import {AuthenticationService} from '../services/authentication.service'

@Injectable({
  providedIn: 'root'
})
export class BlocotrabalhoService {

  private blocoURL = environment.mdv_url + '/bloco'
  
  constructor(private http: HttpClient,private authenticationService: AuthenticationService) { }

  addBloco(bloco: BlocoTrabalho): Observable<BlocoTrabalho> {
    return this.http.post<BlocoTrabalho>(this.blocoURL,bloco);
  }

  addBlocos(info: CriacaoBlocosTrabalho): Observable<List<BlocoTrabalho>> {
    return this.http.post<List<BlocoTrabalho>>(this.blocoURL,info);
  }

  getBlocos(): Observable<BlocoTrabalho[]> {
    return this.http.get<BlocoTrabalho[]>(this.blocoURL);
  }

  getBlocoById(idBloco: string): Observable<BlocoTrabalho> {
    const url = `${this.blocoURL}?idBloco=${idBloco}`;
    return this.http.get<BlocoTrabalho>(url);
  }

}
