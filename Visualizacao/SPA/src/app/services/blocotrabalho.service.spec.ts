// import { HttpClient } from '@angular/common/http';
// import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
// import { TestBed } from '@angular/core/testing';
// import { environment } from 'src/environments/environment';
// import {BlocoTrabalho} from '../model/BlocoTrabalho';
// import { BlocotrabalhoService } from './blocotrabalho.service';

// describe('BlocotrabalhoService', () => {
//   let service: BlocotrabalhoService;
//   let httpMock: HttpTestingController;
//   let httpClient: HttpClient;

//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       imports: [HttpClientTestingModule],
//       providers: [BlocotrabalhoService]
//     });
//     service = TestBed.inject(BlocotrabalhoService);
//     httpMock = TestBed.inject(HttpTestingController);
//     httpClient = TestBed.inject(HttpClient);
//   });

//   it('should be created', () => {
//     expect(service).toBeTruthy();
//   });

//   it('addBloco test with HTTP mock', () => {
//     const bloco: BlocoTrabalho = { codigosViagem: [1], idServicoViatura: 1}
//     const expectedBloco: BlocoTrabalho = { codigosViagem: [1], idServicoViatura: 1}

//     service.addBloco(bloco).subscribe((res) => {
//       expect(res).toEqual(expectedBloco)
//     })
    
//     httpMock.expectOne(environment.mdv_url + '/bloco');
    
//     httpMock.verify();
//   })
// });
