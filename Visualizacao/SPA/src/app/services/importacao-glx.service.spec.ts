import { TestBed } from '@angular/core/testing';

import { ImportacaoGlxService } from './importacao-glx.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http'
import Linha from '../model/Linha'

describe('Importacao-glxService', () => {
  let service: ImportacaoGlxService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ImportacaoGlxService]
    });
    service = TestBed.inject(ImportacaoGlxService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  /*it('upload test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    var file: any;//File = {null,"new": File};
    //Act

    service.upload(file).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(file);
    });

    //Assert -2
    const req = httpMock.expectOne('http://localhost:3000/api/importacaoglx');

    //Assert -3
    httpMock.verify();

  });*/
});
