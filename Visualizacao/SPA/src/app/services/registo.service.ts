import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Registo } from '../model/Registo';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistoService {
  // para registar clientes
  private registoURL = environment.baseUrl + '/register/cliente';
  private deleteURL = environment.baseUrl + '/delete';
  
  
  constructor(private http: HttpClient) {}

  addUser(registo: Registo): Observable<Registo> {
    return this.http.post<Registo>(this.registoURL, registo);
  }

  removeUser(username: string) { 
    const url = `${this.deleteURL}/${username}`;
    console.log(url)
    return this.http.delete(url).subscribe(data => {
      console.log(data);
    });
  }


  getUserByEmail(email: string): Observable<Registo> {
    const url = `${this.registoURL}?email=${email}`;
    return this.http.get<Registo>(url);
  }

  getUserByUsername(username: string): Observable<Registo> {
    const url = `${this.registoURL}?username=${username}`;
    return this.http.get<Registo>(url);
  }

  getAllUsers(): Observable<Registo[]> {
    return this.http.get<Registo[]>(this.registoURL);
  }
}
