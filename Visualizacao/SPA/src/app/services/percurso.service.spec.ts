import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import Linha from '../model/Linha';
import { Percurso } from '../model/percurso';
import { Segmento } from '../model/Segmento';

import { environment } from '../../environments/environment';

import { PercursoService } from './percurso.service';

describe('PercursoService', () => {
  let service: PercursoService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PercursoService]
    });
    service = TestBed.inject(PercursoService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });
  

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addPercurso test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const segmentos: Segmento[] = [];
    const seg1: Segmento = {no1: "AGUIA", no2: "BALTR", distancia: 12, duracao: 12, ordem: 1};
    segmentos.push(seg1);
    const percurso: Percurso = { orientacao: "Return", distancia: 12, descricao: "percurso teste1", duracao: 12, linha: "Paredes_Aguiar", noInicio: "AGUIA", noFim: "BALTR", segmentos }
    const expectedPercurso: Percurso = { orientacao: "Return", distancia: 12, descricao: "percurso teste1", duracao: 12, linha: "Paredes_Aguiar", noInicio: "AGUIA", noFim: "BALTR", segmentos }

    //Act
    service.addPercurso(percurso).subscribe((res) => {
      //Assert-1
      expect(res).toEqual(expectedPercurso)
    });

    //Assert -2
    httpMock.expectOne(environment.baseUrl + '/percursos');

    //Assert -3
    httpMock.verify();
  });

  it('getPercursos test with HTTP mock', () => {

    //Arrange
    //Set Up Data 
    const segmentos: Segmento[] = [];
    const seg1: Segmento = {no1: "AGUIA", no2: "BALTR", distancia: 12, duracao: 12, ordem: 1};
    segmentos.push(seg1);
    const percursos: Percurso[] = [{ orientacao: "Return", distancia: 12, descricao: "percurso teste1", duracao: 12, linha: "Paredes_Aguiar", noInicio: "AGUIA", noFim: "BALTR", segmentos },
    { orientacao: "Return", distancia: 12, descricao: "percurso teste2", duracao: 12, linha: "Paredes_Baltar", noInicio: "AGUIA", noFim: "BALTR", segmentos }]

    //Act
    service.getPercursos().subscribe((res) => {
      //Assert-1
      expect(res).toEqual(percursos);
    });

    //Assert -2
    const req = httpMock.expectOne(environment.baseUrl + '/percursos');

    //Assert -3
    httpMock.verify();
  });
});
