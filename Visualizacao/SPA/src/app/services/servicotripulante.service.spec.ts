import { TestBed } from '@angular/core/testing';

import { ServicoTripulanteService } from './servicotripulante.service';

describe('ServicotripulanteService', () => {
  let service: ServicoTripulanteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicoTripulanteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
