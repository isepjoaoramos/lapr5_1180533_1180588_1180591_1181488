import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import  Login  from '../model/Login';
import  Token  from '../model/Token';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginURL = environment.baseUrl + '/login'

  constructor(private http: HttpClient) {}

  doLogin(login: Login): Observable<Token> {
    return this.http.post<Token>(this.loginURL, login)
  }
  
}
