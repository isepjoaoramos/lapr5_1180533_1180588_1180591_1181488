import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AlgoritmoGeneticoComponent } from './algoritmo-genetico.component';

describe('AlgoritmoGeneticoComponent', () => {
  let component: AlgoritmoGeneticoComponent;
  let fixture: ComponentFixture<AlgoritmoGeneticoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlgoritmoGeneticoComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlgoritmoGeneticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
