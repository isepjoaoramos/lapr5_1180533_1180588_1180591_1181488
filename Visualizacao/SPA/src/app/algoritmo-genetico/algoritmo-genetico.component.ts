import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlgoritmoGeneticoService } from '../services/algoritmo-genetico.service';

@Component({
  selector: 'app-algoritmo-genetico',
  templateUrl: './algoritmo-genetico.component.html',
  styleUrls: ['./algoritmo-genetico.component.css']
})
export class AlgoritmoGeneticoComponent implements OnInit {

  algoritmo_genetico_form;
  dimensaoPop : number = 0;
  numNovasGeracoes : number = 0;
  probCruzamento : number = 0;
  probMutacao : number = 0;
  tempoLimite : number = 0;
  menorAvaliacao : number = 0;
  fatorEstabilizacao : number = 0;

  constructor(private agService : AlgoritmoGeneticoService, private formBuilder: FormBuilder) { 
    this.algoritmo_genetico_form = this.formBuilder.group({
      dimensaoPop:'',
      numNovasGeracoes: '',
      probCruzamento: '',
      probMutacao: '',
      tempoLimite: '',
      menorAvaliacao: '',
      fatorEstabilizacao: '',
    })
  }

  ngOnInit(): void {
  }

  onClick(): void {
    var dimensaoPop = (document.getElementById("dimensaoPop") as HTMLInputElement).value;
    var numNovasGeracoes = (document.getElementById("numNovasGeracoes") as HTMLInputElement).value;
    var probCruzamento = parseInt((document.getElementById("probCruzamento") as HTMLInputElement).value);
    var probMutacao = parseInt((document.getElementById("probMutacao") as HTMLInputElement).value);
    var tempoLimite = parseInt((document.getElementById("tempoLimite") as HTMLInputElement).value);
    var menorAvaliacao = parseInt((document.getElementById("menorAvaliacao") as HTMLInputElement).value);
    var fatorEstabilizacao = parseInt((document.getElementById("fatorEstabilizacao") as HTMLInputElement).value);
    var inicioAlmoco = parseInt((document.getElementById("inicioAlmoco") as HTMLInputElement).value);
    var fimAlmoco = parseInt((document.getElementById("fimAlmoco") as HTMLInputElement).value);
    var inicioJantar = parseInt((document.getElementById("inicioJantar") as HTMLInputElement).value);
    var fimJantar = parseInt((document.getElementById("fimJantar") as HTMLInputElement).value);
    var inicioTurnos = parseInt((document.getElementById("inicioTurnos") as HTMLInputElement).value);
    var fimTurnos = parseInt((document.getElementById("fimTurnos") as HTMLInputElement).value);
    var url = "http://uvm007.dei.isep.ipp.pt:5002/algoritmo_genetico?dimensaoPop="+dimensaoPop+"&numNovasGeracoes="+numNovasGeracoes+"&probCruzamento="+probCruzamento+"&probMutacao="+probMutacao+"&tempoLimite="+tempoLimite+"&menorAvaliacao="+menorAvaliacao+"&fatorEstabilizacao="+fatorEstabilizacao+"&inicioAlmoco="+inicioAlmoco+"&fimAlmoco="+fimAlmoco+"&inicioJantar="+inicioJantar+"&fimJantar="+fimJantar+"&inicioTurnos="+inicioTurnos+"&fimTurnos="+fimTurnos;
    //var url = "http://localhost:5002/algoritmo_genetico?dimensaoPop="+dimensaoPop+"&numNovasGeracoes="+numNovasGeracoes+"&probCruzamento="+probCruzamento+"&probMutacao="+probMutacao+"&tempoLimite="+tempoLimite+"&menorAvaliacao="+menorAvaliacao+"&fatorEstabilizacao="+fatorEstabilizacao+"&inicioAlmoco="+inicioAlmoco+"&fimAlmoco="+fimAlmoco+"&inicioJantar="+inicioJantar+"&fimJantar="+fimJantar+"&inicioTurnos="+inicioTurnos+"&fimTurnos="+fimTurnos;
    var outputPost = this.agService.sendPost(url);
    location.assign(url);
}
}
