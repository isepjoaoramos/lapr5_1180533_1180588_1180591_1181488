import { Component, OnInit } from '@angular/core';

import {VisualizacaoMapa3dService} from '../visualizacao-mapa-3d.service';


@Component({
  selector: 'app-visualizacao-mapa3d',
  templateUrl: './visualizacao-mapa3d.component.html',
  styleUrls: ['./visualizacao-mapa3d.component.css']
})
export class VisualizacaoMapa3dComponent implements OnInit {

  constructor(private mapaservice : VisualizacaoMapa3dService) { }

  ngOnInit(): void {
    this.mapaservice.buildMap3D();
  }

}
