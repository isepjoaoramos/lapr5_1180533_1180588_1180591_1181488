import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoMapa3dComponent } from './visualizacao-mapa3d.component';

describe('VisualizacaoMapa3dComponent', () => {
  let component: VisualizacaoMapa3dComponent;
  let fixture: ComponentFixture<VisualizacaoMapa3dComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoMapa3dComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoMapa3dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
