import { Component, OnInit } from '@angular/core';
import { ServicoTripulanteService } from '../services/servicotripulante.service';
import ServicoTripulante from '../model/ServicoTripulante';
import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-listar-servico-tripulante',
  templateUrl: './listar-servico-tripulante.component.html',
  styleUrls: ['./listar-servico-tripulante.component.css']
})
export class ListarServicoTripulanteComponent implements OnInit {

  listarServicosTripulanteForm;
  servicosTripulante: ServicoTripulante[] = [];

  constructor(private formBuilder: FormBuilder, private servicoTripulanteService: ServicoTripulanteService) {

    this.listarServicosTripulanteForm = this.formBuilder.group({
      servicosTripulante: []
    });

    this.servicoTripulanteService.getAllServicosTripulante().subscribe(response => {
      this.servicosTripulante = response;
      console.log(response)
    });
   }

  ngOnInit(): void {
  }

}
