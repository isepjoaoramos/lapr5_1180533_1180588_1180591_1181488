import { Component, OnInit } from '@angular/core';
import Linha from '../model/Linha';
import {LinhaService} from '../services/linha.service';


@Component({
  selector: 'app-listar-linhas',
  templateUrl: './listar-linhas.component.html',
  styleUrls: ['./listar-linhas.component.css']
})
export class ListarLinhasComponent implements OnInit {

  linhas: Linha[] = [];

  constructor(private linhaService: LinhaService) {
    this.linhaService.getLinhas().subscribe(
      response =>{
        this.linhas = response;
      }
    );
   }

  ngOnInit(): void {
  }

  


}
