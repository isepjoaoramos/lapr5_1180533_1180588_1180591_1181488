import { Injectable } from '@angular/core';

import * as THREE from 'three';


import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import * as mapboxgl from 'mapbox-gl';

import { environment } from "../environments/environment";
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class VisualizacaoMapa3dService {



  constructor() {
    // @ts-ignore
    mapboxgl.accessToken = environment.mapbox.accessToken;
  }


  buildMap3D() {

    var nos: any[] = [];
    var percursos: any[][] = [];
    var linhas: any[] = [];

    var layers: any[] = [];

    const url_nos = environment.baseUrl + '/no';
    fetch(url_nos)
      .then(data => { return data.json() })
      .then((res) => {
        for (var i = 0; i < res.length; ++i) {
          var latitude: number = res[i].latitude
          var longitude: number = res[i].longitude
          var nomeNo = res[i].abreviatura
          nos.push([nomeNo, latitude, longitude])
        }
      })
    const url_percursos = environment.baseUrl + '/percursos'

    fetch(url_percursos)
      .then(data => { return data.json() })
      .then((res) => {

        var resLength = res.length;
        for (var i = 0; i < resLength; ++i) {
          var linhaX = res[i].linha;

          var resSegmentosLength = res[i].segmentos.length;
          for (var j = 0; j < resSegmentosLength; j++) {
            var nomeNo1 = res[i].segmentos[j].no1;
            var nomeNo2 = res[i].segmentos[j].no2;
            percursos.push([linhaX, nomeNo1, nomeNo2]);
          }
        }
      })

    const url_linhas = environment.baseUrl + '/linha'
    fetch(url_linhas)
      .then(data => { return data.json() })
      .then((res) => {

        var resLength = res.length;
        for (var i = 0; i < resLength; ++i) {
          var nome = res[i].nome;
          var r = res[i].vermelho.toString(16);
          if (r.length == 0) { r = "0" + r; }
          var g = res[i].verde.toString(16);
          if (g.length == 0) { g = "0" + g; }
          var b = res[i].azul.toString(16);
          if (b.length == 0) { b = "0" + b; }
          linhas.push([nome, r, g, b]);

        }
      })


    // @ts-ignore
    var camera = new THREE.Camera();

    var scene = new THREE.Scene();

    var renderer = new THREE.WebGLRenderer();


    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/light-v10',
      zoom: 12,
      center: [-8.391265, 41.212067],
      pitch: 60,
      antialias: true
    })


     // @ts-ignore

    map.on('style.load', function () {
      var modelOrigins: any[] = []
      var modelAltitudes: any[] = []
      var modelRotates: any[] = []
      var modelAsMercatorCoordinates: any[] = []
      var modelTransforms: any[] = []
      var customLayers: any[] = []


      for (var i = 0; i < nos.length; i++) {
        var modelOrigin: [number, number] = [nos[i][2], nos[i][1]];
        modelOrigins.push(modelOrigin);
        // console.log(nos[i][2]);
        // console.log(nos[i][1]);
        var modelAltitude = 0;
        modelAltitudes.push(modelAltitude)
        var modelRotate = [Math.PI / 2, 0, 0];
        modelRotates.push(modelRotate)

        console.log(modelOrigins);
        console.log(nos);
        var modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(
          modelOrigins[i],
          modelAltitudes[i]
        );
        modelAsMercatorCoordinates.push(modelAsMercatorCoordinate)  
          
        console.log(modelAsMercatorCoordinate)
  
  
        // transformation parameters to position, rotate and scale the 3D model onto the map
        var modelTransform = {
          translateX: modelAsMercatorCoordinates[i].x,
          translateY: modelAsMercatorCoordinates[i].y,
          translateZ: modelAsMercatorCoordinates[i].z,
          rotateX: modelRotate[0],
          rotateY: modelRotate[1],
          rotateZ: modelRotate[2],
          /* Since our 3D model is in real world meters, a scale transform needs to be
          * applied since the CustomLayerInterface expects units in MercatorCoordinates.
          */
          scale: modelAsMercatorCoordinates[i].meterInMercatorCoordinateUnits()
        };
  modelTransforms.push(modelTransform)
  console.log("TRANSF: "+modelTransforms[i])
  console.log("IIIIII: "+i)}
        var customLayer = {
          id: 1,
          type: 'custom',
          renderingMode: '3d',
  
          // função que vai ser chamada quando a layer é adicionada ao mapa
  
          // @ts-ignore    
          onAdd: function (map: mapboxgl.Map, gl: WebGLRenderingContext) {
  
            // create two three.js lights to illuminate the model
            var directionalLight = new THREE.DirectionalLight(0xffffff);
            directionalLight.position.set(0, -70, 100).normalize();
            scene.add(directionalLight);
  
            var directionalLight2 = new THREE.DirectionalLight(0xffffff);
            directionalLight2.position.set(0, 70, 100).normalize();
            scene.add(directionalLight2);
  
  
  
            var loader = new OBJLoader();
            loader.load(
              '../assets/3d-model.obj',
              function (obj3d: THREE.Object3D) {
                // @ts-ignore    
                scene.add(obj3d);
              }.bind(this)
            );
            console.log("iii: "+i)
  
            // use the Mapbox GL JS map canvas for three.js
            // @ts-ignore    
            renderer = new THREE.WebGLRenderer({
              canvas: map.getCanvas(),
              context: gl,
              antialias: true
            });
            // @ts-ignore    
            renderer.autoClear = false;
  
          },
  
          // @ts-ignore
          render: function (gl: WebGLRenderingContext, matrix: number[]) {
            for(var j = 0; j<nos.length; j++){
            var rotationX = new THREE.Matrix4().makeRotationAxis(
              new THREE.Vector3(1, 0, 0),
              modelTransforms[j].rotateX
            );
            var rotationY = new THREE.Matrix4().makeRotationAxis(
              new THREE.Vector3(0, 1, 0),
              modelTransforms[j].rotateY
            );
            var rotationZ = new THREE.Matrix4().makeRotationAxis(
              new THREE.Vector3(0, 0, 1),
              modelTransforms[j].rotateZ
            );
  
            var m = new THREE.Matrix4().fromArray(matrix);
            var l = new THREE.Matrix4()
              .makeTranslation(
                modelTransforms[j].translateX,
                modelTransforms[j].translateY,
                // @ts-ignore
                modelTransforms[j].translateZ,
              )
              .scale(
                new THREE.Vector3(
                  modelTransforms[j].scale,
                  -modelTransforms[j].scale,
                  modelTransforms[j].scale
                )
              )
              .multiply(rotationX)
              .multiply(rotationY)
              .multiply(rotationZ);
  
  
            camera.projectionMatrix = m.multiply(l);
  
            renderer.state.reset();
            renderer.render(scene, camera);
            map.triggerRepaint();
                }
          }
        };
customLayers.push(customLayer)
 // @ts-ignore
 map.addLayer(customLayer, 'waterway-label');
 console.log("IIIIEEEE: "+i)

    });

  }

}
