import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Linha from '../model/Linha';
import { Percurso } from '../model/percurso';
import { Viagem } from '../model/Viagem';
import { LinhaService } from '../services/linha.service';
import { PercursoService } from '../services/percurso.service';
import { ViagemService } from '../services/viagem.service';

@Component({
  selector: 'app-viagens',
  templateUrl: './viagens.component.html',
  styleUrls: ['./viagens.component.css']
})
export class ViagensComponent implements OnInit {

  createViagemForm;
  linhas: Linha[] = [];
  percursos: Percurso[] = [];
  viagens: Viagem[] = [];

  constructor(private viagemService: ViagemService, private linhaService: LinhaService, private percursoService: PercursoService, private formBuilder: FormBuilder) {
    this.createViagemForm = this.formBuilder.group({
      linha: ['', Validators.required],
      horario: ['', Validators.pattern("[0-9]+")],
      percurso: ['', Validators.required],
    })
    this.linhaService.getLinhas().subscribe(
      response_linhas => {
        this.linhas = response_linhas;
      })
  }

  ngOnInit(): void {
  }


  onFocusOutEventHorario(event: any) {
    var warningtext = document.getElementById("horarioText");
    var input = (document.getElementById("horario") as HTMLInputElement).value;
    var input2 = (document.getElementById("horarioM") as HTMLInputElement).value;
    var input3 = (document.getElementById("horarioS") as HTMLInputElement).value;
    if (parseFloat(input) < 0 || parseFloat(input) >= 24 || parseFloat(input2) < 0 ||
     parseFloat(input2) >= 60 || parseFloat(input3) < 0 || parseFloat(input3) >=60 ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  onFocusOutEventLinha(event: any) {
    this.percursoService.getPercursosByLinha((document.getElementById("linha") as HTMLInputElement).value).subscribe(
      response_percursos => {
        this.percursos = response_percursos; console.log(this.percursos);

        this.createViagemForm.controls.percurso.patchValue(this.percursos[0].descricao);
      })
  }

  addViagem(): void {
    var linha = (document.getElementById("linha") as HTMLInputElement).value;
    //var horarioSaida: number = this.createViagemForm.get('horario')?.value;
    var hora = parseInt((document.getElementById("horario") as HTMLInputElement).value);
    var minuto = parseInt((document.getElementById("horarioM") as HTMLInputElement).value);
    var segundo = parseInt((document.getElementById("horarioS") as HTMLInputElement).value);    
    var horaSaida = hora*3600 + minuto*60 + segundo  
    var percurso = (document.getElementById("percurso") as HTMLInputElement).value;
    var horariosNos = Array<number>();
    console.log(horaSaida);
    //horariosNos = this.percursoService.getHorarios(percurso,horaSaida);
    this.viagemService.addViagem({ linha, horaSaida, percurso} as Viagem)
      .subscribe(viagens => {
        this.viagens.push(viagens);
      

      var warningtextHorario = document.getElementById("horarioText");
      var warningtextFinal = document.getElementById("finalText");

      if(warningtextHorario != null){
        if(
        warningtextHorario.style.visibility == "hidden" && linha != '' && percurso != '' && horaSaida != NaN
         ){
          alert("Viagem criada com sucesso!"); 
          //location.assign('sucesso');
        } else {
          if (warningtextFinal != null){
            warningtextFinal.style.visibility = "visible";
          }
        }
      }
    });
  }
}
