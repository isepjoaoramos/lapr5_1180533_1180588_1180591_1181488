import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViagensComponent } from './viagens.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViagemService } from '../services/viagem.service';

describe('ViagemComponent', () => {
  let component: ViagensComponent;
  let fixture: ComponentFixture<ViagensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViagensComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ ViagemService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViagensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  
  it('form with valid viagem', () => {
    let linha = component.createViagemForm.controls["linha"]
    linha.setValue("Paredes_Aguiar");
    let horario = component.createViagemForm.controls["horario"]
    horario.setValue(1234);
    let percurso = component.createViagemForm.controls["percurso"]
    percurso.setValue("PARED>AGUIA");
    expect(component.createViagemForm.valid).toBeTruthy();
  });

  it('form with empty linha', () => {
    let linha = component.createViagemForm.controls["linha"]
    linha.setValue("");
    let horario = component.createViagemForm.controls["horario"]
    horario.setValue(1234);
    let percurso = component.createViagemForm.controls["percurso"]
    percurso.setValue("PARED>AGUIA");
    expect(component.createViagemForm.valid).toBeFalsy();
  });

  it('form with invalid horario', () => {
    let linha = component.createViagemForm.controls["linha"]
    linha.setValue("Paredes_Aguiar");
    let horario = component.createViagemForm.controls["horario"]
    horario.setValue(-1);
    let percurso = component.createViagemForm.controls["percurso"]
    percurso.setValue("PARED>AGUIA");
    expect(component.createViagemForm.valid).toBeFalsy();
  });
 

  it('form with empty percurso', () => {
    let linha = component.createViagemForm.controls["linha"]
    linha.setValue("Paredes_Aguiar");
    let horario = component.createViagemForm.controls["horario"]
    horario.setValue(1234);
    let percurso = component.createViagemForm.controls["percurso"]
    percurso.setValue("");
    expect(component.createViagemForm.valid).toBeFalsy();
  });


});
