import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import TipoTripulante from '../model/TipoTripulante';
import { TipoTripulanteService } from '../services/tipotripulante.service';

@Component({
  selector: 'app-tipotripulante',
  templateUrl: './tipotripulante.component.html',
  styleUrls: ['./tipotripulante.component.css']
})
export class TipotripulanteComponent implements OnInit {
  createTipoTripulanteForm;

  constructor(
    private tipoTripulanteService: TipoTripulanteService,
    private formBuilder: FormBuilder
  ) {
    this.createTipoTripulanteForm = this.formBuilder.group({
      codigo: '',
      descricao: ''
    });
  }
  ngOnInit(): void {}

  onSubmit(tipoTripulante: TipoTripulante) {
    var codigo = (document.getElementById('codigo') as HTMLInputElement).value;
    var descricao = (document.getElementById('descricao') as HTMLInputElement)
      .value;

    if (!codigo || !descricao) {
      return;
    }

    var warningtextCodigo = document.getElementById('codigoText');
    var warningtextDescricao = document.getElementById('descricaoText');
    var warningtextFinal = document.getElementById('finalText');

    if (warningtextCodigo != null && warningtextDescricao != null) {
      if (
        warningtextCodigo.style.visibility == 'hidden' &&
        warningtextDescricao.style.visibility == 'hidden'
      ) {
        //location.assign('sucesso');
        this.tipoTripulanteService.addTipoTripulante(tipoTripulante).subscribe(
          response => this.sucesso(response),
          error => console.log('Error', error)
        );
    
        
        this.createTipoTripulanteForm.reset();
      } else {
        if (warningtextFinal != null) {
          warningtextFinal.style.visibility = 'visible';
        }
      }
    }
  
  }

  sucesso(response: TipoTripulante) {
    //location.assign('sucesso');
    alert("Tipo de Tripulante criado com sucesso!")
    console.log('Sucess', response);
  }

  descricaoChange(event: any) {
    var warningtext = document.getElementById('descricaoText');
    var input = (document.getElementById('descricao') as HTMLInputElement)
      .value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
      this.tipoTripulanteService.getTipoTripulanteByDescricao(input).subscribe(
        response => {
          if (warningtext != null) {
            warningtext.style.visibility = 'visible';
          }
        },
        error => console.log(error)
      );
    }
  }

  codigoChange(event: any) {
    var warningtext = document.getElementById('codigoText');
    var input = (document.getElementById('codigo') as HTMLInputElement).value;
    console.log(input);
    if (input == null || input == ""|| input.length==0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
      this.tipoTripulanteService.getTipoTripulanteByCod(input).subscribe(
        response => {
          if (warningtext != null) {
            warningtext.style.visibility = 'visible';
          }
        },
        error => console.log(error)
      );
    }
  }
}
