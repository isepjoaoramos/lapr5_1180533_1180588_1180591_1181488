import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipotripulanteComponent } from './tipotripulante.component';
import { TipoTripulanteService} from '../services/tipotripulante.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


describe('TipotripulanteComponent', () => {
  let component: TipotripulanteComponent;
  let fixture: ComponentFixture<TipotripulanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TipotripulanteComponent],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ TipoTripulanteService ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipotripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form with empty codigo', () => {
    let codigoItem = component.createTipoTripulanteForm.controls["codigo"]
    codigoItem.setValue("");
    expect(component.createTipoTripulanteForm.valid).toBeFalsy;
  });

  it('form with valid codigo', () => {
    let codigoItem = component.createTipoTripulanteForm.controls["codigo"]
    codigoItem.setValue("codigoTeste");
    expect(component.createTipoTripulanteForm.valid).toBeTruthy();
  });

  it('form with invalid codigo', () => {
    let codigoItem = component.createTipoTripulanteForm.controls["codigo"]
    codigoItem.setValue("codigoInvalido11111111111111111111");
    expect(component.createTipoTripulanteForm.valid).toBeFalsy();
  });

  it('form with empty descricao', () => {
    let descricaoItem = component.createTipoTripulanteForm.controls["descricao"]
    descricaoItem.setValue("");
    expect(component.createTipoTripulanteForm.valid).toBeFalsy;
  });

  it('form with valid descricao', () => {
    let descricaoItem = component.createTipoTripulanteForm.controls["descricao"]
    descricaoItem.setValue("desc");
    expect(component.createTipoTripulanteForm.valid).toBeTruthy;
  });



});
