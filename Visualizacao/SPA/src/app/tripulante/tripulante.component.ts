import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { TripulanteService } from '../services/tripulante.service';
import { TipoTripulanteService } from '../services/tipotripulante.service';
import Tripulante from '../model/Tripulante';
import TipoTripulante from '../model/TipoTripulante';
import { response } from 'express';
import { Console } from 'console';

@Component({
  selector: 'app-tripulante',
  templateUrl: './tripulante.component.html',
  styleUrls: ['./tripulante.component.css']
})
export class TripulanteComponent implements OnInit {
  createTripulanteForm;
  tiposTripulanteBox: TipoTripulante[] = [];
  tiposTripulante_escolhidos: TipoTripulante[] = [];

  constructor(
    private tripulanteService: TripulanteService,
    private tipotripulanteService: TipoTripulanteService,
    private formBuilder: FormBuilder
  ) {
    this.createTripulanteForm = this.formBuilder.group({
      numMecanografico: '',
      nome: '',
      dataNascimento: '',
      numCartaoCidadao: '',
      nif: '',
      numCartaConducao: '',
      dataEmissaoCartaConducao: '',
      tiposTripulante: '',
      dataEntradaEmpresa: '',
      dataSaidaEmpresa: ''
    });

    this.tiposTripulante_escolhidos = [];

    this.tipotripulanteService.getAllTipos().subscribe(response_tipos => {
      this.tiposTripulanteBox = response_tipos;
    });

    const date = new Date();
    date.setMonth(1);
    date.setDate(1);
    date.setFullYear(1990);
    this.createTripulanteForm.controls.dataNascimento.patchValue(
      this.formatDate(date)
    );
    this.createTripulanteForm.controls.dataEmissaoCartaConducao.patchValue(
      this.formatDate(date)
    );
    this.createTripulanteForm.controls.dataEntradaEmpresa.patchValue(
      this.formatDate(date)
    );
    this.createTripulanteForm.controls.dataSaidaEmpresa.patchValue(
      this.formatDate(date)
    );
  }

  private formatDate(date: any) {
    const d = new Date(date);
    let month = '' + d.getMonth();
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  ngOnInit(): void {}

  onSubmit(tripulante: Tripulante) {
    var warningtextNumMecanografico = document.getElementById(
      'numMecanograficoText'
    );
    var warningtextNome = document.getElementById('nomeText');
    var warningtextDataNascimento = document.getElementById(
      'dataNascimentoText'
    );
    var warningtextNumCartaoCidadao = document.getElementById(
      'numCartaoCidadaoText'
    );
    var warningtextNif = document.getElementById('nifText');

    var warningtextNumCartaConducao = document.getElementById(
      'numCartaConducaoText'
    );

    var warningtextDataEmissaoCartaConducao = document.getElementById(
      'dataEmissaoCartaConducaotext'
    );

    var warningtextTiposTripulante = document.getElementById(
      'tiposTripulanteText'
    );
    var warningtextDataEntradaEmpresa = document.getElementById(
      'dataEntradaEmpresaText'
    );
    var warningtextDataSaidaEmpresa = document.getElementById(
      'dataSaidaEmpresaText'
    );
    var warningtextFinal = document.getElementById('finalText');

    if (
      warningtextNumMecanografico != null &&
      warningtextNome != null &&
      warningtextDataNascimento != null &&
      warningtextNumCartaoCidadao != null &&
      warningtextNif != null &&
      warningtextNumCartaConducao != null &&
      warningtextDataEmissaoCartaConducao != null &&
      warningtextTiposTripulante != null &&
      warningtextDataEntradaEmpresa != null &&
      warningtextDataSaidaEmpresa != null
    ) {
      if (
        warningtextNumMecanografico.style.visibility == 'hidden' &&
        warningtextNome.style.visibility == 'hidden' &&
        warningtextDataNascimento.style.visibility == 'hidden' &&
        warningtextNumCartaoCidadao.style.visibility == 'hidden' &&
        warningtextNif.style.visibility == 'hidden' &&
        warningtextNumCartaConducao.style.visibility == 'hidden' &&
        warningtextDataEmissaoCartaConducao.style.visibility == 'hidden' &&
        warningtextTiposTripulante.style.visibility == 'hidden' &&
        warningtextDataEntradaEmpresa.style.visibility == 'hidden' &&
        warningtextDataSaidaEmpresa.style.visibility == 'hidden'
      ) {
        console.log('entrou submit');

        location.assign('http://localhost:4200/sucesso');
      } else {
        if (warningtextFinal != null) {
          warningtextFinal.style.visibility = 'visible';
        }
      }
    }
    var tiposTripulante = this.tiposTripulante_escolhidos;
    console.log(tiposTripulante);

    var numMecanografico = this.createTripulanteForm.get('numMecanografico')
      ?.value;
    var nome = this.createTripulanteForm.get('nome')?.value;
    var dataNascimento = this.createTripulanteForm.get('dataNascimento')?.value;
    var numCartaoCidadao = this.createTripulanteForm.get('numCartaoCidadao')
      ?.value;
    var nif = this.createTripulanteForm.get('nif')?.value;
    var numCartaConducao = this.createTripulanteForm.get('numCartaConducao')
      ?.value;
    var dataEmissaoCartaConducao = this.createTripulanteForm.get(
      'dataEmissaoCartaConducao'
    )?.value;
    var dataEmissaoCartaConducao = this.createTripulanteForm.get(
      'dataEmissaoCartaConducao'
    )?.value;
    var dataEntradaEmpresa = this.createTripulanteForm.get('dataEntradaEmpresa')
      ?.value;
    var dataSaidaEmpresa = this.createTripulanteForm.get('dataSaidaEmpresa')
      ?.value;

    this.tripulanteService
      .addTripulante({
        numMecanografico,
        nome,
        dataNascimento,
        numCartaoCidadao,
        nif,
        numCartaConducao,
        dataEmissaoCartaConducao,
        tiposTripulante,
        dataEntradaEmpresa,
        dataSaidaEmpresa
      } as Tripulante)
      .subscribe(
        response => this.sucess(response),
        error => console.log('Error', error)
      );

    this.createTripulanteForm.reset();
    this.tiposTripulante_escolhidos = [];
  }

  sucess(response: Tripulante) {
    alert("Tripulante adicionado!")
    console.log('Sucess', response);
    //location.assign('http://localhost:4200/sucesso');
  }

  // verificar se existe na bd com metodo no service getby numMecanografico
  numMecanograficoChange(event: any) {
    var warningtext = document.getElementById('numMecanograficoText');
    var input = (document.getElementById(
      'numMecanografico'
    ) as HTMLInputElement).value;
    if (
      input == null ||
      input == '' ||
      input.length == 0 ||
      input.length != 9
    ) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';

      this.tripulanteService.getByNumMecanografico(input).subscribe(
        response => {
          if (warningtext != null) {
            warningtext.style.visibility = 'visible';
          }
        },
        error => console.log(error)
      );
    }
  }

  nomeChange(event: any) {
    var warningtext = document.getElementById('nomeText');
    var input = (document.getElementById('nome') as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
    }
  }

  dataNascimentoChange(event: any) {
    var warningtext = document.getElementById('dataNascimentoText');
    var input = (document.getElementById('dataNascimento') as HTMLInputElement)
      .valueAsDate;
    if (input == null) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';

      let dateNow = new Date(Date.now());

      let age = dateNow.getFullYear().valueOf() - input.getFullYear().valueOf();
      console.log(age);
      if (age < 21) {
        warningtext.style.visibility = 'visible';
      }
    }
  }

  numCartaoCidadaoChange(event: any) {
    var warningtext = document.getElementById('numCartaoCidadaoText');
    var input = (document.getElementById(
      'numCartaoCidadao'
    ) as HTMLInputElement).valueAsNumber;
    if (input == null || input > 99999999 || input < 1000000) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
    }
  }

  nifChange(event: any) {
    var warningtext = document.getElementById('nifText');
    var input = (document.getElementById('nif') as HTMLInputElement)
      .valueAsNumber;
    if (input == null || input > 999999999 || input < 100000000) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
    }
  }

  numCartaConducaoChange(event: any) {
    var warningtext = document.getElementById('numCartaConducaoText');
    var input = (document.getElementById(
      'numCartaConducao'
    ) as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
    }
  }

  dataEmissaoCartaConducaoChange(event: any) {
    var warningtext = document.getElementById('dataEmissaoCartaConducaoText');
    var input = (document.getElementById(
      'dataEmissaoCartaConducao'
    ) as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';

      let dateEntrada = new Date(input).getTime();
      let dateAtual = new Date(Date.now()).getTime();

      if (dateEntrada > dateAtual) {
        warningtext.style.visibility = 'visible';
      }
    }
  }

  tipoTripulanteChange(event: any) {
    var warningtext = document.getElementById('tiposTripulanteText');
    var input = (document.getElementById('tiposTripulante') as HTMLInputElement)
      .value;

    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
    }
  }

  dataEntradaEmpresaChange(event: any) {
    var warningtext = document.getElementById('dataEntradaEmpresaText');
    var input = (document.getElementById(
      'dataEntradaEmpresa'
    ) as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';

      let dateEntrada = new Date(input).getTime();
      let dateAtual = new Date(Date.now()).getTime();

      if (dateEntrada > dateAtual) {
        warningtext.style.visibility = 'visible';
      }
    }
  }

  dataSaidaEmpresaChange(event: any) {
    var warningtext = document.getElementById('dataSaidaEmpresaText');
    var input = (document.getElementById(
      'dataSaidaEmpresa'
    ) as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
      let insertedDateSaida = Date.parse(input);

      var inputDataEntrada = (document.getElementById(
        'dataEntradaEmpresa'
      ) as HTMLInputElement).value;
      let insertedDateEntrada = new Date(input.toString());

      let dateAtual = new Date(Date.now()).getTime();
      let dateSaida = new Date(input).getTime();
      let dateEntrada = new Date(inputDataEntrada).getTime();

      if (dateSaida < dateEntrada) {
        warningtext.style.visibility = 'visible';
      }
      if (dateSaida < dateAtual) {
        warningtext.style.visibility = 'visible';
      }
    }
  }

  addTipo(): void {
    var tipoEscolhido = (document.getElementById(
      'tiposTripulante'
    ) as HTMLInputElement).value;

    this.tipotripulanteService
      .getTipoTripulanteByDescricao(tipoEscolhido)
      .subscribe(
        response => {
          //adiciona ao array de tipos de tripulante
          this.tiposTripulante_escolhidos.push(response);
          alert('Tipo de Tripulante adicionado!');
        },

        error => console.log(error)
      );

   
  }
}
