import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TripulanteComponent } from './tripulante.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TripulanteService } from '../services/tripulante.service';

describe('TripulanteComponent', () => {
  let component: TripulanteComponent;
  let fixture: ComponentFixture<TripulanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TripulanteComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ TripulanteService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form with empty numMecanografico', () => {
    let numMecItem = component.createTripulanteForm.controls["numMecanografico"]
    numMecItem.setValue("");
    expect(component.createTripulanteForm.valid).toBeFalsy;
  });

  it('form with valid numMecanografico', () => {
    let numMecItem = component.createTripulanteForm.controls["numMecanografico"]
    numMecItem.setValue("123456789");
    expect(component.createTripulanteForm.valid).toBeTruthy;
  });

  it('form with invalid numMecanografico', () => {
    let numMecItem = component.createTripulanteForm.controls["numMecanografico"]
    numMecItem.setValue("12345678999");
    expect(component.createTripulanteForm.valid).toBeFalsy;
  });

  it('form with empty nome', () => {
    let nomeItem = component.createTripulanteForm.controls["nome"]
    nomeItem.setValue("");
    expect(component.createTripulanteForm.valid).toBeFalsy;
  });

  it('form with valid nome', () => {
    let nomeItem = component.createTripulanteForm.controls["nome"]
    nomeItem.setValue("nome");
    expect(component.createTripulanteForm.valid).toBeFalsy;
  });

  


});
