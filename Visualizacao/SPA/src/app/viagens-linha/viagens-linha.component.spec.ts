import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViagensLinhaComponent } from './viagens-linha.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViagemService } from '../services/viagem.service';

describe('ViagemComponent', () => {
  let component: ViagensLinhaComponent;
  let fixture: ComponentFixture<ViagensLinhaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViagensLinhaComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ ViagemService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViagensLinhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  
  it('form with valid viagem', () => {
    let linha = component.createViagemForm.controls["linha"]
    linha.setValue("Paredes_Aguiar");
    let horario = component.createViagemForm.controls["horario"]
    horario.setValue(1234);
    let horarioFim = component.createViagemForm.controls["horarioFim"]
    horarioFim.setValue(1280);
    let frequencia = component.createViagemForm.controls["frequencia"]
    frequencia.setValue(30);
    let nViagens = component.createViagemForm.controls["nViagens"]
    nViagens.setValue(5);
    let percursoIda = component.createViagemForm.controls["percursoIda"]
    percursoIda.setValue("PARED>AGUIA");
    let percursoVolta = component.createViagemForm.controls["percursoVolta"]
    percursoVolta.setValue("AGUIA>PARED");
    expect(component.createViagemForm.valid).toBeTruthy();
  });

  it('form with empty linha', () => {
    let linha = component.createViagemForm.controls["linha"]
    linha.setValue("");
    let horario = component.createViagemForm.controls["horario"]
    horario.setValue(1234);
    let horarioFim = component.createViagemForm.controls["horarioFim"]
    horarioFim.setValue(1280);
    let frequencia = component.createViagemForm.controls["frequencia"]
    frequencia.setValue(30);
    let nViagens = component.createViagemForm.controls["nViagens"]
    nViagens.setValue(5);
    let percursoIda = component.createViagemForm.controls["percursoIda"]
    percursoIda.setValue("PARED>AGUIA");
    let percursoVolta = component.createViagemForm.controls["percursoVolta"]
    percursoVolta.setValue("AGUIA>PARED");
    expect(component.createViagemForm.valid).toBeFalsy();
  });

  it('form with invalid viagem', () => {
    let linha = component.createViagemForm.controls["linha"]
    linha.setValue("Paredes_Aguiar");
    let horario = component.createViagemForm.controls["horario"]
    horario.setValue(-1);
    let horarioFim = component.createViagemForm.controls["horarioFim"]
    horarioFim.setValue(1280);
    let frequencia = component.createViagemForm.controls["frequencia"]
    frequencia.setValue(30);
    let nViagens = component.createViagemForm.controls["nViagens"]
    nViagens.setValue(5);
    let percursoIda = component.createViagemForm.controls["percursoIda"]
    percursoIda.setValue("PARED>AGUIA");
    let percursoVolta = component.createViagemForm.controls["percursoVolta"]
    percursoVolta.setValue("AGUIA>PARED");
    expect(component.createViagemForm.valid).toBeFalsy();
  });
 

});
