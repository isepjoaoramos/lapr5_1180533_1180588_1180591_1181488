import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Linha from '../model/Linha';
import { Percurso } from '../model/percurso';
import { Viagem } from '../model/Viagem';
import { LinhaService } from '../services/linha.service';
import { PercursoService } from '../services/percurso.service';
import { ViagemService } from '../services/viagem.service';


@Component({
  selector: 'app-viagens-linha',
  templateUrl: './viagens-linha.component.html',
  styleUrls: ['./viagens-linha.component.css']
})
export class ViagensLinhaComponent implements OnInit {

  createViagemForm;
  linhas: Linha[] = [];
  percursos: Percurso[] = [];
  viagens: Viagem[] = [];
  percursosDesc: Percurso[] = [];

  constructor(private viagemService: ViagemService, private linhaService: LinhaService, private percursoService: PercursoService, private formBuilder: FormBuilder) {
    this.createViagemForm = this.formBuilder.group({
      linha: ['', Validators.required],
      horario: ['', Validators.pattern("[0-9]+")],
      horarioFim: ['', Validators.pattern("[0-9]+")],
      frequencia: ['', Validators.pattern("[0-9]+")],
      nViagens: ['', Validators.pattern("[0-9]+")],
      percurso: [''],
      percursoIda: ['', Validators.required],
      percursoVolta: ['', Validators.required],    
    })
    this.linhaService.getLinhas().subscribe(
      response_linhas => {
        this.linhas = response_linhas;
      })
  }

  ngOnInit(): void {
  }


  onFocusOutEventHorario(event: any) {
    var warningtext = document.getElementById("horarioText");
    var input = (document.getElementById("horario") as HTMLInputElement).value;
    var input2 = (document.getElementById("horarioM") as HTMLInputElement).value;
    var input3 = (document.getElementById("horarioS") as HTMLInputElement).value;
    if (parseFloat(input) < 0 || parseFloat(input) >= 24 || parseFloat(input2) < 0 ||
     parseFloat(input2) >= 60 || parseFloat(input3) < 0 || parseFloat(input3) >=60 ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  onFocusOutEventHorarioFim(event: any) {
    var warningtext = document.getElementById("horarioFimText");
    var input = (document.getElementById("horarioFim") as HTMLInputElement).value;
    var input2 = (document.getElementById("horarioMFim") as HTMLInputElement).value;
    var input3 = (document.getElementById("horarioSFim") as HTMLInputElement).value;
    if (parseFloat(input) < 0 || parseFloat(input) >= 24 || parseFloat(input2) < 0 ||
     parseFloat(input2) >= 60 || parseFloat(input3) < 0 || parseFloat(input3) >=60 ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
   
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  onFocusOutEventFrequencia(event: any) {
    var warningtext = document.getElementById("frequenciaText");
    var input = (document.getElementById("frequencia") as HTMLInputElement).value;
    if (parseFloat(input) <= 0 ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  onFocusOutEventNViagens(event: any) {
    var warningtext = document.getElementById("nViagensText");
    var input = (document.getElementById("nViagens") as HTMLInputElement).value;
    if (parseFloat(input) <= 0 ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  onFocusOutEventLinha(event: any) {
    this.percursoService.getPercursosByLinha((document.getElementById("linha") as HTMLInputElement).value).subscribe(
      response_percursos => {
        this.percursos = response_percursos; console.log(this.percursos);

        this.createViagemForm.controls.percurso.patchValue(this.percursos[0].descricao);
      })
  }

  addViagens(): void {
    
    var linha = (document.getElementById("linha") as HTMLInputElement).value;
    //var horarioSaida: number = this.createViagemForm.get('horario')?.value;
   // var horaInicio = parseInt((document.getElementById("horario") as HTMLInputElement).value);
    var hora = parseInt((document.getElementById("horario") as HTMLInputElement).value);
    var minuto = parseInt((document.getElementById("horarioM") as HTMLInputElement).value);
    var segundo = parseInt((document.getElementById("horarioS") as HTMLInputElement).value);    
    var horaInicio = hora*3600 + minuto*60 + segundo  
    var horaF = parseInt((document.getElementById("horarioFim") as HTMLInputElement).value);
    var minutoFim = parseInt((document.getElementById("horarioM") as HTMLInputElement).value);
    var segundoFim = parseInt((document.getElementById("horarioS") as HTMLInputElement).value);    
    var horaFim = horaF*3600 + minutoFim*60 + segundoFim  
    var percursoIda = (document.getElementById("percursoIda") as HTMLInputElement).value;
    var percursoVolta = (document.getElementById("percursoVolta") as HTMLInputElement).value;
    var frequencia = parseInt((document.getElementById("frequencia") as HTMLInputElement).value);
    var nViagens = parseInt((document.getElementById("nViagens") as HTMLInputElement).value);
    var percurso = "";
    var horaSaida=0;
    var tempoIda=0;
    var tempoVolta=0;
    var tempo=0;
    var last=false;

    console.log(linha);
    console.log(horaInicio);
    console.log(percursoIda)
    console.log(percursoVolta)
    console.log(frequencia)
    console.log(nViagens)

  var warningtextFrequencia = document.getElementById("frequenciaText");
  var warningtextNViagens = document.getElementById("nViagensText");
  var warningtextFinal = document.getElementById("finalText");
  var warningtextHorario = document.getElementById("horarioText");
  var warningTextSucesso = document.getElementById("successText")

  if(warningtextFrequencia != null && warningtextNViagens != null && warningtextHorario != null){
    if(
    warningtextFrequencia.style.visibility == "hidden" && warningtextNViagens.style.visibility == "hidden" && 
    warningtextHorario.style.visibility == "hidden" && linha != '' && percursoIda != '' && percursoVolta != '' && horaInicio != NaN
    && frequencia != NaN && nViagens != NaN
     ){
      //location.assign('sucesso');
      if (warningtextFinal != null){
        warningtextFinal.style.visibility = "hidden";
      }
    } else {
      if (warningtextFinal != null){
        warningtextFinal.style.visibility = "visible";
      }
    }
  }

    this.percursoService.getPercursoByDescricao(percursoIda).subscribe(
      response => {
        this.percursosDesc = response;
      
    if (this.percursosDesc.length>0){
      console.log(this.percursosDesc)
        tempoIda = this.percursosDesc[0].duracao;
        console.log("TEMPO: "+tempoIda)
      }

      this.percursoService.getPercursoByDescricao(percursoVolta).subscribe(
        response => {
          this.percursosDesc = response;
        
      if (this.percursosDesc.length>0){
        console.log(this.percursosDesc)
          tempoVolta = this.percursosDesc[0].duracao;
          console.log("TEMPO: "+tempoIda)
        }

//console.log("TEMPOFORA: "+tempo)
    for(var i=0; i<nViagens && horaSaida<=horaFim; i++){
    if(i==nViagens-1){
      last=true;
    }
      if(i%2==0){
        percurso = percursoIda;
        horaSaida = horaInicio + (i/2)*frequencia;
        tempo=tempoIda;
        
        if(horaSaida+frequencia+tempoIda > horaFim && horaSaida+tempoIda+tempoVolta > horaFim){
          last=true
        }
      } else{
        percurso = percursoVolta;
        horaSaida += tempoIda;
        tempo=tempoVolta;

        if(horaSaida+frequencia > horaFim){
          last=true
        }
      }

        if(horaSaida + tempo <= horaFim){
          if (warningtextFinal != null){
            if(warningtextFinal.style.visibility == "hidden"){
              this.viagemService.addViagem({ linha, horaSaida, percurso } as Viagem)
              .subscribe(viagens => {console.log(viagens)
              this.viagens.push(viagens);
                if(last){
                  if(warningTextSucesso!=null)
                  warningTextSucesso.style.visibility="visible";
                }
            })
          }
        }
      }
     // var horariosNos = Array<number>();
    }
      })
    })
  }

}

