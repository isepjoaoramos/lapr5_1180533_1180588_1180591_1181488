import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import No from '../model/No';
import { MudancaMotoristasService } from '../services/mudanca-motoristas.service';
import { NoService } from '../services/no.service';

@Component({
  selector: 'app-mudanca-motoristas',
  templateUrl: './mudanca-motoristas.component.html',
  styleUrls: ['./mudanca-motoristas.component.css']
})
export class MudancaMotoristasComponent implements OnInit {
  noInicio : string = "";
  noFim : string = "";
  horario : number = 0;
  nos: No[] = [];
  plan_mud_mot_form;

  private fs = require('fs');

  constructor(private mudancaMotoristasService : MudancaMotoristasService, private noService: NoService, private formBuilder: FormBuilder) { 
    this.plan_mud_mot_form = this.formBuilder.group({
      noInicio: ['',Validators.required],
      noFim: ['',Validators.required],
      horario: ''
    })
    this.noService.getNos().subscribe(
      response_nos => {this.nos = response_nos;console.log(this.nos);
      this.plan_mud_mot_form.controls.noInicio.patchValue(this.nos[0].abreviatura);
      this.plan_mud_mot_form.controls.noFim.patchValue(this.nos[1].abreviatura);});
  }

  ngOnInit(): void {
  }

  onClick(): void {
      var noInicio = (document.getElementById("noInicio") as HTMLInputElement).value;
      var noFim = (document.getElementById("noFim") as HTMLInputElement).value;
      var horario = parseInt((document.getElementById("horario") as HTMLInputElement).value);
      var url = "http://uvm007.dei.isep.ipp.pt:5002/mudanca_motoristas?no_inicio="+noInicio+"&no_fim="+noFim+"&horario="+horario;
      //var url = "http://localhost:5002/mudanca_motoristas?no_inicio="+noInicio+"&no_fim="+noFim+"&horario="+horario;
      this.mudancaMotoristasService.sendPost(url);
      location.assign(url);

      // try {
      //   var data = this.fs.readFileSync('../../../../../Planeamento/output.txt', 'utf8');
      //   console.log(data);    
      // } catch(e) {
      //   console.log('Error:', e.stack);
      // }
  }
}
