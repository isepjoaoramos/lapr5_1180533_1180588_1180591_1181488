import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MudancaMotoristasComponent } from './mudanca-motoristas.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MudancaMotoristasService } from '../services/mudanca-motoristas.service';

describe('MudancaMotoristasComponent', () => {
  let component: MudancaMotoristasComponent;
  let fixture: ComponentFixture<MudancaMotoristasComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MudancaMotoristasComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ MudancaMotoristasService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MudancaMotoristasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form with empty no1', () => {
    component.plan_mud_mot_form.controls["noInicio"].setValue("");
    component.plan_mud_mot_form.controls["noFim"].setValue("noteste2");
    expect(component.plan_mud_mot_form.valid).toBeFalsy();
  });

  it('form with empty no1', () => {
    component.plan_mud_mot_form.controls["noInicio"].setValue("noteste1");
    component.plan_mud_mot_form.controls["noFim"].setValue("");
    expect(component.plan_mud_mot_form.valid).toBeFalsy();
  });

  it('valid form', () => {
    component.plan_mud_mot_form.controls["noInicio"].setValue("noteste1");
    component.plan_mud_mot_form.controls["noFim"].setValue("noteste2");
    expect(component.plan_mud_mot_form.valid).toBeTruthy();
  });
});
