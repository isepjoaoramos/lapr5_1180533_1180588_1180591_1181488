import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ImportacaoGlxComponent } from './importacao-glx/importacao-glx.component';
import { PercursosComponent } from './percursos/percursos.component';
import { TipoviaturaComponent } from './tipoviatura/tipoviatura.component';
import { TipotripulanteComponent } from './tipotripulante/tipotripulante.component';
import { NoComponent } from './no/no.component';
import { LinhaComponent } from './linha/linha.component';

import { VisualizacaoMapaHarpComponent } from './visualizacao-mapa-harp/visualizacao-mapa-harp.component';

import { VisualizacaoMapaComponent } from './visualizacao-mapa/visualizacao-mapa.component';

import { SucessoComponent } from './sucesso/sucesso.component';
import { MudancaMotoristasComponent } from './mudanca-motoristas/mudanca-motoristas.component';

import { VisualizacaoMapa3dComponent } from './visualizacao-mapa3d/visualizacao-mapa3d.component';
import { BlocotrabalhoComponent } from './blocotrabalho/blocotrabalho.component';
import { TripulanteComponent } from './tripulante/tripulante.component';
import { ViagensComponent } from './viagens/viagens.component';
import { ViagensLinhaComponent } from './viagens-linha/viagens-linha.component';
import { ViaturaComponent } from './viatura/viatura.component';
import { AlgoritmoGeneticoComponent } from './algoritmo-genetico/algoritmo-genetico.component';
import { ServicoviaturaComponent } from './servicoviatura/servicoviatura.component';

import { LoginComponent } from './login/login.component';


import { ListarServicoViaturaComponent } from './listar-servico-viatura/listar-servico-viatura.component';
import { ListarLinhasComponent } from './listar-linhas/listar-linhas.component';

import { ServicotripulanteComponent } from './servicotripulante/servicotripulante.component';
import { ListarServicoTripulanteComponent } from './listar-servico-tripulante/listar-servico-tripulante.component';
// import { ListarViagensComponent } from './listar-viagens/listar-viagens.component';
import { ListarPercursosComponent } from './listar-percursos/listar-percursos.component';
import { ListarNosComponent } from './listar-nos/listar-nos.component';
import { RegistoComponent } from './registo/registo.component';

import { ListarViagensComponent } from './listar-viagens/listar-viagens.component';

import { HomeComponent } from './home/home.component';

import{ AuthGuard} from'./guards/auth.guard';
import{ ClienteGuard} from'./guards/clienteGuard';
import{ DataAdminGuard} from'./guards/dataAdminGuard';
import{ GestorGuard} from'./guards/gestorGuard';


const routes: Routes = [

  //exclusivo do data_admin 
  { path: 'percursos', component: PercursosComponent,canActivate:[AuthGuard,DataAdminGuard] },
  { path: 'importacaoglx', component: ImportacaoGlxComponent,canActivate:[AuthGuard,DataAdminGuard] },
  { path: 'blocos', component: BlocotrabalhoComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'tripulantes', component: TripulanteComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'viagens', component: ViagensComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'viagensLinha', component: ViagensLinhaComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'viaturas', component: ViaturaComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'tipoViatura', component: TipoviaturaComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'servicoviatura', component: ServicoviaturaComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'listarservicoviatura', component: ListarServicoViaturaComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'servicotripulante', component: ServicotripulanteComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'listar_percursos', component: ListarPercursosComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'listar_nos', component: ListarNosComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'listarlinhas', component: ListarLinhasComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'linhas', component: LinhaComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'tipoTripulante', component: TipotripulanteComponent,canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'nos', component: NoComponent, canActivate:[AuthGuard,DataAdminGuard]},
  { path: 'listar_viagens', component: ListarViagensComponent, canActivate:[AuthGuard,DataAdminGuard]},  
  { path: 'listar_servicos_tripulante', component: ListarServicoTripulanteComponent, canActivate:[AuthGuard,DataAdminGuard]},  
  
  //exclusivo do gestor 
  { path: 'algoritmo_genetico', component: AlgoritmoGeneticoComponent,canActivate:[AuthGuard,GestorGuard]},
  { path: 'mudanca_motoristas', component: MudancaMotoristasComponent,canActivate:[AuthGuard,GestorGuard]},

  //geral com  autenticação
  { path: 'visualizacao_mapa', component: VisualizacaoMapaComponent,canActivate:[AuthGuard]},
  { path: 'visualizacao_mapa_3d', component: VisualizacaoMapa3dComponent, canActivate:[AuthGuard]},
  { path: 'visualizacaoharp', component: VisualizacaoMapaHarpComponent,canActivate:[AuthGuard]},
  { path: 'home', component: HomeComponent,canActivate:[AuthGuard]},

  //geral sem autenticação
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'registo', component: RegistoComponent},
  { path: 'login', component: LoginComponent},
//  { path: 'sucesso', component: SucessoComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
