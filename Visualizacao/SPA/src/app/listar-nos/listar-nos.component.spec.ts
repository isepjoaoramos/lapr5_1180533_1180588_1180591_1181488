import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarNosComponent } from './listar-nos.component';

describe('ListarNosComponent', () => {
  let component: ListarNosComponent;
  let fixture: ComponentFixture<ListarNosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarNosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarNosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
