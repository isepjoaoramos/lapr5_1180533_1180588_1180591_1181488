import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import No from '../model/No';
import { NoService } from '../services/no.service';

@Component({
  selector: 'app-listar-nos',
  templateUrl: './listar-nos.component.html',
  styleUrls: ['./listar-nos.component.css']
})
export class ListarNosComponent implements OnInit {
  listarNosForm;
  nos: No[] = [];

  constructor(private formBuilder: FormBuilder, private noService: NoService) {
    this.listarNosForm = this.formBuilder.group({
      nos: []
    });

    this.noService.getNos().subscribe(response => {
      this.nos = response;
    });
  }

  ngOnInit(): void {}
}