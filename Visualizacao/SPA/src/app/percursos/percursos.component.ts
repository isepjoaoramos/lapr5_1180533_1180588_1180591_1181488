import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { LinhaService } from '../services/linha.service';
import Linha from '../model/Linha';
import No from '../model/No';

import { Percurso } from '../model/percurso';
import { Segmento } from '../model/Segmento';
import { NoService } from '../services/no.service';
import { PercursoService } from '../services/percurso.service';

@Component({
  selector: 'app-percursos',
  templateUrl: './percursos.component.html',
  styleUrls: ['./percursos.component.css']
})
export class PercursosComponent implements OnInit {
  @ViewChild('no1') no1: string = "";
  @ViewChild('no2') no2: string = "";
  @ViewChild('distancia') distancia: number = 0;
  @ViewChild('duracao') duracao: number = 0;
  @ViewChild('ordem') ordem: number = 0;

  percursos: Percurso[] = [];
  orientacao: string = "";
  descricao: string = "";
  linha: string = "";
  segmentos: Array<Segmento> = [];
  i: number = 0;
  no1_: string = "";
  no2_: string = "";
  distancia_: number = 0;
  duracao_: number = 0;
  ordem_: number = 0;
  checkOrientacao: string = "";
  createPercursoForm;
  noInicio: string = "";
  noFim: string = "";
  nos: No[] = [];
  linhas: Linha[] = [];

  constructor(private percursoService: PercursoService, private linhaService: LinhaService, private noService: NoService, private formBuilder: FormBuilder) {
    this.createPercursoForm = this.formBuilder.group({
      orientacao: ['',[Validators.required,Validators.pattern("Return|Go")]],
      descricao: ['',Validators.required],
      linha: ['',Validators.required],
      noInicio: ['',Validators.required],
      noFim: ['',Validators.required],
      distancia_: ['',Validators.pattern("[0-9]+")],
      duracao_: ['',Validators.pattern("[0-9]+")],
      ordem_: '',
    })
    this.noService.getNos().subscribe(
      response_nos => {this.nos = response_nos;console.log(this.nos);
      this.createPercursoForm.controls.noInicio.patchValue(this.nos[0].abreviatura)
      this.createPercursoForm.controls.noFim.patchValue(this.nos[1].abreviatura);
    })

      this.linhaService.getLinhas().subscribe(
        response_linhas => {this.linhas = response_linhas;
          this.createPercursoForm.controls.linha.patchValue(this.linhas[0].nome);})
  }

  ngOnInit() {
    
  }

  getPercursos(): void {
    this.percursoService.getPercursos()
    .subscribe(percursos => this.percursos = percursos);
  }

  onFocusOutEventOrientacao(event: any){
    var warningtext = document.getElementById("oriText");
    var input = (document.getElementById("orientacao") as HTMLInputElement).value;
    console.log(input);
    if (input != "Return" && input != "Go" && input != "Reforço"){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
}

  onFocusOutEventDescricao(event: any){
    var warningtext = document.getElementById("descriText");
    var input = (document.getElementById("descricao") as HTMLInputElement).value;
    if (input == null || input == "" ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  onFocusOutEventDistancia(event: any){
    var warningtext = document.getElementById("distText");
    var input = (document.getElementById("distancia_") as HTMLInputElement).value;
    if (parseFloat(input) <= 0 ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  onFocusOutEventDuracao(event: any){
    var warningtext = document.getElementById("durText");
    var input = (document.getElementById("duracao_") as HTMLInputElement).value;
    if (parseFloat(input) <= 0 ){
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
    }
  }
  else if(warningtext != null){
    warningtext.style.visibility="hidden";
  }
  }

  checkOrientacaoCorreta(): boolean{
    if (this.orientacao != "Return"){
      this.orientacao="ERRO";
      return false;
    }
    return true;
  }

  addPercurso(): void {
      var orientacao = (document.getElementById("orientacao") as HTMLInputElement).value; console.log(orientacao)
      var descricao = "undefined";
      var linha = (document.getElementById("linha") as HTMLInputElement).value;
      let distancia = 0;
      let duracao = 0;
      let noInicio = "undefined";
      let noFim = "undefined";
      if (!orientacao || !descricao || !linha || !this.segmentos) { return; }
      var segmentos = this.segmentos;
     

        
        var warningtextDistancia = document.getElementById("distText");
        var warningtextDuracao = document.getElementById("durText");
        var warningtextFinal = document.getElementById("finalText");
        
        if(warningtextDistancia != null && warningtextDuracao != null){
          if(
          warningtextDistancia.style.visibility == "hidden" && warningtextDuracao.style.visibility == "hidden"){
            this.percursoService.addPercurso({ orientacao,distancia,descricao,duracao,linha,noInicio,noFim,segmentos } as Percurso)
                .subscribe(percurso => {
                this.percursos.push(percurso);  
            });
            alert("Percurso criado com sucesso!")
            //location.assign('sucesso');
          } else {
            if (warningtextFinal != null){
              warningtextFinal.style.visibility = "visible";
            }
          }
        }
    }

addSegmento(): void{
  var no1 = (document.getElementById("noInicio") as HTMLInputElement).value;
  var no2 = (document.getElementById("noFim") as HTMLInputElement).value;
  var distancia = parseFloat((document.getElementById("distancia_") as HTMLInputElement).value);
  var dur = parseFloat((document.getElementById("duracao_") as HTMLInputElement).value);
  var duracao = dur*60
  var ordem = this.segmentos.length+1;
  var segmentos = this.segmentos;
  if(this.segmentos.length==0){
  let segmento = {no1,no2,distancia,duracao,ordem};
  this.segmentos.push(segmento);
  } else {
    no1 = segmentos[segmentos.length-1].no2;
    let segmento = {no1,no2,distancia,duracao,ordem};
    this.segmentos.push(segmento);
  }
}
  }