import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PercursosComponent } from './percursos.component';

describe('PercursosComponent', () => {
  let component: PercursosComponent;
  let fixture: ComponentFixture<PercursosComponent>;
  let ori = HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PercursosComponent],
      imports: [HttpClientModule,FormsModule,ReactiveFormsModule],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true }, PercursosComponent, FormBuilder
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PercursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    ori = fixture.nativeElement.querySelector('orientacao');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form with valid orientacao', () => {
    component.createPercursoForm.controls["orientacao"].setValue("Return");
    component.createPercursoForm.controls["descricao"].setValue("Novo");
    component.createPercursoForm.controls["linha"].setValue("Novo");
    component.createPercursoForm.controls["noInicio"].setValue("Novo");
    component.createPercursoForm.controls["noFim"].setValue("Novo2");
    component.createPercursoForm.controls["distancia_"].setValue(1);
    component.createPercursoForm.controls["duracao_"].setValue(1);
    expect(component.createPercursoForm.valid).toBeTruthy();
  });

  it('form with wrong orientacao', () => {
    component.createPercursoForm.controls["orientacao"].setValue("Ret");
    component.createPercursoForm.controls["descricao"].setValue("Novo");
    component.createPercursoForm.controls["linha"].setValue("Novo");
    component.createPercursoForm.controls["noInicio"].setValue("Novo");
    component.createPercursoForm.controls["noFim"].setValue("Novo2");
    component.createPercursoForm.controls["distancia_"].setValue(1);
    component.createPercursoForm.controls["duracao_"].setValue(1);
    expect(component.createPercursoForm.valid).toBeFalsy();
  });

  it('form with empty descricao', () => {
    component.createPercursoForm.controls["orientacao"].setValue("Return");
    component.createPercursoForm.controls["descricao"].setValue("");
    component.createPercursoForm.controls["linha"].setValue("Novo");
    component.createPercursoForm.controls["noInicio"].setValue("Novo");
    component.createPercursoForm.controls["noFim"].setValue("Novo2");
    component.createPercursoForm.controls["distancia_"].setValue(1);
    component.createPercursoForm.controls["duracao_"].setValue(1);
    expect(component.createPercursoForm.valid).toBeFalsy();
  });

  it('form with wrong distancia', () => {
    component.createPercursoForm.controls["orientacao"].setValue("Return");
    component.createPercursoForm.controls["descricao"].setValue("");
    component.createPercursoForm.controls["linha"].setValue("Novo");
    component.createPercursoForm.controls["noInicio"].setValue("Novo");
    component.createPercursoForm.controls["noFim"].setValue("Novo2");
    component.createPercursoForm.controls["distancia_"].setValue("failedTest");
    component.createPercursoForm.controls["duracao_"].setValue(1);
    expect(component.createPercursoForm.valid).toBeFalsy();
  });
});
