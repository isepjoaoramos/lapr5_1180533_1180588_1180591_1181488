import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Linha from '../model/Linha';
import { Viagem } from '../model/Viagem';
import { LinhaService } from '../services/linha.service';
import { ViagemService } from '../services/viagem.service';

@Component({
  selector: 'app-listar-viagens',
  templateUrl: './listar-viagens.component.html',
  styleUrls: ['./listar-viagens.component.css']
})
export class ListarViagensComponent implements OnInit {

  listarViagensForm;
  linhas: Linha[] = [];
  viagens: Viagem[] = [];

  constructor(private linhaService: LinhaService, private viagensService: ViagemService, private formBuilder: FormBuilder) {
    this.listarViagensForm = this.formBuilder.group({
      linha: ['', Validators.required],
      viagens: []
    })
    this.linhaService.getLinhas().subscribe(
      response_linhas => {console.log(response_linhas)
        this.linhas = response_linhas;
        this.listarViagensForm.controls.linha.patchValue(this.linhas[0].nome);

        this.viagensService.getViagensByLinha(this.linhas[0].nome).subscribe(
          response_viagens => {
            this.viagens = response_viagens;
    
           
          })
          
      }
      )
      

  }

  ngOnInit(): void {
  }

 /*  changeToCreateViagemMode(): void{
    location.assign('viagens')
  }

  changeToCreateViagensMode(): void{
    location.assign('viagensLinha')
  } */

  onFocusOutEventLinha(event: any) {
    this.viagensService.getViagensByLinha((document.getElementById("linha") as HTMLInputElement).value).subscribe(
      response_viagens => {
        this.viagens = response_viagens; console.log(this.viagens);
      // for (let i=0; i<this.viagens.length; i++){
      //   this.
      // }
       // this.listarPercursosForm.controls.percurso.patchValue(this.percursos[0].descricao);
      })
  }
}
