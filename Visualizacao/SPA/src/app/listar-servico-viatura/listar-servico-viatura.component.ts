import { Component, OnInit } from '@angular/core';
import ServicoViatura from '../model/ServicoViatura';
import {ServicoviaturaService} from '../services/servicoviatura.service';

@Component({
  selector: 'app-listar-servico-viatura',
  templateUrl: './listar-servico-viatura.component.html',
  styleUrls: ['./listar-servico-viatura.component.css']
})
export class ListarServicoViaturaComponent implements OnInit {

  servicosViatura: ServicoViatura[] = [];

  constructor(private servicoViaturaService: ServicoviaturaService) { 
    this.servicoViaturaService.getAllServicosViatura().subscribe(
      responseServicosViatura => {
        this.servicosViatura = responseServicosViatura;
      }
    )
  }

  ngOnInit(): void {
  }


  /* changeToCreateMode(): void{
    location.assign('servicoviatura')
  } */

}
