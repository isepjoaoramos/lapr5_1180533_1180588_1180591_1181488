import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicotripulanteComponent } from './servicotripulante.component';

describe('ServicotripulanteComponent', () => {
  let component: ServicotripulanteComponent;
  let fixture: ComponentFixture<ServicotripulanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicotripulanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicotripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
