import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import ServicoTripulante from '../model/ServicoTripulante';
import { BlocoTrabalho } from '../model/BlocoTrabalho';
import { BlocoTrabalhoIn } from '../model/BlocoTrabalhoIn';
import { ServicoTripulanteService } from '../services/servicotripulante.service';
import { BlocotrabalhoService } from '../services/blocotrabalho.service';

@Component({
  selector: 'app-servicotripulante',
  templateUrl: './servicotripulante.component.html',
  styleUrls: ['./servicotripulante.component.css']
})
export class ServicotripulanteComponent implements OnInit {
  createServicoTripulanteForm;

  
  blocosBox: BlocoTrabalho[] = [];
  
  blocosEscolhidos: BlocoTrabalhoIn[] = [];

  constructor(
    private servicoTripulanteService: ServicoTripulanteService,
    private blocoService: BlocotrabalhoService,
    private formBuilder: FormBuilder
  ) {
    this.createServicoTripulanteForm = this.formBuilder.group({
      codigo: '',
      blocos: ''
    });

    this.blocoService.getBlocos().subscribe(response_blocos => {
      this.blocosBox = response_blocos;console.log("blocos");
      console.log(response_blocos)
    });

  }

  ngOnInit(): void {}

  onSubmit(servico: ServicoTripulante) {
    var codServicoTripulante = (document.getElementById('codigo') as HTMLInputElement).value;
    var blocos = this.blocosEscolhidos;

    if (!codServicoTripulante) {
      return;
    }

    var warningtextCodigo = document.getElementById('codigoText');
    var warningtextDescricao = document.getElementById('descricaoText');
    var warningtextFinal = document.getElementById('finalText');

    if (warningtextCodigo != null && warningtextDescricao != null) {
      if (
        warningtextCodigo.style.visibility == 'hidden' &&
        warningtextDescricao.style.visibility == 'hidden'
      ) {
       // location.assign('sucesso');
      } else {
        if (warningtextFinal != null) {
          warningtextFinal.style.visibility = 'visible';
        }
      }
    }
    this.servicoTripulanteService.addServicoTripulante({codServicoTripulante,blocos} as ServicoTripulante).subscribe(
      response => this.sucesso(response),
      error => console.log('Error', error)
    );

    this.createServicoTripulanteForm.reset();
  }

  sucesso(response: ServicoTripulante) {
    alert("Serviço de Tripulante criado com sucesso!")
    //location.assign('sucesso');
    console.log('Sucess', response);
  }

  codigoChange(event: any) {
    var warningtext = document.getElementById('codigoText');
    var input = (document.getElementById('codigo') as HTMLInputElement).value;
    console.log(input);
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
      this.servicoTripulanteService.getByCodServicoTripulante(input).subscribe(
        response => {
          if (warningtext != null) {
            warningtext.style.visibility = 'visible';
          }
        },
        error => console.log(error)
      );
    }
  }

  addBloco(){
    var selected_blocoId = this.createServicoTripulanteForm.get('blocos')?.value;
    var blocoId= selected_blocoId;
    console.log("addBloco")
    console.log(blocoId);
    this.blocosEscolhidos.push({blocoId} as BlocoTrabalhoIn);
    alert('Bloco de trabalho adicionado!');


    /* this.blocoService.getBlocoById(selected_blocoId)
    .subscribe(
      bloco => {
        //adiciona ao array de blocos de trabalho do servico
        this.blocosEscolhidos.push(bloco);
        console.log("encontra bloco")
        console.log(bloco);
        alert('Bloco de trabalho adicionado!');
      },

      error => console.log(error)
    ); */

    
  }

}
