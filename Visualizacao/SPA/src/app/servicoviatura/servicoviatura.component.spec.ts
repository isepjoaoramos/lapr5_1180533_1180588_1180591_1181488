import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicoviaturaComponent } from './servicoviatura.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServicoviaturaService } from '../services/servicoviatura.service';

describe('ServicoViaturaComponent', () => {
  let component: ServicoviaturaComponent;
  let fixture: ComponentFixture<ServicoviaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServicoviaturaComponent],
      imports: [HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ServicoviaturaService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicoviaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('form without codServicoViatura', () => {
    expect(component.createServicoViaturaForm.valid).toBeFalsy();
  });


});
