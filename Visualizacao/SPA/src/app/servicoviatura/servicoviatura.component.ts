import { Component, OnInit } from '@angular/core';

import { ViaturaService } from '../services/viatura.service';

import { ServicoviaturaService } from '../services/servicoviatura.service'

import ServicoViatura from '../model/ServicoViatura';

import { Viagem } from '../model/Viagem';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { List } from 'lodash';
import { ViagemService } from '../services/viagem.service';

@Component({
  selector: 'app-servicoviatura',
  templateUrl: './servicoviatura.component.html',
  styleUrls: ['./servicoviatura.component.css']
})
export class ServicoviaturaComponent implements OnInit {

  createServicoViaturaForm: FormGroup;


  viagens: Viagem[] = [];

  selected_viagens: Viagem[] = [];

  constructor(private servicoViaturaService: ServicoviaturaService,
    private viagensService: ViagemService, private formBuilder: FormBuilder) {
    this.createServicoViaturaForm = this.formBuilder.group({
      codServicoViatura: new FormControl('', Validators.required),
      descricao: '',
      viagens: '',
    })

    this.selected_viagens = [];

    this.viagensService.getViagens().subscribe(
      response_viagens => {
        this.viagens = response_viagens;
        this.createServicoViaturaForm.controls.viagens.patchValue(this.viagens[0].percurso)
    })

  }

  ngOnInit(): void {
  }



  onChangeCodServicoViatura(event: any){
    var text = document.getElementById("CodServicoViaturaWarningText");
    if (text != null) {
      text.style.visibility = "hidden";
    }
    let codServicoViatura: string = this.createServicoViaturaForm.get('codServicoViatura')?.value;

    let servicoViatura: any;

    this.servicoViaturaService.getByCodServicoViatura(codServicoViatura).subscribe(
      response => {
        var warningtext = document.getElementById("CodServicoViaturaWarningText")
        if (warningtext != null) {
          warningtext.style.visibility = "visible";
        }
      },
      error => console.log(error)
    );
  }

  addViagem(){
    var selected_viagemId = this.createServicoViaturaForm.get('viagens')?.value;
    for(var i=0; i<this.viagens.length; i++){
      console.log(this.viagens[i].viagemId);
      if(selected_viagemId == this.viagens[i].viagemId){
        var warning_text: any = document.getElementById("EmptyViagensWarning");
        if(warning_text != null){
          warning_text.style.visibility = "hidden";
        }
        var selected_viagem = this.viagens[i];
        this.selected_viagens.push(selected_viagem);
      }
    }
    
  }




  onSubmit(){
    var warning_text: any = document.getElementById("EmptyViagensWarning");

    if(this.selected_viagens.length == 0 && warning_text != null){
      warning_text.style.visibility = "visible";
    }else{
      var codServicoViatura = this.createServicoViaturaForm.get('codServicoViatura')?.value;
      var descricao = this.createServicoViaturaForm.get('descricao')?.value;
      var viagens = this.selected_viagens;
  
      this.servicoViaturaService.addServicoViatura({ codServicoViatura, descricao,viagens} as ServicoViatura)
        .subscribe(
          response => this.sucesso(response),
          error => console.log('Error', error)
        );
  
      this.createServicoViaturaForm.reset();
      this.selected_viagens = [];
    }
  }

  
  sucesso(response: ServicoViatura) {
    alert("Serviço de Viatura criado com sucesso!")
    //location.assign('sucesso');
    console.log('Sucess', response);
  }

}
