import { Component, OnInit } from '@angular/core';

import { FormBuilder } from '@angular/forms';
import { response } from 'express';

import { Registo } from '../model/Registo';
import { RegistoService } from '../services/registo.service';

@Component({
  selector: 'app-registo',
  templateUrl: './registo.component.html',
  styleUrls: ['./registo.component.css']
})
export class RegistoComponent implements OnInit {
  createRegistoForm;

  constructor(
    private registoService: RegistoService,
    private formBuilder: FormBuilder
  ) {
    this.createRegistoForm = this.formBuilder.group({
      email: '',
      username: '',
      password: ''
    });
  }

  ngOnInit(): void {}

  onSubmit(registo: Registo) {
    var email = (document.getElementById('email') as HTMLInputElement).value;
    var username = (document.getElementById('username') as HTMLInputElement)
      .value;
    var password = (document.getElementById('password') as HTMLInputElement)
      .value;
    var password_repeat = (document.getElementById(
      'password_repeat'
    ) as HTMLInputElement).value;

    if (!email || !username || !password || !password_repeat ) {
      var warningtextFinal = document.getElementById('falhaText');

      if (warningtextFinal != null) {
        warningtextFinal.style.visibility = 'visible';
      }
    }

    var warningtextEmail = document.getElementById('emailText');
    var warningtextUsername1 = document.getElementById('usernameText1');
    var warningtextUsername2 = document.getElementById('usernameText2');
    var warningtextPassword = document.getElementById('passwordText');
    var warningtextPassword_repeat = document.getElementById(
      'password_repeatText'
    );
    var warningtextFinal = document.getElementById('falhaTextLabel');
    var warningtextRadio = document.getElementById('termosText');

    if (
      warningtextEmail != null &&
      warningtextUsername1 != null &&
      warningtextUsername2 != null &&
      warningtextPassword != null &&
      warningtextPassword_repeat != null
    ) {
      if (
        warningtextEmail.style.visibility == 'hidden' &&
        warningtextUsername1.style.visibility == 'hidden' &&
        warningtextUsername2.style.visibility == 'hidden' &&
        warningtextPassword.style.visibility == 'hidden' &&
        warningtextPassword_repeat.style.visibility == 'hidden'
      ) {
        if (
          (document.getElementById('concordaTermos') as HTMLInputElement)
            .checked
        ) {
          console.log("adicionou");
          //location.assign('sucesso');
          
          this.registoService.addUser(registo).subscribe(
            response => this.sucesso(response),
            error => console.log('Error', error)
          );
        } else {
          if (warningtextFinal != null) {
            warningtextFinal.style.visibility = 'visible';
          }
          if (warningtextRadio != null) {
            warningtextRadio.style.visibility = 'visible';
          }

        }

        //this.createUserForm.reset();
      } else {
        if (warningtextFinal != null) {
          warningtextFinal.style.visibility = 'visible';
        }
      }
    }
  }

  sucesso(response: Registo) {
    alert("Utilizador Registado com sucesso !")
    this.createRegistoForm.reset()
    console.log('Sucess', response);
  }

  mostrarTermos(){
    var termosDiv = document.getElementById("termosBoxId");
    if (termosDiv != null) {
      termosDiv.style.visibility = 'visible';
    }

  }

  hideTermos(){
    var termosDiv = document.getElementById("termosBoxId");
    if (termosDiv != null) {
      termosDiv.style.visibility = 'hidden';
    }

  }

  emailChange(event: any) {
    var warningtext = document.getElementById('emailText');
    var input = (document.getElementById('email') as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      this.registoService.getUserByEmail(input).subscribe(
        response => {
          if (warningtext != null) {
            warningtext.style.visibility = 'visible';
          }
        },
        error => console.log(error)
      );

      //verificar formato do email  "[AZ-az]@[email.com],[AZ-az]@[gmail.com],,[AZ-az]@[hotmail.com]"
      let regex = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+[.com]');

      if (regex.test(input)) {
        warningtext.style.visibility = 'hidden';
      } else {
        warningtext.style.visibility = 'visible';
      }
    }
  }

  usernameChange(event: any) {
    var warningtext1 = document.getElementById('usernameText1');
    var warningtext2 = document.getElementById('usernameText2');
    var input = (document.getElementById('username') as HTMLInputElement).value;
    if (input == null || input == '' || input.length <=5) {
      if (warningtext1 != null) {
        warningtext1.style.visibility = 'visible';
      }
    } else if (warningtext2 != null) {
      //verificar se username existe
      this.registoService.getUserByUsername(input).subscribe(
        response => {
          if (warningtext2 != null) {
            warningtext2.style.visibility = 'visible';
          }
        },
        error => console.log(error)
      );

      warningtext1.style.visibility = 'hidden';
      warningtext2.style.visibility = 'hidden';
    }
  }

  passwordChange(event: any) {
    var warningtext_password = document.getElementById('passwordText');

    var input = (document.getElementById('password') as HTMLInputElement).value;
    if (input == null || input == '' || input.length < 6) {
      if (warningtext_password != null) {
        warningtext_password.style.visibility = 'visible';
      }
    } else if (warningtext_password != null) {
      warningtext_password.style.visibility = 'hidden';
    }
  }

  password_repeatChange(event: any) {
    var warningtext_password_repeat = document.getElementById(
      'password_repeatText'
    );
    var input_repeat = (document.getElementById(
      'password_repeat'
    ) as HTMLInputElement).value;
    var input_password = (document.getElementById(
      'password'
    ) as HTMLInputElement).value;

    if (
      input_repeat == null ||
      input_repeat == '' ||
      input_repeat.length == 0 ||
      input_repeat != input_password //passwords tem de coincidir
    ) {
      if (warningtext_password_repeat != null) {
        warningtext_password_repeat.style.visibility = 'visible';
      }
    } else if (warningtext_password_repeat != null) {
      warningtext_password_repeat.style.visibility = 'hidden';
    }
  }
}
