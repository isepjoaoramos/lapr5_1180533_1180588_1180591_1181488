import { ComponentFixture, TestBed } from '@angular/core/testing';

  import { RegistoComponent } from './registo.component';
  import { HttpClientModule } from '@angular/common/http';
  import { FormsModule, ReactiveFormsModule } from '@angular/forms';
  import { RegistoService } from '../services/registo.service';
  
  
  describe('RegistoComponent', () => {
    let component: RegistoComponent;
    let fixture: ComponentFixture<RegistoComponent>;
  
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        declarations: [ RegistoComponent ],
        imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
        providers: [ RegistoService ]
      })
      .compileComponents();
    });
  
    beforeEach(() => {
      fixture = TestBed.createComponent(RegistoComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  
    it('form with empty email', () => {
      let emailItem = component.createRegistoForm.controls["email"]
      emailItem.setValue("");
      expect(component.createRegistoForm.valid).toBeFalsy;
    });
  
    it('form with valid email', () => {
      let emailItem = component.createRegistoForm.controls["email"]
      emailItem.setValue("email@email.com");
      expect(component.createRegistoForm.valid).toBeTruthy;
    });
  
    it('form with valid username', () => {
      let usernameItem = component.createRegistoForm.controls["username"]
      usernameItem.setValue("username");
      expect(component.createRegistoForm.valid).toBeTruthy;
    });
  
    it('form with invalid username', () => {
      let usernameItem = component.createRegistoForm.controls["username"]
      usernameItem.setValue("");
      expect(component.createRegistoForm.valid).toBeFalsy;
    });
  
  });