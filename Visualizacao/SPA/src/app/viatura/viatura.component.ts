import { Component, OnInit } from '@angular/core';

import { ViaturaService } from '../services/viatura.service';

import { TipoViaturaService } from '../services/tipo-viatura.service';
import Viatura from '../model/Viatura';

import TipoViatura from '../model/TipoViatura';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { List } from 'lodash';


@Component({
  selector: 'app-viatura',
  templateUrl: './viatura.component.html',
  styleUrls: ['./viatura.component.css']
})
export class ViaturaComponent implements OnInit {

  createViaturaForm: FormGroup;

  tiposViatura: TipoViatura[] = [];


  constructor(private viaturaService: ViaturaService, private tipoViaturaService: TipoViaturaService,
    private formBuilder: FormBuilder) {

    this.createViaturaForm = this.formBuilder.group({
      matricula: new FormControl('', [
        Validators.required,
        Validators.pattern(new RegExp("([A-Z][A-Z]-[0-9][0-9]-[A-Z][A-Z])|([0-9][0-9]-[0-9][0-9]-[A-Z][A-Z])|([0-9][0-9]-[A-Z][A-Z]-[0-9][0-9])")),
      ]),
      vin: new FormControl('', [
        Validators.required,
        Validators.pattern(new RegExp("^[A-HJ-NPR-Z\\d]{8}[\\dX][A-HJ-NPR-Z\\d]{2}\\d{6}$"))
      ]),
      codTipoViatura: '',
      data_entrada_servico: '',
    })


    // Inicializa valores default no campo Tipo de Viatura 
    this.tipoViaturaService.getAllTiposViatura().subscribe(
      response_tipos_viatura => {
        this.tiposViatura = response_tipos_viatura;
        this.createViaturaForm.controls.codTipoViatura.patchValue(this.tiposViatura[0].descricao)
      })

  }


  ngOnInit(): void {
  }


  get matricula() { return this.createViaturaForm.get('matricula'); }

  get vin() { return this.createViaturaForm.get('vin'); }

  get codTipoViatura() { return this.createViaturaForm.get('codTipoViatura'); }

  get dataEntradaServico() { return this.createViaturaForm.get('data_entrada_servico'); }



  onSubmit() {
    var matricula = this.createViaturaForm.get('matricula')?.value;
    var vin = this.createViaturaForm.get('vin')?.value;
    var cod_tipo_viatura = this.createViaturaForm.get('codTipoViatura')?.value;
    var data_entrada_servico = this.createViaturaForm.get('data_entrada_servico')?.value;

    this.viaturaService.addViatura({ matricula, vin, cod_tipo_viatura, data_entrada_servico } as Viatura)
      .subscribe(
        response => this.sucesso(response),
        error => console.log('Error', error)
      );

    this.createViaturaForm.reset();
  }

  
  sucesso(response: Viatura) {
    alert("Viatura criada com sucesso!")
    //location.assign('sucesso');
    console.log('Sucess', response);
  }

  onChangeVin(event: any) {
    var text = document.getElementById("VinWarningText");
    if (text != null) {
      text.style.visibility = "hidden";
    }
    let vin: string = this.createViaturaForm.get('vin')?.value;

    let viatura: any;

    this.viaturaService.getViaturaByVin(vin).subscribe(
      response => {
        var warningtext = document.getElementById("VinWarningText")
        if (warningtext != null) {
          warningtext.style.visibility = "visible";
        }
      },
      error => console.log(error)
    );
  }

  onChangeMatricula(event: any) {
    var text = document.getElementById("MatriculaWarningText");
    if (text != null) {
      text.style.visibility = "hidden";
    }
    let matricula: string = this.createViaturaForm.get('matricula')?.value;

    let viatura: any;

    this.viaturaService.getViaturaByMatricula(matricula).subscribe(
      response => {
        var warningtext = document.getElementById("MatriculaWarningText")
        if (warningtext != null) {
          warningtext.style.visibility = "visible";
        }
      },
      error => console.log(error)
    );
  }




}
