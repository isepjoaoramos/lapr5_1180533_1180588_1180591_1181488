import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViaturaComponent } from './viatura.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViaturaService } from '../services/viatura.service';

describe('ViaturaComponent', () => {
  let component: ViaturaComponent;
  let fixture: ComponentFixture<ViaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ViaturaComponent],
      imports: [HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ViaturaService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('form without matricula', () => {
    let vinItem = component.createViaturaForm.controls["vin"]
    vinItem.setValue("1G1YZ23J9P5803427");
    let matricula = component.createViaturaForm.controls["matricula"]
    matricula.setValue("")
    expect(component.createViaturaForm.valid).toBeFalsy();
  });


  it('form without vin', () => {
    let vinItem = component.createViaturaForm.controls["vin"]
    vinItem.setValue("");
    let matricula = component.createViaturaForm.controls["matricula"]
    matricula.setValue("XQ-56-PO")
    expect(component.createViaturaForm.valid).toBeFalsy();
  });

  it('valid form', () => {
    let vinItem = component.createViaturaForm.controls["vin"]
    vinItem.setValue("1G1YZ23J9P5803427");
    let matricula = component.createViaturaForm.controls["matricula"]
    matricula.setValue("XQ-56-PO")
    expect(component.createViaturaForm.valid).toBeTruthy();
  });

  it('form with invalid vin', () => {
    let vinItem = component.createViaturaForm.controls["vin"]
    vinItem.setValue("asda");
    let matricula = component.createViaturaForm.controls["matricula"]
    matricula.setValue("XQ-56-PO")
    expect(component.createViaturaForm.valid).toBeFalsy();
  });


  it('form with invalid matricula', () => {
    let vinItem = component.createViaturaForm.controls["vin"]
    vinItem.setValue("1G1YZ23J9P5803427");
    let matricula = component.createViaturaForm.controls["matricula"]
    matricula.setValue("asdasd")
    expect(component.createViaturaForm.valid).toBeFalsy();
  });


  


});
