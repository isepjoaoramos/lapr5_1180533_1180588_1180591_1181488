import { Component, OnInit, ViewChild, Input, ElementRef, Self } from '@angular/core';
import { environment } from '../../environments/environment';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader.js';
import { VisualizacaoMapa3dService } from '../services/visualizacao-mapa-3d.service';
import { NoService } from '../services/no.service';
import { LinhaService } from '../services/linha.service';
import { PercursoService } from '../services/percurso.service';
import { Object3D, Scene } from 'three';
import * as G from "@here/harp-datasource-protocol/lib/GeoJsonDataType";
import { APIFormat, GeoJsonDataProvider, VectorTileDataSource } from "@here/harp-vectortile-datasource";
import { OmvDataSource } from "@here/harp-omv-datasource";
import { MapControls, MapControlsUI} from "@here/harp-map-controls";
import {
  FeaturesDataSource,
  MapViewFeature,
  MapViewLineFeature,
  MapViewMultiPointFeature
} from "@here/harp-features-datasource";

import No from '../model/No';
import { StyleSet } from "@here/harp-datasource-protocol";
import { MapAnchor, MapAnchors, MapView } from '@here/harp-mapview';
import { MapViewEnvironment } from '@here/harp-mapview/lib/MapViewEnvironment';
import { MapViewEnvironmentOptions } from '@here/harp-mapview/lib/MapViewEnvironment';
import { GeoCoordinates } from '@here/harp-geoutils';
//import { GeoCoordinates } from "@here/harp-geoutils";
//import { GeoJsonDataProvider, VectorTileDataSource } from "@here/harp-vectortile-datasource";
//import { StyleSet } from "@here/harp-datasource-protocol";
//import { MapAnchor, MapAnchors, MapView } from "@here/harp-mapview";

declare var harp: any;
declare var THREE: any;
declare var GUI: any;

const dat = require('dat.gui');


@Component({
  selector: 'app-visualizacao-mapa-harp',
  templateUrl: './visualizacao-mapa-harp.component.html',
  styleUrls: ['./visualizacao-mapa-harp.component.css']
})
export class VisualizacaoMapaHarpComponent implements OnInit {

  @ViewChild("map", { static: false })//@ts-ignore
  private mapElement: ElementRef;

  nos: any[] = [];
  percursos: any[][] = [];
  linhas: any[] = [];
  linhasCoord: any[] = [];




  public constructor(private mapaservice: VisualizacaoMapa3dService, private noService: NoService, private linhaService: LinhaService, private percursoService: PercursoService) {
    let self = this;
  }

  getNos(map: any, scene: any, renderer: any) {
    this.noService.getNos().subscribe(res => {
      for (var i = 0; i < res.length; ++i) {
        var latitude: number = res[i].latitude
        var longitude: number = res[i].longitude
        var nomeNo = res[i].abreviatura
        var modelo = res[i].modelo
        this.nos.push([nomeNo, latitude, longitude, modelo])
      }
      this.percursoService.getPercursos().subscribe((res) => {

        var resLength = res.length;
        for (var i = 0; i < resLength; ++i) {
          var linhaX = res[i].linha;

          var resSegmentosLength = res[i].segmentos.length;
          for (var j = 0; j < resSegmentosLength; j++) {
            var nomeNo1 = res[i].segmentos[j].no1;
            var nomeNo2 = res[i].segmentos[j].no2;
            this.percursos.push([linhaX, nomeNo1, nomeNo2]);
          }
        }
        this.linhaService.getLinhas().subscribe((res) => {

          var resLength = res.length;
          for (var i = 0; i < resLength; ++i) {
            var nome = res[i].nome;
            var r = res[i].vermelho.toString(16);
            if (r.length == 0) { r = "0" + r; }
            var g = res[i].verde.toString(16);
            if (g.length == 0) { g = "0" + g; }
            var b = res[i].azul.toString(16);
            if (b.length == 0) { b = "0" + b; }
            this.linhas.push([nome, r, g, b]);

          }
          this.dropPoints(map, scene, renderer)

        })

      })
    })
  }

  getPercursos() {
    this.percursoService.getPercursos().subscribe((res) => {

      var resLength = res.length;
      for (var i = 0; i < resLength; ++i) {
        var linhaX = res[i].linha;

        var resSegmentosLength = res[i].segmentos.length;
        for (var j = 0; j < resSegmentosLength; j++) {
          var nomeNo1 = res[i].segmentos[j].no1;
          var nomeNo2 = res[i].segmentos[j].no2;
          this.percursos.push([linhaX, nomeNo1, nomeNo2]);
        }
      }
    })
  }


  getPercursosCoord(map: any, scene: any) {
    this.noService.getNoByAbreviatura("BALTR").subscribe(res => {
      //no2=res as No;
      console.log("TESTE111: " + res)
      console.log("TESTE: " + res.abreviatura)
      //this.linhasCoord.push([linhaX,no1.latitude,no1.longitude,no2.latitude,no2.longitude])
    })

    this.percursoService.getPercursos().subscribe((res) => {
      var nomeNo1 = ""
      var nomeNo2 = ""
      var no1: any;
      var no2;
      var resLength = res.length;
      var lat1
      var lat2
      var long1
      var long2
      for (var i = 0; i < resLength; ++i) {
        var linhaX = res[i].linha;

        var resSegmentosLength = res[i].segmentos.length;
        for (var j = 0; j < resSegmentosLength; j++) {
          nomeNo1 = res[i].segmentos[j].no1;
          nomeNo2 = res[i].segmentos[j].no2;
          console.log("NO1: " + nomeNo1)
          this.noService.getNoByAbreviatura(nomeNo1).subscribe(res1 => {
            no1 = res1 as No;
            lat1 = res1.latitude
            console.log("NOOOO1: " + (res1 as No).latitude)
            this.noService.getNoByAbreviatura(nomeNo2).subscribe(res => {
              no2 = res as No;
              console.log("atrib: " + linhaX, no1.latitude, no1.longitude, no2.latitude, no2.longitude)
              this.linhasCoord.push([linhaX, no1.latitude, no1.longitude, no2.latitude, no2.longitude])
            })
          })
          //this.percursos.push([linhaX, nomeNo1, nomeNo2]);
        }
      }
    })
    this.dropLines(map, scene)
  }

  getLinhas() {
    this.linhaService.getLinhas().subscribe((res) => {

      var resLength = res.length;
      for (var i = 0; i < resLength; ++i) {
        var nome = res[i].nome;
        var r = res[i].vermelho.toString(16);
        if (r.length == 0) { r = "0" + r; }
        var g = res[i].verde.toString(16);
        if (g.length == 0) { g = "0" + g; }
        var b = res[i].azul.toString(16);
        if (b.length == 0) { b = "0" + b; }
        this.linhas.push([nome, r, g, b]);

      }
    })
  }


  public ngOnInit() { }

  public ngAfterViewInit() {

    const scene = new THREE.Scene();
    const camera: any = new THREE.PerspectiveCamera();
    const renderer = new THREE.WebGLRenderer();
    var map: any;

    var viewModeFlag = 1;

    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    map = new MapView({
      canvas: this.mapElement.nativeElement,
      theme: {
        extends: './resources/berlin_tilezen_base.json'},
      enableShadows: true,
      decoderUrl: './decoder.bundle.js',

    });

    const controls = new MapControls(map);

    const omvDataSource = new OmvDataSource({
      baseUrl: "https://xyz.api.here.com/tiles/herebase.02",
      apiFormat: APIFormat.XYZOMV,
      styleSetName: "tilezen",
      authenticationCode: environment.harpgl.accessToken,
    });
    map.addDataSource(omvDataSource);
    map.setCameraGeolocationAndZoom(new GeoCoordinates(Number("41.1779438"), Number("-8.4091053")), 11.5);


    // White directional light shining from the top.
    var directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);
    directionalLight.position.set(0, -70, 100).normalize();
    directionalLight.target.position.set(-5, 0, 0);
    map.scene.add(directionalLight)

    var mapAnchors: MapAnchors;
    var ui = new MapControlsUI(controls, { zoomLevel: 'input' });
    ui.controls.tiltEnabled = false;

    var color: any;
    var lighting: any;
    var positionx: any;
    var positiony: any;
    var positionz: any;

    const gui = new dat.GUI();

    var hasCreatedFlag = 0;

    var btn = document.getElementById("button2D");
    if (btn != null) {
      btn.addEventListener('click', async () => {
        viewModeFlag = 1;
        ui.controls.tiltEnabled = false;
        map.setCameraGeolocationAndZoom(new GeoCoordinates(Number("41.1779438"), Number("-8.4091053")), 11.5);
        gui.remove(color);
        gui.remove(lighting);
        gui.remove(positionx);
        gui.remove(positiony);
        gui.remove(positionz);
        hasCreatedFlag = 0;
      });
    }


    var btn2 = document.getElementById("button3D");
    if (btn2 != null) {
      btn2.addEventListener('click', async () => {
        viewModeFlag = 2;
        ui.controls.tiltEnabled = true;
        ui.controls.toggleTilt()
        map.setCameraGeolocationAndZoom(new GeoCoordinates(Number("41.1779438"), Number("-8.4091053")), 11.5);
        if (hasCreatedFlag == 1){
          gui.remove(color);
          gui.remove(lighting);
          gui.remove(positionx);
          gui.remove(positiony);
          gui.remove(positionz);
        }else{
          color = gui.addColor(new ColorGUIHelper(directionalLight, 'color'), 'value').name('color');
          lighting = gui.add(directionalLight, 'intensity', 0, 2, 0.01);
          positionx = gui.add(directionalLight.position, 'x', -10, 10, .01);
          positiony = gui.add(directionalLight.position, 'z', -10, 10, .01);
          positionz = gui.add(directionalLight.position, 'y', 0, 10, .01);
          hasCreatedFlag = 1;
        }
      });
    }



    var geom = new THREE.BoxGeometry(500, 200, 200);
    const mat = new THREE.MeshPhongMaterial({
      color: "#0fb2fc"
    });

    const cube = new THREE.Object3D();

    var mesh = new THREE.Mesh(geom, mat);
    mesh.renderOrder = Number.MAX_SAFE_INTEGER;
    cube.add(mesh);



    const geoPosition = new GeoCoordinates(41.1779438, -8.4091053);

    geoPosition.altitude = 50;

    cube.anchor = geoPosition;
    console.log("NOVO CUBO: " + cube)
    map.mapAnchors.add(cube);
  

    // movement - please calibrate these values deppending on preference
    var movementSpeed = 0.0001;
    var zRotation = Math.PI / 80;

    const nos = this.nos;

    document.addEventListener("keydown", function (e) { onDocumentKeyDown(e, cube.anchor.latitude, cube.anchor.longitude, nos, map, viewModeFlag) }, false);
    function onDocumentKeyDown(event: any, latitude: any, longitude: any, nos: any, map: any, viewModeFlag: any) {
      if (viewModeFlag == 2) {
        var keyCode = event.which;
        let flag = 0;
        const nos_legth = nos.length;
        const lat_difference = 0.0015;
        const long_difference = 0.0035;

        // Key W - Go forward
        if (keyCode == 87) {
          map.lookAt({
            target: new GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude),
            tilt: 80,
            zoomLevel: 14.5,
            heading: 90,
          })
          flag = 0;
          for (var i = 0; i < nos_legth; i++) {
            const dif_latitude = Math.abs(latitude - nos[i][1]);
            const dif_longitude = Math.abs(longitude - nos[i][2]);
            if ((dif_latitude <= lat_difference) && (dif_longitude <= long_difference)) {
              flag = -2;
              console.log("Collision detected");
            }
          }
          if (flag == -2) {
            cube.anchor.longitude -= movementSpeed * 2;
          } else {
            cube.anchor.longitude += movementSpeed;
          }
          // key S - Go Backward
        } else if (keyCode == 83) {
          //map.setCameraGeolocationAndZoom(new harp.GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude), 13.5)
          map.lookAt({
            target: new GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude),
            tilt: 80,
            zoomLevel: 14.5,
            heading: 90,
          })
          flag = 0;
          for (var i = 0; i < nos_legth; i++) {
            const dif_latitude = Math.abs(latitude - nos[i][1]);
            const dif_longitude = Math.abs(longitude - nos[i][2]);
            if ((dif_latitude <= lat_difference) && (dif_longitude <= long_difference)) {
              flag = -3;
              console.log("Collision detected");
            }
          }
          if (flag == -3) {
            cube.anchor.longitude += movementSpeed * 2;
          } else {
            cube.anchor.longitude -= movementSpeed;
          }
          // Key E - Rotate
        } else if (keyCode == 69) {
          //map.setCameraGeolocationAndZoom(new harp.GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude), 13.5);
          map.lookAt({
            target: new GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude),
            tilt: 80,
            zoomLevel: 14.5,
            heading: 90
          })
          cube.rotateZ(zRotation);
          // Key A - Go Left
        } else if (keyCode == 65) {
          //map.setCameraGeolocationAndZoom(new harp.GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude), 13.5);
          map.lookAt({
            target: new GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude),
            tilt: 80,
            zoomLevel: 14.5,
            heading: 90
          })
          flag = 0;
          for (var i = 0; i < nos_legth; i++) {
            const dif_latitude = Math.abs(latitude - nos[i][1]);
            const dif_longitude = Math.abs(longitude - nos[i][2]);
            if ((dif_latitude <= lat_difference) && (dif_longitude <= long_difference)) {
              flag = -4;
              console.log("Collision detected");
            }
          }
          if (flag == -4) {
            cube.anchor.latitude -= movementSpeed * 2;
          } else {
            cube.anchor.latitude += movementSpeed;
          }
          // Key D - Go Right
        } else if (keyCode == 68) {
          //map.setCameraGeolocationAndZoom(new harp.GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude), 13.5)
          map.lookAt({
            target: new GeoCoordinates(cube.anchor.latitude, cube.anchor.longitude),
            tilt: 80,
            zoomLevel: 14.5,
            heading: 90
          })
          flag = 0;
          for (var i = 0; i < nos_legth; i++) {
            const dif_latitude = Math.abs(latitude - nos[i][1]);
            const dif_longitude = Math.abs(longitude - nos[i][2]);
            if ((dif_latitude <= lat_difference) && (dif_longitude <= long_difference)) {
              flag = -5;
              console.log("Collision detected");
            }
          }
          if (flag == -5) {
            cube.anchor.latitude += movementSpeed * 2;
          } else {
            cube.anchor.latitude -= movementSpeed;
          }
        }
      }

    }


    renderer.state.reset();
    renderer.render(scene, camera);

    function resizeRendererToDisplaySize(renderer: any) {
      const canvas = renderer.domElement;
      const width = canvas.clientWidth;
      const height = canvas.clientHeight;
      const needResize = canvas.width !== width || canvas.height !== height;
      if (needResize) {
        renderer.setSize(width, height, false);
      }
      return needResize;
    }


    const pickPosition = { x: 0, y: 0 };
    const pickHelper = new PickHelper();
    clearPickPosition();

    function render(time: any) {
      time *= 0.001;  // convert to seconds;

      if (resizeRendererToDisplaySize(renderer)) {
        const canvas = renderer.domElement;
        camera.aspect = canvas.clientWidth / canvas.clientHeight;
        camera.updateProjectionMatrix();
      }

      // cameraPole.rotation.y = time * .1;

      pickHelper.pick(pickPosition, scene, camera, time, map);

      renderer.render(scene, camera);

      requestAnimationFrame(render);
    }
    requestAnimationFrame(render);

    function getCanvasRelativePosition(event: any) {
      const rect = map.canvas.getBoundingClientRect();
      return {
        x: (event.clientX - rect.left) * map.canvas.width / rect.width,
        y: (event.clientY - rect.top) * map.canvas.height / rect.height,
      };
    }

    function setPickPosition(event: any) {
      const pos = getCanvasRelativePosition(event);
      // console.log(pos)
      pickPosition.x = (pos.x / map.canvas.width) * 2 - 1;
      pickPosition.y = (pos.y / map.canvas.height) * -2 + 1;  // note we flip Y
      
      document.getElementById('infoX').innerHTML = pickPosition.x.toString();
      document.getElementById('infoY').innerHTML = pickPosition.y.toString();
    }

    function clearPickPosition() {
      // unlike the mouse which always has a position
      // if the user stops touching the screen we want
      // to stop picking. For now we just pick a value
      // unlikely to pick something
      pickPosition.x = -100000;
      pickPosition.y = -100000;
    }
    window.addEventListener('mousemove', setPickPosition);
    window.addEventListener('mouseout', clearPickPosition);
    window.addEventListener('mouseleave', clearPickPosition);

    window.addEventListener('touchstart', (event) => {
      // prevent the window from scrolling
      event.preventDefault();
      setPickPosition(event.touches[0]);
    }, { passive: false });

    window.addEventListener('touchmove', (event) => {
      setPickPosition(event.touches[0]);
    });

    window.addEventListener('touchend', clearPickPosition);

    this.getNos(map, scene, renderer);

    /* const loader = new FBXLoader();
     loader.load('../../assets/3d-model.fbx', (obj: any) => {
      
       // console.log("LAT: "+nos[i][1])
       // console.log("LONG: "+nos[i][2])
       console.log(this.nos.length)
       //@ts-ignore
       obj.traverse(child => child.renderOrder = 10000);
       obj.renderOrder = 10000;
       obj.rotateX(Math.PI / 2);
       obj.scale.set(4, 4, 4);
       obj.name = "bus";
 
       //Assign the coordinates to the obj
       obj.geoPosition = new harp.GeoCoordinates(41.1679438, -8.6391053);
       this.map.mapAnchors.add(obj);
     });*/

    //}
  }

  public dropPoints(map: any, scene: any, renderer: any) {
    var sources: any[] = []
    var styles: any[] = [];
    var arrayLines: G.Feature[] = [];
    var lat1
    var lat2
    var long1
    var long2
    var r: any;
    var g: any;
    var b: any;
    var red: any[] = []
    var green: any[] = []
    var blue: any[] = []

    console.log("LINHAS: " + this.linhas)
    console.log("NOSLENGTH: " + this.nos.length)
    for (var i = 0; i < this.percursos.length; i++) {
      r = 0; g = 0; b = 0;
      for (var j = 0; j < this.nos.length; j++) {
        if (this.nos[j][0] == this.percursos[i][1]) {
          console.log("NO1: " + this.nos[j][0])
          lat1 = this.nos[j][1]
          long1 = this.nos[j][2]
        }
        if (this.nos[j][0] == this.percursos[i][2]) {
          console.log("NO2: " + this.nos[j][0])
          lat2 = this.nos[j][1]
          long2 = this.nos[j][2]
        }
      }

      for (var k = 0; k < this.linhas.length; k++) {

        if (this.percursos[i][0] == this.linhas[k][0]) {
          r = this.linhas[k][1]
          if (r.length == 1) {
            r = "0" + r;
          } red[i] = r
          g = this.linhas[k][2]
          if (g.length == 1) {
            g = "0" + g;
          } green[i] = g
          b = this.linhas[k][3]
          if (b.length == 1) {
            b = "0" + b;
          } blue[i] = b
        }
      }
      console.log("R: " + r)
      console.log("G: " + g)
      console.log("B: " + b)
      console.log("LAT1IYVU: " + lat1)
      console.log("LONG1IYVU: " + long1)
      console.log("LAT2IYVU: " + lat2)
      console.log("LONG2IYVU: " + long2)

      console.log("perc: " + this.percursos.length)

      console.log("LENGTH123123: " + arrayLines.length)
      arrayLines.push({
        type: "Feature",
        geometry: {
          type: "LineString",
          coordinates: [[long1, lat1], [long2, lat2]]
        },
        properties: {
          text: "linha",
        }
      });



      console.log("ARRAYLines " + arrayLines)
      var tam = this.percursos.length


      const geoJsonDataProvider = new GeoJsonDataProvider(
        "lines",
        {
          "type": "FeatureCollection",
          "features": arrayLines
        }
      );

      const geoJsonDataSource = new OmvDataSource({
        name: "geojson" + i,
        dataProvider: geoJsonDataProvider,
        styleSetName: "geojsonLines"
      });
      sources.push(geoJsonDataSource)

      //sources.add(geoJsonDataSource)
      var stylesAux: any[] = []
      map.addDataSource(geoJsonDataSource).then(() => {//console.log("COLOR: "+styles[0].attr.color); stylesAux.push(styles[i])
        //for(var z=0;z<this.percursos.length;z++){
        styles = [];
        console.log("LENGTH FIN: " + styles.length)
        styles.push({
          when: "$geometryType == 'line'",
          renderOrder: 10003,
          technique: "solid-line",
          attr: {
            color: "#" + r + g + b,
            opacity: 1,
            metricUnit: "Pixel",
            lineWidth: 4
          }
        }); console.log("COLOR: " + styles[0].attr.color);
        geoJsonDataSource.setStyleSet(styles);
        map.update();
        //}
      });
    }
    map.update();



    const material = new THREE.LineBasicMaterial({ color: 0x0000ff });

    const points = [];
    points.push(new THREE.Vector3(this.nos[0][2], this.nos[0][1], 0));
    points.push(new THREE.Vector3(this.nos[1][2], this.nos[1][1], 0));

    const geometry = new THREE.BufferGeometry().setFromPoints(points);


    //this.map.mapAnchors().add(line)

    //this.map.update;
    var a;
    /*this.scene.add( line );
    this.renderer.render( this.scene, this.camera );
    
    console.log("LINHAAAAAAAAA: "+line)
    
    this.map.update();*/
    var geometry1;


    for (var i = 0; i < this.nos.length; i++) {
      const scale = 50;
      console.log("AAAA: " + this.nos[i][3])
      switch (this.nos[i][3]) {
        case "default":
          geometry1 = new THREE.SphereGeometry(3 * scale, 50, 50);
          break;
        case "Esfera":
          geometry1 = new THREE.SphereGeometry(3 * scale, 50, 50);
          break;
        case "Cubo":
          geometry1 = new THREE.BoxGeometry(50, 50, 50);
          break;
        case "Cilindro":
          geometry1 = new THREE.CylinderGeometry(25, 25, 100, 100);
      }

      const material = new THREE.MeshPhongMaterial({
        color: "#ff8000",
        // opacity: 0.8,
        // transparent: false,
      });


      const cube = new THREE.Object3D();

      const mesh = new THREE.Mesh(geometry1, material);
      mesh.renderOrder = Number.MAX_SAFE_INTEGER;
      cube.add(mesh);



      const geoPosition = new GeoCoordinates(this.nos[i][1], this.nos[i][2]);
      if (geoPosition === null) {
        return;
      }

      geoPosition.altitude = 50;
      // mesh.position.set(this.nos[i][1], this.nos[i][2],50);
      //scene.add(mesh)

      cube.anchor = geoPosition;
      map.mapAnchors.add(cube);
      scene.add(cube);
      map.update();
    }


  }

  public dropLines(map: any, scene: any) {
    console.log("PERC COORD: " + this.linhasCoord);
  }
}


class ColorGUIHelper {
  constructor(object: any, prop: any) {
    // @ts-ignore
    this.object = object;
    // @ts-ignore
    this.prop = prop;
  }
  get value() {
    // @ts-ignore
    return `#${this.object[this.prop].getHexString()}`;
  }
  set value(hexString) {
    // @ts-ignore
    this.object[this.prop].set(hexString);
  }
}



class PickHelper {
  raycaster;
  pickedObject: any;
  pickedObjectSavedColor: number;
  constructor() {
    this.raycaster = new THREE.Raycaster();
    this.pickedObject = null;
    this.pickedObjectSavedColor = 0;
  }
  pick(normalizedPosition: any, scene: any, camera: any, time: any, map: any) {
    // restore the color if there is a picked object
    if (this.pickedObject) {
      console.log("OBJETO: " + this.pickedObject)
      this.pickedObject.material.emissive.setHex(this.pickedObjectSavedColor);
      this.pickedObject = undefined;
    }

    // cast a ray through the frustum
    this.raycaster.setFromCamera(normalizedPosition, camera);
    // get the list of objects the ray intersected
    //console.log("CHILDREN: "+scene.children.length)
    //const intersectedObjects = this.raycaster.intersectObjects(scene.children);
    const intersectedObjects = this.raycaster.intersectObjects(map.mapAnchors.children);
    if (intersectedObjects.length) {
      for (var i = 0; i < intersectedObjects.length; i++) {
        console.log("OBJ " + intersectedObjects[i]);
      }
      // pick the first object. It's the closest one
      this.pickedObject = intersectedObjects[0].object;
      // save its color
      this.pickedObjectSavedColor = this.pickedObject.material.emissive.getHex();
      // set its emissive color to flashing red/yellow
      this.pickedObject.material.emissive.setHex((time * 8) % 2 > 1 ? 0xFFFF00 : 0xFF0000);
    }
  }


}
