import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { VisualizacaoMapaHarpComponent } from './visualizacao-mapa-harp.component';

describe('VisualizacaoMapaHarpComponent', () => {
  let component: VisualizacaoMapaHarpComponent;
  let fixture: ComponentFixture<VisualizacaoMapaHarpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoMapaHarpComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ VisualizacaoMapaHarpComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoMapaHarpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  
});
