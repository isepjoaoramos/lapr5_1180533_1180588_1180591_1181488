export interface User{
    token: string;
    tokenExp: number;
    cliente: boolean;
    data_admin: boolean;
    gestor: boolean;
}