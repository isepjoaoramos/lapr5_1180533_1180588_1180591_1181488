export interface Viagem{
    viagemId: number;
    linha: string;
    horaSaida: number;
    percurso: string
}