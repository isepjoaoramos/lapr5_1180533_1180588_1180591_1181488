import { Viagem } from "./Viagem";

export default interface ServicoViatura{
   idServicoViatura: number;
   codServicoViatura: string;
   descricao: string;
   viagens: Viagem[];
}