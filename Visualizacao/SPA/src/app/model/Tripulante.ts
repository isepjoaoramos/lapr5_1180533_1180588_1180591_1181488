import TipoTripulante from "./TipoTripulante";


export default interface Tripulante {
  //idTripulante: number;
  numMecanografico: string;
  nome: string;
  dataNascimento: Date;
  numCartaoCidadao: number;
  nif: number;
  numCartaConducao: string;
  dataEmissaoCartaConducao: Date;
  tiposTripulante: TipoTripulante[];
  dataEntradaEmpresa: Date;
  dataSaidaEmpresa: Date;
}
