export  interface CriacaoBlocosTrabalho {
    idServicoViatura: number;
    duracao: number;
    nMaximoBlocos: number;
}