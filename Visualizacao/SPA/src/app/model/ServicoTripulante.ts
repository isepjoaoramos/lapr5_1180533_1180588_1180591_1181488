import { BlocoTrabalho } from "./BlocoTrabalho";
import { BlocoTrabalhoIn } from "./BlocoTrabalhoIn";

export default interface ServicoTripulante{
   idServicoTripulante: number;
   codServicoTripulante: string;
   blocos: BlocoTrabalhoIn[];
}