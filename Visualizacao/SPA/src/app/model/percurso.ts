import { Segmento } from './Segmento';

export interface Percurso {
  orientacao: string;
  distancia: number;
  descricao: string;
  duracao: number;
  linha: string;
  noInicio: string;
  noFim: string;
  segmentos: Array<Segmento>
  }