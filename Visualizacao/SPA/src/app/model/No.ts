export default interface No {
    nome: string;
    latitude: number,
    longitude: number,
    abreviatura: string;
    isEstacaoRecolha: boolean;
    isPontoRendicao: boolean;
    capacidade: number;
    tempoMaxParagem: number;
    modelo: string
  }