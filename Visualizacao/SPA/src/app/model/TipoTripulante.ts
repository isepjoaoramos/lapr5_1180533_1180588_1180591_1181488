export default interface TipoTripulante {
  tipoTripulanteId: number,
  codigo: string,
  descricao: string
}
  