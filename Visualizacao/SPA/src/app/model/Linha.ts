export default interface Linha {
    codLinha: string;
    nome: string;
    vermelho: number;
    verde: number;
    azul: number;
    noInicio: string;
    noFim: string;
    restricoes: string;
}