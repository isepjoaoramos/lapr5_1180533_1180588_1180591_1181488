export default interface Viatura{
    matricula: string;
    vin: string;
    cod_tipo_viatura: string;
    data_entrada_servico: Date;

}