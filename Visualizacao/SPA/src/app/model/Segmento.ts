export interface Segmento{
    no1: string;
    no2: string;
    distancia: number;
    duracao: number;
    ordem: number
}