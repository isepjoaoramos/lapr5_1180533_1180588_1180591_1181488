import { Component, OnInit } from '@angular/core';

import { FormBuilder } from '@angular/forms';
import { response } from 'express';


import No from '../model/No';
import { NoService } from '../services/no.service';



@Component({
  selector: 'app-no',
  templateUrl: './no.component.html',
  styleUrls: ['./no.component.css']
})
export class NoComponent implements OnInit {

  isEstacaoRecolha = 'false';
  isPontoRendicao = 'false';
  createNoForm;


  constructor(private noService: NoService,
    private formBuilder: FormBuilder) {

    this.createNoForm = this.formBuilder.group({
      nome: '',
      latitude: '',
      longitude: '',
      abreviatura: '',
      isEstacaoRecolha: '',
      isPontoRendicao: '',
      capacidade: '',
      tempoMaxParagem: '',
      modelo: ''
    })


  }

  ngOnInit(): void {
  }

  onSubmit(no: No) {
    var nome = (document.getElementById("nome") as HTMLInputElement).value;
    var latitude = (document.getElementById("latitude") as HTMLInputElement).value;
    var longitude = (document.getElementById("longitude") as HTMLInputElement).value;
    var abreviatura = (document.getElementById("abreviatura") as HTMLInputElement).value;
    var isEstacaoRecolha = (document.getElementById("isEstacaoRecolha") as HTMLInputElement).checked;
    var isPontoRendicao = (document.getElementById("isPontoRendicao") as HTMLInputElement).checked;
    var capacidade = (document.getElementById("capacidade") as HTMLInputElement).value;
    var tempoMaxParagem = (document.getElementById("tempoMaxParagem") as HTMLInputElement).value;


    if (!nome || !latitude || !longitude
      || !abreviatura || !tempoMaxParagem || !capacidade) { return; }


    var warningtextNome = document.getElementById("nomeText");
    var warningtextLatitude = document.getElementById("latitudeText");
    var warningtextLongitude = document.getElementById("longitudeText");
    var warningtextAbreviatura = document.getElementById("abreviaturaText");
    var warningtextisEstacaoRecolha = document.getElementById("isEstacaoRecolhaText");
    var warningtextisPontoRendicao = document.getElementById("isPontoRendicaoText");
    var warningtextCapacidade = document.getElementById("capacidadeText");
    var warningtextTempoMaxParagem = document.getElementById("tempoMaxParagemText");
    var warningtextFinal = document.getElementById("finalText");

    if (warningtextNome != null && warningtextLatitude != null && warningtextLongitude != null && warningtextAbreviatura != null &&
      warningtextTempoMaxParagem != null && warningtextCapacidade != null) {
      if (warningtextNome.style.visibility == "hidden" && warningtextLatitude.style.visibility == "hidden" &&
        warningtextLongitude.style.visibility == "hidden" && warningtextAbreviatura.style.visibility == "hidden" &&
        warningtextTempoMaxParagem.style.visibility == "hidden" && warningtextCapacidade.style.visibility == "hidden") {
        //location.assign('sucesso');
      } else {
        if (warningtextFinal != null) {
          warningtextFinal.style.visibility = "visible";
        }
      }
    }

    this.noService.addNo(no)
      .subscribe(
        response => this.sucesso(response),
        error => console.log('Error', error)
      );
    this.createNoForm.reset();

  }

  sucesso(response: No) {
    alert("Nó criado!")
    //location.assign('sucesso');
    console.log('Sucess', response);
  }

  normalizeBoolean = (value: string) => {
    if (value === "true") {
      return true;
    }

    if (value === "false") {
      return false;
    }

    return value;
  };

  nomeChange(event: any) {

    var warningtext = document.getElementById("nomeText");
    var input = (document.getElementById("nome") as HTMLInputElement).value;
    if (input == null || input == "") {
      if (warningtext != null) {
        warningtext.style.visibility = "visible";
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = "hidden";
      this.noService.getNoByNome(input).subscribe(
        response => {
          if (warningtext != null) {
            warningtext.style.visibility = "visible";
          }
        },
        error => console.log(error)
      );
    }

  }

  latitudeChange(event: any) {

    var warningtext = document.getElementById("latitudeText");
    var input = (document.getElementById("latitude") as HTMLInputElement).valueAsNumber;
    if (input > 90 || input < -90) {
      if (warningtext != null) {
        warningtext.style.visibility = "visible";
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = "hidden";
    }

  }

  longitudeChange(event: any) {

    var warningtext = document.getElementById("longitudeText");
    var input = (document.getElementById("longitude") as HTMLInputElement).valueAsNumber;
    if (input > 180 || input < -180) {
      if (warningtext != null) {
        warningtext.style.visibility = "visible";
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = "hidden";
    }

  }

  abreviaturaChange(event: any) {

    var warningtext = document.getElementById("abreviaturaText");
    var input = (document.getElementById("abreviatura") as HTMLInputElement).value;
    if (input.length == 0 || input == null) {
      if (warningtext != null) {
        warningtext.style.visibility = "visible";
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = "hidden";
      this.noService.getNoByAbreviatura(input).subscribe(
        response => {
          if (warningtext != null) {
            warningtext.style.visibility = "visible";
          }
        },
        error => console.log(error)
      );
    }

  }

  capacidadeChange(event: any) {

    var warningtext = document.getElementById("capacidadeText");
    var input = (document.getElementById("capacidade") as HTMLInputElement).valueAsNumber;
    if (input <= 0) {
      if (warningtext != null) {
        warningtext.style.visibility = "visible";
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = "hidden";
    }

  }

  tempoMaxParagemChange(event: any) {

    var warningtext = document.getElementById("tempoMaxParagemText");
    var input = (document.getElementById("tempoMaxParagem") as HTMLInputElement).valueAsNumber;
    if (input <= 0) {
      if (warningtext != null) {
        warningtext.style.visibility = "visible";
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = "hidden";
    }

  }



}
