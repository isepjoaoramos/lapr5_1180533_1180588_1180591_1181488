import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoComponent } from './no.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoService } from '../services/no.service';


describe('NoComponent', () => {
  let component: NoComponent;
  let fixture: ComponentFixture<NoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoComponent],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ NoService ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form with empty nome', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("");
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with valid nome', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with valid latitude', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let latitudeItem = component.createNoForm.controls["latitude"]
    latitudeItem.setValue(5);
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with empty latitude', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let latitudeItem = component.createNoForm.controls["latitude"]
    latitudeItem.setValue(null);
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with invalid latitude', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let latitudeItem = component.createNoForm.controls["latitude"]
    latitudeItem.setValue(200);
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with valid longitude', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let longitudeItem = component.createNoForm.controls["longitude"]
    longitudeItem.setValue(20);
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with invalid longitude', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let longitudeItem = component.createNoForm.controls["longitude"]
    longitudeItem.setValue(200);
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with empty longitude', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let longitudeItem = component.createNoForm.controls["longitude"]
    longitudeItem.setValue(null);
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with valid abreviatura', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with invalid abreviatura', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREVIATURACOMMAISDE20CARACTERES");
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with empty abreviatura', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("");
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with valid isEstacaoRecolha', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let isEstacaoRecolhaItem = component.createNoForm.controls["isEstacaoRecolha"]
    isEstacaoRecolhaItem.setValue("false");
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with valid isPontoRendicao', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let isPontoRendicaoItem = component.createNoForm.controls["isPontoRendicao"]
    isPontoRendicaoItem.setValue("false");
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with empty capacidade', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let capacidadeItem = component.createNoForm.controls["capacidade"]
    capacidadeItem.setValue(null);
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with valid capacidade', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let capacidadeItem = component.createNoForm.controls["capacidade"]
    capacidadeItem.setValue(5);
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with invalid capacidade', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let capacidadeItem = component.createNoForm.controls["capacidade"]
    capacidadeItem.setValue(-5);
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with valid tempoMaxParagem', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let tempoItem = component.createNoForm.controls["tempoMaxParagem"]
    tempoItem.setValue(5);
    expect(component.createNoForm.valid).toBeTruthy;
  });

  it('form with invalid tempoMaxParagem', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let tempoItem = component.createNoForm.controls["tempoMaxParagem"]
    tempoItem.setValue(-5);
    expect(component.createNoForm.valid).toBeFalsy;
  });

  it('form with empty tempoMaxParagem', () => {
    let nomeItem = component.createNoForm.controls["nome"]
    nomeItem.setValue("nomeNó");
    let abreviaturaItem = component.createNoForm.controls["abreviatura"]
    abreviaturaItem.setValue("ABREV");
    let tempoItem = component.createNoForm.controls["tempoMaxParagem"]
    tempoItem.setValue(null);
    expect(component.createNoForm.valid).toBeFalsy;
  });




});
