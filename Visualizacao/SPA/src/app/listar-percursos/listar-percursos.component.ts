import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Linha from '../model/Linha';
import { Percurso } from '../model/percurso';
import { LinhaService } from '../services/linha.service';
import { PercursoService } from '../services/percurso.service';

@Component({
  selector: 'app-listar-percursos',
  templateUrl: './listar-percursos.component.html',
  styleUrls: ['./listar-percursos.component.css']
})
export class ListarPercursosComponent implements OnInit {

  listarPercursosForm;
  linhas: Linha[] = [];
  percursos: Percurso[] = [];

  constructor(private linhaService: LinhaService, private percursoService: PercursoService, private formBuilder: FormBuilder) {
    this.listarPercursosForm = this.formBuilder.group({
      linha: ['', Validators.required],
      percursos: []
    })
    this.linhaService.getLinhas().subscribe(
      response_linhas => {
        this.linhas = response_linhas;
        this.listarPercursosForm.controls.linha.patchValue(this.linhas[0].nome);

        this.percursoService.getPercursosByLinha(this.linhas[0].nome).subscribe(
          response_percursos => {
            this.percursos = response_percursos;
    
           
          })
      }
      )
      

  }

  onFocusOutEventLinha(event: any) {
    this.percursoService.getPercursosByLinha((document.getElementById("linha") as HTMLInputElement).value).subscribe(
      response_percursos => {
        this.percursos = response_percursos; console.log(this.percursos);

       // this.listarPercursosForm.controls.percurso.patchValue(this.percursos[0].descricao);
      })
  }
  
  changeToCreateMode(): void{
    location.assign('percursos')
  }

  ngOnInit(): void {
  }

}
