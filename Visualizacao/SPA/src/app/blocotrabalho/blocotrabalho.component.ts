import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Viagem } from '../model/Viagem';
import Tripulante from '../model/Tripulante';
import {BlocoTrabalho} from '../model/BlocoTrabalho';
import { TripulanteService } from '../services/tripulante.service';
import { ViagemService } from '../services/viagem.service';
import { BlocotrabalhoService } from '../services/blocotrabalho.service';
import ServicoViatura from '../model/ServicoViatura';
import { ServicoviaturaService } from '../services/servicoviatura.service';
import { ViagensComponent } from '../viagens/viagens.component';
import { response } from 'express';
import { CriacaoBlocosTrabalho } from '../model/CriacaoBlocosTrabalho';

@Component({
  selector: 'app-blocotrabalho',
  templateUrl: './blocotrabalho.component.html',
  styleUrls: ['./blocotrabalho.component.css']
})
export class BlocotrabalhoComponent implements OnInit {
  createBlocoForm;
  viagens: Viagem[] = [];
  allViagens: Viagem[] = [];
  servicosViatura: ServicoViatura[] = [];
  tripulantes: Tripulante[] = [];
  viagensBloco: number[] = [];

  constructor(private blocoService: BlocotrabalhoService, private viagemService: ViagemService, private servicoViaturaService: ServicoviaturaService, private formBuilder: FormBuilder) { 
    this.createBlocoForm = this.formBuilder.group({
      idViagem: ['',Validators.pattern("[0-9]+")],
      idServicoViatura: ['',Validators.pattern("[0-9]+")]
    })

    this.servicoViaturaService.getAllServicosViatura().subscribe(
      responseServicosViatura => {
        this.servicosViatura = responseServicosViatura;  
      }
    )
  }

  onFocusOutServicoEvent(event: any) {
    var idServicoViatura = this.createBlocoForm.get('idServicoViatura')?.value;
    var nServicos = this.servicosViatura.length;
    for (var i = 0; i < nServicos; i++) {
      var currentId = this.servicosViatura[i].idServicoViatura.toString();
      if (currentId == idServicoViatura) {
        this.viagens = this.servicosViatura[i].viagens;
        break;
      }
    }
  }

  ngOnInit(): void {
    this.viagemService.getViagens().subscribe(
      responseViagens => {
        this.allViagens = responseViagens;
      }
    );
  }

  addBloco(){
    var idServicoViatura = parseInt(this.createBlocoForm.get('idServicoViatura')?.value);
    var codigosViagem = this.viagensBloco;
   
    console.log("idServicoViatura: "+idServicoViatura);
    var viagensLength = codigosViagem.length;
    for (var i = 0; i < viagensLength; i++) { 
      console.log("viagens["+i+"] = "+codigosViagem[i]);
    }

    this.blocoService.addBloco({codigosViagem, idServicoViatura} as BlocoTrabalho)
      .subscribe(
        response => this.sucesso(response),
        error => console.log('Error', error)
      );
    
    this.createBlocoForm.reset();
    this.viagensBloco = [];
  }

  createBlocos() {
    var idServicoViatura = parseInt(this.createBlocoForm.get('idServicoViatura')?.value);
    var duracao = parseInt(this.createBlocoForm.get('duracaoBlocos')?.value);
    var nMaximoBlocos = parseInt(this.createBlocoForm.get('maxBlocos')?.value);

    this.blocoService.addBlocos({idServicoViatura,duracao,nMaximoBlocos} as CriacaoBlocosTrabalho).subscribe(
      response => this.sucesso(response),
      error => console.log('Error', error)
    );
  }

  sucesso(response:any) {
    alert("Bloco de Trabalho criado!")
    //location.assign('sucesso');
    console.log('Sucess', response);
  }

  onChange(event: any) {
    
  }

  addViagem() {
    var idViagem = this.createBlocoForm.get('idViagem')?.value;
    this.viagensBloco.push(idViagem);
  }

}
