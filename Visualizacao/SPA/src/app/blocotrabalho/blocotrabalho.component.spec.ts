import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BlocotrabalhoComponent } from './blocotrabalho.component';

describe('BlocotrabalhoComponent', () => {
  let component: BlocotrabalhoComponent;
  let fixture: ComponentFixture<BlocotrabalhoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlocotrabalhoComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocotrabalhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('valid form', () => {
    let linha = component.createBlocoForm.controls["idServicoViatura"]
    linha.setValue(1);
    let idViagem = component.createBlocoForm.controls["idViagem"]
    idViagem.setValue(2);
    expect(component.createBlocoForm.valid).toBeTruthy();
  });

  it('form with invalid viagem', () => {
    let linha = component.createBlocoForm.controls["idServicoViatura"]
    linha.setValue(1);
    let idViagem = component.createBlocoForm.controls["idViagem"]
    idViagem.setValue("A");
    expect(component.createBlocoForm.valid).toBeFalsy();
  });

  it('form with invalid idServicoViatura', () => {
    let linha = component.createBlocoForm.controls["idServicoViatura"]
    linha.setValue("A");
    let idViagem = component.createBlocoForm.controls["idViagem"]
    idViagem.setValue(1);
    expect(component.createBlocoForm.valid).toBeFalsy();
  });
});
