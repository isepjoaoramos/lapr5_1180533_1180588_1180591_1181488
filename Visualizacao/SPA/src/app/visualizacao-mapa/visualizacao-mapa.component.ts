import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as THREE from 'three';
declare var Mappa: any;

import { environment } from '../../environments/environment';

@Component({
  selector: 'app-visualizacao-mapa',
  templateUrl: './visualizacao-mapa.component.html',
  styleUrls: ['./visualizacao-mapa.component.css']
})
export class VisualizacaoMapaComponent implements OnInit {

  @ViewChild('myCanvas', { static: true })
  myCanvas: ElementRef<HTMLCanvasElement>;

  constructor() {
  }


  ngOnInit(): void {
    // Scene Configurations
    const WIDTH = 1000;
    const HEIGHT = 500;
    const VIEW_ANGLE = 45;
    const ASPECT = WIDTH / HEIGHT;
    const NEAR = 0.1;
    const FAR = 10000;
    // Load the CSV with d3-request
    var dataLoaded = false;
    var nos: any[] = [];
    var linhas: any[] = [];
    var meshes_nos: THREE.Mesh<THREE.SphereGeometry, THREE.MeshBasicMaterial>[] = [];
    var three_lines: THREE.Line<THREE.BufferGeometry, THREE.LineBasicMaterial>[] = [];

    var percursos: any[][] = [];
    var linhas : any[] = [];

    const url_nos = environment.baseUrl + '/no';
    fetch(url_nos)
      .then(data => { return data.json() })
      .then((res) => {
        for (var i = 0; i < res.length; ++i) {
          var latitude : number = res[i].latitude
          var longitude : number = res[i].longitude
          var nomeNo = res[i].abreviatura
          nos.push([nomeNo, latitude, longitude])
        }

        for (var i = 0; i < nos.length; ++i) {
          const geometry = new THREE.SphereGeometry( 3, 3, 3 );
          const material = new THREE.MeshBasicMaterial({ color: 0xf80000 });
          const mesh = new THREE.Mesh(geometry, material);
          meshes_nos.push(mesh);
        }
      })

    const url_percursos = environment.baseUrl + '/percursos'   

    fetch(url_percursos)
      .then(data => { return data.json() })
        .then((res) => {
          
          var resLength = res.length;
          for (var i = 0; i < resLength; ++i) {
            var linhaX = res[i].linha;
            
            var resSegmentosLength = res[i].segmentos.length;
            for (var j = 0; j < resSegmentosLength; j++) {
              var nomeNo1 = res[i].segmentos[j].no1;
              var nomeNo2 = res[i].segmentos[j].no2;
              percursos.push([linhaX, nomeNo1, nomeNo2]);
            }
          } 
        })

      const url_linhas = environment.baseUrl + '/linha'  
      fetch(url_linhas)
        .then(data => { return data.json() })
          .then((res) => {

            var resLength = res.length;
            for (var i = 0; i < resLength; ++i) {
              var nome = res[i].nome;
              var r = res[i].vermelho.toString(16);
              if (r.length == 0) { r = "0"+r; }
              var g = res[i].verde.toString(16);
              if (g.length == 0) { g = "0"+g; }
              var b = res[i].azul.toString(16);
              if (b.length == 0) { b = "0"+b; }
              linhas.push([nome, r, g, b]);
            }
          })
          
          dataLoaded = true;


      


      // Scene, camera, canvas, renderer
      var scene = new THREE.Scene();
      var camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
      //var canvas = this.myCanvas.nativeElement;
      var renderer = new THREE.WebGLRenderer({ alpha: true, canvas: this.myCanvas.nativeElement })

      camera.position.z = 300;
      scene.add(camera);
      renderer.setSize(WIDTH, HEIGHT);

      // Light
      var light = new THREE.PointLight(0xffffff, 1.2);
      light.position.set(0, 0, 6);
      scene.add(light);

      // API Key for Mapboxgl. Get one here:
      // https://www.mapbox.com/studio/account/tokens/
      const key = 'pk.eyJ1Ijoiam9hb3JhbW9zMjAwMCIsImEiOiJja2k0cXhxcWQwcnU5MzBxc2Zwc2QyY2phIn0.rHiExdouSrEzjOCc4I9Rvg';

      var options = {
        lat: 41.212067,
        lng: -8.391265,
        zoom: 11,
        //style: 'mapbox://styles/mapbox/traffic-night-v2',
        pitch: 50,
      }

      var mappa = new Mappa('MapboxGL', key);

      var myMap = mappa.tileMap(options);
      myMap.overlay(this.myCanvas.nativeElement);
      myMap.onChange(update);
    
    function update() {
      if (dataLoaded) {
        scene.clear();
        meshes_nos.forEach(function (mesh, item) {
          var pos = myMap.latLngToPixel(nos[item][1], nos[item][2]);
          var point = new THREE.Vector3();
          point.set((pos.x / WIDTH) * 2 - 1, -(pos.y / HEIGHT) * 2 + 1, 0.5);
          point.unproject(camera);
          var dir = point.sub(camera.position).normalize();
          var distance = -camera.position.z / dir.z;
          var newPos = camera.position.clone().add(dir.multiplyScalar(distance));
          mesh.position.set(newPos.x, newPos.y, newPos.z);
          scene.add(mesh);
        });
        
        var nPercursos = percursos.length;
        var nNos = nos.length;
        var linhaAtual = "";
        var points = [];

        for (var j = 0; j < nPercursos; j++) {
          var linhaX = percursos[j][0];
          var nomeNo1 = percursos[j][1];
          var nomeNo2 = percursos[j][2];
          //console.log("["+linhaX+","+nomeNo1+","+nomeNo2+"]");
          if (j == 0) {
            linhaAtual = linhaX;
            for (var k = 0; k < nNos; k++) {
              if (nos[k][0] == nomeNo1) {
                var pos = myMap.latLngToPixel(nos[k][1], nos[k][2]);
                var point = new THREE.Vector3();
                point.set((pos.x / WIDTH) * 2 - 1, -(pos.y / HEIGHT) * 2 + 1, 0.5);
                point.unproject(camera);
                var dir = point.sub(camera.position).normalize();
                var distance = -camera.position.z / dir.z;
                var newPos = camera.position.clone().add(dir.multiplyScalar(distance));
                point.set(newPos.x, newPos.y, newPos.z);
                points.push(point);
              } else if (nos[k][0] == nomeNo2) {
                var pos = myMap.latLngToPixel(nos[k][1], nos[k][2]);
                var point = new THREE.Vector3();
                point.set((pos.x / WIDTH) * 2 - 1, -(pos.y / HEIGHT) * 2 + 1, 1.0);
                point.unproject(camera);
                var dir = point.sub(camera.position).normalize();
                var distance = -camera.position.z / dir.z;
                var newPos = camera.position.clone().add(dir.multiplyScalar(distance));
                point.set(newPos.x, newPos.y, newPos.z);
                points.push(point);
              }
            }
          } else {
            console.log("LINHA ATUAL: "+linhaAtual)
            console.log("LINHAX: "+linhaX)
            console.log("NO1: "+nomeNo1)
            console.log("NO2: "+nomeNo2)
            if (linhaAtual != linhaX) { 
              var nLinhas = linhas.length;
              var color = "0x";
              for (var l = 0; l < nLinhas; l++) {
                if (linhas[l][0] == linhaX) {
                  color = color+linhas[l][1]+linhas[l][2]+linhas[l][3];
                }
              }
              var colorHex = parseInt(color,16);
              //console.log("Vou desenhar a linha "+linhaAtual+"! "+colorHex);
              console.log("ITERAÇÃO "+j+"PONTOS:"+points)
              const geometry = new THREE.BufferGeometry().setFromPoints( points );
              const material = new THREE.LineBasicMaterial({ color: colorHex});
              const line = new THREE.Line( geometry, material );
              points = [];
              linhaAtual = linhaX;
              scene.add( line );
              renderer.render( scene, camera );
            }
            
            for (var z = 0; z < nNos; z++) {
              if (nos[z][0] == nomeNo2) {
                var pos = myMap.latLngToPixel(nos[z][1], nos[z][2]);
                //console.log("Lat: "+nos[z][1]+"; Lon: "+nos[z][2]+"; Pos: "+pos.x +" | "+pos.y);
                var point = new THREE.Vector3();
                point.set((pos.x / WIDTH) * 2 - 1, -(pos.y / HEIGHT) * 2 + 1, 0.5);
                point.unproject(camera);
                var dir = point.sub(camera.position).normalize();
                var distance = -camera.position.z / dir.z;
                var newPos = camera.position.clone().add(dir.multiplyScalar(distance));
                point.set(newPos.x, newPos.y, newPos.z);
                points.push(point);
              }
            }
          }
        }

        if (percursos.length != 0) {
          var nLinhas = linhas.length;
          var color = "0x";
          for (var l = 0; l < nLinhas; l++) {
            if (linhas[l][0] == linhaX) {
              color = color+linhas[l][1]+linhas[l][2]+linhas[l][3];
            }
          }
          var colorHex = parseInt(color,16);
          const geometry = new THREE.BufferGeometry().setFromPoints( points );
          const material = new THREE.LineBasicMaterial({ color: colorHex });
          const line = new THREE.Line( geometry, material );
          points = [];
          scene.add( line );
          renderer.render( scene, camera );
        }      
    }

    
    }
  }


  visualizar3D(): void{
    location.assign('visualizacaoharp')
  }
}


