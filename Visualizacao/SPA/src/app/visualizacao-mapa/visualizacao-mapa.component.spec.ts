import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { VisualizacaoMapaComponent } from './visualizacao-mapa.component';

describe('VisualizacaoMapaComponent', () => {
  let component: VisualizacaoMapaComponent;
  let fixture: ComponentFixture<VisualizacaoMapaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoMapaComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ VisualizacaoMapaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoMapaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
