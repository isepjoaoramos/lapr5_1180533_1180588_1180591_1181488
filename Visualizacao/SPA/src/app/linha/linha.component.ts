import { Component, OnInit } from '@angular/core';
import { LinhaService } from '../services/linha.service';
import Linha from '../model/Linha';
import { NoService } from '../services/no.service';

import { FormBuilder, Validators } from '@angular/forms';
import No from '../model/No';

const hexRgb = require('hex-rgb');
@Component({
  selector: 'app-linha',
  templateUrl: './linha.component.html',
  styleUrls: ['./linha.component.css']
})
export class LinhaComponent implements OnInit {

  createLinhaForm;
  nos: No[] = [];
  corLinha: any;

  constructor(private linhaService: LinhaService, private noService: NoService, private formBuilder: FormBuilder) {
    this.createLinhaForm = this.formBuilder.group({
      codLinha: ['', Validators.required],
      nome: '',
      noInicio: [''],
      noFim: [''],
      restricoes: '',
    })

    this.noService.getNos().subscribe(
      response_nos => {this.nos = response_nos;
      this.createLinhaForm.controls.noInicio.patchValue(this.nos[0].abreviatura)
      this.createLinhaForm.controls.noFim.patchValue(this.nos[1].abreviatura)})
  
  }


  ngOnInit(): void {

  }


  onSubmit(){

    var codLinha = this.createLinhaForm.get('codLinha')?.value;
    var cor = this.corLinha;
    var nome = this.createLinhaForm.get('nome')?.value;
    var noInicio = this.createLinhaForm.get('noInicio')?.value;
    var noFim = this.createLinhaForm.get('noFim')?.value;
    var restricoes = this.createLinhaForm.get('restricoes')?.value;

    // converts rgb in hexadecimal format to components values and stores in array 
    var rgb_components =  hexRgb(cor, {format: 'array'});

    var vermelho = rgb_components[0];
    var verde = rgb_components[1];
    var azul = rgb_components[2];
    
    this.linhaService.addLinha({codLinha, nome, vermelho, verde, azul, noInicio, noFim, restricoes } as Linha)
    .subscribe(
        response => this.sucesso(response),
        error => console.log('Error', error)
      );
    
    this.createLinhaForm.reset();
  }

  
  sucesso(response: Linha) {
    alert("Linha criada!")
    //location.assign('sucesso');
    console.log('Sucess', response);
  }

  onChange(event: any){
    var text = document.getElementById("codLinhaText");
    if(text != null){
      text.style.visibility="hidden";
    }
    let codLinha:string = this.createLinhaForm.get('codLinha')?.value;

    let linha:any;

    this.linhaService.getCodigosLinhaById(codLinha).subscribe(
      response =>  {var warningtext = document.getElementById("codLinhaText")
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
                          }}, 
      error => console.log(error)
    );

  }




}
