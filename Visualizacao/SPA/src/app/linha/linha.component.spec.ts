import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinhaComponent } from './linha.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LinhaService } from '../services/linha.service';

describe('LinhaComponent', () => {
  let component: LinhaComponent;
  let fixture: ComponentFixture<LinhaComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinhaComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ LinhaService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form with empty codLinha', () => {
    let codLinhaItem = component.createLinhaForm.controls["codLinha"]
    codLinhaItem.setValue("");
    expect(component.createLinhaForm.valid).toBeFalsy();
  });

  it('form with valid codLinha', () => {
    let codLinhaItem = component.createLinhaForm.controls["codLinha"]
    codLinhaItem.setValue("L1");
    expect(component.createLinhaForm.valid).toBeTruthy();
  });



});
