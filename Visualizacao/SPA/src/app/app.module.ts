import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TipoviaturaComponent } from './tipoviatura/tipoviatura.component';
import { TipotripulanteComponent } from './tipotripulante/tipotripulante.component';
import { PercursosComponent } from './percursos/percursos.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportacaoGlxComponent } from './importacao-glx/importacao-glx.component';
import { NoComponent } from './no/no.component';
import { LinhaComponent } from './linha/linha.component';
import { VisualizacaoMapaComponent } from './visualizacao-mapa/visualizacao-mapa.component'; 
import { SucessoComponent } from './sucesso/sucesso.component';
import { MudancaMotoristasComponent } from './mudanca-motoristas/mudanca-motoristas.component'; 

import { ColorPickerModule } from 'ngx-color-picker';
import { VisualizacaoMapa3dComponent } from './visualizacao-mapa3d/visualizacao-mapa3d.component';
import { BlocotrabalhoComponent } from './blocotrabalho/blocotrabalho.component';
import { TripulanteComponent } from './tripulante/tripulante.component';
import { ViagensComponent } from './viagens/viagens.component';
import { ViagensLinhaComponent } from './viagens-linha/viagens-linha.component';
import { ViaturaComponent } from './viatura/viatura.component';
import { AlgoritmoGeneticoComponent } from './algoritmo-genetico/algoritmo-genetico.component';
import { ServicoviaturaComponent } from './servicoviatura/servicoviatura.component';

import { RegistoComponent } from './registo/registo.component';



import { VisualizacaoMapaHarpComponent } from './visualizacao-mapa-harp/visualizacao-mapa-harp.component';
import { ListarServicoViaturaComponent } from './listar-servico-viatura/listar-servico-viatura.component';
import { ServicotripulanteComponent } from './servicotripulante/servicotripulante.component';
import { ListarPercursosComponent } from './listar-percursos/listar-percursos.component';
import { ListarNosComponent } from './listar-nos/listar-nos.component';
import { ListarLinhasComponent } from './listar-linhas/listar-linhas.component';
import { LoginComponent } from './login/login.component';
import { ListarViagensComponent } from './listar-viagens/listar-viagens.component';


import{ AuthGuard} from'./guards/auth.guard';
import{ ClienteGuard} from'./guards/clienteGuard';
import{ DataAdminGuard} from'./guards/dataAdminGuard';
import{ GestorGuard} from'./guards/gestorGuard';
import { HomeComponent } from './home/home.component';
import { ListarServicoTripulanteComponent } from './listar-servico-tripulante/listar-servico-tripulante.component';


@NgModule({
  declarations: [
    AppComponent,
    TipoviaturaComponent,
    TipotripulanteComponent,
    PercursosComponent,
    DashboardComponent,
    ImportacaoGlxComponent,
    NoComponent,
    LinhaComponent,
    VisualizacaoMapaComponent,
    SucessoComponent,
    MudancaMotoristasComponent,
    VisualizacaoMapa3dComponent,
    BlocotrabalhoComponent,
    TripulanteComponent,
    ViagensComponent,
    ViagensLinhaComponent,
    ViaturaComponent,
    AlgoritmoGeneticoComponent,
    ServicoviaturaComponent,
    VisualizacaoMapaHarpComponent,
    ListarServicoViaturaComponent,
    ServicotripulanteComponent,
    ListarPercursosComponent,
    ListarNosComponent,
    ListarLinhasComponent,
    LoginComponent,
    RegistoComponent,
    HomeComponent,
    
    ListarViagensComponent,
    
    ListarServicoTripulanteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ColorPickerModule,
  ],
  providers: [AuthGuard,ClienteGuard,DataAdminGuard,GestorGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
