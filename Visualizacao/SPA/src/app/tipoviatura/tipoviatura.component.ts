import { Component, OnInit } from '@angular/core';

import { TipoViaturaService } from '../services/tipo-viatura.service';
import TipoViatura from '../model/TipoViatura';

import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-tipoviatura',
  templateUrl: './tipoviatura.component.html',
  styleUrls: ['./tipoviatura.component.css']
})
export class TipoviaturaComponent implements OnInit {

  createTipoViaturaForm;


  constructor(private tipoViaturaService: TipoViaturaService,
    private formBuilder: FormBuilder) {

    this.createTipoViaturaForm = this.formBuilder.group({
      codTipoViatura: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(20)]],
      descricao: '',
      velocidade_media: ['',Validators.min(1)],
      tipocombustivel: '',
      consumo_medio: ['', Validators.min(1)],
      custo_km: ['', Validators.min(1)],
      autonomia: ['',Validators.min(1)],
      emissoes: '',
    })
  }


  ngOnInit(): void {
  }

  onSubmit(tipoViatura: TipoViatura) {
    this.tipoViaturaService.addTipoViatura(tipoViatura)
      .subscribe(
        response => console.log('Sucess', response),
        error => console.log('Error', error)
      );
      alert("Tipo Viatura criado com sucesso!")
      //location.assign('sucesso');
    
    this.createTipoViaturaForm.reset();
  }

  onChange(event: any){
    var text = document.getElementById("codTipoViaturaText");
    if(text != null){
      text.style.visibility="hidden";
    }
    let codTipoViatura:string = this.createTipoViaturaForm.get('codTipoViatura')?.value;

    let tipoViatura:any;

    this.tipoViaturaService.getTipoViaturaByCod(codTipoViatura).subscribe(
      response =>  {var warningtext = document.getElementById("codTipoViaturaText")
                      if(warningtext != null){
                        warningtext.style.visibility="visible";
                          }}, 
      error => console.log(error)
    );
  }

}
