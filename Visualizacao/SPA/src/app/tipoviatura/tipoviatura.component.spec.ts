import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoviaturaComponent } from './tipoviatura.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TipoViaturaService } from '../services/tipo-viatura.service';

describe('TipoviaturaComponent', () => {
  let component: TipoviaturaComponent;
  let fixture: ComponentFixture<TipoviaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoviaturaComponent ],
      imports: [ HttpClientModule, FormsModule, ReactiveFormsModule],
      providers: [ TipoViaturaService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoviaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('form with empty codTipoViatura', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("");
    expect(component.createTipoViaturaForm.valid).toBeFalsy();
  });

  it('form with valid codTipoViatura', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    expect(component.createTipoViaturaForm.valid).toBeTruthy();
  });

  it('form with invalid codTipoViatura', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("1249892384");
    expect(component.createTipoViaturaForm.valid).toBeFalsy();
  });


  it('form with invalid velocidadeMedia', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let velocidadeMediaItem = component.createTipoViaturaForm.controls["velocidade_media"]
    velocidadeMediaItem.setValue(-5);
    expect(component.createTipoViaturaForm.valid).toBeFalsy();
  });


  it('form with valid velocidadeMedia', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let velocidadeMediaItem = component.createTipoViaturaForm.controls["velocidade_media"]
    velocidadeMediaItem.setValue(5);
    expect(component.createTipoViaturaForm.valid).toBeTruthy();
  });


  
  it('form with invalid consumoMedio', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let consumoMedioItem = component.createTipoViaturaForm.controls["consumo_medio"]
    consumoMedioItem.setValue(-5);
    expect(component.createTipoViaturaForm.valid).toBeFalsy();
  });


  it('form with valid consumoMedio', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let consumoMedioItem = component.createTipoViaturaForm.controls["consumo_medio"]
    consumoMedioItem.setValue(5);
    expect(component.createTipoViaturaForm.valid).toBeTruthy();
  });

  it('form with invalid custo_km', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let custoKmItem = component.createTipoViaturaForm.controls["custo_km"]
    custoKmItem.setValue(-5);
    expect(component.createTipoViaturaForm.valid).toBeFalsy();
  });


  it('form with valid custo_km', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let custoKmItem = component.createTipoViaturaForm.controls["custo_km"]
    custoKmItem.setValue(5);
    expect(component.createTipoViaturaForm.valid).toBeTruthy();
  });


  it('form with invalid autonomia', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let autonomiaItem = component.createTipoViaturaForm.controls["autonomia"]
    autonomiaItem.setValue(-5);
    expect(component.createTipoViaturaForm.valid).toBeFalsy();
  });


  it('form with valid autonomia', () => {
    let codTipoViaturaItem = component.createTipoViaturaForm.controls["codTipoViatura"]
    codTipoViaturaItem.setValue("123456789123456789PT");
    let autonomiaItem = component.createTipoViaturaForm.controls["autonomia"]
    autonomiaItem.setValue(5);
    expect(component.createTipoViaturaForm.valid).toBeTruthy();
  });



});
