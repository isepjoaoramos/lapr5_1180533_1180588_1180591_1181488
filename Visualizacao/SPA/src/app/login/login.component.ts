import { Component, OnInit } from '@angular/core';

import { FormBuilder } from '@angular/forms';
import { response } from 'express';

import Login from '../model/Login';
import Token from '../model/Token';
import { LoginService } from '../services/login.service';
import { AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  createLoginForm;
  loading = false;
  error = '';

  constructor(
    private loginService: LoginService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder
  ) {
    this.createLoginForm = this.formBuilder.group({
      username: '',
      password: ''
    });
  }

  ngOnInit() {
    this.authenticationService.logout();

    this.activatedRoute.params.subscribe(params => {
      if (params['u'] !== undefined) {
        this.error = 'Your user cannot access ';
      }
    });
  }

  onSubmit(login: Login) {
    var username = (document.getElementById('username') as HTMLInputElement)
      .value;
    var password = (document.getElementById('password') as HTMLInputElement)
      .value;

    if (!username || !password) {
      var warningtextFinal = document.getElementById('falhaText');

      if (warningtextFinal != null) {
        warningtextFinal.style.visibility = 'visible';
      }
    }

    var warningtextUsername = document.getElementById('usernameText');
    var warningtextPassword = document.getElementById('passwordText');
    var warningtextFinal = document.getElementById('falhaTextLabel');

    if (warningtextUsername != null && warningtextPassword != null) {
      if (
        warningtextUsername.style.visibility == 'hidden' &&
        warningtextPassword.style.visibility == 'hidden'
      ) {
        console.log('login');



        this.authenticationService.doLogin(login).subscribe(
          result=>{
            this.loading=false;
            if(result === true){
              this.router.navigate(['/home']);
            }else{
              //apresenta mensagem de erro
              this.error='Username or password is incorrect';
              
              if (warningtextFinal != null) {
                warningtextFinal.style.visibility = 'visible';
              }
            }
          });


      } else {
        if (warningtextFinal != null) {
          warningtextFinal.style.visibility = 'visible';
        }
      }

      //this.createUserForm.reset();
    } else {
      if (warningtextFinal != null) {
        warningtextFinal.style.visibility = 'visible';
      }
    }
  }

  sucesso(response: any) {
    location.assign('sucesso');
    console.log('Sucess', response);
  }

  usernameChange(event: any) {
    var warningtext = document.getElementById('usernameText');
    var input = (document.getElementById('username') as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext != null) {
        warningtext.style.visibility = 'visible';
      }
    } else if (warningtext != null) {
      warningtext.style.visibility = 'hidden';
    }
  }

  passwordChange(event: any) {
    var warningtext_password = document.getElementById('passwordText');

    var input = (document.getElementById('password') as HTMLInputElement).value;
    if (input == null || input == '' || input.length == 0) {
      if (warningtext_password != null) {
        warningtext_password.style.visibility = 'visible';
      }
    } else if (warningtext_password != null) {
      warningtext_password.style.visibility = 'hidden';
    }
  }
}