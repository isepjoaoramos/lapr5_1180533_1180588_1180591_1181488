// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://localhost:3000/api',
  mdv_url: 'http://localhost:5000/api',

  mapbox:{
    accessToken: 'pk.eyJ1Ijoiam9hb3JhbW9zMjAwMCIsImEiOiJja2k0cXhxcWQwcnU5MzBxc2Zwc2QyY2phIn0.rHiExdouSrEzjOCc4I9Rvg'
  },

  harpgl:{
    accessToken: 'ADcnwfDBSWOqR_noq9bRowA'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
