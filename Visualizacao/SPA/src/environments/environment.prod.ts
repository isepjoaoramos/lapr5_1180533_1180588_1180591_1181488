export const environment = {
  production: true,
  baseUrl: 'https://pacific-retreat-73499.herokuapp.com/api',
  mdv_url: 'https://mdvapideploy.azurewebsites.net/api',

  mapbox:{
    accessToken: 'pk.eyJ1Ijoiam9hb3JhbW9zMjAwMCIsImEiOiJja2k0cXhxcWQwcnU5MzBxc2Zwc2QyY2phIn0.rHiExdouSrEzjOCc4I9Rvg'
  },

  harpgl:{
    accessToken: 'ADcnwfDBSWOqR_noq9bRowA'
  }
};
