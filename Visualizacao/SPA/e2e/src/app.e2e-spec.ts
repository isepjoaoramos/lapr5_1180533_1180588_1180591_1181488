import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', async () => {
    await page.navigateTo();
    expect(await page.getTitleText()).toEqual('SPA');
  });


  it('should display dashboard button', async () => {
    await page.navigateTo();
    expect(await page.getDashboardButton().getText()).toEqual('Dashboard');
  });


  it('should display Percursos button', async () => {
    await page.navigateTo();
    expect(await page.getPercursosButton().getText()).toEqual('Percursos');
  });


  it('should display Importacao button', async () => {
    await page.navigateTo();
    expect(await page.getImportacaoButton().getText()).toEqual('Importação de ficheiro');
  });

  it('should display CriarTipoViatura button', async () => {
    await page.navigateTo();
    expect(await page.getCriarTipoViaturaButton().getText()).toEqual('Tipos de Viatura');
  });

  it('should display Tipos de Tripulante button', async () => {
    await page.navigateTo();
    expect(await page.getTiposTripulanteButton().getText()).toEqual('Tipos de Tripulante');
  });



  it('should display Tripulante button', async () => {
    await page.navigateTo();
    expect(await page.getTripulanteButton().getText()).toEqual('Tripulantes');
  });


  it('should display Visualizar button', async () => {
    await page.navigateTo();
    expect(await page.getVisualizarButton().getText()).toEqual('Visualizar Mapa da Rede');
  });

  it('should display PlanMudMot button', async () => {
    await page.navigateTo();
    expect(await page.getPlanMudMotButton().getText()).toEqual('Planeamento Mudança de Tripulante');
  });


  it('should display CriarLinha button', async () => {
    await page.navigateTo();
    expect(await page.getCriarLinhaButton().getText()).toEqual('Linhas');
  });

  


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
