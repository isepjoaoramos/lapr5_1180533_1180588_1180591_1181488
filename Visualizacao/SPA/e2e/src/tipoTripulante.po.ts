import { browser, by, element } from 'protractor';

export class TipoTripulantePage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl + "/tipoTripulante");
    }

    async getTitleText(): Promise<string> {
        return element(by.css('app-tipotripulante h2')).getText();
    }

    async getSubTitleText(): Promise<string> {
        return element(by.css('app-tipotripulante h4')).getText();
    }

    getCodigoLabel() {
        return element(by.id('codigoLabel'));
    }

    getDescricaoLabel() {
        return element(by.id('descricaoLabel'));
    }

    getSubmitButton() {
        return element(by.id('submitButton'));
    }
}

