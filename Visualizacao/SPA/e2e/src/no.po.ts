import { browser, by, element } from 'protractor';

export class NoPage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl + "/nos");
    }

    async getTitleText(): Promise<string> {
        return element(by.css('app-no h2')).getText();
    }

    async getSubTitleText(): Promise<string> {
        return element(by.css('app-no h4')).getText();
    }

    getNomeLabel() {
        return element(by.id('nomeLabel'));
    }

    getLatitudeLabel() {
        return element(by.id('latitudeLabel'));
    }

    getLongitudeLabel() {
        return element(by.id('longitudeLabel'));
    }

    getAbreviaturaLabel() {
        return element(by.id('abreviaturaLabel'));
    }

    getIsEstacaoRecolhaLabel() {
        return element(by.id('isEstacaoRecolhaLabel'));
    }

    getIsPontoRendicaoLabel() {
        return element(by.id('isPontoRendicaoLabel'));
    }

    getCapacidadeLabel() {
        return element(by.id('capacidadeLabel'));
    }

    getTempoMaxParagemLabel() {
        return element(by.id('tempoMaxParagemLabel'));
    }

    getSubmitButton() {
        return element(by.id('submitButton'));
    }
}

