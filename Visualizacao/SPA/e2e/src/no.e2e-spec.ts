import { NoPage } from './no.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
    let page: NoPage;

    beforeEach(() => {
        page = new NoPage();
    });

    it('should display No Page title', async () => {
        await page.navigateTo();
        expect(await page.getTitleText()).toEqual('Criar Nó');
    });

    it('should display No Page subtitle', async () => {
        await page.navigateTo();
        expect(await page.getSubTitleText()).toEqual('Inserir dados');
    });


    it('should display Nome label', async () => {
        await page.navigateTo();
        expect(await page.getNomeLabel().getText()).toEqual('Nome');
    });

    it('should display Latitude label', async () => {
        await page.navigateTo();
        expect(await page.getLatitudeLabel().getText()).toEqual('Latitude');
    });

    it('should display Longitude label', async () => {
        await page.navigateTo();
        expect(await page.getLongitudeLabel().getText()).toEqual('Longitude');
    });

    it('should display Abreviatura label', async () => {
        await page.navigateTo();
        expect(await page.getAbreviaturaLabel().getText()).toEqual('Abreviatura');
    });

    it('should display isEstacaoRecolha label', async () => {
        await page.navigateTo();
        expect(await page.getIsEstacaoRecolhaLabel().getText()).toEqual('Estação de Recolha? Sim');
    });

    it('should display isPontoRendicao label', async () => {
        await page.navigateTo();
        expect(await page.getIsPontoRendicaoLabel().getText()).toEqual('Ponto de Rendição? Sim');
    });

    it('should display Capacidade label', async () => {
        await page.navigateTo();
        expect(await page.getCapacidadeLabel().getText()).toEqual('Capacidade de Veículos');
    });

    it('should display TempoMaxParagem label', async () => {
        await page.navigateTo();
        expect(await page.getTempoMaxParagemLabel().getText()).toEqual('Tempo Máximo de Paragem (minutos)');
    });



    it('should display Submit Button', async () => {
        await page.navigateTo();
        expect(await page.getSubmitButton().getText()).toEqual('Submeter dados');
    });


    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
