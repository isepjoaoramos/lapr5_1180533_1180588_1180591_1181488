import { browser, by, element } from 'protractor';

export class TripulantePage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl + "/tripulantes");
    }

    async getTitleText(): Promise<string> {
        return element(by.css('app-tripulante h2')).getText();
    }

    async getSubTitleText(): Promise<string> {
        return element(by.css('app-tripulante h4')).getText();
    }

    getNumLabel() {
        return element(by.id('numMecanograficoLabel'));
    }

    getNomeLabel() {
        return element(by.id('nomeLabel'));
    }

    getDataNascimentoLabel() {
        return element(by.id('dataNascimentoLabel'));
    }

    getNumCartaoCidadaoLabel() {
        return element(by.id('numCartaoCidadaoLabel'));
    }

    getNifLabel() {
        return element(by.id('nifLabel'));
    }

    getNumCartaConducaoLabel() {
        return element(by.id('numCartaConducaoLabel'));
    }

    getDataEmissaoCartaLabel() {
        return element(by.id('dataEmissaoCartaConducaoLabel'));
    }

    getTipoTripulanteLabel() {
        return element(by.id('tipoTripulanteLabel'));
    }

    getDataEntradaEmpresaLabel() {
        return element(by.id('dataEntradaEmpresaLabel'));
    }

    getDataSaidaEmpresaLabel() {
        return element(by.id('dataSaidaEmpresaLabel'));
    }

    getSubmitButton() {
        return element(by.id('submitButton'));
    }
}

