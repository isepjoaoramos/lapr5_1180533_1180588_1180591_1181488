import { TipoViaturaPage } from './tipoViatura.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: TipoViaturaPage;

  beforeEach(() => {
    page = new TipoViaturaPage();
  });

  // it('should display TipoViatura Page title', async () => {
  //   await page.navigateTo();
  //   expect(await page.getTitleText()).toEqual('Criar Tipo Viatura');
  // });


  it('should display CodigoViatura label', async () => {
    await page.navigateTo();
    expect(await page.getCodTipoViaturaLabel().getText()).toEqual('Código de Tipo de Viatura (20 caracteres)');
  });


  it('should display descricao label', async () => {
    await page.navigateTo();
    expect(await page.getDescricaoLabel().getText()).toEqual('Descrição da Viatura');
  });


  it('should display VelocidadeMedia label', async () => {
    await page.navigateTo();
    expect(await page.getVelocidadeMediaLabel().getText()).toEqual('Velocidade Média');
  });


  it('should display TipoCombustivel label', async () => {
    await page.navigateTo();
    expect(await page.getTipoCombustivelLabel().getText()).toEqual('Tipo de Combustível');
  });


  it('should display ConsumoMedio label', async () => {
    await page.navigateTo();
    expect(await page.getConsumoMedioLabel().getText()).toEqual('Consumo Médio (L/KM ou KWH/KM)');
  });


  it('should display CustoKm label', async () => {
    await page.navigateTo();
    expect(await page.getCustoKmLabel().getText()).toEqual('Custo por KM');
  });


  
  it('should display Autonomia label', async () => {
    await page.navigateTo();
    expect(await page.getAutonomiaLabel().getText()).toEqual('Autonomia');
  });


  it('should display Emissoes label', async () => {
    await page.navigateTo();
    expect(await page.getEmissoesLabel().getText()).toEqual('Emissões');
  });


  it('should display Submit Button', async () => {
    await page.navigateTo();
    expect(await page.getSubmitButton().getText()).toEqual('Criar Tipo de Viatura');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
