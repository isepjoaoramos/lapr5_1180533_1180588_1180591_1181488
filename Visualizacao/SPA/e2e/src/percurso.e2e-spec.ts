import { browser, logging } from 'protractor';
import { PercursoPage } from './percurso.po';

describe('workspace-project App', () => {
  let page: PercursoPage;

  beforeEach(() => {
    page = new PercursoPage();
  });

  // it('should display Percurso Page title', async () => {
  //   await page.navigateTo();
  //   expect(await page.getTitleText()).toEqual('Definição de um percurso');
  // });

  it('should display Percurso Page title', async () => {
    await page.navigateTo();
    expect(await page.getTitleText2()).toEqual('Introduza os detalhes do percurso');
  });

  it('should display Percurso Page title', async () => {
    await page.navigateTo();
    expect(await page.getTitleText3()).toEqual('Segmentos do percurso (Introduza o nó seguinte do percurso):');
  });

  it('should display orientacao label', async () => {
    await page.navigateTo();
    expect(await page.getOrientacaoLabel().getText()).toEqual('Orientação (Return/Go)');
  });


  // it('should display descricao label', async () => {
  //   await page.navigateTo();
  //   expect(await page.getDescricaoLabel().getText()).toEqual('Descrição');
  // });


  it('should display linha label', async () => {
    await page.navigateTo();
    expect(await page.getLinhaLabel().getText()).toEqual('Linha');
  });


  it('should display noInicial label', async () => {
    await page.navigateTo();
    expect(await page.getNoInicioLabel().getText()).toEqual('Nó inicial');
  });


  it('should display noSeguinte label', async () => {
    await page.navigateTo();
    expect(await page.getNoFimLabel().getText()).toEqual('Nó seguinte');
  });


  it('should display distancia label', async () => {
    await page.navigateTo();
    expect(await page.getDistanciaLabel().getText()).toEqual('Distância');
  });


  
  it('should display duracao label', async () => {
    await page.navigateTo();
    expect(await page.getDuracaoLabel().getText()).toEqual('Duração');
  });


  it('should display segmento Button', async () => {
    await page.navigateTo();
    expect(await page.getSubmitSegmentoButton().getText()).toEqual('Adicionar segmento');
  });


  it('should display SubmitCreateLinha Button', async () => {
    await page.navigateTo();
    expect(await page.getSubmitPercursoButton().getText()).toEqual('Criar Percurso');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
