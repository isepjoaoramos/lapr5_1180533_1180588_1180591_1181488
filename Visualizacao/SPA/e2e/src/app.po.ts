import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-root h1')).getText();
  }

  getDashboardButton(){
    return element(by.css('[routerlink= "/dashboard"]'));
  }

  getPercursosButton(){
    return element(by.css('[routerlink= "/percursos"]'));
  }
  getImportacaoButton(){
    return element(by.css('[routerlink= "/importacaoglx"]'));
  }
  getCriarTipoViaturaButton(){
    return element(by.css('[routerlink= "/tipoViatura"]'));
  }
  getTiposTripulanteButton(){
    return element(by.css('[routerlink= "/tipoTripulante"]'));
  }
  getTripulanteButton(){
    return element(by.css('[routerlink= "/tripulantes"]'));
  }

  getVisualizarButton(){
    return element(by.css('[routerlink= "/visualizacao_mapa"]'));
  }

  getPlanMudMotButton(){
    return element(by.css('[routerlink= "/mudanca_motoristas"]'));
  }

  getCriarLinhaButton(){
    return element(by.css('[routerlink= "/linhas"]'));
  }
}
