import { browser, logging } from 'protractor';
import { PercursoPage } from './percurso.po';
import { ViagemPage } from './viagem.po';

describe('workspace-project App', () => {
  let page: ViagemPage;

  beforeEach(() => {
    page = new ViagemPage();
  });

  it('add Viagem', async() => {
      await page.navigateTo();
      let fieldLinha = 'horario';
      //let fieldHorario = 'horario';
      //let fieldPercurso = 'percurso';
      //console.log(fieldLinha)
      await browser.waitForAngularEnabled(true);
      //await page.getField(fieldLinha).clear();
      await page.setFieldNumber(fieldLinha,1234);
      //await page.getField(fieldHorario).clear();
      //await page.getField(fieldPercurso).clear();
      
      expect(await page.getField(fieldLinha).getText()).toEqual('');
      //expect(await page.getField(fieldHorario).getText()).toEqual('');
      //expect(await page.getField(fieldPercurso).getText()).toEqual('');
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
