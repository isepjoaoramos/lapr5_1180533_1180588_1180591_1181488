import { browser, by, element } from 'protractor';

export class TipoViaturaPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + "/tipoViatura");
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-tipoviatura h1')).getText();
  }


  getCodTipoViaturaLabel() {
    return element(by.id('codTipoViaturalabel'));
  }

  getDescricaoLabel() {
    return element(by.id('descricaolabel'));
  }

  getVelocidadeMediaLabel() {
    return element(by.id('velocidadeMediaLabel'));
  }


  getTipoCombustivelLabel() {
    return element(by.id('tipoCombustivelLabel'));
  }

  getConsumoMedioLabel() {
    return element(by.id('consumoMedioLabel'));
  }

  getCustoKmLabel() {
    return element(by.id('custoKmLabel'));
  }


  getAutonomiaLabel() {
    return element(by.id('autonomiaLabel'));
  }

  getEmissoesLabel() {
    return element(by.id('emissoesLabel'));
  }


  getSubmitButton() {
    return element(by.id('submitButton'));
  }
}

