import { browser, by, element, Locator } from 'protractor';

export class ViagemPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + "/viagens");
  }

  getField(field: string) {
    return element(by.id(field));
}
  
setField(field: string,value: string) {
  element(by.id(field)).sendKeys(value);
}

setFieldNumber(field: string,value: number) {
  element(by.id(field)).sendKeys(value);
}
}

