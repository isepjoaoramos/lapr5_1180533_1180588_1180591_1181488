import { LinhaPage } from './linha.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: LinhaPage;

  beforeEach(() => {
    page = new LinhaPage();
  });

  // it('should display Linha Page title', async () => {
  //   await page.navigateTo();
  //   expect(await page.getTitleTextLinha().getText()).toEqual('Criar Linhas');
  // });


  it('should display CodigoLinha label', async () => {
    await page.navigateTo();
    expect(await page.getCodLinhaLabel().getText()).toEqual('Código da Linha');
  });


  it('should display Nome da Linha label', async () => {
    await page.navigateTo();
    expect(await page.getNomeLinhaLabel().getText()).toEqual('Nome da Linha');
  });


  // it('should display VermelhoLinha label', async () => {
  //   await page.navigateTo();
  //   expect(await page.getVermelhoLinhaLabel().getText()).toEqual('Cor da Linha (Componente Vermelha)');
  // });


  // it('should display VerdeLinha label', async () => {
  //   await page.navigateTo();
  //   expect(await page.getVerdeLinhaLabel().getText()).toEqual('Cor da Linha (Componente Verde)');
  // });


  // it('should display AzulLinha label', async () => {
  //   await page.navigateTo();
  //   expect(await page.getAzulLinhaLabel().getText()).toEqual('Cor da Linha (Componente Azul)');
  // });


  it('should display NoInicioLinha label', async () => {
    await page.navigateTo();
    expect(await page.getNoInicioLinhaLabel().getText()).toEqual('Estação Inicial');
  });


  
  it('should display NoFimLinha label', async () => {
    await page.navigateTo();
    expect(await page.getNoFimLinhaLabel().getText()).toEqual('Estação Final');
  });


  it('should display RestricoesLinha label', async () => {
    await page.navigateTo();
    expect(await page.getRestricoesLinhaLabel().getText()).toEqual('Restricões');
  });


  it('should display SubmitCreateLinha Button', async () => {
    await page.navigateTo();
    expect(await page.getSubmitLinhaButton().getText()).toEqual('Criar Linha');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
