import { browser, by, element } from 'protractor';

export class PercursoPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + "/percursos");
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-percursos h2')).getText();
  }

  async getTitleText2(): Promise<string> {
    return element(by.css('app-percursos h3')).getText();
  }

  getOrientacaoLabel() {
    return element(by.id('orientacaoLabel'));
  }

  getDescricaoLabel() {
    return element(by.id('descricaoLabel'));
  }

  getLinhaLabel() {
    return element(by.id('linhaLabel'));
  }

  async getTitleText3(): Promise<string> {
    return element(by.css('app-percursos h4')).getText();
  }

  getNoInicioLabel() {
    return element(by.id('noInicioLabel'));
  }

  getNoFimLabel() {
    return element(by.id('noFimLabel'));
  }

  getDistanciaLabel() {
    return element(by.id('distanciaLabel'));
  }


  getDuracaoLabel() {
    return element(by.id('duracaoLabel'));
  }

  getSubmitSegmentoButton() {
    return element(by.id('buttonSegmento'));
  }

  getSubmitPercursoButton() {
    return element(by.id('buttonPercurso'));
  }
}

