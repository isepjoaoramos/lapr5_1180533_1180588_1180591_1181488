import { TipoTripulantePage } from './tipoTripulante.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
    let page: TipoTripulantePage;

    beforeEach(() => {
        page = new TipoTripulantePage();
    });

    // it('should display TipoTripulante Page title', async () => {
    //     await page.navigateTo();
    //     expect(await page.getTitleText()).toEqual('Criar Tipo Tripulante');
    // });

    it('should display TipoTripulante Page subtitle', async () => {
        await page.navigateTo();
        expect(await page.getSubTitleText()).toEqual('Inserir dados');
    });

    it('should display Codigo label', async () => {
        await page.navigateTo();
        expect(await page.getCodigoLabel().getText()).toEqual('Código');
    });


    it('should display Descricao label', async () => {
        await page.navigateTo();
        expect(await page.getDescricaoLabel().getText()).toEqual('Descrição');
    });


    it('should display Submit Button', async () => {
        await page.navigateTo();
        expect(await page.getSubmitButton().getText()).toEqual('Criar Tipo de Tripulante');
    });


    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
