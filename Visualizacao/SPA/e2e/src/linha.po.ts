import { browser, by, element } from 'protractor';

export class LinhaPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + "/linhas");
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-linha h1')).getText();
  }

  getTitleTextLinha(){
    return element(by.id('TitlePageLinhas'));
  }

  getCodLinhaLabel() {
    return element(by.id('codLinhaLabel'));
  }

  getNomeLinhaLabel() {
    return element(by.id('nomeLinhaLabel'));
  }

  getVermelhoLinhaLabel() {
    return element(by.id('vermelhoLinhaLabel'));
  }


  getVerdeLinhaLabel() {
    return element(by.id('verdeLinhaLabel'));
  }

  getAzulLinhaLabel() {
    return element(by.id('azulLinhaLabel'));
  }

  getNoInicioLinhaLabel() {
    return element(by.id('noInicioLinhaLabel'));
  }


  getNoFimLinhaLabel() {
    return element(by.id('noFimLinhaLabel'));
  }

  getRestricoesLinhaLabel() {
    return element(by.id('restricoesLinhaLabel'));
  }


  getSubmitLinhaButton() {
    return element(by.id('submitLinhaButton'));
  }
}

