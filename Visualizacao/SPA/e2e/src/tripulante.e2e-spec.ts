import  {TripulantePage} from './tripulante.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: TripulantePage;

  beforeEach(() => {
    page = new TripulantePage();
  });

  it('should display Tripulante Page title', async () => {
    await page.navigateTo();
    expect(await page.getTitleText()).toEqual('Adicionar Tripulante');
  });


  it('should display NumMecanografico label', async () => {
    await page.navigateTo();
    expect(await (await page.getNumLabel().getText())).toEqual('Número Mecanográfico (9 caracteres)');
  });


  it('should display nome label', async () => {
    await page.navigateTo();
    expect(await page.getNomeLabel().getText()).toEqual('Nome');
  });

  it('should display dataNascimento label', async () => {
    await page.navigateTo();
    expect(await page.getDataNascimentoLabel().getText()).toEqual('Data de Nascimento');
  });

  it('should display Nº Cartão Cidadão label', async () => {
    await page.navigateTo();
    expect(await page.getNumCartaoCidadaoLabel().getText()).toEqual('Nº Cartão de Cidadão');
  });

  it('should display NIF label', async () => {
    await page.navigateTo();
    expect(await page.getNifLabel().getText()).toEqual('NIF');
  });

  it('should display Nº Carta de Condução label', async () => {
    await page.navigateTo();
    expect(await page.getNumCartaConducaoLabel().getText()).toEqual('Nº Carta de Condução');
  });


  it('should display Data Emissão Carta Condução label', async () => {
    await page.navigateTo();
    expect(await page.getDataEmissaoCartaLabel().getText()).toEqual('Data de emissão da Carta de Condução');
  });

  it('should display TipoTripulante label', async () => {
    await page.navigateTo();
    expect(await page.getTipoTripulanteLabel().getText()).toEqual('Tipo de Tripulante');
  });

  it('should display Data Entrada Empresa label', async () => {
    await page.navigateTo();
    expect(await page.getDataEntradaEmpresaLabel().getText()).toEqual('Data de entrada na empresa');
  });

  it('should display Data Saida Empresa label', async () => {
    await page.navigateTo();
    expect(await page.getDataSaidaEmpresaLabel().getText()).toEqual('Data de saída na empresa');
  });

  it('should display Submit Button', async () => {
    await page.navigateTo();
    expect(await page.getSubmitButton().getText()).toEqual('Criar Tripulante');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
