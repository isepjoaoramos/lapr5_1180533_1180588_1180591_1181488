# Instalacao

Antes de executar a aplicação é necessário instalar os módulos de npm necessários.

Para tal executar na linha de comandos:

```bash
npm install 
```


# Execucao 

Para executar a aplicação (irá abrir um separador no browser predefinido):

```bash
ng serve --open
```

# Onde encontrar o código fonte da componente de visualização do mapa 

O código fonte pode ser encontrado no ficheiro /src/app/visualizacao-mapa/visualizacao-mapa.component.ts
dentro da função ngOnInit()


# Bibliotecas usadas

--> Mappa.js 
--> Three.js


