using System;

namespace MDV_API.Dto
{
    public class ViaturaDto
    {
        public string matricula { get; set; }
        public string vin { get; set; }

        public string cod_tipo_viatura { get; set; }

        public DateTime data_entrada_servico { get; set; }

       
    }
}
