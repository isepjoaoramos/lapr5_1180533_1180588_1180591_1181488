using System;
using System.Collections.Generic;

namespace MDV_API.Dto
{

    public class ServicoTripulanteDto{

        public int idServicoTripulante {get; set;}
        public string codServicotripulante {get; set;}
        public BlocoDTO[] blocos  {get; set;}


    }

}