using System;
using MDV_API.Domain;

namespace MDV_API.Dto
{
    public class BlocoDTO {

        public int blocoId { get; set; }
        public int[] codigosViagem  {get; set;}
        public int idServicoViatura {get; set;}

    }
}