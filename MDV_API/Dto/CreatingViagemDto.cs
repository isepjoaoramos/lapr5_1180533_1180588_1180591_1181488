namespace MDV_API.Dto
{
    public class CreatingViagemDto
    {
        public string Linha { get; set; }

        public int HoraSaida { get; set; }

        public string Percurso { get; set; }


        public CreatingViagemDto(string linha, int horaSaida, string percurso)
        {
             this.Linha = linha;
            this.HoraSaida = horaSaida;
            this.Percurso = percurso;
        }
    }
}