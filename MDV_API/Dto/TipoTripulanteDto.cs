using System;
using System.Collections.Generic;
using MDV_API.Dto;

namespace MDV_API.Dto
{

    public class TipoTripulanteDto{


        public int tipoTripulanteId {get; set;}

        public string codigo {get; set;}

        public string descricao {get; set;}
    }

}