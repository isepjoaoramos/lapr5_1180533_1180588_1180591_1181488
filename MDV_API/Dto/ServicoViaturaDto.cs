using System;
using System.Collections.Generic;
using MDV_API.Dto;

namespace MDV_API.Dto
{

    public class ServicoViaturaDto{


        public int idServicoViatura {get; set;}

        public string codServicoViatura {get; set;}

        public string descricao {get; set;}

        public ViagemOutDto[] viagens  {get; set;}
    }

}