using System;
using Microsoft.AspNetCore.Mvc;
namespace MDV_API.Dto
{
    public class ViagemOutDto
    {
        public int viagemId { get; set; }
        public string Linha { get; set; }
        
        public int HoraSaida { get; set; }

        public string Percurso { get; set; }

        //public int[] HorariosNos {get; set; }
    }
}
