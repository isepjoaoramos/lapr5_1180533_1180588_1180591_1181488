using System;
using MDV_API.Dto;
using System.Collections.Generic;


namespace MDV_API.Dto
{
    public class TripulanteDto
    {
        public int tripulanteId {get; set;}

        public string numMecanografico { get; set; }
        public string nome { get; set; }
        public DateTime dataNascimento { get; set; }

        public long numCartaoCidadao { get; set; }
        public long nif { get; set; }

        public string numCartaConducao { get; set; }
        public DateTime dataEmissaoCartaConducao { get; set; }
        
        public TipoTripulanteDto[] tiposTripulante { get; set; }
        public DateTime dataEntradaEmpresa { get; set; }

        public DateTime dataSaidaEmpresa { get; set; }

    }
}
