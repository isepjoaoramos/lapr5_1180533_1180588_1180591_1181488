using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV_API.Services;
using MDV_API.Dto;
using MDV_API.Domain;
using MDV_API.Infrastructure;
using System.Xml;

namespace MDV_API.Controllers
{
    //[Route("api/[controller]")]
    [Route("api/importacao")]
    [ApiController]
    public class ImportacaoGlxController : ControllerBase
    {

        private readonly IImportacaoGlxService importacaoGlxService;
        public ImportacaoGlxController(IImportacaoGlxService importacaoGlxService)
        {
            this.importacaoGlxService = importacaoGlxService;
        }

        // POST: api/blocos
        [HttpPost]
        public async Task<Boolean> importEntities()
        {
            try {
                var file = Request.Form.Files[0];
                
                var fileAsText = ReadFormFileAsync(file).Result;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(fileAsText);
                await importacaoGlxService.criaViagens(doc);
                await importacaoGlxService.criaServicosViatura(doc);
                await importacaoGlxService.criaServicosTripulanteEBlocosTrabalho(doc);
            } catch (Exception e) {
                Console.WriteLine("Error: "+e.ToString());
            }
            return true;
        }

        public static async Task<string> ReadFormFileAsync(IFormFile file) {
            if (file == null || file.Length == 0) {
                return await Task.FromResult((string)null);
            }
            
            using (var reader = new StreamReader(file.OpenReadStream())) {
                return await reader.ReadToEndAsync();
            }
        }

        
       
    }
}