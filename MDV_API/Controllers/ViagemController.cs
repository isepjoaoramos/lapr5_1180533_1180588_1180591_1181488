using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Services;
using MDV_API.Dto;
using System.Web.Http.Cors;

namespace MDV_API.Controllers
{
    //[EnableCorsAttribute("_myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class ViagemController : ControllerBase
    {
        private readonly IViagemService _service;

        public ViagemController(IViagemService service)
        {
            _service = service;
        }

        // GET: api/Viagem/5
        [HttpGet]
        public async Task<List<ViagemOutDto>> GetAllViagem()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/viagem/linha={line}
        [HttpGet("linha={line}")]
        public async Task<ActionResult<List<ViagemDto>>> GetByLinha([FromRoute] string line)
        {
            var viagem = await _service.GetByLinha(line);

            if (viagem == null)
            {
                return NotFound();
            }

            return viagem;
        }

        // POST: api/viagem
        [ActionName("Create")]
        [HttpPost]
        public async Task<ActionResult<ViagemDto>> Create(ViagemDto dto)
        {
            Console.WriteLine("Percurso: "+dto.Percurso);
            Console.WriteLine("HoraSaida: "+dto.HoraSaida);
            Console.WriteLine("Linha: "+dto.Linha);

            var viagem = await _service.AddAsync(dto);

            //return CreatedAtAction("Criar viagem", viagem);
            return viagem;
        }

        
       
    }
}