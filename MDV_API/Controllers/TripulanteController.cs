using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MDV_API.Dto;
using MDV_API.Services;
using MDV_API.Domain;
using System.Web.Http.Cors;




namespace MDV_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripulanteController : ControllerBase
    {
        private readonly ITripulanteService _service;

        public TripulanteController(ITripulanteService service)
        {
            _service = service;
        }

        //GET: api/tripulante
        [HttpGet]
        public async Task<List<TripulanteDto>> getAllTripulante()
        {
            return await _service.GetAllAsync();
        }

        // POST: api/tripulante
        [Route("/api/tripulante")]
        [ActionName("Create")]
        [HttpPost]
        public async Task<ActionResult<TripulanteDto>> createTripulante(TripulanteDto dto)
        {
            var tripulante = await _service.AddAsync(dto);

            // return CreatedAtAction("criacao tripulante", tripulante);
            return tripulante;

        }

        // GET: api/tripulante/numMecanografico={numMecanografico}
        [HttpGet("numMecanografico={num}")]
        public async Task<ActionResult<TripulanteDto>> GetByNumMecanograficoAsync([FromRoute] string num)
        {
            var tripulante = await _service.GetByNumMecanograficoAsync(num);

            if (tripulante == null)
            {
                return NotFound();
            }

            return tripulante;
        }



    }
}
