using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Services;
using MDV_API.Dto;


namespace MDV_API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ServicoTripulanteController : ControllerBase
    {
        private readonly IServicoTripulanteService servico_tripulante_service;

        public ServicoTripulanteController(IServicoTripulanteService service)
        {
            this.servico_tripulante_service = service;
        }

        // GET: api/servicoTripulante
        [HttpGet]
        public async Task<List<ServicoTripulanteDto>> GetAllServicosTripulante()
        {
            return await this.servico_tripulante_service.GetAllAsync();
        }


        // GET: api/servicoTripulante/codServicoTripulante={codServicoTripulante}
        [HttpGet("codServicoTripulante={codServicoTripulante}")]
        public async Task<ActionResult<ServicoTripulanteDto>> GetByCodServicoTripulante([FromRoute] string codServicoTripulante)
        {
            var servico_tripulante = await this.servico_tripulante_service.GetByCodServicoTripulante(codServicoTripulante);

            if (servico_tripulante == null)
            {
                return NotFound();
            }

            return servico_tripulante;
        }

        // POST: api/servicoTripulante
        [HttpPost]
        public async Task<ActionResult<ServicoTripulanteDto>> CreateServicoTripulante(ServicoTripulanteDto servicoTripulanteDto)
        {
            var servico_tripulante = await this.servico_tripulante_service.AddAsync(servicoTripulanteDto);

            return servico_tripulante;
        }

    }


}