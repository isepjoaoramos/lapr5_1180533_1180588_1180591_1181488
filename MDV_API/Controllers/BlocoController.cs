using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Services;
using MDV_API.Infrastructure;
using MDV_API.Dto;

namespace MDV_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlocoController : ControllerBase
    {
        private readonly IBlocoService blocoService;
        private readonly IInformacaoBlocoService infoBlocoService;

        public BlocoController(IBlocoService blocoService, IInformacaoBlocoService infoBlocoService)
        {
            this.blocoService = blocoService;
            this.infoBlocoService = infoBlocoService;
        }

        // GET: api/Blocos/5
        /*[HttpGet("{id}")]
        public async Task<ActionResult<BlocoDTO>> GetById(Guid id)
        {
            var cat = await _service.GetByIdAsync(new BlocoId(id));

            if (cat == null)
            {
                return NotFound();
            }

            return cat;
        }*/

        // GET: api/Viagem/5
        [HttpGet]
        public async Task<List<InformacaoBlocoTrabalho>> GetAllBlocos()
        {
            return await infoBlocoService.GetAllAsync();
        }

        // POST: api/blocos
        [HttpPost]
        public async Task<ActionResult<BlocoDTO>> Create(BlocoDTO dto)
        {
            var bloco = await blocoService.AddAsync(dto);
            return bloco;
        }

        
       
    }
}