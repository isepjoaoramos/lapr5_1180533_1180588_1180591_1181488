using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Services;
using MDV_API.Dto;


namespace MDV_API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ServicoViaturaController : ControllerBase
    {
        private readonly IServicoViaturaService servico_viatura_service;

        public ServicoViaturaController(IServicoViaturaService service)
        {
            this.servico_viatura_service = service;
        }

        // GET: api/servicoViatura
        [HttpGet]
        public async Task<List<ServicoViaturaDto>> GetAllServicosViatura()
        {
            return await this.servico_viatura_service.GetAllAsync();
        }


        // GET: api/servicoviatura/codservicoviatura={codServicoViatura}
        [HttpGet("codservicoviatura={codServicoViatura}")]
        public async Task<ActionResult<ServicoViaturaDto>> GetByCodServicoViatura([FromRoute] string codServicoViatura)
        {
            var servico_viatura = await this.servico_viatura_service.GetByCodServicoViatura(codServicoViatura);

            if (servico_viatura == null)
            {
                return NotFound();
            }

            return servico_viatura;
        }

        // POST: api/servicoviatura
        [HttpPost]
        public async Task<ActionResult<ServicoViaturaDto>> CreateViatura(ServicoViaturaDto servicoViaturaDto)
        {
            var servico_viatura = await this.servico_viatura_service.AddAsync(servicoViaturaDto);

            return servico_viatura;
        }

    }


}