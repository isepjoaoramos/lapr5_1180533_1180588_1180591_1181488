using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Services;
using MDV_API.Dto;

namespace MDV_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViaturaController : ControllerBase
    {
        private readonly IViaturaService viatura_service;

        public ViaturaController(IViaturaService service)
        {
            this.viatura_service = service;
        }

        // GET: api/viatura
        [HttpGet]
        public async Task<List<ViaturaDto>> GetAllViatura()
        {
            return await viatura_service.GetAllAsync();
        }


        // GET: api/viatura/matricula={matricula}
        [HttpGet("matricula={matr}")]
        public async Task<ActionResult<ViaturaDto>> GetByMatricula([FromRoute] string matr)
        {
            var viatura = await viatura_service.GetByMatricula(matr);

            if (viatura == null)
            {
                return NotFound();
            }

            return viatura;
        }

        // GET: api/viatura/vin={vin}
        [HttpGet("vin={vin}")]
        public async Task<ActionResult<ViaturaDto>> GetByVin([FromRoute] string vin)
        {
            var viatura = await viatura_service.GetByVin(vin);

            if (viatura == null)
            {
                return NotFound();
            }

            return viatura;
        }


        // POST: api/viatura
        [HttpPost]
        public async Task<ActionResult<ViaturaDto>> CreateViatura(ViaturaDto viatura_dto)
        {

            Console.WriteLine(viatura_dto.matricula);
            Console.WriteLine(viatura_dto.vin);
            Console.WriteLine(viatura_dto.cod_tipo_viatura);
            Console.WriteLine(viatura_dto.data_entrada_servico);


            var viatura = await viatura_service.AddAsync(viatura_dto);

            return viatura;
        }
        
    }
}