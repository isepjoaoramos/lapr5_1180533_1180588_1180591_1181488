using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MDV_API.Dto;
using MDV_API.Services;
using MDV_API.Domain;
using System.Web.Http.Cors;




namespace MDV_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoTripulanteController : ControllerBase
    {
        private readonly ITipoTripulanteService _service;

        public TipoTripulanteController(ITipoTripulanteService service)
        {
            _service = service;
        }

        // POST: api/tripulante
        [Route("/api/tipoTripulante")]
        [ActionName("Create")]
        [HttpPost]
        public async Task<ActionResult<TipoTripulanteDto>> createTipoTripulante(TipoTripulanteDto dto)
        {
            var tipo = await _service.AddAsync(dto);

            // return CreatedAtAction("criacao tripulante", tripulante);
            return tipo;

        }

        // GET: api/tipoTripulante?codigoTripulante={codcodigoTripulante}
        [HttpGet("codigo={cod}")]
        public async Task<ActionResult<TipoTripulanteDto>> GetByCodAsync([FromRoute] string cod)
        {
            var tipo = await _service.GetByCodAsync(cod);

            if (tipo == null)
            {
                return NotFound();
            }

            return tipo;
        }

         // GET: api/tipoTripulante?codigoTripulante={codcodigoTripulante}
        [HttpGet("descricao={desc}")]
        public async Task<ActionResult<TipoTripulanteDto>> GetByDescricaoAsync([FromRoute] string desc)
        {
            var tipo = await _service.GetByDescricaoAsync(desc);

            if (tipo == null)
            {
                return NotFound();
            }

            return tipo;
        }

        //GET: api/tipoTripulante
        [HttpGet]   
        public async Task<List<TipoTripulanteDto>> getAllTipoTripulante()
        {
            return await _service.GetAllAsync();
        }

    }
}
