using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MDV_API.Infrastructure;
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Services;
using System.Web.Http.Cors;

namespace MDV_API
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
        {
            options.AddDefaultPolicy(
                              builder =>
                              {
                                  builder.WithOrigins("http://localhost:4200").AllowAnyHeader().WithMethods("PUT", "DELETE", "GET", "POST");
                              });
        });
            services.AddControllers();

            ConfigureMyServices(services);
            
            services.AddDbContext<MDV_APIDbContext>(options =>
              options.UseSqlServer(
                  Configuration.GetConnectionString("MDV_APIDbContext")
              )
            );

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MDV_API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MDV_API v1"));
            }

            //app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


        // Add Interfaces, Services, Controllers to use in dependency injection
         public void ConfigureMyServices(IServiceCollection services)
        {
             services.AddTransient<IUnitOfWork,UnitOfWork>();

             services.AddTransient<IViagemRepository,ViagemRepository>();
             services.AddTransient<IViagemService,ViagemService>();

             services.AddTransient<ITripulanteRepository,TripulanteRepository>();
             services.AddTransient<ITripulanteService,TripulanteService>();

             services.AddTransient<IImportacaoGlxService,ImportacaoGlxService>(); 
             
             services.AddTransient<ITipoTripulanteRepository,TipoTripulanteRepository>();
             services.AddTransient<ITipoTripulanteService,TipoTripulanteService>();

             services.AddTransient<IViaturaRepository,ViaturaRepository>();
             services.AddTransient<IViaturaService,ViaturaService>();

             services.AddTransient<IServicoViaturaRepository,ServicoViaturaRepository>();
             services.AddTransient<IServicoViaturaService,ServicoViaturaService>();

             services.AddTransient<IServicoTripulanteRepository,ServicoTripulanteRepository>();
             services.AddTransient<IServicoTripulanteService,ServicoTripulanteService>();

             services.AddTransient<IBlocoServicoTripulanteRepository, BlocoServicoTripulanteRepository>();
             
             services.AddTransient<IBlocoRepository, BlocoRepository>();
             services.AddTransient<IBlocoService, BlocoService>();

             services.AddTransient<IInformacaoBlocoRepository, InformacaoBlocoTrabalhoRepository>();
             services.AddTransient<IInformacaoBlocoService,InformacaoBlocoService>();

             services.AddTransient<IViagemServicoViaturaRepository, ViagemServicoViaturaRepository>();
             
             services.AddTransient<ITiposTripulante_TripulanteRepository, TiposTripulante_TripulanteRepository>();
        }
    }
}
