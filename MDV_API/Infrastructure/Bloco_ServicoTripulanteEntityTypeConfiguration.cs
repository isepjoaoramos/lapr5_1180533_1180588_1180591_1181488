using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class Bloco_ServicoTripulanteEntityTypeConfiguration : IEntityTypeConfiguration<Bloco_ServicoTripulante>
    {
        public void Configure(EntityTypeBuilder<Bloco_ServicoTripulante> builder)
        {
            // cf. https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx

            builder.HasKey(b => b.Id);

            builder.HasOne<ServicoTripulante>(s => s.servicoTripulante)
            .WithMany(g => g.listaBlocosTrabalho)
            .HasForeignKey(s => s.ServicoTripulanteId);

            builder.HasOne<BlocoTrabalho>(s => s.bloco)
            .WithMany(g => g.listaBlocosTrabalho)
            .HasForeignKey(s => s.BlocoId);
        }
    }
}