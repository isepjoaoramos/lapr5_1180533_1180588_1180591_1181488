using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class BlocoEntityTypeConfiguration : IEntityTypeConfiguration<BlocoTrabalho>
    {
        public void Configure(EntityTypeBuilder<BlocoTrabalho> builder)
        {
            // cf. https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx

            builder.HasKey(b => b.Id);
        }
    }
}