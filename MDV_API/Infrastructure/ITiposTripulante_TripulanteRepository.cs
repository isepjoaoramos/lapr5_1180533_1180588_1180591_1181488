using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface ITiposTripulante_TripulanteRepository
    {

        Task<List<TiposTripulante_Tripulante>> GetAllAsync();

        Task<List<TiposTripulante_Tripulante>> GetByTripulanteId(int id);

        Task<TiposTripulante_Tripulante> AddAsync(TiposTripulante_Tripulante tiposTripulante_Tripulante);

    }
}