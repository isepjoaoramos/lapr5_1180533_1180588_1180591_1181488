using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class TipoTripulanteEntityTypeConfiguration : IEntityTypeConfiguration<TipoTripulante>
    {
        public void Configure(EntityTypeBuilder<TipoTripulante> builder)
        {


            builder.HasKey(b => b.Id);

            builder.OwnsOne(x => x.codigo, c =>
           {
               c.Property(x => x.cod_tipo_tripulante).IsRequired().HasColumnName("Código");

               c.HasIndex(z => z.cod_tipo_tripulante).IsUnique();
           });

            builder.OwnsOne(x => x.descricao, c =>
            {
                c.Property(x => x.descricaoTipoTripulante).IsRequired().HasColumnName("Descrição");

                c.HasIndex(z => z.descricaoTipoTripulante).IsUnique();

            });


        }
    }
}