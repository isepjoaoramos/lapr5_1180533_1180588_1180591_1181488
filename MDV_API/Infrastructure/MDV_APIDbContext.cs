using Microsoft.EntityFrameworkCore;
using MDV_API.Domain;
using MDV_API.Infrastructure;


namespace MDV_API.Infrastructure
{
    public class MDV_APIDbContext : DbContext
    {
        public DbSet<Viagem> Viagens { get; set; }
        public DbSet<Tripulante> Tripulantes { get; set; }
        public DbSet<TipoTripulante> TiposTripulante { get; set; }
        public DbSet<TiposTripulante_Tripulante> TiposTripulante_Tripulante { get; set; }

        public DbSet<BlocoTrabalho> Blocos {get; set; }

        public DbSet<Viatura> Viaturas{get; set;}

        public DbSet<ServicoViatura> ServicosViatura {get;set;}

        public DbSet<ServicoTripulante> ServicosTripulante {get;set;}

        public DbSet<Viagem_ServicoViatura> ViagensServicoViatura {get;set;} 

        public DbSet<InformacaoBlocoTrabalho> InformacaoBlocoTrabalhos {get;set;}        
        public DbSet<Bloco_ServicoTripulante> BlocosServicosTripulante {get;set;}        

        public MDV_APIDbContext(DbContextOptions<MDV_APIDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.HasDefaultSchema("MDV_API");
            
            modelBuilder.ApplyConfiguration(new ViagemEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new ViaturaEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new TripulanteEntityTypeConfiguration());
            
            modelBuilder.ApplyConfiguration(new TipoTripulanteEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new ServicoViaturaEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new ServicoTripulanteEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new BlocoEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new Viagem_ServicoViaturaEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new InformacaoBlocoTrabalhoEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new TiposTripulante_TripulanteEntityTypeConfiguration());

        }
    }
}