using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class TiposTripulante_TripulanteEntityTypeConfiguration : IEntityTypeConfiguration<TiposTripulante_Tripulante>
    {
        public void Configure(EntityTypeBuilder<TiposTripulante_Tripulante> builder)
        {
            // Define Primary Key
            builder.HasKey(b => b.Id);

            builder.HasOne<Tripulante>(t => t.tripulante)
            .WithMany(g => g.tiposTripulante)
            .HasForeignKey(t => t.TripulanteId);

            builder.HasOne<TipoTripulante>(t => t.tipoTripulante)
            .WithMany(g => g.tiposTripulante)
            .HasForeignKey(t => t.TipoTripulanteId);
        }
    }
}