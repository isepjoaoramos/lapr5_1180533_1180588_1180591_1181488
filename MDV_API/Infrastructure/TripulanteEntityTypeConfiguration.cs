using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class TripulanteEntityTypeConfiguration : IEntityTypeConfiguration<Tripulante>
    {
        public void Configure(EntityTypeBuilder<Tripulante> builder)
        {


            builder.HasKey(b => b.Id);

             builder.OwnsOne(x => x.numMecanografico, c =>
            {
                c.Property(x => x.numMecanografico).IsRequired().HasColumnName("Número Mecanografico");

                c.HasIndex(z => z.numMecanografico).IsUnique();
            }); 

            builder.OwnsOne(x => x.nome, c =>
            {
                c.Property(x => x.nome).IsRequired().HasColumnName("Nome");
            });

            builder.OwnsOne(x => x.dataNascimento, c =>
            {
                c.Property(x => x.dataNascimento).IsRequired().HasColumnName("Data Nascimento");
            });

            builder.OwnsOne(x => x.numCartaoCidadao, c =>
            {
                c.Property(x => x.numCartaoCidadao).IsRequired().HasColumnName("Número Cartão Cidadão");

                c.HasIndex(z => z.numCartaoCidadao).IsUnique();
            });

            builder.OwnsOne(x => x.nif, c =>
            {
                c.Property(x => x.nif).IsRequired().HasColumnName("NIF");
                c.HasIndex(z => z.nif).IsUnique();

            });

            builder.OwnsOne(x => x.numCartaConducao, c =>
            {
                c.Property(x => x.numCartaConducao).IsRequired().HasColumnName("Número Carta Conducão");
                c.HasIndex(z => z.numCartaConducao).IsUnique();

            });

            builder.OwnsOne(x => x.dataEmissaoCartaConducao, c =>
            {
                c.Property(x => x.dataEmissaoCartaConducao).IsRequired().HasColumnName("Data Emissão Carta Conducão");
            });


            builder.OwnsOne(x => x.dataEntradaEmpresa, c =>
            {
                c.Property(x => x.dataEntradaEmpresa).IsRequired().HasColumnName("Data Entrada Empresa");
            });

            builder.OwnsOne(x => x.dataSaidaEmpresa, c =>
            {
                c.Property(x => x.dataSaidaEmpresa).IsRequired().HasColumnName("Data Saida Empresa");
            });


        }
    }
}