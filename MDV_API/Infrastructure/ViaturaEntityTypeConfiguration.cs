using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class ViaturaEntityTypeConfiguration : IEntityTypeConfiguration<Viatura>
    {
        public void Configure(EntityTypeBuilder<Viatura> builder)
        {
            // cf. https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx

            //builder.ToTable("Categories", SchemaNames.MDV_API);

            // Define Primary Key
            builder.HasKey(b => b.Id);

            builder.OwnsOne(x => x.matricula, c =>
            {
                c.Property(x => x.matricula).IsRequired().HasColumnName("Matricula");

                // Neste caso pretendo que a matricula, portanto crio um index e defino como IsUnique()
                c.HasIndex(z=> z.matricula).IsUnique();

            });

            // Define Objetos Embutidos --> Value Objects

            builder.OwnsOne(x => x.cod_tipo_viatura, c =>
            {
                c.Property(x => x.codigo_tipo_viatura).IsRequired().HasColumnName("CodTipoViatura");
            });

            builder.OwnsOne(x => x.numero_identificacao_viatura, c =>
            {
                // Neste caso pretendo que o vin seja único, portanto crio um index e defino como IsUnique()
                c.Property(x => x.numero_identificacao_viatura).IsRequired().HasColumnName("Vin");
                c.HasIndex(z=> z.numero_identificacao_viatura).IsUnique();

            });


             builder.OwnsOne(x => x.data_entrada_servico, c =>
            {
                c.Property(x => x.data_entrada).IsRequired().HasColumnName("DataEntradaServico");
            });
        }
    }
}