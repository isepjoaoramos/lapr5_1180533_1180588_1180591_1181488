using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class ServicoViaturaEntityTypeConfiguration : IEntityTypeConfiguration<ServicoViatura>
    {
        public void Configure(EntityTypeBuilder<ServicoViatura> builder)
        {
            // cf. https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx

            //builder.ToTable("Categories", SchemaNames.MDV_API);

            // Define Primary Key
            builder.HasKey(b => b.Id);

            builder.OwnsOne(x => x.cod_servico_viatura, c =>
            {
                c.Property(x => x.cod_servico_viatura).IsRequired().HasColumnName("CodServicoViatura");

                 c.HasIndex(z=> z.cod_servico_viatura).IsUnique();
            });

            // Define Objetos Embutidos --> Value Objects

            builder.OwnsOne(x => x.descricao, c =>
            {
                c.Property(x => x.descricao).IsRequired().HasColumnName("Descricao_ServicoViatura");
            });
        }
    }
}