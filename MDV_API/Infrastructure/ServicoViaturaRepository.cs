using MDV_API.Domain;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;


namespace MDV_API.Infrastructure
{
    public class ServicoViaturaRepository : IServicoViaturaRepository
    {
        
        private readonly DbSet<ServicoViatura> servicos_viaturas;

        public ServicoViaturaRepository(MDV_APIDbContext context){
            this.servicos_viaturas = context.ServicosViatura;
        }

        public async Task<List<ServicoViatura>> GetAllAsync(){

            return await this.servicos_viaturas.ToListAsync();

        }

        public async Task<ServicoViatura> GetByCodServicoViaturaAsync(string param_cod_servico_viatura){
            return await this.servicos_viaturas
                 .Where(x => param_cod_servico_viatura.Equals(x.cod_servico_viatura.cod_servico_viatura)).FirstOrDefaultAsync();
        }


        public async Task<ServicoViatura> AddAsync(ServicoViatura param_serv_viatura){

            var created_servico_viatura = await this.servicos_viaturas.AddAsync(param_serv_viatura);

            return created_servico_viatura.Entity;

        }
    
    }
}