
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IBlocoRepository
    {
        Task<List<BlocoTrabalho>> GetAllAsync();
        Task<BlocoTrabalho> AddAsync(BlocoTrabalho bloco);
        Task<BlocoTrabalho> GetByIdAsync(int id);
    }
}