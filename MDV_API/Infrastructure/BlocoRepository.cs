using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace MDV_API.Infrastructure
{
    public class BlocoRepository : IBlocoRepository
    {
      private readonly DbSet<BlocoTrabalho> blocos;
      public BlocoRepository(MDV_APIDbContext context)
      {
          this.blocos = context.Blocos;
      }
      public Task<List<BlocoTrabalho>> GetAllAsync(){

          return this.blocos.ToListAsync();

      }
      public async Task<BlocoTrabalho> AddAsync(BlocoTrabalho bloco){

          var created_bloco = await this.blocos.AddAsync(bloco);

          return created_bloco.Entity;

      }

       public async Task<BlocoTrabalho> GetByIdAsync(int id){
            return await this.blocos
                 .Where(x => id.Equals(x.Id)).FirstOrDefaultAsync();
        }


    }
}