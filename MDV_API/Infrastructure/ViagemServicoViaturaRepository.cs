using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;


namespace MDV_API.Infrastructure
{
    public class ViagemServicoViaturaRepository : IViagemServicoViaturaRepository
    {
        
        private readonly DbSet<Viagem_ServicoViatura> viagens_viaturas;

        public ViagemServicoViaturaRepository(MDV_APIDbContext context){
            this.viagens_viaturas = context.ViagensServicoViatura;
        }

        public Task<List<Viagem_ServicoViatura>> GetAllAsync(){

            return this.viagens_viaturas.ToListAsync();

        }

        public async Task<List<Viagem_ServicoViatura>> GetByServicoId(int id){
            return await this.viagens_viaturas
                 .Where(x => id.Equals(x.ServicoViaturaId)).ToListAsync();
        }


        public async Task<Viagem_ServicoViatura> AddAsync(Viagem_ServicoViatura viagem_servicoViatura){

            var created_servico_viatura = await this.viagens_viaturas.AddAsync(viagem_servicoViatura);
            return created_servico_viatura.Entity;
        }
    
    }
}