using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class ViagemEntityTypeConfiguration : IEntityTypeConfiguration<Viagem>
    {
        public void Configure(EntityTypeBuilder<Viagem> builder)
        {
            // cf. https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx

            //builder.ToTable("Categories", SchemaNames.MDV_API);
            builder.HasKey(b => b.Id);
            //builder.Property<bool>("_active").HasColumnName("Active");
             builder.OwnsOne(x => x.Linha, c =>
            {
                c.Property(x => x.nome_linha).IsRequired().HasColumnName("Linha");

            });

            // Define Objetos Embutidos --> Value Objects

            builder.OwnsOne(x => x.HoraSaida, c =>
            {
                c.Property(x => x.hora_viagem).IsRequired().HasColumnName("HoraViagem");
            });

            builder.OwnsOne(x => x.Percurso, c =>
            {
          
                c.Property(x => x.descricao_percurso).IsRequired().HasColumnName("Percurso");
               
            });

        }
    }
}