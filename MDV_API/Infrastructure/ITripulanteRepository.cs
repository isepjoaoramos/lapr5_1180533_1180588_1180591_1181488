using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface ITripulanteRepository
    {
        Task<List<Tripulante>> GetAllAsync();

        Task<Tripulante> GetByNumMecanograficoAsync(string num);

        Task<Tripulante> AddAsync(Tripulante tripulante);

    }
}