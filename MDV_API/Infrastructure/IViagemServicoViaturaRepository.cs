
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IViagemServicoViaturaRepository
    {

        Task<List<Viagem_ServicoViatura>> GetAllAsync();

        Task<List<Viagem_ServicoViatura>> GetByServicoId(int matricula);

        Task<Viagem_ServicoViatura> AddAsync(Viagem_ServicoViatura viagem_servicoViatura);

    }
}