using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;



namespace MDV_API.Infrastructure
{
    public class BlocoServicoTripulanteRepository : IBlocoServicoTripulanteRepository
    {
        
        private readonly DbSet<Bloco_ServicoTripulante> blocos_servicosTripulante;

        public BlocoServicoTripulanteRepository(MDV_APIDbContext context){
            this.blocos_servicosTripulante = context.BlocosServicosTripulante;
        }

        public Task<List<Bloco_ServicoTripulante>> GetAllAsync(){

            return this.blocos_servicosTripulante.ToListAsync();

        }

        public async Task<List<Bloco_ServicoTripulante>> GetByServicoId(int id){
            return await this.blocos_servicosTripulante
                 .Where(x => id.Equals(x.ServicoTripulanteId)).ToListAsync();
        }


        public async Task<Bloco_ServicoTripulante> AddAsync(Bloco_ServicoTripulante bloco_ServicoTripulante){

            var created_servico_tripulante = await this.blocos_servicosTripulante.AddAsync(bloco_ServicoTripulante);
            return created_servico_tripulante.Entity;
        }
    
    }
}