using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IServicoTripulanteRepository
    {

        Task<List<ServicoTripulante>> GetAllAsync();

        Task<ServicoTripulante> GetByCodServicoTripulanteAsync(string cod);

        Task<ServicoTripulante> AddAsync(ServicoTripulante tripulante);

    }
}