
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IViagemRepository
    {
        Task<List<Viagem>> GetAllAsync();
        Task<Viagem> AddAsync(Viagem viagem);
        Task<Viagem> GetByIdAsync(int id);
        Task<Viagem> GetViagemByLinhaHorarioAndPercurso(string linha, int horario, string percurso);
        Task<List<Viagem>> GetByLinhaAsync(string line);
    }
}