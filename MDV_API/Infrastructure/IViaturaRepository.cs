
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IViaturaRepository
    {

        Task<List<Viatura>> GetAllAsync();

        Task<Viatura> GetByMatriculaAsync(string matricula);

        Task<Viatura> GetByVinAsync(string vin);

        Task<Viatura> AddAsync(Viatura viatura);

    }
}