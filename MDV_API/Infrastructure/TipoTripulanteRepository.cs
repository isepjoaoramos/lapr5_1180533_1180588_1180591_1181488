using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;


namespace MDV_API.Infrastructure
{
    public class TipoTripulanteRepository : ITipoTripulanteRepository
    {
        private readonly DbSet<TipoTripulante> tipostripulante;
        public TipoTripulanteRepository(MDV_APIDbContext context)
        {
            this.tipostripulante = context.TiposTripulante;
        }

        public Task<List<TipoTripulante>> GetAllAsync()
        {
            return this.tipostripulante.ToListAsync();
        }

        public async Task<TipoTripulante> AddAsync(TipoTripulante tipoTripulante)
        {

            var created_tipo = await this.tipostripulante.AddAsync(tipoTripulante);

            return created_tipo.Entity;
        }

        public async Task<TipoTripulante> GetByCodAsync(string cod)
        {
            return await this.tipostripulante
                .Where(x => cod.Equals(x.codigo.cod_tipo_tripulante)).FirstOrDefaultAsync();
        }

         public async Task<TipoTripulante> GetByDescricaoAsync(string desc)
        {
            return await this.tipostripulante
                .Where(x => desc.Equals(x.descricao.descricaoTipoTripulante)).FirstOrDefaultAsync();
        }

         public async Task<TipoTripulante> GetByIdAsync(int id){
            return await this.tipostripulante
                 .Where(x => id.Equals(x.Id)).FirstOrDefaultAsync();
        }

    }
}