
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IInformacaoBlocoRepository
    {
        Task<List<InformacaoBlocoTrabalho>> GetAllAsync();
        Task<InformacaoBlocoTrabalho> AddAsync(InformacaoBlocoTrabalho bloco);
        Task<List<InformacaoBlocoTrabalho>> GetByServicoViatura(CodServicoViatura idServicoViatura);
    }
}