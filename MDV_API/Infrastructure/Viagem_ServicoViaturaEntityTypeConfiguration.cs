using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class Viagem_ServicoViaturaEntityTypeConfiguration : IEntityTypeConfiguration<Viagem_ServicoViatura>
    {
        public void Configure(EntityTypeBuilder<Viagem_ServicoViatura> builder)
        {
            // cf. https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx

            //builder.ToTable("Categories", SchemaNames.MDV_API);

            // Define Primary Key
            builder.HasKey(b => b.Id);

            builder.HasOne<ServicoViatura>(s => s.servicoViatura)
            .WithMany(g => g.ListaViagem_ServicoViatura)
            .HasForeignKey(s => s.ServicoViaturaId);

            builder.HasOne<Viagem>(s => s.viagem)
            .WithMany(g => g.ListaViagem_ServicoViatura)
            .HasForeignKey(s => s.ViagemId);
        }
    }
}