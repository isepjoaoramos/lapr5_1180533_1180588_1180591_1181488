
using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IBlocoServicoTripulanteRepository
    {

        Task<List<Bloco_ServicoTripulante>> GetAllAsync();

        Task<List<Bloco_ServicoTripulante>> GetByServicoId(int matricula);

        Task<Bloco_ServicoTripulante> AddAsync(Bloco_ServicoTripulante bloco_servicoTripulante);

    }
}