using MDV_API.Domain;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;


namespace MDV_API.Infrastructure
{
    public class ServicoTripulanteRepository : IServicoTripulanteRepository
    {

        private readonly DbSet<ServicoTripulante> servicosTripulante;

        public ServicoTripulanteRepository(MDV_APIDbContext context)
        {
            this.servicosTripulante = context.ServicosTripulante;
        }

        public Task<List<ServicoTripulante>> GetAllAsync()
        {

            return this.servicosTripulante.ToListAsync();

        }

        public async Task<ServicoTripulante> GetByCodServicoTripulanteAsync(string codServicoTripulante)
        {
            return await this.servicosTripulante
                 .Where(x => codServicoTripulante.Equals(x.codServicoTripulante.cod_servico_tripulante)).FirstOrDefaultAsync();
        }


        public async Task<ServicoTripulante> AddAsync(ServicoTripulante servicoTripulante)
        {

            var new_servicoTripulante = await this.servicosTripulante.AddAsync(servicoTripulante);

            return new_servicoTripulante.Entity;

        }

    }
}