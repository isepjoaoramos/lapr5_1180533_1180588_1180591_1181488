using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace MDV_API.Infrastructure
{
    public class ViagemRepository : IViagemRepository
    {
        private readonly DbSet<Viagem> viagens;
        public ViagemRepository(MDV_APIDbContext context)
        {
           this.viagens = context.Viagens;
        }

         public Task<List<Viagem>> GetAllAsync(){

            return this.viagens.ToListAsync();

        }
        public async Task<Viagem> AddAsync(Viagem viagem){

            var created_viagem = await this.viagens.AddAsync(viagem);

            return created_viagem.Entity;

        }

        public async Task<List<Viagem>> GetByLinhaAsync(string line){
            return await this.viagens
                 .Where(x => line.Equals(x.Linha.nome_linha)).ToListAsync();
        }

        public async Task<Viagem> GetByIdAsync(int id){
            return await this.viagens
                 .Where(x => id.Equals(x.Id)).FirstOrDefaultAsync();
        }

        public async Task<Viagem> GetViagemByLinhaHorarioAndPercurso(string linha, int horario, string percurso) {
            return await this.viagens.Where(x => linha.Equals(x.Linha.nome_linha) && horario.Equals(x.HoraSaida.hora_viagem)
            && percurso.Equals(x.Percurso.descricao_percurso)).FirstOrDefaultAsync();
        }
    }
}