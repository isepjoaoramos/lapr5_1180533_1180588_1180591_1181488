using System.Threading.Tasks;
using MDV_API.Domain.Shared;

namespace MDV_API.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MDV_APIDbContext _context;

        public UnitOfWork(MDV_APIDbContext context)
        {
            this._context = context;
        }

        public async Task<int> CommitAsync()
        {
            return await this._context.SaveChangesAsync();
        }
    }
}