 using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
     internal class ServicoTripulanteEntityTypeConfiguration : IEntityTypeConfiguration<ServicoTripulante>
     {
         public void Configure(EntityTypeBuilder<ServicoTripulante> builder)
         {


             builder.HasKey(b => b.Id);

              builder.OwnsOne(x => x.codServicoTripulante, c =>
             {
                 c.Property(x => x.cod_servico_tripulante).IsRequired().HasColumnName("Código");

                 c.HasIndex(z => z.cod_servico_tripulante).IsUnique();
             });
         }
     }
 }