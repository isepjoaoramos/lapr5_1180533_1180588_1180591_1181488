using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace MDV_API.Infrastructure
{
    public class InformacaoBlocoTrabalhoRepository : IInformacaoBlocoRepository
    {
      private readonly DbSet<InformacaoBlocoTrabalho> infoBlocos;
      public InformacaoBlocoTrabalhoRepository(MDV_APIDbContext context)
      {
          this.infoBlocos = context.InformacaoBlocoTrabalhos;
      }
      public Task<List<InformacaoBlocoTrabalho>> GetAllAsync(){

          return this.infoBlocos.ToListAsync();

      }
      public async Task<InformacaoBlocoTrabalho> AddAsync(InformacaoBlocoTrabalho infoBloco){

          var created_info_bloco = await this.infoBlocos.AddAsync(infoBloco);

          return created_info_bloco.Entity;

      }

      public async Task<List<InformacaoBlocoTrabalho>> GetByServicoViatura(CodServicoViatura idServicoViatura) {
        string idServicoViaturaStr = idServicoViatura.cod_servico_viatura;
        return await this.infoBlocos
                .Where(x => idServicoViaturaStr.Equals(x.servicoViatura.cod_servico_viatura)).ToListAsync();
    }


    }
}