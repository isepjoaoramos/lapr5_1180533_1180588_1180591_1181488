using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;


namespace MDV_API.Infrastructure
{
    public class TripulanteRepository : ITripulanteRepository
    {
        private readonly DbSet<Tripulante> tripulantes;
        public TripulanteRepository(MDV_APIDbContext context)
        {
            this.tripulantes = context.Tripulantes;
        }

        public Task<List<Tripulante>> GetAllAsync()
        {
            return this.tripulantes.ToListAsync();
        }

        public async Task<Tripulante> AddAsync(Tripulante tripulante)
        {

            var created_tripulante = await this.tripulantes.AddAsync(tripulante);

            return created_tripulante.Entity;
        }

        public async Task<Tripulante> GetByNumMecanograficoAsync(string num)
        {
            return await this.tripulantes
                .Where(x => num.Equals(x.numMecanografico.numMecanografico)).FirstOrDefaultAsync();
        }

    }
}