using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;


namespace MDV_API.Infrastructure
{
    public class ViaturaRepository : IViaturaRepository
    {
        
        private readonly DbSet<Viatura> viaturas;

        public ViaturaRepository(MDV_APIDbContext context){
            this.viaturas = context.Viaturas;
        }

        public Task<List<Viatura>> GetAllAsync(){

            return this.viaturas.ToListAsync();

        }

        public async Task<Viatura> GetByMatriculaAsync(string matricula){
            return await this.viaturas
                 .Where(x => matricula.Equals(x.matricula.matricula)).FirstOrDefaultAsync();
        }


         public async Task<Viatura> GetByVinAsync(string vin){
            return await this.viaturas
                 .Where(x => vin.Equals(x.numero_identificacao_viatura.numero_identificacao_viatura)).FirstOrDefaultAsync();
        }



        public async Task<Viatura> AddAsync(Viatura viatura){

            var created_viatura = await this.viaturas.AddAsync(viatura);

            return created_viatura.Entity;

        }
    
    }
}