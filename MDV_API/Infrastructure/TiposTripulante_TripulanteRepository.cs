using MDV_API.Domain;
using MDV_API.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;


namespace MDV_API.Infrastructure
{
    public class TiposTripulante_TripulanteRepository : ITiposTripulante_TripulanteRepository
    {
        
        private readonly DbSet<TiposTripulante_Tripulante> tiposTripulante_Tripulante;

        public TiposTripulante_TripulanteRepository(MDV_APIDbContext context){
            this.tiposTripulante_Tripulante = context.TiposTripulante_Tripulante;
        }

        public Task<List<TiposTripulante_Tripulante>> GetAllAsync(){

            return this.tiposTripulante_Tripulante.ToListAsync();

        }

        public async Task<List<TiposTripulante_Tripulante>> GetByTripulanteId(int id){
            return await this.tiposTripulante_Tripulante
                 .Where(x => id.Equals(x.TripulanteId)).ToListAsync();
        }


        public async Task<TiposTripulante_Tripulante> AddAsync(TiposTripulante_Tripulante tiposTripulante_Tripulante){

            var created_Tripulante = await this.tiposTripulante_Tripulante.AddAsync(tiposTripulante_Tripulante);
            return created_Tripulante.Entity;
        }
    
    }
}