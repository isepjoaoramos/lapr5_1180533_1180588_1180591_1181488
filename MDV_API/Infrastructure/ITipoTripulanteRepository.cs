using MDV_API.Domain.Shared;
using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface ITipoTripulanteRepository
    {
        Task<List<TipoTripulante>> GetAllAsync();

        Task<TipoTripulante> GetByCodAsync(string cod);
        Task<TipoTripulante> GetByDescricaoAsync(string desc);

        Task<TipoTripulante> AddAsync(TipoTripulante tipoTripulante);
        Task<TipoTripulante> GetByIdAsync(int id);

    }
}