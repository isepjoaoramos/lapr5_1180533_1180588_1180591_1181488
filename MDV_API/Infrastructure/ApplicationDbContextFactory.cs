using Microsoft.EntityFrameworkCore.Design;
using MDV_API.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.Extensions.Configuration;

public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<MDV_APIDbContext>
{

    private readonly IConfiguration _config;
    public MDV_APIDbContext CreateDbContext(string[] args)
    {
        var builder = new DbContextOptionsBuilder<MDV_APIDbContext>();
        builder.UseSqlServer("Server=tcp:lapr5-3db-g01.database.windows.net,1433;Initial Catalog=LAPR5_3DB_01;Persist Security Info=False;User ID=JoaoRamos;Password=*w6x6R6Ggtc8jw;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;", b => b.MigrationsAssembly("MDV_API"));
        return new MDV_APIDbContext(builder.Options);
    }
}