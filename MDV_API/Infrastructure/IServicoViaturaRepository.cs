using MDV_API.Domain;
using MDV_API.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV_API.Infrastructure
{
    public interface IServicoViaturaRepository
    {

        Task<List<ServicoViatura>> GetAllAsync();

        Task<ServicoViatura> GetByCodServicoViaturaAsync(string codServicoViatura);

        Task<ServicoViatura> AddAsync(ServicoViatura viatura);

    }
}