using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV_API.Domain;

namespace MDV_API.Infrastructure
{
    internal class InformacaoBlocoTrabalhoEntityTypeConfiguration : IEntityTypeConfiguration<InformacaoBlocoTrabalho>
    {
        public void Configure(EntityTypeBuilder<InformacaoBlocoTrabalho> builder)
        {
            // cf. https://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx

            builder.HasKey(b => b.Id);

            builder.HasOne<ServicoViatura>(s => s.servicoViatura)
            .WithMany(g => g.ListaInformacaoBloco)
            .HasForeignKey(s => s.idServicoViatura);

            builder.HasOne<Viagem>(s => s.viagem)
            .WithMany(g => g.ListaInformacaoBloco)
            .HasForeignKey(s => s.idViagem);

            builder.HasOne<BlocoTrabalho>(s => s.bloco)
            .WithMany(g => g.ListaInformacaoBloco)
            .HasForeignKey(s => s.idBloco);
        }
    }
}