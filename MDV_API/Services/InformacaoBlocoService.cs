using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;

namespace MDV_API.Services
{
    public class InformacaoBlocoService: IInformacaoBlocoService
    {
        private readonly IInformacaoBlocoRepository _repoInformacaoBloco;

        public InformacaoBlocoService(IInformacaoBlocoRepository repoInformacaoBloco)
        {
            this._repoInformacaoBloco = repoInformacaoBloco;
        }

        public Task<List<InformacaoBlocoTrabalho>> GetAllAsync() {
            return _repoInformacaoBloco.GetAllAsync();
        }
        public Task<InformacaoBlocoTrabalho> AddAsync(InformacaoBlocoTrabalho bloco) {
            return _repoInformacaoBloco.AddAsync(bloco);
        }
        public Task<List<InformacaoBlocoTrabalho>> GetByServicoViatura(CodServicoViatura idServicoViatura) {
            return _repoInformacaoBloco.GetByServicoViatura(idServicoViatura);
        }
    }
}