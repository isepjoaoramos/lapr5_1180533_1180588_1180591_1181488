using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System.Xml;

namespace MDV_API.Services
{
    public interface IImportacaoGlxService
    {
        public Task<List<ServicoTripulanteDto>> criaServicosTripulanteEBlocosTrabalho(XmlDocument doc);
        public Task<List<ServicoViaturaDto>> criaServicosViatura(XmlDocument doc);
        public Task<List<ViagemDto>> criaViagens(XmlDocument doc);
    }
}