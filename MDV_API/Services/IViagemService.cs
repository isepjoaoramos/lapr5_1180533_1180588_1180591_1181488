using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface IViagemService
    {
        //public Task<ViagemDto> GetByIdAsync(ViagemId id);
        
        public Task<List<ViagemOutDto>> GetAllAsync();

        public Task<List<ViagemDto>> GetByLinha(string line);

        public Task<ViagemDto> AddAsync(ViagemDto dto);   

        public Task<ViagemOutDto> GetViagemByLinhaHorarioAndPercurso(string line, int horario, string percurso);     
    }
}