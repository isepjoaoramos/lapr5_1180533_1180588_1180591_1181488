using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;

using System.Linq;

namespace MDV_API.Services
{


    public class ServicoViaturaService : IServicoViaturaService
    {


        private readonly IUnitOfWork _unitOfWork;
        private readonly IServicoViaturaRepository servico_viatura_repo;

        private readonly IViagemServicoViaturaRepository viagem_servicoViatura_repo;

        private readonly IViagemRepository viagemRepository;



        public ServicoViaturaService(IUnitOfWork unitOfWork, IServicoViaturaRepository param_servicoViaturaRepo, IViagemServicoViaturaRepository param_viagem_servicoViatura_repo, IViagemRepository param_viagemRepository)
        {
            this._unitOfWork = unitOfWork;
            this.servico_viatura_repo = param_servicoViaturaRepo;
            this.viagem_servicoViatura_repo = param_viagem_servicoViatura_repo;
            this.viagemRepository = param_viagemRepository;
        }



        public async Task<ServicoViaturaDto> GetByCodServicoViatura(string codServicoViatura)
        {
            var servico_viatura = await this.servico_viatura_repo.GetByCodServicoViaturaAsync(codServicoViatura);

            if (servico_viatura == null)
            {
                return null;
            }

            return new ServicoViaturaDto
            {
                idServicoViatura = servico_viatura.Id,
                codServicoViatura = servico_viatura.cod_servico_viatura.cod_servico_viatura,
                descricao = servico_viatura.descricao.descricao,
            };

        }



        public async Task<List<ServicoViaturaDto>> GetAllAsync()
        {

            var list_servicos_viatura = await this.servico_viatura_repo.GetAllAsync();

            await this._unitOfWork.CommitAsync();

            List<ServicoViaturaDto> listdto = new List<ServicoViaturaDto>();

            foreach (ServicoViatura servico_viatura in list_servicos_viatura)
            {
                var list_viagem_servico = await this.viagem_servicoViatura_repo.GetByServicoId(servico_viatura.Id);

                await this._unitOfWork.CommitAsync();

                List<ViagemOutDto> viagens = new List<ViagemOutDto>();

                foreach (Viagem_ServicoViatura viagem_servico in list_viagem_servico)
                {
                    var viagem = await this.viagemRepository.GetByIdAsync(viagem_servico.ViagemId);
                    viagens.Add(new ViagemOutDto
                    {
                        viagemId = viagem.Id,
                        Linha = viagem.Linha.nome_linha,
                        HoraSaida = viagem.HoraSaida.hora_viagem,
                        Percurso = viagem.Percurso.descricao_percurso,
                    });
                }

                var serv_viatura_dto = new ServicoViaturaDto{
                    idServicoViatura = servico_viatura.Id,
                    codServicoViatura = servico_viatura.cod_servico_viatura.cod_servico_viatura,
                    descricao = servico_viatura.descricao.descricao,
                    viagens = viagens.ToArray(),
                };

                listdto.Add(serv_viatura_dto);
            }
            return listdto;
        }



        public async Task<ServicoViaturaDto> AddAsync(ServicoViaturaDto servico_viatura_dto)
        {
            try
            {
                var servico_viatura = new ServicoViatura(servico_viatura_dto);

                await this.servico_viatura_repo.AddAsync(servico_viatura);

                await this._unitOfWork.CommitAsync();

                ViagemOutDto[] listViagens = servico_viatura_dto.viagens;


                // Para cada viagem 
                for (int i = 0; i < listViagens.Length; i++)
                {
                    var viagem_servicoViatura = new Viagem_ServicoViatura(servico_viatura.Id, listViagens[i].viagemId);

                    await this.viagem_servicoViatura_repo.AddAsync(viagem_servicoViatura);

                    await this._unitOfWork.CommitAsync();
                }

                return new ServicoViaturaDto
                {
                    idServicoViatura = servico_viatura.Id,
                    codServicoViatura = servico_viatura.cod_servico_viatura.cod_servico_viatura,
                    descricao = servico_viatura.descricao.descricao,
                };

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }

}