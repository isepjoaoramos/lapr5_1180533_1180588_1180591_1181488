using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface ITipoTripulanteService
    {
       public Task<TipoTripulanteDto> GetByCodAsync(string cod);
       public Task<TipoTripulanteDto> GetByDescricaoAsync(string desc);
        
        public Task<List<TipoTripulanteDto>> GetAllAsync();

        public Task<TipoTripulanteDto> AddAsync(TipoTripulanteDto dto);        
    }
}