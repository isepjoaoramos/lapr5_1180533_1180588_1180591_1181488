using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;

namespace MDV_API.Services
{
    public class BlocoService: IBlocoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBlocoRepository _repo;
        private readonly IInformacaoBlocoRepository _repoInformacaoBloco;

        public BlocoService(IUnitOfWork unitOfWork, IBlocoRepository repo, IInformacaoBlocoRepository repoInformacaoBloco)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._repoInformacaoBloco = repoInformacaoBloco;
        }

        // public async Task<BlocoDTO> GetByIdAsync(BlocoId id)
        // {
        //     var bloco = await this._repo.GetByIdAsync(id);
            
        //     if(bloco == null)
        //         return null;

        //     return new BlocoDTO{Id = bloco.Id.AsGuid(), viagemId = bloco.viagemId, idServiceViatura = bloco.idServicoViatura, idServicoTripulante = bloco.idServicoTripulante};
        // }

        // public async Task<List<BlocoDTO>> GetAllAsync()
        // {
        //     var list = await this._repo.GetAllAsync();
            
        //     List<BlocoDTO> listDto = list.ConvertAll<BlocoDTO>(bloco => new BlocoDTO{idViagem = bloco.idViagem, idServicoViatura = bloco.codServicoViatura.cod_servico_viatura, idServicoTripulante = bloco.numMecanograficoTripulante.numMecanografico });

        //     return listDto;
        // }

        public async Task<BlocoDTO> AddAsync(BlocoDTO dto)
        {
            var bloco = new BlocoTrabalho(dto);
            var blocoEntity = await this._repo.AddAsync(bloco);
            await this._unitOfWork.CommitAsync();

            var blocoId = blocoEntity.Id;

            var idServicoViatura = dto.idServicoViatura;
            var viagens = dto.codigosViagem;
            var nViagens = viagens.Length;

            var blocos = await this._repoInformacaoBloco.GetAllAsync();
            var length = blocos.Count;

            var verificaBloco = true;
            for (var i = 0; i < nViagens; i++) {
                verificaBloco = true;
                for (var j = 0; j < length; j++) {
                    if (blocos[j].idViagem == viagens[i]) {
                        verificaBloco = false;
                    }
                }
                if (verificaBloco) {
                    var associacao = new InformacaoBlocoTrabalho(idServicoViatura,bloco.Id,viagens[i]);
                    await this._repoInformacaoBloco.AddAsync(associacao);
                    await this._unitOfWork.CommitAsync();
                } else {
                    throw new System.Exception("Bloco repetido");
                }
            }

            return new BlocoDTO {blocoId=blocoId, codigosViagem = viagens, idServicoViatura=dto.idServicoViatura};
        }

    }
}