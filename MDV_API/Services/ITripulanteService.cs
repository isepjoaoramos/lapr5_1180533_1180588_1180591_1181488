using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface ITripulanteService
    {
       public Task<TripulanteDto> GetByNumMecanograficoAsync(string num);
        
        public Task<List<TripulanteDto>> GetAllAsync();

        public Task<TripulanteDto> AddAsync(TripulanteDto dto);        
    }
}