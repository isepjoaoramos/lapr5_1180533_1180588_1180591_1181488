using MDV_API.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;

namespace MDV_API.Services
{

    public class TripulanteService : ITripulanteService
    {


        private readonly IUnitOfWork _unitOfWork;
        private readonly ITripulanteRepository _repo;
        private readonly ITiposTripulante_TripulanteRepository _repo_TiposTripulante_Tripulante;
        private readonly ITipoTripulanteRepository _repo_TipoTripulante;



        public TripulanteService(IUnitOfWork unitOfWork, ITripulanteRepository repo, ITiposTripulante_TripulanteRepository _repo_TiposTripulante_Tripulante, ITipoTripulanteRepository _repo_TipoTripulante)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._repo_TiposTripulante_Tripulante = _repo_TiposTripulante_Tripulante;
            this._repo_TipoTripulante = _repo_TipoTripulante;
        }

        public async Task<TripulanteDto> GetByNumMecanograficoAsync(string num)
        {
            var tripulante = await this._repo.GetByNumMecanograficoAsync(num);

            if (tripulante == null)
                return null;

            return new TripulanteDto
            {
                tripulanteId = tripulante.Id,
                numMecanografico = tripulante.numMecanografico.numMecanografico,
                nome = tripulante.nome.nome,

            };
        }

        public async Task<List<TripulanteDto>> GetAllAsync()
        {
            var list_tripulantes = await this._repo.GetAllAsync();

            await this._unitOfWork.CommitAsync();

            List<TripulanteDto> listdto = new List<TripulanteDto>();

            foreach (Tripulante tripulante in list_tripulantes)
            {
                var list_tipos = await this._repo_TiposTripulante_Tripulante.GetByTripulanteId(tripulante.Id);

                await this._unitOfWork.CommitAsync();

                List<TipoTripulanteDto> tipos = new List<TipoTripulanteDto>();

                foreach (TiposTripulante_Tripulante tipo_Tripulante in list_tipos)
                {
                    var tipo = await this._repo_TipoTripulante.GetByIdAsync(tipo_Tripulante.TipoTripulanteId);
                    tipos.Add(new TipoTripulanteDto
                    {
                        tipoTripulanteId = tipo.Id,
                        codigo = tipo.codigo.cod_tipo_tripulante,
                        descricao = tipo.descricao.descricaoTipoTripulante,
                    });
                }

                var tripulante_dto = new TripulanteDto
                {
                    tripulanteId = tripulante.Id,
                    numMecanografico = tripulante.numMecanografico.numMecanografico,
                    nome = tripulante.nome.nome,
                    dataNascimento = tripulante.dataNascimento.dataNascimento,
                    numCartaoCidadao = tripulante.numCartaoCidadao.numCartaoCidadao,
                    nif = tripulante.nif.nif,
                    numCartaConducao = tripulante.numCartaConducao.numCartaConducao,
                    tiposTripulante = tipos.ToArray(),
                    dataEmissaoCartaConducao = tripulante.dataEmissaoCartaConducao.dataEmissaoCartaConducao,
                    dataEntradaEmpresa = tripulante.dataEntradaEmpresa.dataEntradaEmpresa,
                    dataSaidaEmpresa = tripulante.dataSaidaEmpresa.dataSaidaEmpresa,

                };

                listdto.Add(tripulante_dto);

            }
            return listdto;

        }

        public async Task<TripulanteDto> AddAsync(TripulanteDto dto)
        {
            try
            {
                var tripulante = new Tripulante(dto);

                await this._repo.AddAsync(tripulante);

                await this._unitOfWork.CommitAsync();

                TipoTripulanteDto[] listTiposTripulante = dto.tiposTripulante;

                // Para cada tipo de tripulante 
                for (int i = 0; i < listTiposTripulante.Length; i++)
                {
                    var tipo_tripulante = new TiposTripulante_Tripulante(tripulante.Id, listTiposTripulante[i].tipoTripulanteId);

                    await this._repo_TiposTripulante_Tripulante.AddAsync(tipo_tripulante);

                    await this._unitOfWork.CommitAsync();
                }

                return new TripulanteDto
                {
                    tripulanteId = tripulante.Id,
                    numMecanografico = tripulante.numMecanografico.numMecanografico,
                    nome = tripulante.nome.nome,
                    dataNascimento = tripulante.dataNascimento.dataNascimento,
                    numCartaoCidadao = tripulante.numCartaoCidadao.numCartaoCidadao,
                    nif = tripulante.nif.nif,
                    numCartaConducao = tripulante.numCartaConducao.numCartaConducao,
                    dataEmissaoCartaConducao = tripulante.dataEmissaoCartaConducao.dataEmissaoCartaConducao,
                    dataEntradaEmpresa = tripulante.dataEntradaEmpresa.dataEntradaEmpresa,
                    dataSaidaEmpresa = tripulante.dataSaidaEmpresa.dataSaidaEmpresa,
                };

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                return null;
            }


        }

    }


}


