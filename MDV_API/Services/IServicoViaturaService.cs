using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface IServicoViaturaService
    {
        public Task<ServicoViaturaDto> GetByCodServicoViatura(string codServicoViatura);

        public Task<List<ServicoViaturaDto>> GetAllAsync();

        public Task<ServicoViaturaDto> AddAsync(ServicoViaturaDto dto);
    }
}