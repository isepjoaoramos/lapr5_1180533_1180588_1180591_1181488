using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface IServicoTripulanteService
    {
        public Task<ServicoTripulanteDto> GetByCodServicoTripulante(string codServicoTripulante);

        public Task<List<ServicoTripulanteDto>> GetAllAsync();

        public Task<ServicoTripulanteDto> AddAsync(ServicoTripulanteDto dto);
    }
}