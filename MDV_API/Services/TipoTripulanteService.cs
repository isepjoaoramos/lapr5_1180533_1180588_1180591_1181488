using MDV_API.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;


namespace MDV_API.Services
{

    public class TipoTripulanteService : ITipoTripulanteService
    {


        private readonly IUnitOfWork _unitOfWork;
        private readonly ITipoTripulanteRepository _repo;

        public TipoTripulanteService(IUnitOfWork unitOfWork, ITipoTripulanteRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<TipoTripulanteDto> GetByCodAsync(string cod)
        {
            var tipoTripulante = await this._repo.GetByCodAsync(cod);

            if (tipoTripulante == null)
                return null;

            return new TipoTripulanteDto
            {
                tipoTripulanteId = tipoTripulante.Id,
                codigo = tipoTripulante.codigo.cod_tipo_tripulante,
                descricao = tipoTripulante.descricao.descricaoTipoTripulante,
               
            };
        }

        public async Task<TipoTripulanteDto> GetByDescricaoAsync(string desc)
        {
            var tipoTripulante = await this._repo.GetByDescricaoAsync(desc);

            if (tipoTripulante == null)
                return null;

            return new TipoTripulanteDto
            {
                tipoTripulanteId = tipoTripulante.Id,
                codigo = tipoTripulante.codigo.cod_tipo_tripulante,
                descricao = tipoTripulante.descricao.descricaoTipoTripulante,
               
            };
        }

        public async Task<List<TipoTripulanteDto>> GetAllAsync()
        {
            var list_tipos = await this._repo.GetAllAsync();

            await this._unitOfWork.CommitAsync();

           

            List<TipoTripulanteDto> listDto = list_tipos.ConvertAll<TipoTripulanteDto>(tipo => new TipoTripulanteDto
            {
                tipoTripulanteId = tipo.Id,
                codigo = tipo.codigo.cod_tipo_tripulante,
                descricao = tipo.descricao.descricaoTipoTripulante,
            });

            return listDto;
        }

        public async Task<TipoTripulanteDto> AddAsync(TipoTripulanteDto dto)
        {
            var tipo = new TipoTripulante(dto);

            await this._repo.AddAsync(tipo);

            await this._unitOfWork.CommitAsync();

            return new TipoTripulanteDto
            {
                tipoTripulanteId = tipo.Id,
                codigo = tipo.codigo.cod_tipo_tripulante,
                descricao = tipo.descricao.descricaoTipoTripulante,
            };

        }

    }



}


