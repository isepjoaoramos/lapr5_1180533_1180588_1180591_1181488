using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;

namespace MDV_API.Services
{


    public class ServicoTripulanteService : IServicoTripulanteService
    {


        private readonly IUnitOfWork _unitOfWork;
        private readonly IServicoTripulanteRepository servicoTripulante_repo;
        private readonly IBlocoRepository bloco_repo;
        private readonly IBlocoServicoTripulanteRepository bloco_servicoTripulante_repo;





        public ServicoTripulanteService(IUnitOfWork unitOfWork, IServicoTripulanteRepository servicoTripulanteRepo, IBlocoRepository bloco_repo, IBlocoServicoTripulanteRepository _bloco_servicoTripulante_repo)
        {
            this._unitOfWork = unitOfWork;
            this.servicoTripulante_repo = servicoTripulanteRepo;
            this.bloco_repo = bloco_repo;
            this.bloco_servicoTripulante_repo = _bloco_servicoTripulante_repo;
        }

        public async Task<ServicoTripulanteDto> AddAsync(ServicoTripulanteDto servicoTripulanteDto)
        {
            try
            {
                var servicoTripulante = new ServicoTripulante(servicoTripulanteDto);

                await this.servicoTripulante_repo.AddAsync(servicoTripulante);

                await this._unitOfWork.CommitAsync();

                BlocoDTO[] listBlocos = servicoTripulanteDto.blocos;


                // Para cada Bloco 
                for (int i = 0; i < listBlocos.Length; i++)
                {
                    var bloco_ServicoTripulante = new Bloco_ServicoTripulante(servicoTripulante.Id, listBlocos[i].blocoId);

                    await this.bloco_servicoTripulante_repo.AddAsync(bloco_ServicoTripulante);

                    await this._unitOfWork.CommitAsync();
                }

                return new ServicoTripulanteDto
                {
                    codServicotripulante = servicoTripulante.codServicoTripulante.cod_servico_tripulante,

                };

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                return null;
            }
        }


        public async Task<ServicoTripulanteDto> GetByCodServicoTripulante(string codServicoTripulante)
        {
            var servicoTripulante = await this.servicoTripulante_repo.GetByCodServicoTripulanteAsync(codServicoTripulante);

            if (servicoTripulante == null)
            {
                return null;
            }

            return new ServicoTripulanteDto
            {
                codServicotripulante = servicoTripulante.codServicoTripulante.cod_servico_tripulante,

            };

        }


        public async Task<List<ServicoTripulanteDto>> GetAllAsync()
        {

            var list_servicosTripulante = await this.servicoTripulante_repo.GetAllAsync();

            await this._unitOfWork.CommitAsync();

            List<ServicoTripulanteDto> listdto = new List<ServicoTripulanteDto>();


            foreach (ServicoTripulante servico_tripulante in list_servicosTripulante)
            {
                var list_bloco_servicoTripulante = await this.bloco_servicoTripulante_repo.GetByServicoId(servico_tripulante.Id);

                await this._unitOfWork.CommitAsync();

                List<BlocoDTO> blocos = new List<BlocoDTO>();

                foreach (Bloco_ServicoTripulante bloco_ServicoTripulante in list_bloco_servicoTripulante)
                {
                    var bloco = await this.bloco_repo.GetByIdAsync(bloco_ServicoTripulante.BlocoId);
                    blocos.Add(new BlocoDTO
                    {
                        blocoId = bloco.Id,
                        //faltam codigos de viagem (?)
                    });
                }

                var serv_tripulante_dto = new ServicoTripulanteDto
                {
                    idServicoTripulante = servico_tripulante.Id,
                    codServicotripulante = servico_tripulante.codServicoTripulante.cod_servico_tripulante,
                    blocos = blocos.ToArray(),
                };

                listdto.Add(serv_tripulante_dto);
            }
            return listdto;
        }


    }

}