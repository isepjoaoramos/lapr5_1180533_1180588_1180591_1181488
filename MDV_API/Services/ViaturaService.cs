using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;

namespace MDV_API.Services
{


    public class ViaturaService : IViaturaService
    {


        private readonly IUnitOfWork _unitOfWork;
        private readonly IViaturaRepository viatura_repo;

        public ViaturaService(IUnitOfWork unitOfWork, IViaturaRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this.viatura_repo = repo;
        }





        public async Task<ViaturaDto> GetByMatricula(string matricula)
        {

            var viatura = await this.viatura_repo.GetByMatriculaAsync(matricula);

            if (viatura == null)
            {
                return null;
            }

            return new ViaturaDto
            {
                matricula = viatura.matricula.matricula,
                vin = viatura.numero_identificacao_viatura.numero_identificacao_viatura,
                cod_tipo_viatura = viatura.cod_tipo_viatura.codigo_tipo_viatura,
                data_entrada_servico = viatura.data_entrada_servico.data_entrada
            };

        }

         public async Task<ViaturaDto> GetByVin(string vin)
        {

            var viatura = await this.viatura_repo.GetByVinAsync(vin);

            if (viatura == null)
            {
                return null;
            }

            return new ViaturaDto
            {
                matricula = viatura.matricula.matricula,
                vin = viatura.numero_identificacao_viatura.numero_identificacao_viatura,
                cod_tipo_viatura = viatura.cod_tipo_viatura.codigo_tipo_viatura,
                data_entrada_servico = viatura.data_entrada_servico.data_entrada
            };

        }



        public async Task<List<ViaturaDto>> GetAllAsync()
        {

            var list_viaturas = await this.viatura_repo.GetAllAsync();

            List<ViaturaDto> listdto = list_viaturas.ConvertAll<ViaturaDto>(viat => new ViaturaDto
            {
                matricula = viat.matricula.matricula,
                vin = viat.numero_identificacao_viatura.numero_identificacao_viatura,
                cod_tipo_viatura = viat.cod_tipo_viatura.codigo_tipo_viatura,
                data_entrada_servico = viat.data_entrada_servico.data_entrada
            });

            return listdto;

        }



        public async Task<ViaturaDto> AddAsync(ViaturaDto viatura_dto)
        {
            try
            {
                var viatura = new Viatura(viatura_dto);

                await this.viatura_repo.AddAsync(viatura);

                await this._unitOfWork.CommitAsync();

                return new ViaturaDto
                {
                    matricula = viatura.matricula.matricula,
                    vin = viatura.numero_identificacao_viatura.numero_identificacao_viatura,
                    cod_tipo_viatura = viatura.cod_tipo_viatura.codigo_tipo_viatura,
                    data_entrada_servico = viatura.data_entrada_servico.data_entrada
                };

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }

}