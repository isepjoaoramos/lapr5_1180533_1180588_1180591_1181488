using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface IViaturaService
    {
        public Task<ViaturaDto> GetByMatricula(string matricula);
         public Task<ViaturaDto> GetByVin(string vin);

        public Task<List<ViaturaDto>> GetAllAsync();

        public Task<ViaturaDto> AddAsync(ViaturaDto dto);        
    }
}