using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface IInformacaoBlocoService
    {
        public Task<List<InformacaoBlocoTrabalho>> GetAllAsync();
        public Task<InformacaoBlocoTrabalho> AddAsync(InformacaoBlocoTrabalho bloco);
        public Task<List<InformacaoBlocoTrabalho>> GetByServicoViatura(CodServicoViatura idServicoViatura);
    }
}