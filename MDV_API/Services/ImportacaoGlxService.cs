using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections;
using MDV_API.Domain.Shared;
using System.Xml;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;

namespace MDV_API.Services
{
    public class ImportacaoGlxService : IImportacaoGlxService
    {
        private readonly IViagemService viagemService;

        private readonly IServicoViaturaService servicoViaturaService;

        private readonly IServicoTripulanteService servicoTripulanteService;

        private readonly IInformacaoBlocoService informacaoBlocoService;

        private readonly IBlocoService blocoService;

        public ImportacaoGlxService(IViagemService viagemService, IServicoViaturaService servicoViaturaService , IServicoTripulanteService servicoTripulanteService, IBlocoService blocoService, IInformacaoBlocoService informacaoBlocoService)
        {
            this.servicoViaturaService = servicoViaturaService;
            this.viagemService = viagemService;
            this.servicoTripulanteService = servicoTripulanteService;
            this.blocoService = blocoService;
            this.informacaoBlocoService = informacaoBlocoService;
        }


        public async Task<List<ServicoTripulanteDto>> criaServicosTripulanteEBlocosTrabalho(XmlDocument doc) {
            DateTime inicio = DateTime.Now;

            XmlNodeList servicosTripulante = doc.GetElementsByTagName("DriverDuty");
            XmlNodeList blocos = doc.GetElementsByTagName("WorkBlock");
            XmlNodeList viagens = doc.GetElementsByTagName("Trip");
            XmlNodeList percursos = doc.GetElementsByTagName("Path");
            XmlNodeList nos = doc.GetElementsByTagName("Node");
            XmlNodeList linhas = doc.GetElementsByTagName("Line");
            XmlNodeList servicosViatura = doc.GetElementsByTagName("VehicleDuty");
            
            var nServicos = servicosTripulante.Count;
            var nServicosViatura = servicosViatura.Count;
            var nBlocos = blocos.Count;
            var nViagens = viagens.Count;
            var nLinhas = linhas.Count;

            List<ServicoTripulanteDto> listaReturn = new List<ServicoTripulanteDto>();

            // Percorre cada <DriverDuty>
            for (var i = 0; i < nServicos; i++) {

                var servicoTripulanteXml = servicosTripulante[i].OuterXml;
                var splitNome = servicoTripulanteXml.Split("Name=\"");
                var codServicoTripulante = splitNome[1].Split("\"")[0];

                var splitWorkblocks = splitNome[1].Split("key=\"");
                var nSplit = splitWorkblocks.Length;
                
                ArrayList blocosTrabalho = new ArrayList();

                // Percorre cada <DriverDuty><WorkBlock>
                for (var j = 1; j < nSplit; j++) {
                    // Guarda a key desse workblock
                    var blocoKey = splitWorkblocks[j].Split("\"")[0];
                    Console.WriteLine("Workblock key: "+blocoKey);

                    ArrayList viagensDesteBloco = new ArrayList();
                    // Descobre informacao sobre o workblock (viagens do mesmo)
                    for (var k = 0; k < nBlocos; k++) {
                        var blocoXml = blocos[k].OuterXml;
                        var blocoAtualKey = blocoXml.Split("key=\"")[1].Split("\"")[0];
                        if (blocoAtualKey.Equals(blocoKey)) {
                            var splitViagens = blocoXml.Split("Trip:");
                            var nSplitViagens = splitViagens.Length;

                            // Percorre cada <WorkBlock><Trips><ref>
                            for (var l = 1; l < nSplitViagens; l++) {

                                var viagemKey = "Trip:"+splitViagens[l].Split("\"")[0];
                                Console.WriteLine("Trip key: "+viagemKey);

                                // Descobre informacao sobre a viagem, para poder criar um ViagemOutDto
                                for (var m = 0; m < nViagens; m++) {

                                    var viagemXml = viagens[m].OuterXml;
                                    var viagemAtualKey = viagemXml.Split("key=\"")[1].Split("\"")[0];
                                    
                                    // Vai buscar o horario, nome da linha e descricao do percurso dessa <Trip>
                                    if (viagemAtualKey.Equals(viagemKey)) {
                                        var horario = Int32.Parse(viagemXml.Split("Time=\"")[1].Split("\"")[0]);

                                        var pathKey = viagemXml.Split("Path=\"")[1].Split("\"")[0];
                                        var percursoXml = findNodeByKey(percursos, pathKey);
                                        var splitNosPercurso = percursoXml.Split("Node=\"");
                                        var nNos = splitNosPercurso.Length;
                                        var percurso = "";
                                        for (var n = 1; n < nNos; n++) {
                                            var nodeKey = splitNosPercurso[n].Split("\"")[0];
                                            var noXml = findNodeByKey(nos, nodeKey);
                                            var abreviaturaNo = noXml.Split("ShortName=\"")[1].Split("\"")[0];

                                            if (n == 1) {
                                                percurso += abreviaturaNo;
                                            } else {
                                                percurso += ">"+abreviaturaNo;
                                            }
                                        }

                                        var nomeLinha = "";
                                        var lineSplit = viagemXml.Split("Line=\"");
                                        if (lineSplit.Length == 1) {
                                            var percursoKey = viagemXml.Split("Path=\"")[1].Split("\"")[0];
                                            for (var o = 0; o < nLinhas; o++) {
                                                var linhaXml = linhas[o].OuterXml;
                                                var percursosDaLinhaSplit = linhaXml.Split("Path=\"");
                                                var tamanhoSplitPercursos = percursosDaLinhaSplit.Length;
                                                for(var p = 1; p < tamanhoSplitPercursos; p++) {
                                                    var percursoAtualKey = percursosDaLinhaSplit[p].Split("\"")[0];
                                                    if (percursoKey.Equals(percursoAtualKey)) {
                                                        nomeLinha = linhaXml.Split("Name=\"")[1].Split("\"")[0];
                                                    }
                                                }
                                            }
                                        } else {
                                            var lineKey = lineSplit[1].Split("\"")[0]; 
                                            var linhaXml = findNodeByKey(linhas,lineKey);
                                            nomeLinha = linhaXml.Split("Name=\"")[1].Split("\"")[0];
                                        }

                                        if (!nomeLinha.Equals("")) {
                                            //Console.WriteLine("viagemService.GetViagemByLinhaHorarioAndPercurso("+nomeLinha+","+horario+","+percurso+")");
                                            var viagemOutDto = await viagemService.GetViagemByLinhaHorarioAndPercurso(nomeLinha,horario,percurso);
                                            //Console.WriteLine("Encontrada viagem! Id: "+viagemOutDto.viagemId+" | Linha: "+viagemOutDto.Linha +" | Percurso: "+viagemOutDto.Percurso+" | HoraSaida: "+viagemOutDto.HoraSaida);
                                            // Vamos precisar de criar blocos de trabalho. Um BlocoDTO e composto por 1) uma lista codigosViagem e
                                            // 2) o idServicoViatura. Portanto, vamos aproveitar para acrescentar o codigo da viagem criada a um
                                            // array list, para no fim de percorrermos todas as viagens deste bloco, podermos criar um BlocoDTO
                                            var viagemId = viagemOutDto.viagemId;
                                            
                                            // verifica se esta viagem ja esta atribuida a algum bloco
                                            List<InformacaoBlocoTrabalho> informacaoBlocosTrabalho = await informacaoBlocoService.GetAllAsync();
                                            var nInfo = informacaoBlocosTrabalho.Count;
                                            var isRepetido = false;
                                            for (var n = 0; n < nInfo; n++) {
                                                if (informacaoBlocosTrabalho[n].idViagem==viagemId) { isRepetido = true; }
                                            }
                                            if (!isRepetido) { viagensDesteBloco.Add(viagemId); }
                                        }
                                        break;
                                    } // Descobriu a linha, horario e percurso, que em conjunto conseguem identificar a viagem do bloco.
                                      // Atraves do metodo GetViagemByLinhaHorarioAndPercurso(l,h,p) descobriu viagem e adicionou-a ao arraylist viagensOutDto
                                } // Descobriu informacao sobre a viagem
                            } // Percorreu todas as viagens do workblock
                            break;
                        }
                    } // Descobriu informacao sobre o workblock (viagens) 

                    var nViagensBloco = viagensDesteBloco.Count;
                    int[] codigosViagem = new int[nViagensBloco];
                    //Console.WriteLine("codServicoTripulante: "+codServicoTripulante);
                    for (var k = 0; k < nViagensBloco; k++) {
                        codigosViagem[k] = (int) viagensDesteBloco[k];
                        //Console.WriteLine("codigosViagem["+k+"]: "+codigosViagem[k]);
                    }

                    var codServicoViatura = "";
                    for (var k = 0; k < nServicosViatura; k++) {
                        var servicoViaturaXml = servicosViatura[k].OuterXml;
                        var splitName = servicoViaturaXml.Split("Name=\"");
                        var codServicoViaturaAtual = splitName[1].Split("\"")[0];
                        var splitBlocos = splitName[1].Split("key=\"");
                        var nSplitBlocos = splitBlocos.Length;
                        for (var l = 1; l < nSplitBlocos; l++) {
                            var blocoAtualKey = splitBlocos[l].Split("\"")[0];
                            if (blocoKey.Equals(blocoAtualKey)) {
                                codServicoViatura = codServicoViaturaAtual;
                                break;
                            }
                        }
                    }

                    var servicoViaturaDto = await servicoViaturaService.GetByCodServicoViatura(codServicoViatura);
                    var idServicoViatura = servicoViaturaDto.idServicoViatura;

                    if(codigosViagem.Length>0) {
                        BlocoDTO blocoDto = new BlocoDTO{codigosViagem=codigosViagem, idServicoViatura=idServicoViatura};
                        //Console.WriteLine("codigosViagem size: "+blocoDto.codigosViagem.Length+" | idServicoViatura= "+blocoDto.idServicoViatura);
                        var blocoResponse = await blocoService.AddAsync(blocoDto);
                        blocosTrabalho.Add(blocoResponse);
                    }

                } // Percorreu todos os workblocks do servico de tripulante

                var nBlocosTrabalho = blocosTrabalho.Count;
                BlocoDTO[] blocosTrabalhoTripulante = new BlocoDTO[nBlocosTrabalho];
                for (var j = 0; j < nBlocosTrabalho; j++) {
                    blocosTrabalhoTripulante[j] = (BlocoDTO) blocosTrabalho[j];
                }

                // Cria dto ServicoTripulante
                ServicoTripulanteDto dto = new ServicoTripulanteDto{codServicotripulante = codServicoTripulante, blocos = blocosTrabalhoTripulante};
                var blocosServicoTripulante = dto.blocos;
                var nBlocosServicoTripulante = blocosServicoTripulante.Length;
                for (var j = 0; j < nBlocosServicoTripulante; j++) {
                    var blocoDto = blocosServicoTripulante[j];
                    var blocoViagens = blocoDto.codigosViagem;
                }
                var servicoTripulante = await servicoTripulanteService.AddAsync(dto);
                listaReturn.Add(dto);
            }
            DateTime fim = DateTime.Now;
            var diferenca = fim.Subtract(inicio).TotalSeconds;
            Console.WriteLine("Importacao de servicos de tripulante e blocos de trabalho terminada. Duracao: "+diferenca+" segundos");
            return listaReturn;
        }

        public async Task<List<ServicoViaturaDto>> criaServicosViatura(XmlDocument doc) { 
            DateTime inicio = DateTime.Now;
            XmlNodeList servicosViatura = doc.GetElementsByTagName("VehicleDuty");
            XmlNodeList blocos = doc.GetElementsByTagName("WorkBlock");
            XmlNodeList viagens = doc.GetElementsByTagName("Trip");
            XmlNodeList percursos = doc.GetElementsByTagName("Path");
            XmlNodeList linhas = doc.GetElementsByTagName("Line");
            XmlNodeList nos = doc.GetElementsByTagName("Node");

            var nServicos = servicosViatura.Count;
            var nBlocos = blocos.Count;
            var nViagens = viagens.Count;
            var nLinhas = linhas.Count;

            List<ServicoViaturaDto> listaReturn = new List<ServicoViaturaDto>();
            
            // Percorre cada <VehicleDuty>
            for (var i = 0; i < nServicos; i++) {

                var servicoViaturaXml = servicosViatura[i].OuterXml;
                var splitNome = servicoViaturaXml.Split("Name=\"");
                var codServicoViatura = splitNome[1].Split("\"")[0];

                var splitWorkblocks = splitNome[1].Split("key=\"");
                var nSplit = splitWorkblocks.Length;
                ArrayList viagensOutDto = new ArrayList();
                
                //ArrayList blocosTrabalho = new ArrayList();
                Console.WriteLine("A percorrer todos os <VehicleDuty>. Atual: "+codServicoViatura);

                // Percorre cada <VehicleDuty><WorkBlock>
                for (var j = 1; j < nSplit; j++) {
                    // Guarda a key desse workblock
                    var blocoKey = splitWorkblocks[j].Split("\"")[0];

                    //ArrayList viagensDesteBloco = new ArrayList();
                    // Descobre informacao sobre o workblock (viagens do mesmo)
                    for (var k = 0; k < nBlocos; k++) {

                        var blocoXml = blocos[k].OuterXml;
                        var blocoAtualKey = blocoXml.Split("key=\"")[1].Split("\"")[0];
                        if (blocoAtualKey.Equals(blocoKey)) {
                            var splitViagens = blocoXml.Split("Trip:");
                            var nSplitViagens = splitViagens.Length;

                            // Percorre cada <WorkBlock><Trips><ref>
                            for (var l = 1; l < nSplitViagens; l++) {

                                var viagemKey = "Trip:"+splitViagens[l].Split("\"")[0];
                                // Descobre informacao sobre a viagem, para poder criar um ViagemOutDto
                                for (var m = 0; m < nViagens; m++) {

                                    var viagemXml = viagens[m].OuterXml;
                                    var viagemAtualKey = viagemXml.Split("key=\"")[1].Split("\"")[0];
                                    
                                    // Vai buscar o horario, nome da linha e descricao do percurso dessa <Trip>
                                    if (viagemAtualKey.Equals(viagemKey)) {
                                        var horario = Int32.Parse(viagemXml.Split("Time=\"")[1].Split("\"")[0]);

                                        var pathKey = viagemXml.Split("Path=\"")[1].Split("\"")[0];
                                        var percursoXml = findNodeByKey(percursos, pathKey);
                                        var splitNosPercurso = percursoXml.Split("Node=\"");
                                        var nNos = splitNosPercurso.Length;
                                        var percurso = "";
                                        for (var n = 1; n < nNos; n++) {
                                            var nodeKey = splitNosPercurso[n].Split("\"")[0];
                                            var noXml = findNodeByKey(nos, nodeKey);
                                            var abreviaturaNo = noXml.Split("ShortName=\"")[1].Split("\"")[0];

                                            if (n == 1) {
                                                percurso += abreviaturaNo;
                                            } else {
                                                percurso += ">"+abreviaturaNo;
                                            }
                                        }

                                        var nomeLinha = "";
                                        var lineSplit = viagemXml.Split("Line=\""); 
                                        if (lineSplit.Length == 1) {
                                            var percursoKey = viagemXml.Split("Path=\"")[1].Split("\"")[0];
                                            for (var o = 0; o < nLinhas; o++) {
                                                var linhaXml = linhas[o].OuterXml;
                                                var percursosDaLinhaSplit = linhaXml.Split("Path=\"");
                                                var tamanhoSplitPercursos = percursosDaLinhaSplit.Length;
                                                for(var p = 1; p < tamanhoSplitPercursos; p++) {
                                                    var percursoAtualKey = percursosDaLinhaSplit[p].Split("\"")[0];
                                                    if (percursoKey.Equals(percursoAtualKey)) {
                                                        nomeLinha = linhaXml.Split("Name=\"")[1].Split("\"")[0];
                                                    }
                                                }
                                            }
                                        } else {
                                            var lineKey = lineSplit[1].Split("\"")[0]; 
                                            var linhaXml = findNodeByKey(linhas,lineKey);
                                            nomeLinha = linhaXml.Split("Name=\"")[1].Split("\"")[0];
                                        }

                                        if (!nomeLinha.Equals("")) {
                                            var viagem = await viagemService.GetViagemByLinhaHorarioAndPercurso(nomeLinha,horario,percurso);
                                            viagensOutDto.Add(viagem);  
                                        }

                                        break;
                                    } // Descobriu a linha, horario e percurso, que em conjunto conseguem identificar a viagem do bloco.
                                      // Atraves do metodo GetViagemByLinhaHorarioAndPercurso(l,h,p) descobriu viagem e adicionou-a ao arraylist viagensOutDto
                                } // Descobriu informacao sobre a viagem
                            } // Percorreu todas as viagens do workblock
                            break;
                        }
                    } // Descobriu informacao sobre o workblock (viagens) 
                } // Percorreu todos os workblocks do servico de viatura

                // Cria um ViagemOutDto[], copia o conteudo do ArrayList
                var tamArrayList = viagensOutDto.Count;
                var viagensOutDtoArray = new ViagemOutDto[tamArrayList];
                for (var j = 0; j < tamArrayList; j++) {
                    ViagemOutDto viagemOutDto = (ViagemOutDto) viagensOutDto[j];
                    viagensOutDtoArray[j] = viagemOutDto;
                }

                // Cria dto ServicoViatura
                ServicoViaturaDto dto = new ServicoViaturaDto{codServicoViatura = codServicoViatura, descricao = "descricao "+codServicoViatura, viagens = viagensOutDtoArray};
                listaReturn.Add(dto);
                var servicoViatura = await servicoViaturaService.AddAsync(dto);
            }
            DateTime fim = DateTime.Now;
            var diferenca = fim.Subtract(inicio).TotalSeconds;
            Console.WriteLine("Importacao de servicos de viatura terminada. Duracao: "+diferenca+" segundos");
            return listaReturn;
        }

        public async Task<List<ViagemDto>> criaViagens(XmlDocument doc) {
            DateTime inicio = DateTime.Now;
            XmlNodeList viagens = doc.GetElementsByTagName("Trip");
            XmlNodeList percursos = doc.GetElementsByTagName("Path");
            XmlNodeList nos = doc.GetElementsByTagName("Node");
            XmlNodeList linhas = doc.GetElementsByTagName("Line");

            List<ViagemDto> listaReturn = new List<ViagemDto>();
            // Percorre lista de viagens. Para criar um ViagemDTO precisamos de 1) percurso; 2) horaSaida; 3) nome da linha
            var nViagens = viagens.Count;
            for (int i=0; i < nViagens; i++) { 
                var viagemAtualXml = viagens[i].OuterXml;

                // 1) Cria a descricao do percurso desta viagem no seguinte formato: ABREV1>ABREV2>ABREV3>...>ABREVX
                var percursoDestaViagem = viagemAtualXml.Split("Path=\"")[1].Split("\"")[0];
                var percursoXml = findNodeByKey(percursos, percursoDestaViagem);
                var splitInicialNosDestePercurso = percursoXml.Split("Node=\"");
                var tamanhoSplitInicial = splitInicialNosDestePercurso.Length;
                
                String percurso = "";
                for (var j = 1; j < tamanhoSplitInicial; j++) {
                    var noKey = splitInicialNosDestePercurso[j].Split("\"")[0];
                    var noXml = findNodeByKey(nos, noKey);
                    var abreviatura = noXml.Split("ShortName=\"")[1].Split("\"")[0];

                    if (j == 1) {
                        percurso += abreviatura;
                    } else {
                        percurso += ">"+abreviatura;
                    }
                }

                // 2) Vai buscar a hora de saida desta viagem, ou seja a hora de passagem do primeiro no
                var horaSaida = Int32.Parse(viagemAtualXml.Split("Time=\"")[1].Split("\"")[0]);

                // 3) Vai buscar o nome da linha desta viagem
                var splitViagens = viagemAtualXml.Split("Line=\"");
                var nomeLinha = "";
                // No entanto, algumas destas viagens nao tem uma linha associada, mas sim apenas o path. Por isso a ideia sera
                // percorrer todas as linhas e verificar qual delas e composta pelo path associado a viagem.
                if (splitViagens.Length > 1) {
                    // Se o tamanho do array > 1, entao e porque a viagem tem uma linha associada
                    var linhaDestaViagem = splitViagens[1].Split("\"")[0];
                    var linhaXml = findNodeByKey(linhas, linhaDestaViagem);
                    nomeLinha = linhaXml.Split("Name=\"")[1].Split("\"")[0];
                } else {
                    var nLinhas = linhas.Count;
                    for (var k = 0; k < nLinhas; k++) {
                        var pathSplit = linhas[k].OuterXml.Split("Path=\"");
                        var tamanhoPathSplit = pathSplit.Length;
                        for (var l = 1; l < tamanhoPathSplit; l++) {
                            var percursoDestaLinha = pathSplit[l].Split("\"")[0];
                            if (percursoDestaLinha.Equals(percursoDestaViagem)) {
                                nomeLinha = linhas[k].OuterXml.Split("Name=\"")[1].Split("\"")[0];
                            }
                        }
                    }
                }

                if (!nomeLinha.Equals("")) {
                    ViagemDto dto = new ViagemDto {Linha = nomeLinha, HoraSaida = horaSaida, Percurso = percurso };
                    Console.WriteLine("Linha="+dto.Linha+" | HoraSaida="+horaSaida+" | Percurso="+percurso);
                    listaReturn.Add(dto);
                    await viagemService.AddAsync(dto);
                }
            }
            DateTime fim = DateTime.Now;
            var diferenca = fim.Subtract(inicio).TotalSeconds;
            Console.WriteLine("Importacao de viagens terminada. Duracao: "+diferenca+" segundos");
            return listaReturn;
        }

        public String findNodeByKey(XmlNodeList list, String key) {
            var nNodes = list.Count;
            for (var i = 0; i < nNodes; i++) {
                var nodeKey = list[i].OuterXml.Split("key=\"")[1].Split("\"")[0];
                if (nodeKey.Equals(key)) {
                    return list[i].OuterXml;
                }
            }
            return null;
        }

    }
}