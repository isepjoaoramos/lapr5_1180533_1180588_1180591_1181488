using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using System;
using MDV_API.Mappers;

namespace MDV_API.Services
{
    public class ViagemService: IViagemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IViagemRepository _repo;

        public ViagemService(IUnitOfWork unitOfWork, IViagemRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

     /* public async Task<ViagemDto> GetByIdAsync(ViagemId id)
        {
            var viagem = await this._repo.GetByIdAsync(id);
            
            if(viagem == null)
                return null;

            return new ViagemDto{Id = viagem.Id.AsGuid(), Linha = viagem.Linha, HoraSaida = viagem.HoraSaida, Percurso = viagem.Percurso};
        }*/

        public async Task<List<ViagemOutDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();
            
            List<ViagemOutDto> listDto = list.ConvertAll<ViagemOutDto>(viagem => ViagemMap.toOutDTO(viagem));

            return listDto;
        }

        public async Task<List<ViagemDto>> GetByLinha(string line)
        {

            var viagens = await this._repo.GetByLinhaAsync(line);

            if (viagens == null)
            {
                return null;
            }

           List<ViagemDto> listDto = viagens.ConvertAll<ViagemDto>(viagem => ViagemMap.toDTO(viagem));

            return listDto;
        }

        public async Task<ViagemOutDto> GetViagemByLinhaHorarioAndPercurso(string line, int horario, string percurso)
        {

            var viagem = await this._repo.GetViagemByLinhaHorarioAndPercurso(line, horario, percurso);

            if (viagem == null)
            {
                return null;
            }
            return ViagemMap.toOutDTO(viagem);
        }

        public async Task<ViagemDto> AddAsync(ViagemDto dto)
        {

            var viagens = await this.GetAllAsync();
           
            //dto.codViagem = viagens.Count+1;
            var verificaViagem = true;
            for (var k=0; k<viagens.Count; k++){
                if(viagens[k].HoraSaida == dto.HoraSaida && viagens[k].Linha == dto.Linha && viagens[k].Percurso == dto.Percurso){
                    verificaViagem=false;
                }
            }

            if(verificaViagem){
            var viagem = new Viagem(dto);

            await this._repo.AddAsync(viagem);

            await this._unitOfWork.CommitAsync();

           // return new ViagemDto {Linha = viagem.Linha.nome_linha, HoraSaida = viagem.HoraSaida.hora_viagem, Percurso = viagem.Percurso.descricao_percurso };
            return ViagemMap.toDTO(viagem);
            }
            else throw new System.Exception("Viagem repetida");
        }

    }
}