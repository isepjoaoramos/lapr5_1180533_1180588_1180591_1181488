using System.Threading.Tasks;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;

namespace MDV_API.Services
{
    public interface IBlocoService
    {
        //public Task<BlocoDTO> GetByIdAsync(BlocoId id);
        
        //public Task<List<BlocoDTO>> GetAllAsync();

        public Task<BlocoDTO> AddAsync(BlocoDTO dto);        
    }
}