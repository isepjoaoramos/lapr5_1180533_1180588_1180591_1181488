using MDV_API.Domain;
using MDV_API.Dto;
using System;

namespace MDV_API.Mappers{

public static class ViagemMap{

    public static ViagemDto toDTO(Viagem viagem){
        return new ViagemDto {Linha = viagem.Linha.nome_linha, HoraSaida = viagem.HoraSaida.hora_viagem, Percurso = viagem.Percurso.descricao_percurso };
    }

    public static ViagemOutDto toOutDTO(Viagem viagem){
        return new ViagemOutDto {viagemId = viagem.Id, Linha = viagem.Linha.nome_linha, HoraSaida = viagem.HoraSaida.hora_viagem, Percurso = viagem.Percurso.descricao_percurso };
    }
    /*public toPersistence(Viagem viagem){

        
    }*/

    }
}