using System;
using MDV_API.Domain.Shared;
using MDV_API.Dto;
using System.Collections.Generic;

namespace MDV_API.Domain
{
    public class BlocoTrabalho : IAggregateRoot
    {
        public int Id {get; set;}
        public ICollection<Bloco_ServicoTripulante> listaBlocosTrabalho{get; set;}
        public ICollection<InformacaoBlocoTrabalho> ListaInformacaoBloco{get; set;}

        private BlocoTrabalho()
        {
        }

        public BlocoTrabalho(BlocoDTO dto)
        {
            // try {
            //     this.idViagem = dto.idViagem;
            //     this.codServicoViatura = new CodServicoViatura(dto.idServicoViatura);
            //     this.numMecanograficoTripulante = new NumMecanografico(dto.idServicoTripulante);
            // }catch (ArgumentException e){
            //     Console.WriteLine(e);
            //     throw new ArgumentException("Não foi possível criar o Bloco de trabalho");
            // }
        }
    }
}