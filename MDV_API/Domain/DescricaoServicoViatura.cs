using System;

namespace MDV_API.Domain
{

    public class DescricaoServicoViatura
    {

        public const string DescricaoDefault = "Serviço de Viatura Default";

        public String descricao { get; set; }

        public DescricaoServicoViatura(string param_descricao)
        {
            // Se for vazia dá valor default
            if(param_descricao.Length == 0){
                this.descricao = DescricaoDefault;
            }else{
                this.descricao = param_descricao;
            }
        }


        public DescricaoServicoViatura() { }

    }

}
