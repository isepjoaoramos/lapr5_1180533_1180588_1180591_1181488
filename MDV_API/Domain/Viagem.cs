using System;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Dto;

namespace MDV_API.Domain
{
    public class Viagem
    {
        public int Id { get; set; }

        public NomeLinha Linha { get; private set; }
        public HoraViagem HoraSaida { get; private set; }
        public DescricaoPercurso Percurso { get; private set; }
        // public int[] HorariosNos {get; private set; }
        // public bool Active{ get;  private set; }


        public ICollection<Viagem_ServicoViatura> ListaViagem_ServicoViatura { get; set; }
        public ICollection<InformacaoBlocoTrabalho> ListaInformacaoBloco{get; set;}


        private Viagem()
        {

        }

        public Viagem(int codViagem, NomeLinha linha, HoraViagem horaSaida, DescricaoPercurso percurso)
        {
            //this.Id = new ViagemId(Guid.NewGuid());

            this.Linha = linha;
            this.HoraSaida = horaSaida;
            this.Percurso = percurso;
            // this.HorariosNos = horariosNos;
        }

        public Viagem(ViagemDto viagem_dto)
        {

            // Tenta construir value objects a partir da informação do DTO --> apanha exceções (dos value objects) na criação.
            try
            {

                NomeLinha linha = new NomeLinha(viagem_dto.Linha);
                HoraViagem horaSaida = new HoraViagem(viagem_dto.HoraSaida);
                DescricaoPercurso percurso = new DescricaoPercurso(viagem_dto.Percurso);
                // int[] horariosNos = viagem_dto.HorariosNos;

                this.Linha = linha;
                this.HoraSaida = horaSaida;
                this.Percurso = percurso;
                //this.HorariosNos = horariosNos;

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                throw new ArgumentException("Não foi possível criar Viagem");
            }
        }
    }
}