using System;

namespace MDV_API.Domain
{

    public class DataEntradaEmpresa
    {

        // NIF = Numero de Identificação Fiscal

        public DateTime dataEntradaEmpresa { get; private set; }

        public DataEntradaEmpresa(DateTime dataEntradaEmpresa)
        {

            bool isDataValid_result = isDataValid(dataEntradaEmpresa);

            if (isDataValid_result)
            {
                this.dataEntradaEmpresa = dataEntradaEmpresa;
            }
            else
            {
                throw new ArgumentException("Data Inválida!");
            }

        }

        //Numero tem de ter 8 algarismos
        private static bool isDataValid(DateTime dataEntradaEmpresa)
        {
            // caso a data atual seja anterior à data de entrada na empresa
            if (DateTime.Compare(dataEntradaEmpresa, DateTime.Now) > 0)
            {
                return false;
            }

            return true;
        }

        public DataEntradaEmpresa()
        {

        }


    }
}