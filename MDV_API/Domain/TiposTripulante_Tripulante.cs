using MDV_API.Domain.Shared;
using MDV_API.Dto;
using System;

namespace MDV_API.Domain
{

    public class TiposTripulante_Tripulante
    {

        public int Id { get; set; }

        public int TripulanteId { get; set; }

        public Tripulante tripulante { get; set; }

        public int TipoTripulanteId { get; set; }

        public TipoTripulante tipoTripulante { get; set; }


        public TiposTripulante_Tripulante(int tripulanteId, int tipoId)
        {
            this.TripulanteId = tripulanteId;
            this.TipoTripulanteId = tipoId; 
        }


        // É necessário para o EF
        public TiposTripulante_Tripulante()
        {

        }
    }

}
