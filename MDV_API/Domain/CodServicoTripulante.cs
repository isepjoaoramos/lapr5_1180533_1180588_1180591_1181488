using System;

namespace MDV_API.Domain
{

    public class CodServicoTripulante
    {

        public string cod_servico_tripulante { get; set; }

        public CodServicoTripulante(string param_cod_servico_tripulante)
        {
            if (isCodigoValid(param_cod_servico_tripulante))
            {
                this.cod_servico_tripulante = param_cod_servico_tripulante;
            }
            else
            {
                throw new ArgumentException("Código de Serviço de Tripulante inválido! (10 caracteres)");
            }

        }

        //codigo é constituido por 10 caracteres
        public bool isCodigoValid(string cod_serv_tripulante)
        {
            //if (cod_serv_tripulante.Length != 10)
            if (cod_serv_tripulante.Length < 2)
            {
                return false;
            }
            return true;
        }


        public CodServicoTripulante() { }

    }

}
