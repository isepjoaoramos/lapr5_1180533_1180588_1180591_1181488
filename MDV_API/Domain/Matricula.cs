
using System.Text.RegularExpressions;
using System;

namespace MDV_API.Domain{

    public class Matricula{

        public String matricula {get; set;}

        public Matricula(string param_matricula){
            
            bool is_valid_matricula = isValidMatricula(param_matricula);

            if(is_valid_matricula){
                this.matricula = param_matricula;
            }else{
                throw new ArgumentException("Matrícula não é válida");
            }

        }


        public Matricula(){}


        // Verifica se a matrícula é do formato válido
        private static bool isValidMatricula (string matricula){

            if(string.IsNullOrWhiteSpace(matricula)){
                return false;
            }

            try{
                Regex.Match("[0-9]{2}-[0-9]{2}-[A-Z]{2})|([0-9]{2}-[A-Z]{2}-[0-9]{2})|([A-Z]{2}-[0-9]{2}-[0-9]{2}",matricula);
            }catch (ArgumentException){
                return false;
            }

            return true;
        }




    } 



















}





