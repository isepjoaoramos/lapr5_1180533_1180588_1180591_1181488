using MDV_API.Domain.Shared;
using MDV_API.Dto;
using System;

namespace MDV_API.Domain
{

    public class InformacaoBlocoTrabalho
    {

        public int Id { get; set; }

        public int idServicoViatura { get; set; }

        public ServicoViatura servicoViatura { get; set; }

        public int idBloco { get; set; }

        public BlocoTrabalho bloco { get; set; }

        public int idViagem { get; set; }

        public Viagem viagem { get; set; }


        public InformacaoBlocoTrabalho(Int32 idServicoViatura, Int32 idBloco, Int32 idViagem)
        {
            this.idServicoViatura = idServicoViatura;
            this.idBloco = idBloco;
            this.idViagem = idViagem;
        }


        // É necessário para o EF
        public InformacaoBlocoTrabalho()
        {

        }
    }

}
