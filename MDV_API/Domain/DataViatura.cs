using System;

namespace MDV_API.Domain{

    public class DataViatura{

        public DateTime data_entrada {get; set;}

        public DataViatura(DateTime param_data_entrada){
            
            // Compara duas datas do tipo DateTime
            // retorna >0 se a data atual é antes da data passada por parâmetro.
            int is_valid_date = DateTime.Compare(param_data_entrada, DateTime.Now);

            if(is_valid_date > 0){
                throw new ArgumentException("Data de Entrada de Servico inválida!");
            }else{
                this.data_entrada = param_data_entrada;
            } 

        }

        public DataViatura(){}

    }
} 
