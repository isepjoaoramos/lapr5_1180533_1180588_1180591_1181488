using System;

namespace MDV_API.Domain
{

    public class NifTripulante
    {

// NIF = Numero de Identificação Fiscal

        public long nif { get; private set; }

        public NifTripulante(long nif)
        {

            bool isNifValido_result = isNumeroValid(nif);

            if (isNifValido_result)
            {
                this.nif = nif;
            }
            else
            {
                throw new ArgumentException("Numero de Identificação Fiscal é constituido por 9 algarismos!");
            }

        }

        //Numero tem de ter 8 algarismos
        private static bool isNumeroValid(long nif)
        {

            if (nif.ToString().Length != 9)
            {
                return false;
            }

            return true;
        }

        public NifTripulante(){
            
        }


    }
}