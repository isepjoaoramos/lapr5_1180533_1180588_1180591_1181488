using System;


namespace MDV_API.Domain{

    public class DescricaoTipoTripulante{

        public string descricaoTipoTripulante {get; set;}

        public DescricaoTipoTripulante(string descricaoTipoTripulante){

            if(descricaoTipoTripulante.Length < 1 ){
                throw new ArgumentException("Código Tipo Viatura inválido");
            }else{
                this.descricaoTipoTripulante = descricaoTipoTripulante;
            }

        }

        public DescricaoTipoTripulante(){
            
        }

    } 
}