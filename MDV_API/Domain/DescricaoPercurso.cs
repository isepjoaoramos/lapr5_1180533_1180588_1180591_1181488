
using System;


namespace MDV_API.Domain{

    public class DescricaoPercurso{

        public string descricao_percurso {get; set;}

        public DescricaoPercurso(string descricao_percurso){

            if(descricao_percurso.Length == 0){
                throw new ArgumentException("Percurso inválido");
            }else{
                this.descricao_percurso = descricao_percurso;
            }

        }


        public DescricaoPercurso(){}
    } 
}