using System;

namespace MDV_API.Domain
{

    public class DataSaidaEmpresa
    {

        // NIF = Numero de Identificação Fiscal

        public DateTime dataSaidaEmpresa { get; private set; }

        public DataSaidaEmpresa(DateTime dataSaidaEmpresa)
        {

            bool isDataValid_result = isDataValid(dataSaidaEmpresa);

            if (isDataValid_result)
            {
                this.dataSaidaEmpresa = dataSaidaEmpresa;
            }
            else
            {
                throw new ArgumentException("Data Inválida!");
            }

        }

        //Numero tem de ter 8 algarismos
        private static bool isDataValid(DateTime dataSaidaEmpresa)
        {
            // caso a data atual seja anterior à data de entrada na empresa
            if (DateTime.Compare(dataSaidaEmpresa, DateTime.Now) > 0)
            {
                return true;
            }

            return false;
        }

        public DataSaidaEmpresa() { }

    }
}