using System;

namespace MDV_API.Domain
{

    public class CodServicoViatura
    {

        public String cod_servico_viatura { get; set; }

        public CodServicoViatura(string param_cod_servico_viatura)
        {
            if (param_cod_servico_viatura.Length == 0)
            {
                throw new ArgumentException("É necessário Código de Serviço de Viatura !!");
            }else{
                this.cod_servico_viatura = param_cod_servico_viatura;
            }

        }


        public CodServicoViatura() { }

    }

}
