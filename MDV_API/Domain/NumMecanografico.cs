using System.Text.RegularExpressions;
using System;

namespace MDV_API.Domain
{

    public class NumMecanografico
    {

        public string numMecanografico { get; set; }

        public NumMecanografico(string numero)
        {

            bool isnumeroValid_result = isNumeroValid(numero);

            if (isnumeroValid_result)
            {
                this.numMecanografico = numero;
            }
            else
            {
                throw new ArgumentException("Número Mecanográfico é constituido por 9 caracteres!");
            }

        }

        //alfanumerico com 9 caracteres
        private static bool isNumeroValid(string num)
        {

            if (num.Length != 9)
            {
                return false;
            }

            return true;
        }

        public NumMecanografico() { }


    }


}