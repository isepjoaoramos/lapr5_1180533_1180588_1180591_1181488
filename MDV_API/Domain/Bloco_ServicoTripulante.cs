using MDV_API.Domain;
using MDV_API.Dto;
using System;

namespace MDV_API.Domain
{

    public class Bloco_ServicoTripulante
    {

        public int Id { get; set; }

        public int ServicoTripulanteId { get; set; }

        public ServicoTripulante servicoTripulante { get; set; }

        public int BlocoId { get; set; }

        public BlocoTrabalho bloco { get; set; }


        public Bloco_ServicoTripulante(int servico_tripulante_id, int bloco_id)
        {
            this.ServicoTripulanteId = servico_tripulante_id;
            this.BlocoId = bloco_id;
        }

         // É necessário para o EF
        public Bloco_ServicoTripulante()
        {

        }
        
    }
}
