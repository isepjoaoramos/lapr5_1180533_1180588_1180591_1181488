using System;

namespace MDV_API.Domain
{

    public class NumCartaConducao
    {


        public string numCartaConducao { get; private set; }

        public NumCartaConducao(string numCartaConducao)
        {

            bool isNumeroValido_result = isNumeroValid(numCartaConducao);

            if (isNumeroValido_result)
            {
                this.numCartaConducao = numCartaConducao;
            }
            else
            {
                throw new ArgumentException("Numero da Carta de Condução não é válido");
            }

        }


        private static bool isNumeroValid(string numCartaConducao)
        {

            if (numCartaConducao.Length > 0)
            {
                return true;
            }

            return false;
        }

        public NumCartaConducao(){
            
        }


    }
}