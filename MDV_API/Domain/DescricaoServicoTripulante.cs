using System;

namespace MDV_API.Domain
{

    public class DescricaoServicoTripulante
    {

        public const string DescricaoDefault = "Serviço de Tripulante Default";

        public string descricao { get; set; }

        public DescricaoServicoTripulante(string param_descricao)
        {
            // Se for vazia dá valor default
            if(param_descricao.Length == 0){
                this.descricao = DescricaoDefault;
            }else{
                this.descricao = param_descricao;
            }
        }


        public DescricaoServicoTripulante() { }

    }

}
