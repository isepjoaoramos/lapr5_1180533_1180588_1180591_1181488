using System;

namespace MDV_API.Domain
{

    public class NumCartaoCidadao
    {


        public long numCartaoCidadao { get; private set; }

        public NumCartaoCidadao(long numCartaoCidadao)
        {

            bool isNumeroValido_result = isNumeroValid(numCartaoCidadao);

            if (isNumeroValido_result)
            {
                this.numCartaoCidadao = numCartaoCidadao;
            }
            else
            {
                throw new ArgumentException("Numero do Cartão de Cidadão é constituido por 7 ou 8 algarismos!");
            }

        }

        //Numero tem de ter 8 algarismos
        private static bool isNumeroValid(long numCartaoCidadao)
        {

            if (numCartaoCidadao.ToString().Length == 8 || numCartaoCidadao.ToString().Length == 7)
            {
                return true;
            }

            return false;
        }

        public NumCartaoCidadao(){}

    }
}