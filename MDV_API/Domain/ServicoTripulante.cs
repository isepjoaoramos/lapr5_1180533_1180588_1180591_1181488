using MDV_API.Domain.Shared;
using MDV_API.Dto;
using MDV_API.Domain;
using System.Collections.Generic;
using System;

namespace MDV_API.Domain
{

    public class ServicoTripulante : IAggregateRoot
    {

        public int Id { get; set; }

        public CodServicoTripulante codServicoTripulante { get; set; }

        public ICollection<Bloco_ServicoTripulante> listaBlocosTrabalho { get; set; }

        public ServicoTripulante(ServicoTripulanteDto servicotripulanteDto)
        {
            try
            {

                CodServicoTripulante codServicoTripulante = new CodServicoTripulante(servicotripulanteDto.codServicotripulante);

                this.codServicoTripulante = codServicoTripulante;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                throw new ArgumentException("Não foi possível criar o Serviço de Tripulante");
            }
        }


        // É necessário para o EF
        public ServicoTripulante() { }
    }

}
