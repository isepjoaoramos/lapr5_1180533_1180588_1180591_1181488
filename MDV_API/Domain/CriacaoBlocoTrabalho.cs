using System;
using MDV_API.Domain.Shared;
using MDV_API.Dto;
using System.Collections.Generic;

namespace MDV_API.Domain
{
    public class CriacaoBlocosTrabalho : IAggregateRoot
    {
        public int Id {get; set;}

        public string idServicoViatura {get; set;}

        public int duracaoMaxima {get; set;}

        public int nMaximoBlocos {get; set;}

        private CriacaoBlocosTrabalho()
        {
        }

        public CriacaoBlocosTrabalho(string idServicoViatura, int duracaoMaxima, int nMaximoBlocos)
        {
            this.idServicoViatura = idServicoViatura;
            this.duracaoMaxima = duracaoMaxima;
            this.nMaximoBlocos = nMaximoBlocos;
            // try {
            //     this.idViagem = dto.idViagem;
            //     this.codServicoViatura = new CodServicoViatura(dto.idServicoViatura);
            //     this.numMecanograficoTripulante = new NumMecanografico(dto.idServicoTripulante);
            // }catch (ArgumentException e){
            //     Console.WriteLine(e);
            //     throw new ArgumentException("Não foi possível criar o Bloco de trabalho");
            // }
        }
    }
}