using System;


namespace MDV_API.Domain
{

    public class NomeTripulante
    {

        public string nome { get; set; }

        public NomeTripulante(string nome)
        {

            if (nome.Length == 0)
            {
                throw new ArgumentException("Nome de Tripulante inválido");
            }
            else
            {
                this.nome = nome;
            }

        }

        public NomeTripulante(){
            
        }

    }
}