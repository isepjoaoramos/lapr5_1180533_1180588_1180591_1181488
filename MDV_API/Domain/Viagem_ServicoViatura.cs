using MDV_API.Domain.Shared;
using MDV_API.Dto;
using System;

namespace MDV_API.Domain
{

    public class Viagem_ServicoViatura
    {

        public int Id { get; set; }

        public int ServicoViaturaId { get; set; }

        public ServicoViatura servicoViatura { get; set; }

        public int ViagemId { get; set; }

        public Viagem viagem { get; set; }


        public Viagem_ServicoViatura(int servico_viatura_id, int viagem_id)
        {
            this.ServicoViaturaId = servico_viatura_id;
            this.ViagemId = viagem_id;
        }


        // É necessário para o EF
        public Viagem_ServicoViatura()
        {

        }
    }

}
