using MDV_API.Domain.Shared;
using MDV_API.Dto;
using System;
using System.Collections.Generic;

namespace MDV_API.Domain{

    public class ServicoViatura: IAggregateRoot{

        public int Id{get; set;}

        public CodServicoViatura cod_servico_viatura {get; set;}

        public  DescricaoServicoViatura descricao{get; set;}
        
        public ICollection<Viagem_ServicoViatura> ListaViagem_ServicoViatura{get; set;}
        public ICollection<InformacaoBlocoTrabalho> ListaInformacaoBloco{get; set;}


        public  ServicoViatura(ServicoViaturaDto servico_viatura_dto){
            
            // Tenta construir value objects a partir da informação do DTO --> apanha exceções (dos value objects) na criação.
            try{
                CodServicoViatura new_cod_servico_viatura = new CodServicoViatura(servico_viatura_dto.codServicoViatura);
                DescricaoServicoViatura new_descricao = new DescricaoServicoViatura(servico_viatura_dto.descricao);
                this.cod_servico_viatura = new_cod_servico_viatura;
                this.descricao = new_descricao;
            }catch (ArgumentException e){
                Console.WriteLine(e);
                throw new ArgumentException("Não foi possível criar o Serviço de Viatura");
            }
        }


        // É necessário para o EF
        public ServicoViatura(){}
    }

}
