using MDV_API.Domain.Shared;
using MDV_API.Dto;
using System;

namespace MDV_API.Domain{

    public class Viatura: IAggregateRoot{

        public int Id{get; set;}

        public Matricula matricula {get; set;}

        public  Vin numero_identificacao_viatura{get; set;}

        public  CodTipoViatura cod_tipo_viatura{get; set;}

        public DataViatura data_entrada_servico{get; set;}

        public  Viatura(ViaturaDto viatura_dto){
            
            // Tenta construir value objects a partir da informação do DTO --> apanha exceções (dos value objects) na criação.
            try{
                Matricula matricula = new Matricula(viatura_dto.matricula);
                Vin numero_identificacao_viatura = new Vin(viatura_dto.vin);
                CodTipoViatura codTipoViatura = new CodTipoViatura(viatura_dto.cod_tipo_viatura);
                DataViatura data_entrada = new DataViatura(viatura_dto.data_entrada_servico);

                this.matricula = matricula;
                this.numero_identificacao_viatura = numero_identificacao_viatura;
                this.cod_tipo_viatura = codTipoViatura;
                this.data_entrada_servico = data_entrada; 
            }catch (ArgumentException e){
                Console.WriteLine(e);
                throw new ArgumentException("Não foi possível criar Viatura");
            }
        }


        // É necessário para o EF
        public Viatura(){}
    }

}
