
using System;


namespace MDV_API.Domain{

    public class CodTipoViatura{

        public string codigo_tipo_viatura {get; set;}

        public CodTipoViatura(string param_cod_tipo_viatura){

            if(param_cod_tipo_viatura.Length != 20){
                throw new ArgumentException("Código Tipo Viatura inválido");
            }else{
                this.codigo_tipo_viatura = param_cod_tipo_viatura;
            }

        }


        public CodTipoViatura(){}
    } 
}