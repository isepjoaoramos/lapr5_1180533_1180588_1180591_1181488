using System;

namespace MDV_API.Domain
{

    public class HoraViagem
    {


        public int hora_viagem { get; private set; }

        public HoraViagem(int hora_viagem)
        {

            bool isNumeroValido_result = isNumeroValid(hora_viagem);

            if (isNumeroValido_result)
            {
                this.hora_viagem = hora_viagem;
            }
            else
            {
                throw new ArgumentException("Horário inválido");
            }

        }

        //Numero tem de ser >= 0
        private static bool isNumeroValid(int hora_viagem)
        {

            if (hora_viagem >= 0)
            {
                return true;
            }

            return false;
        }


    }
}