using MDV_API.Dto;
using System;
using System.Collections.Generic;


namespace MDV_API.Domain
{

    public class TipoTripulante
    {

        public int Id { get; set; }

        public CodTipoTripulante codigo { get; set; }

        public DescricaoTipoTripulante descricao { get; set; }

        public ICollection<TiposTripulante_Tripulante> tiposTripulante { get; set; }


        // É necessário para o EF
        public TipoTripulante()
        {

        }


        public TipoTripulante(TipoTripulanteDto tipoTripulanteDto)
        {
            try
            {
                CodTipoTripulante codTipoTripulante = new CodTipoTripulante(tipoTripulanteDto.codigo);
                DescricaoTipoTripulante descricaoTipoTripulante = new DescricaoTipoTripulante(tipoTripulanteDto.descricao);
                

                this.codigo = codTipoTripulante;
                this.descricao = descricaoTipoTripulante;


                //this.Active = true;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                throw new ArgumentException("Não foi possível criar Tripulante");
            }
        }

    }

}
