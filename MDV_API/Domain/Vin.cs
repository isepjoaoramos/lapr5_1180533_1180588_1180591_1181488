
using System.Text.RegularExpressions;
using System;

namespace MDV_API.Domain{

    public class Vin{

        public string numero_identificacao_viatura {get; set;}

        public Vin(string param_numero_identificacao_viatura){
            
            bool is_valid_vin = isValidVin(param_numero_identificacao_viatura);

            if(is_valid_vin){
               this.numero_identificacao_viatura = param_numero_identificacao_viatura;
            }else{
                throw new ArgumentException("Vin não válido!");
            }
        }

        public Vin(){}


        // Verifica se o Vin está no formato certo
        private static bool isValidVin (string numero_identificacao_viatura){

            if(string.IsNullOrWhiteSpace(numero_identificacao_viatura)){
                return false;
            }

            try{
                Regex.Match("[A-HJ-NPR-Z0-9]{13}[0-9]{4}",numero_identificacao_viatura);
            }catch (ArgumentException){
                return false;
            }

            return true;
        }

    } 
}