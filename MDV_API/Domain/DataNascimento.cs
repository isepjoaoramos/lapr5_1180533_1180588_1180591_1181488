using System;

namespace MDV_API.Domain
{

    public class DataNascimento
    {


        public DateTime dataNascimento { get; private set; }

        public DataNascimento(DateTime dataNascimento)
        {

            bool isdataValid_result = isDataValid(dataNascimento);

            if (isdataValid_result)
            {
                this.dataNascimento = dataNascimento;
            }
            else
            {
                throw new ArgumentException("Tripulante tem de ter idade igual ou superior a 21 anos!");
            }

        }

        //tripulante tem de ter idade >= 21 
        private static bool isDataValid(DateTime dataNascimento)
        {

            DateTime now = DateTime.Today;
        
            int age = now.Year - dataNascimento.Year;
            if (dataNascimento > now.AddYears(-age)) age--;
            if (age >= 21)
            {
                return true;
            }
            return false;

        }

        public DataNascimento() { }


    }
}