using System;

namespace MDV_API.Domain
{

    public class CodTipoTripulante
    {

        public string cod_tipo_tripulante { get; set; }

        public CodTipoTripulante(string param_cod_tipo)
        {
            if (param_cod_tipo.Length == 0)
            {
                throw new ArgumentException("É necessário um Código de Tipo de Tripulante!");
            }
            else if (param_cod_tipo.Length > 20)
            {
                throw new ArgumentException("Código de tripulante contém no máximo 20 caracteres!");
            }
            else
            {
                this.cod_tipo_tripulante = param_cod_tipo;
            }

        }


        public CodTipoTripulante() { }

    }

}
