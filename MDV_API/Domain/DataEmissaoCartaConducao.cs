using System;

namespace MDV_API.Domain
{

    public class DataEmissaoCartaConducao
    {

        // NIF = Numero de Identificação Fiscal

        public DateTime dataEmissaoCartaConducao { get; private set; }

        public DataEmissaoCartaConducao(DateTime dataEmissaoCartaConducao)
        {

            bool isDataValid_result = isDataValid(dataEmissaoCartaConducao);

            if (isDataValid_result)
            {
                this.dataEmissaoCartaConducao = dataEmissaoCartaConducao;
            }
            else
            {
                throw new ArgumentException("Data de emissão Inválida!");
            }

        }

       
        private static bool isDataValid(DateTime dataEmissaoCartaConducao)
        {
            // caso a data de emissão seja superior à data atual
            if (DateTime.Compare(dataEmissaoCartaConducao, DateTime.Now) > 0)
            {
                return false;
            }

            return true;
        }

        public DataEmissaoCartaConducao(){
            
        }


    }
}