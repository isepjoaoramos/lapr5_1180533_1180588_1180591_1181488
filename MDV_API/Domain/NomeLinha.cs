
using System;


namespace MDV_API.Domain{

    public class NomeLinha{

        public string nome_linha {get; set;}

        public NomeLinha(string nome_linha){

            if(nome_linha.Length == 0){
                throw new ArgumentException("Linha inválida");
            }else{
                this.nome_linha = nome_linha;
            }

        }


        public NomeLinha(){}
    } 
}