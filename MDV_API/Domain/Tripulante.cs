using System;
using System.Collections.Generic;
using MDV_API.Domain.Shared;
using MDV_API.Dto;


namespace MDV_API.Domain
{
    public class Tripulante
    {
        public int Id { get; set; }
        public NumMecanografico numMecanografico { get; set; }
        public NomeTripulante nome { get; set; }
        public DataNascimento dataNascimento { get; set; }

        public NumCartaoCidadao numCartaoCidadao { get; set; }
        public NifTripulante nif { get; set; }

        public NumCartaConducao numCartaConducao { get; set; }

        public DataEmissaoCartaConducao dataEmissaoCartaConducao { get; set; }

        public DataEntradaEmpresa dataEntradaEmpresa { get; set; }

        public DataSaidaEmpresa dataSaidaEmpresa { get; set; }

        public ICollection<TiposTripulante_Tripulante> tiposTripulante { get; set; }
        // public bool Active { get; private set; }

        private Tripulante()
        {
            //this.Active = true;
        }

        public Tripulante(TripulanteDto tripulanteDto)
        {
            try
            {
                NumMecanografico numMecanografico = new NumMecanografico(tripulanteDto.numMecanografico);
                NomeTripulante nomeTripulante = new NomeTripulante(tripulanteDto.nome);
                DataNascimento dataNascimento = new DataNascimento(tripulanteDto.dataNascimento);
                NumCartaoCidadao numCartaoCidadao = new NumCartaoCidadao(tripulanteDto.numCartaoCidadao);
                NifTripulante nif = new NifTripulante(tripulanteDto.nif);
                NumCartaConducao numCartaConducao = new NumCartaConducao(tripulanteDto.numCartaConducao);
                DataEmissaoCartaConducao dataEmissaoCartaConducao = new DataEmissaoCartaConducao(tripulanteDto.dataEmissaoCartaConducao);
                DataEntradaEmpresa dataEntradaEmpresa = new DataEntradaEmpresa(tripulanteDto.dataEntradaEmpresa);
                DataSaidaEmpresa dataSaidaEmpresa = new DataSaidaEmpresa(tripulanteDto.dataSaidaEmpresa);


                this.numMecanografico = numMecanografico;
                this.nome = nomeTripulante;
                this.dataNascimento = dataNascimento;
                this.numCartaoCidadao = numCartaoCidadao;
                this.nif = nif;
                this.numCartaConducao = numCartaConducao;
                this.dataEmissaoCartaConducao = dataEmissaoCartaConducao;
                this.dataEntradaEmpresa = dataEntradaEmpresa;
                this.dataSaidaEmpresa = dataSaidaEmpresa;



                //this.Active = true;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                throw new ArgumentException("Não foi possível criar Tripulante");
            }
        }


        /* public void MarkAsInative()
        {
            //this.Active = false;
        } */

    }
}