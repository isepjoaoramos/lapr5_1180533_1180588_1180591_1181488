using System.Threading.Tasks;

namespace MDV_API.Domain.Shared
{
    public interface IUnitOfWork
    {
        Task<int> CommitAsync();
    }
}