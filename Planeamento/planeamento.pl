% Bibliotecas
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_server)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_open)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)).
:- use_module(library(uri)).
:- use_module(library(http/http_cors)).
:- use_module(library(http/http_json)).
:- use_module(library(http/http_session)).
:- use_module(library(http/json_convert)).

% Base conhecimento dinâmica
:- dynamic bloco_trabalho/4.
:- dynamic servico_viatura/2.
:- dynamic viagem/4.
:- dynamic liga/3.
:- dynamic melhor_sol_ntrocas/2.
:- dynamic edge/4.
:- dynamic no/6.
:- dynamic linhaAtlas/2.
:- dynamic percurso/4.
:- dynamic linha/5.
:- dynamic doadorCodigoLinha/1.
:- dynamic rangevd/3.
:- dynamic t/4.
:- dynamic afetacaoBlocoMotorista/3.
:- dynamic lista_motoristas_nblocos/2.
:- dynamic tripulanteLivre/4.
:- dynamic blocoViagemServico/3.

% Base de Conhecimentos
% no(nome_paragem, abrev_paragem, flag_ponto_rendição, flag_estação_recolha,longitude, latitude)
% linha(nome_linha, número_linha, lista_abrev_paragens, tempo_minutos, distância_metros)
% viagem(id_viagem, número_linha, id_horario)
% horario(número_linha, lista_segundos).
horario(1, [36000,36540,36840,37140,37620]).
horario(1, [37800,38340,38640,38940,39420]).
horario(1, [39600,40140,40440,40740,41220]).
horario(1, [41400,41940,42240,42540,43020]).
horario(3, [73800,74280,74580,74880,75420]).
horario(3, [72900,73380,73680,73980,75420]).
horario(3, [72000,72480,72780,73080,73620]).
horario(3, [71100,71580,71880,72180,72720]).
horario(5, [30360,30960,31200,31440,31920]).
horario(5, [33960,34560,34800,35040,35520]).
horario(5, [37560,38160,38400,38640,39120]).
horario(5, [41160,41760,42000,42240,42720]).
horario(8, [28860,29340,29580,29820,30360]).
horario(8, [32400,32880,33120,33360,33960]).
horario(8, [36000,36480,36720,36960,37560]).
horario(8, [39600,40080,40320,40560,41160]).
horario(9, [30180,30480,30720,30960,31320,31560]).
horario(9, [31380,31680,31920,32160,32520,32760]).
horario(9, [32580,32880,33120,33360,33720,33960]).
horario(9, [33780,34080,34320,34560,34920,35160]).
horario(11, [36000,36240,36600,36840,37080,37380]).
horario(11, [37200,37440,37800,38040,38280,38580]).
horario(11, [38400,38640,39000,39240,39480,39780]).
horario(11, [39600,39840,40200,40440,40680,40980]).
horario(24, [71580,71880,72120,72360,72600,72900]).
horario(24, [70260,70560,70800,71040,71280,71580]).
horario(24, [68940,69240,69480,69720,69960,70260]).
horario(24, [67620,67920,68160,68400,68640,68940]).
horario(26, [33600,33900,34140,34380,34620,34920]).
horario(26, [32280,32580,32820,33060,33300,33600]).
horario(26, [30960,31260,31500,31740,31980,32280]).
horario(26, [29640,29940,30180,30420,30660,30960]).
horario(22, [28980,29280,29520,29760,30120,30360]).
horario(22, [30180,30480,30720,30960,31320,31560]).
horario(22, [31380,31680,31920,32160,32520,32760]).
horario(22, [32580,32880,33120,33360,33720,33960]).
horario(20, [34800,35040,35400,35640,35880,36180]).
horario(20, [33600,33840,34200,34440,34680,34980]).
horario(20, [32400,32640,33000,33240,33480,33780]).
horario(20, [31200,31440,31800,32040,32280,32580]).
horario(34, [26580,26700]).
horario(34, [27900,28020]).
horario(34, [29220,29340]).
horario(35, [73200,73320]).
horario(35, [74520,74640]).
horario(35, [75840,75960]).
horario(36, [27300,27600]).
horario(36, [28500,28800]).
horario(36, [29700,30000]).
horario(37, [77160,77460]).
horario(37, [74760,75060]).
horario(37, [75960,76260]).
horario(38, [27300,27420]).
horario(38, [28200,28320]).
horario(38, [28740,28860]).
horario(38, [29100,29220]).
horario(39, [76320,76440]).
horario(39, [89820,89940]).
horario(39, [82320,82440]).
horario(39, [75900,76020]).

% Cors
:- set_setting(http:cors, [*]).

% Predicado a ser executado quando e feito um pedido no url /mudanca_motoristas
:- http_handler(root(algoritmo_genetico), algoritmo_genetico_http, []).
:- http_handler(root(mudanca_motoristas), plan_mud_mot_http, []).

% Inicializacao do servidor
server(Port) :- http_server(http_dispatch, [port(Port)]).

% Adequacao do AG ao problema, factos necessarios:
	% horario(id_percurso, id_viagem, lista_segundos).
	% bloco_trabalho(id_bloco, lista_viagens, tempo_inicio, tempo_fim).
	% servico_viatura(id_servico, lista_blocos_trabalho).
	% lista_motoristas_nblocos(id_servico,lista_duplos_idmotorista_nblocos).
	% motorista(id_motorista). <- na verdade e servicoTripulante(id)
	% viagem(id_viagem).

viagem(459,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(31,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(63,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(33,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(65,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(33,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(67,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(69,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(37,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(71,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(39,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(41,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(73,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(43,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(75,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(45,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(77,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(48,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(82,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(52,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(86,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(56,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(432,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').
viagem(460,'PARED>CETE>PARAD>RECAR>AGUIA',30000,'Paredes_Aguiar').

bloco_trabalho(12,[459],33620,37620).
bloco_trabalho(211,[31,63],37620,41220).
bloco_trabalho(212,[33,65],41220,44820).
bloco_trabalho(213,[35,67],44820,48420).
bloco_trabalho(214,[37,69],48420,52020).
bloco_trabalho(215,[39,71],52020,55620).
bloco_trabalho(216,[41,73],55620,59220).
bloco_trabalho(217,[43,75],59220,62820).
bloco_trabalho(218,[45,77],62820,66420).
bloco_trabalho(219,[48,82],66420,70020).
bloco_trabalho(220,[52,86],70020,73620).
bloco_trabalho(221,[56,432],73620,77220).
bloco_trabalho(222,[460],77220,79340).

%servico_viatura(12,[12,211,212,213,214,215,216,217,218,219,220,221,222]).
%servico_viatura(13,[12,211,212,222,214,215,216,217,218,219,220,221,213]).

%lista_motoristas_nblocos(12,[(276,2),(5188,3),(16690,2),(18107,6)]).

horariomotorista(276,25200,61200,28800, [21600,7200]).
horariomotorista(527, 25200, 61200, 28800, [21600,7200]).
horariomotorista(889, 25200, 61200, 28800, [21600,7200]).
horariomotorista(1055, 25200, 61200, 28800, [14400,14400]).
horariomotorista(1461, 25200, 61200, 28800, [14400,14400]).
horariomotorista(1640, 25200, 61200, 28800, [21600,7200]).
horariomotorista(2049, 25200, 61200, 28800, [21600,7200]).
horariomotorista(5188,33300,69300, 28800, [7200,21600]).
horariomotorista(6616,33300, 69300, 28800, [14400,14400]).
horariomotorista(6697,33300, 69300, 28800, [21600,7200]).
horariomotorista(11018,41400,77400, 28800, [21600,7200]).
horariomotorista(11692,41400, 77400, 28800, [21600,7200]).
horariomotorista(14893,45000,81000, 28800, [10800,18000]).
horariomotorista(16458,50400,86400, 28800, [14400,14400]).
horariomotorista(16690, 50400, 86400, 28800, [7200,21600]).
horariomotorista(16763, 50400, 86400, 28800, [14400,14400]).
horariomotorista(17015, 50400, 86400, 28800, [10800,18000]).
horariomotorista(17552,54000,90000,28800, [10800,18000]).
horariomotorista(17623,25200, 61200, 28800, [21600,7200]).
horariomotorista(17630,25200, 61200, 28800, [21600,7200]).
horariomotorista(17639,27000,48600,21600,[21600]).
horariomotorista(17673,25200, 61200, 28800, [14400,7200,7200]).
horariomotorista(18009,50400, 86400, 28800, [7200,21600]).
horariomotorista(18050,54000,90000,28800, [21600,7200]).
horariomotorista(18097,57600,79200,21600,[21600]).
horariomotorista(18105,57600,79200,21600,[21600]).
horariomotorista(18107,57600,79200,21600,[21600]).
horariomotorista(18119,59400,81000,21600,[21600]).


% nblocos(NBlocos).
nblocos(13).

interpretaInformacaoBlocoTrabalho():-
	retractall(blocoViagemServico(_,_,_)),
	http_get("http://localhost:5000/api/bloco",Json1,[request_header('Accept'='application/json')]),
	json_to_prolog(Json1,InfoBlocosHttp),
	assertaInformacaoBlocoTrabalho(InfoBlocosHttp).

assertaInformacaoBlocoTrabalho([]).
assertaInformacaoBlocoTrabalho([H|RestoBlocos]):-
	arg(1,H,Arg1),
	arg(2,Arg1,Arg2),
	arg(1,Arg2,Arg3),
	arg(2,Arg3,IdServicoViatura),

	arg(2,Arg2,Arg4),
	arg(2,Arg4,Arg5),
	arg(1,Arg5,Arg6),
	arg(2,Arg6,IdBloco),

	arg(2,Arg5,Arg7),
	arg(2,Arg7,Arg8),
	arg(1,Arg8,Arg9),
	arg(2,Arg9,IdViagem),

	assert(blocoViagemServico(IdBloco,IdViagem,IdServicoViatura)),
	assertaInformacaoBlocoTrabalho(RestoBlocos).

criaServicosViatura():-
	findall(servico_viatura(IdServico,BlocosNaoRepetidos),(
		blocoViagemServico(_,_,IdServico),

		findall(IdBloco,(
			blocoViagemServico(IdBloco,_,IdServico)
		),Blocos),

		elimina_repetidos(Blocos,_,BlocosNaoRepetidos),

		retractall(blocoViagemServico(_,_,IdServico))
	),ServicosViatura),
	elimina_repetidos(ServicosViatura,_,ServicosViaturaNaoRepetidos),
	criaServicosViatura(ServicosViaturaNaoRepetidos).

criaServicosViatura([]):-!.
criaServicosViatura([servico_viatura(IdServico,Blocos)|ServicosViatura]):-
	length(Blocos,B),
	((B>0),!,(
		assert(servico_viatura(IdServico,Blocos)),
		criaServicosViatura(ServicosViatura) 
	);(
		criaServicosViatura(ServicosViatura)
	)).

ordenaViagens(Viagens,ViagensOrdenadas):-
	avaliaViagens(Viagens,ViagensAv),!,
	ordena_populacao(ViagensAv,ViagensAvOrdenadas),
	remove_horario(ViagensAvOrdenadas,ViagensOrdenadas).

avaliaViagens([]):-!.
avaliaViagens([V|Viagens],[V*HoraSaida|ViagensAv]):-
	viagem(V,_,HoraSaida,_).

criaBlocos():-
	findall(_,(
		blocoViagemServico(IdBloco,_,_),

		findall(IdViagem,(
			blocoViagemServico(IdBloco,IdViagem,_)
		),Viagens),

		%retractall(blocoViagemServico(IdBloco,_,_)),
		ordenaViagens(Viagens,ViagensOrdenadas),
		criaBlocosTrabalho(IdBloco,ViagensOrdenadas)	
	),_).

criaBlocosTrabalho(IdBloco,Viagens):-
	horaFimBloco(Viagens,0,HFim),!,
	horaInicioBloco(Viagens,99999999,HInicio),!,
	assert(bloco_trabalho(IdBloco,Viagens,HInicio,HFim)).

horaFimBloco([],HFim,HFim):-!.
horaFimBloco([V|Viagens],MaiorHFimAteAgora,HFim):-
	viagem(V,_,HoraSaida,Linha),
	linha(Linha,_,_,Duracao,_),
	HoraFimViagem is HoraSaida+Duracao,
	((HoraFimViagem>MaiorHFimAteAgora),!,(
		horaFimBloco(Viagens,HoraFimViagem,HFim)
	);(
		horaFimBloco(Viagens,MaiorHFimAteAgora,HFim)
	)).

horaInicioBloco([],HInicio,HInicio):-!.
horaInicioBloco([V|Viagens],MenorHInicioAteAgora,HInicio):-
	viagem(V,_,HoraSaida,_),
	((HoraSaida<MenorHInicioAteAgora),!,(
		horaInicioBloco(Viagens,HoraSaida,HInicio)
	);(
		horaInicioBloco(Viagens,MenorHInicioAteAgora,HInicio)
	)).


consomeViagens():-
	retractall(maior_idViagem_atm(_)),
	assert(maior_idViagem_atm(500)),
	http_get("http://localhost:5000/api/viagem",Json1,[request_header('Accept'='application/json')]),
    json_to_prolog(Json1, ViagensHttp),
	assertaViagens(ViagensHttp).

assertaViagens([]).
assertaViagens([H|ViagensHttp]):-
	arg(1,H,Arg1),
	
	arg(1,Arg1,Arg2),
	arg(2,Arg2,CodViagem),	

	arg(2,Arg1,Arg3),

	arg(1,Arg3,Arg7),
	arg(2,Arg7,NomeLinha),

	arg(2,Arg3,Arg4),

	arg(1,Arg4,Arg8),
	arg(2,Arg8,HoraSaida),

	arg(2,Arg4,Arg5),
	arg(1,Arg5,Arg6),
	arg(2,Arg6,Percurso),

	assert(viagem(CodViagem,Percurso,HoraSaida,NomeLinha)),
	assertaViagens(ViagensHttp).

:- dynamic populacao/1.
:- dynamic geracoes/1.
:- dynamic prob_cruzamento/1.
:- dynamic prob_mutacao/1.
:- dynamic tempo_limite/1.
:- dynamic menor_avaliacao/1.
:- dynamic estabilizacao/1.
:- dynamic inicio_almoco/1.
:- dynamic fim_almoco/1.
:- dynamic inicio_jantar/1.
:- dynamic fim_jantar/1.
:- dynamic inicio_turnos/1.
:- dynamic fim_turnos/1.
:- dynamic distribuicaoBlocosTrabalhoPorMotoristas/2.
:- dynamic servico_tripulante/2.

apagaBaseConhecimento():-
    retractall(no(_,_,_,_,_,_)),
    retractall(linha(_,_,_,_,_)),
    retractall(edge(_,_,_,_)),
    retractall(melhor_sol_ntrocas(_,_)),
	retractall(viagem(_,_,_,_)),
	retractall(bloco_trabalho(_,_,_,_)),
	retractall(blocoViagemServico(_,_,_)),
	retractall(servico_viatura(_,_)),
	retractall(lista_motoristas_nblocos(_,_)),
	retractall(liga(_,_,_)),
	retractall(melhor_sol_ntrocas(_,_)),
	retractall(linhaAtlas(_,_)),
	retractall(percurso(_,_,_,_)),
	retractall(doadorCodigoLinha(_,_,_,_)),
	retractall(t(_,_,_,_)),
	retractall(afetacaoBlocoMotorista(_,_,_)),
	retractall(servico_tripulante(_,_)),
	retractall(tripulanteLivre(_,_,_,_)),
	retractall(blocoViagemServico(_,_,_)),
	retractall(distribuicaoBlocosTrabalhoPorMotoristas(_,_)).

% parameterizacao
parameterizacao() :-
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Numero de novas Geracoes: '),read(NG), 			
	(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Probabilidade de Cruzamento (0-100 %):'), read(P1),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (0-100 %):'), read(P2),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	write('Tempo Limite: '),read(TempoLimite),
	(retract(tempo_limite(_));true), asserta(tempo_limite(TempoLimite)),
	write('Menor avaliacao a ser procurada: '),read(MenorAvaliacao),
	(retract(menor_avaliacao(_));true), asserta(menor_avaliacao(MenorAvaliacao)),
	write('Numero de estabilizacao: '),read(NumeroEstabilizacao),
	(retract(estabilizacao(_));true), asserta(estabilizacao(NumeroEstabilizacao)),
	write('Hora de almoco (inicio): '),read(InicioAlmoco),
	(retract(inicio_almoco(_));true), asserta(inicio_almoco(InicioAlmoco)),
	write('Hora de almoco (fim): '),read(FimAlmoco),
	(retract(fim_almoco(_));true), asserta(fim_almoco(FimAlmoco)),
	write('Hora de jantar (inicio): '),read(InicioJantar),
	(retract(inicio_jantar(_));true), asserta(inicio_jantar(InicioJantar)),
	write('Hora de jantar (fim): '),read(FimJantar),
	(retract(fim_jantar(_));true), asserta(fim_jantar(FimJantar)),
	write('Hora de inicio dos turnos: '),read(HInicioTurnos),
	(retract(inicio_turnos(_));true), asserta(inicio_turnos(HInicioTurnos)),
	write('Hora de fim dos turnos: '),read(HFimTurnos),
	(retract(fim_turnos(_));true), asserta(fim_turnos(HFimTurnos)).

somatorioBlocosTrabalho(ListaDuplos,Somatorio):-
	somatorioBlocosTrabalho1(ListaDuplos,0,Somatorio).

somatorioBlocosTrabalho1([],Somatorio,Somatorio):-!.

somatorioBlocosTrabalho1([(_,NBlocos)|ListaDuplos],Somatorio,Resultado):-
	SomatorioAtualizado is Somatorio+NBlocos,
	somatorioBlocosTrabalho1(ListaDuplos,SomatorioAtualizado,Resultado).



consomeDados():-
	adicionaNos(),
	write('Nos adicionados.'),nl,
	adicionaLinhas(),
	write('Linhas adicionadas.'),nl,
	consomeViagens(),
	write('Viagens adicionadas.'),nl,
	interpretaInformacaoBlocoTrabalho(),
	criaBlocos(),
	write('Blocos adicionados.'),nl,
	criaServicosViatura(),
	write('Servicos viatura adicionados.'),nl,nl.

novo_gera():-
	%parameterizacao(),
	assertsTemporarios(),
	consomeDados(),
	afetaMotoristas(),

	nl,write('Algoritmo genetico...'),nl,
	findall(_,(
		lista_motoristas_nblocos(IdServicoViatura,InfoMotoristasERespetivosNBlocos),
	
		verificaCompatibilidadeEntreWorkblocksDoServicoEInformacaoDisponibilizada(IdServicoViatura,InfoMotoristasERespetivosNBlocos,Compativel),
		((Compativel \== 1),!,(
			%write('(Falha) Id:'),write(IdServicoViatura),write(' O numero de blocos do servico nao e compativel com o numero de blocos do facto lista_motoristas_nblocos/2 com o qual fez match.'),nl,
			abort()
		);(
			%write('(Sucesso) Id:'),write(IdServicoViatura),write(' Numero de blocos de trabalho do servico = Soma de blocos em lista_motoristas_nblocos/2'),nl,
			gera(IdServicoViatura)
		))
	),_),

	findall(servico_tripulante(Motorista,Blocos),(
		distribuicaoBlocosTrabalhoPorMotoristas(_,Motorista),
		
		findall(Bloco,(
			distribuicaoBlocosTrabalhoPorMotoristas(Bloco,Motorista)
		),Blocos),
		
		retractall(distribuicaoBlocosTrabalhoPorMotoristas(_,Motorista))
	),ServicosTripulante),
	
	criaServicosTripulante(ServicosTripulante,[],ServicosTripulanteLimpinho),
	
	nl,write('Detecao/Correcao...'),nl,
	verificaConstraints(ServicosTripulanteLimpinho,1),
	criaServicosParaCorrecao(),
	verificaConstraints(ServicosTripulanteLimpinho,0),
	criaServicosParaCorrecao().

enviaServicosTripulanteParaMDV():-
	findall(_,(
		servico_tripulante(CodServico,Blocos),
		http_post([
			protocol(http),
			host(localhost),
			port(5000),
			path('/api/servicotripulante')
		],form([
			codServico = CodServico,
			blocos = Blocos]
		),Reply,[])
	),_).

criaServicosParaCorrecao() :-
	findall(servico_tripulante(Motorista,Blocos),(
		distribuicaoBlocosTrabalhoPorMotoristas(_,Motorista),
		
		findall(Bloco,(
			distribuicaoBlocosTrabalhoPorMotoristas(Bloco,Motorista)
		),Blocos),
		
		retractall(distribuicaoBlocosTrabalhoPorMotoristas(_,Motorista))
	),ServicosTripulante),

	criaServicos(ServicosTripulante).

criaServicos([]):-!.
criaServicos([servico_tripulante(Motorista,Blocos)|ServicosTripulante]):-
	% write('Servicos a criar: '),write([servico_tripulante(Motorista,Blocos)|ServicosTripulante]),nl,
	findall(Encontrado,(
		servico_tripulante(Motorista,BlocosST),
		Encontrado is 1,
		retract(servico_tripulante(Motorista,BlocosST)),
		append(Blocos,BlocosST,NovoBlocos),
		assert(servico_tripulante(Motorista,NovoBlocos))
	),Encontrados),!,

	length(Encontrados,N),
	((N == 0),!,(
		length(Blocos,B),
		((B>0),!,(
			assert(servico_tripulante(Motorista,Blocos))
		))
	);(
		write('')
	)),
	criaServicos(ServicosTripulante).


criaServicosTripulante([],Auxiliar,ServicosTripulanteLimpinho):-reverse(Auxiliar,ServicosTripulanteLimpinho),!.
criaServicosTripulante([servico_tripulante(Motorista,Blocos)|ServicosTripulante],Auxiliar,ServicosTripulanteLimpinho):-
	length(Blocos,B),	
	((B>0),!,(
		write('servico_tripulante('),write(Motorista),write(','),write(Blocos),write(').'),nl,
		assert(servico_tripulante(Motorista,Blocos)),
		criaServicosTripulante(ServicosTripulante,[servico_tripulante(Motorista,Blocos)|Auxiliar],ServicosTripulanteLimpinho)
	);(
		criaServicosTripulante(ServicosTripulante,Auxiliar,ServicosTripulanteLimpinho)
	)).

verificaConstraints([],_):-!.
verificaConstraints([servico_tripulante(IdTripulante,Blocos)|ServicosTripulante],IsPrimeiraVez):-		
	write('@servico_tripulante('),write(IdTripulante),write(','),write(Blocos),write(')'),nl,

	% Restricao 1) Ultrapassar 4 horas consecutivas
	detetaRestricao1(Blocos,Blocos1,IdTripulante,IsPrimeiraVez),!,
	%write('Restricao #1'),nl,

	% Restricao 2) Ultrapassar 8 horas totais
	detetaRestricao2(Blocos1,Blocos2,IdTripulante,IsPrimeiraVez),!,
	% write('Restricao #2'),nl,

	% Restricao 3) Descansa 1h apos 4 horas
	detetaRestricao3(Blocos2, Blocos3,IdTripulante,IsPrimeiraVez),!,
	% write('Restricao #3'),nl,

	% Restricao 4) Nao tem 1h para almocar ou jantar
	%detetaRestricao4(Blocos3, Blocos4,IdTripulante,IsPrimeiraVez),!,
	% write('Restricao #4'),nl,

	% Restricao 5) Horarios turnos
	detetaRestricao5(Blocos3,Blocos5,IdTripulante,IsPrimeiraVez),!,
	% write('Restricao #5'),nl,

	verificaConstraints(ServicosTripulante,IsPrimeiraVez).

detetaRestricao5(Blocos,NovosBlocos,IdTripulante,IsPrimeiraVez):-
	detetaRestricao5_1(Blocos,[],NovosBlocos,IdTripulante,IsPrimeiraVez).

detetaRestricao5_1([],Auxiliar,NovosBlocos,_,_):-reverse(Auxiliar,NovosBlocos),!.
detetaRestricao5_1([B|Blocos],Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez):-
	bloco_trabalho(B,_,HInicio,HFim),
	inicio_turnos(HInicioTurnos),
	fim_turnos(HFimTurnos),

	((HInicio<HInicioTurnos),!,(
		((IsPrimeiraVez1==1),!,(
			atribuiBlocoAOutroMotorista(B,IdTripulante,_),
			detetaRestricao5_1(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez1)
		);(
			atribuiBlocoAMotoristaLivre(B,ResultadoAtrib,IdTripulante),
			((ResultadoAtrib==1),!,(
				detetaRestricao5_1(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez1)
			);(
				write('-> Nao foi possivel corrigir a hard constraint #5 (horarios turnos) no servico tripulante '),write(IdTripulante),write('.'),nl,
				detetaRestricao5_1(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez1)
			))
		))
	);(
		((HFim>HFimTurnos),!,(
			((IsPrimeiraVez1==1),!,(
				atribuiBlocoAOutroMotorista(B,IdTripulante,_),
				detetaRestricao5_1(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez1)
			);(
				atribuiBlocoAMotoristaLivre(B,ResultadoAtrib,IdTripulante),
				((ResultadoAtrib==1),!,(
					detetaRestricao5_1(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez1)
				);(
					write('-> Nao foi possivel corrigir a hard constraint #5 (horarios turnos) no servico tripulante '),write(IdTripulante),write('.'),nl,
					detetaRestricao5_1(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez1)
				))
			))
		);(
			detetaRestricao5_1(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez1)
		))
	)).

detetaRestricao4([B|Blocos],NovosBlocos,IdTripulante,IsPrimeiraVez):-
	write('(1)'),nl,
	calculaTempoLivreAlmoco([B|Blocos],3600,TempoLivreAlmoco),
	write('(2) - verificaSeFazDeslocacoesDuranteAHoraDeAlmoco('),write([B|Blocos]),write(',0,'),write(DuracaoMinimaDeslocacoes),write(','),write(B),write(').'),nl,
	verificaSeFazDeslocacoesDuranteAHoraDeAlmoco([B|Blocos],0,DuracaoMinimaDeslocacoes,B),
	write('(3)'),nl,
	ActualFreeTime is TempoLivreAlmoco-DuracaoMinimaDeslocacoes,
	write('(4)'),nl,
	((ActualFreeTime>=3600),!,(
		write(''),nl
	);(
		trocaBlocoTrabalhoAlmoco([B|Blocos],[],SemiNovosBlocos,IdTripulante,IsPrimeiraVez,1)
	)),
	write('(5)'),nl,
	calculaTempoLivreJantar(Blocos,3600,TempoLivreJantar),
	write('(6)'),nl,
	verificaSeFazDeslocacoesDuranteAHoraDeJantar([B|Blocos],0,DuracaoMinimaDeslocacoesJ,B),
	write('(7)'),nl,
	ActualFreeTimeJ is TempoLivreJantar-DuracaoMinimaDeslocacoes,
	((ActualFreeTimeJ>3600),(
		write(''),nl
	);(
		trocaBlocoTrabalhoJantar(SemiNovosBlocos,[],NovosBlocos,IdTripulante,IsPrimeiraVez,1)
	)).

trocaBlocoTrabalhoAlmoco([],Auxiliar,NovosBlocos,_,_,_):-reverse(Auxiliar,NovosBlocos),!.
trocaBlocoTrabalhoAlmoco([B|Blocos],Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar):-
	bloco_trabalho(B,_,HInicioBloco,HFimBloco),
	intersetaHoraAlmoco(HInicioBloco,HFimBloco,Intersecao),
	((Intersecao>0),!,(
		((IsPrimeiraVez==1),!,(
			((SupostoTrocar==1),!,(
				atribuiBlocoAOutroMotorista(B,Resultado,IdTripulante),
				SupostoTrocar1 is 0,
				trocaBlocoTrabalhoAlmoco(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			);(
				trocaBlocoTrabalhoAlmoco(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			))
		);(
			((SupostoTrocar==1),!,(
				atribuiBlocoAMotoristaLivre(B,Resultado,IdTripulante),
				SupostoTrocar1 is 0,
				trocaBlocoTrabalhoAlmoco(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			);(
				trocaBlocoTrabalhoAlmoco(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			))
		))
	);(
		trocaBlocoTrabalhoAlmoco(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar)
	)).

trocaBlocoTrabalhoJantar([],Auxiliar,NovosBlocos,_,_,_):-reverse(Auxiliar,NovosBlocos),!.
trocaBlocoTrabalhoJantar([B|Blocos],Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar):-
	bloco_trabalho(B,_,HInicioBloco,HFimBloco),
	intersetaHoraJantar(HInicioBloco,HFimBloco,Intersecao),
	((Intersecao>0),!,(
		((IsPrimeiraVez==1),!,(
			((SupostoTrocar==1),!,(
				atribuiBlocoAOutroMotorista(B,Resultado,IdTripulante),
				SupostoTrocar1 is 0,
				trocaBlocoTrabalhoJantar(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			);(
				trocaBlocoTrabalhoJantar(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			))
		);(
			((SupostoTrocar==1),!,(
				atribuiBlocoAMotoristaLivre(B,Resultado,IdTripulante),
				SupostoTrocar1 is 0,
				trocaBlocoTrabalhoJantar(Blocos,Auxiliar,NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			);(
				trocaBlocoTrabalhoJantar(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar1)
			))
		))
	);(
		trocaBlocoTrabalhoJantar(Blocos,[B|Auxiliar],NovosBlocos,IdTripulante,IsPrimeiraVez,SupostoTrocar)
	)).

verificaSeFazDeslocacoesDuranteAHoraDeAlmoco([],TempoDeslocacao,TempoDeslocacao,_):-!.
verificaSeFazDeslocacoesDuranteAHoraDeAlmoco([B|Blocos],DeslocacoesAteAgora,DuracaoMinimaDeslocacoes,BlocoAnterior):-	
	write('@verificaDeslocacoes (1)'),nl,
	blocoTerminaDuranteHoraDeAlmoco(B,SimOuNao),
	write('@verificaDeslocacoes (2)'),nl,
	((SimOuNao==1),!,(
		write('@verificaDeslocacoes (3)'),nl,
		bloco_trabalho(B,_,_,HFimBloco),!,
		write('@verificaDeslocacoes (4)- descobreUltimoNoDoBlocoTrabalho('),write(B),write(',No1).'),nl,
		descobreUltimoNoDoBlocoTrabalho(B,No1),
		write('@verificaDeslocacoes (5)'),nl,
		descobrePrimeiroNoDoProximoBlocoTrabalho(Blocos,No2),
		write('Antes aStar('),write(No1),write(','),write(No2),write(','),write(HFimBloco),write(',_,'),write(TempoDeslocacao),nl,
		aStar(No1,No2,HFimBloco,_,TempoDeslocacao),
		write('Depois aStar! Tempo: '),write(TempoDeslocacao),nl,
		DeslocacoesAteAgora1 is DeslocacoesAteAgora+TempoDeslocacao,
		verificaSeFazDeslocacoesDuranteAHoraDeAlmoco(Blocos,DeslocacoesAteAgora1,DuracaoMinimaDeslocacoes,B)
	);(
		write('@verificaDeslocacoes (6)'),nl,
		blocoComecaDuranteHoraDeAlmoco(B,SimOuNao1),
		((SimOuNao1 == 1),!,(
			write('@verificaDeslocacoes (7)'),nl,
			descobreInfoBlocoAnterior(BlocoAnterior,No1,FimBlocoAnterior),
			descobrePrimeiroNoDoProximoBlocoTrabalho([B|Blocos],No2),
			write('Antes aStar('),write(No1),write(','),write(No2),write(','),write(FimBlocoAnterior),write('_,'),write(TempoDeslocacao),nl,
			aStar(No1,No2,FimBlocoAnterior,_,TempoDeslocacao),
			write('Depois aStar'),nl,
			DuracaoMinimaDeslocacoes1 is DeslocacoesAteAgora+TempoDeslocacao,
			verificaSeFazDeslocacoesDuranteAHoraDeAlmoco(Blocos,DeslocacoesAteAgora1,DuracaoMinimaDeslocacoes,B)
		);(
			write('@verificaDeslocacoes (8)'),nl,
			verificaSeFazDeslocacoesDuranteAHoraDeAlmoco(Blocos,DeslocacoesAteAgora,DuracaoMinimaDeslocacoes,B)
		))
	)).

verificaSeFazDeslocacoesDuranteAHoraDeJantar([],TempoDeslocacao,TempoDeslocacao,_):-!.
verificaSeFazDeslocacoesDuranteAHoraDeJantar([B|Blocos],DeslocacoesAteAgora,DuracaoMinimaDeslocacoes,BlocoAnterior):-	
	blocoTerminaDuranteHoraDeJantar(B,SimOuNao),
	((SimOuNao==1),!,(
		bloco_trabalho(B,_,_,HFimBloco),
		descobreUltimoNoDoBlocoTrabalho(B,No1),
		descobrePrimeiroNoDoProximoBlocoTrabalho(Blocos,No2),
		aStar(No1,No2,HFimBloco,_,TempoDeslocacao),
		DeslocacoesAteAgora1 is DeslocacoesAteAgora+TempoDeslocacao,
		verificaSeFazDeslocacoesDuranteAHoraDeJantar(Blocos,DeslocacoesAteAgora1,DuracaoMinimaDeslocacoes,B)
	);(
		blocoComecaDuranteHoraDeAlmoco(B,SimOuNao1),
		((SimOuNao1 == 1),!,(
			descobreInfoBlocoAnterior(BlocoAnterior,No1,FimBlocoAnterior),
			descobrePrimeiroNoDoProximoBlocoTrabalho([B|Blocos],No2),
			aStar(No1,No2,FimBlocoAnterior,_,TempoDeslocacao),
			DuracaoMinimaDeslocacoes1 is DeslocacoesAteAgora+TempoDeslocacao,
			verificaSeFazDeslocacoesDuranteAHoraDeJantar(Blocos,DeslocacoesAteAgora1,DuracaoMinimaDeslocacoes,B)
		);(
			verificaSeFazDeslocacoesDuranteAHoraDeJantar(Blocos,DeslocacoesAteAgora,DuracaoMinimaDeslocacoes,B)
		))
	)).


descobreInfoBlocoAnterior(Bloco,No,HorarioFim):-
	bloco_trabalho(Bloco,LViagens,_,HorarioFim),
	length(LViagens,L),
	nth1(L,LViagens,Viagem),
	viagem(Viagem,Percurso,_,_),
	split_string(Percurso,">","",Split),
	length(Split,S),
	nth1(S,Percurso,No).

blocoTerminaDuranteHoraDeJantar(Bloco,SimOuNao):-
	bloco_trabalho(Bloco,_,_,HFimBloco),
	inicio_jantar(IJantar),
	fim_jantar(FJantar),
	((HFimBloco>=IJantar),!,(
		((HFimBloco=<FJantar),!,(
			SimOuNao is 1
		);(
			SimOuNao is 0
		))
	);(
		SimOuNao is 0
	)).

blocoTerminaDuranteHoraDeAlmoco(Bloco,SimOuNao):-
	bloco_trabalho(Bloco,_,_,HFimBloco),
	inicio_almoco(IAlmoco),
	fim_almoco(FAlmoco),
	((HFimBloco>=IAlmoco),!,(
		((HFimBloco=<FAlmoco),!,(
			SimOuNao is 1
		);(
			SimOuNao is 0
		))
	);(
		SimOuNao is 0
	)).

blocoComecaDuranteHoraDeAlmoco(Bloco,SimOuNao):-
	bloco_trabalho(Bloco,_,HInicioBloco,_),
	inicio_almoco(IAlmoco),
	fim_almoco(FAlmoco),
	((HInicioBloco>=IAlmoco),!,(
		((HInicioBloco=<FAlmoco),!,(
			SimOuNao is 1
		);(
			SimOuNao is 0
		))
	);(
		SimOuNao is 0
	)).

descobreUltimoNoDoBlocoTrabalho(B,No):-
	bloco_trabalho(B,LViagens,_,_),!,
	length(LViagens,L),
	nth1(L,LViagens,Viagem),
	viagem(Viagem,Percurso,_,_),!,
	split_string(Percurso,">","",Split),
	length(Split,S),
	nth1(S,Split,No).

descobrePrimeiroNoDoProximoBlocoTrabalho([B|Blocos],No):-

	bloco_trabalho(B,LViagens,_,_),!,
	nth1(1,LViagens,Viagem),
	viagem(Viagem,Percurso,_,_),!,
	split_string(Percurso,">","",Split),
	nth1(1,Split,No).

calculaTempoLivreAlmoco([],TempoLivre,TempoLivre):-!.
calculaTempoLivreAlmoco([B|Blocos],TempoLivreAteAgora,TempoLivreAlmoco):-
	bloco_trabalho(B,_,HInicioBloco,HFimBloco),!,
	intersetaHoraAlmoco(HInicioBloco,HFimBloco,Intersecao),
	((Intersecao>0),!,(
		TempoLivreAteAgora1 is (TempoLivreAteAgora-Intersecao),
		calculaTempoLivreAlmoco(Blocos,TempoLivreAteAgora1,TempoLivreAlmoco)
	);(
		calculaTempoLivreAlmoco(Blocos,TempoLivreAteAgora,TempoLivreAlmoco)
	)).

calculaTempoLivreJantar([],TempoLivre,TempoLivre):-!.
calculaTempoLivreJantar([B|Blocos],TempoLivreAteAgora,TempoLivreJantar):-
	bloco_trabalho(B,_,HInicioBloco,HFimBloco),
	intersetaHoraJantar(HInicioBloco,HFimBloco,Intersecao),
	((Intersecao>0),!,(
		TempoLivreAteAgora1 is (TempoLivreAteAgora-Intersecao),
		calculaTempoLivreAlmoco(Blocos,TempoLivreAteAgora1,TempoLivreJantar)
	);(
		calculaTempoLivreAlmoco(Blocos,TempoLivreAteAgora,TempoLivreJantar)
	)).


detetaRestricao1(Blocos,NovosBlocos,IdTripulante,IsPrimeiraVez):-
	detetaRestricao1_1(Blocos,Auxiliar,0,0,NovosBlocos,IdTripulante,IsPrimeiraVez),!.
	
detetaRestricao1_1([],Auxiliar,_,_,NovosBlocos,_,_):-reverse(Auxiliar,NovosBlocos),!.
detetaRestricao1_1([B|Blocos],Auxiliar,HFimUltimoBloco,HorasConsecutivas,NovosBlocos,IdTripulante,IsPrimeiraVez):-
	bloco_trabalho(B,ListaViagens,HInicio,HFim),
	((HInicio == HFimUltimoBloco),!,(
		bloco_trabalho(B,ListaViagens,HInicio,HFim),
		HorasConsecutivas1 is HorasConsecutivas+(HFim-HInicio)
	);(
		bloco_trabalho(B,ListaViagens,HInicio,HFim),
		HorasConsecutivas1 is HFim-HInicio
	)),
	
	((HorasConsecutivas1 > 14400),!,(
		write('Alto! Hard constraint #1 quebrada. Vamos tentar resolver.'),nl,
		(IsPrimeiraVez==1),!,(
			atribuiBlocoAOutroMotorista(B,Resultado,IdTripulante),
			((Resultado==1),!,(
				detetaRestricao1_1(Blocos,Auxiliar,HFim,HorasConsecutivas1,NovosBlocos,IdTripulante,IsPrimeiraVez)
			);(
				write('-> Nao foi possivel corrigir a hard constraint #1 (4 horas consecutivas) no servico tripulante '),write(IdTripulante),write('.'),nl,
				detetaRestricao1_1(Blocos,[B|Auxiliar],HFim,HorasConsecutivas1,NovosBlocos,IdTripulante,IsPrimeiraVez)
			))
		);(
			atribuiBlocoAMotoristaLivre(B,Resultado,IdTripulante),
			((Resultado==1),!,(
				detetaRestricao1_1(Blocos,Auxiliar,HFim,HorasConsecutivas1,NovosBlocos,IdTripulante,IsPrimeiraVez)
			);(
				detetaRestricao1_1(Blocos,[B|Auxiliar],HFim,HorasConsecutivas1,NovosBlocos,IdTripulante,IsPrimeiraVez),
				write('-> Nao foi possivel corrigir a hard constraint #1 (4 horas consecutivas) no servico tripulante '),write(IdTripulante),write('.'),nl
			))
		)
	);(
		bloco_trabalho(B,ListaViagens,HInicio,HFim),
		detetaRestricao1_1(Blocos,[B|Auxiliar],HFim,HorasConsecutivas1,NovosBlocos,IdTripulante,IsPrimeiraVez)
	)).

detetaRestricao2(Blocos,NovosBlocos,IdTripulante,IsPrimeiraVez):-
	detetaRestricao2_1(Blocos,[],NovosBlocos,0,IdTripulante,IsPrimeiraVez).

detetaRestricao2_1([],Auxiliar,NovosBlocos,_,_,_):-reverse(Auxiliar,NovosBlocos),!.
detetaRestricao2_1([B|Blocos],Auxiliar,NovosBlocos,TempoTrabalho,IdTripulante,IsPrimeiraVez):-	
	bloco_trabalho(B,_,HInicioBloco,HFimBloco),
	TempoTrabalho1 is (TempoTrabalho+(HFimBloco-HInicioBloco)),
	((TempoTrabalho1 > 28800),!,(
		((IsPrimeiraVez==1),!,(
			atribuiBlocoAOutroMotorista(B,Resultado,IdTripulante),
			((Resultado==1),!,(
				detetaRestricao2_1(Blocos,Auxiliar,NovosBlocos,TempoTrabalho1,IdTripulante,IsPrimeiraVez)
			);(
				write('-> Nao foi possivel corrigir a hard constraint #2 (mais de 8 horas de trabalho) no servico tripulante '),write(IdTripulante),write('.'),nl,
				detetaRestricao2_1(Blocos,[B|Auxiliar],NovosBlocos,TempoTrabalho1,IdTripulante,IsPrimeiraVez)
			))
		);(
			atribuiBlocoAMotoristaLivre(B,Resultado,IdTripulante),
			((Resultado==1),!,(
				detetaRestricao2_1(Blocos,Auxiliar,NovosBlocos,TempoTrabalho1,IdTripulante,IsPrimeiraVez)
			);(
				write('-> Nao foi possivel corrigir a hard constraint #2 (mais de 8 horas de trabalho) no servico tripulante '),write(IdTripulante),write('.'),nl,
				detetaRestricao2_1(Blocos,[B|Auxiliar],NovosBlocos,TempoTrabalho1,IdTripulante,IsPrimeiraVez)
			))
		))
	);(
		detetaRestricao2_1(Blocos,[B|Auxiliar],NovosBlocos,TempoTrabalho1,IdTripulante,IsPrimeiraVez)
	)).

detetaRestricao3(Blocos,NovosBlocos,IdTripulante,IsPrimeiraVez):-
	detetaRestricao3_1(Blocos,[],NovosBlocos,0,0,IdTripulante).

detetaRestricao3_1([],Auxiliar,NovosBlocos,_,_,_):-reverse(Auxiliar,NovosBlocos),!.	
detetaRestricao3_1([B|Blocos],Auxiliar,NovosBlocos,FimUltimoBloco,TempoConsecutivo,IdTripulante):-
	bloco_trabalho(B,_,HInicioBloco,HFimBloco),
	
	((FimUltimoBloco == 0),!,(FimUltimoBloco1 is HFimBloco);(FimUltimoBloco1 is FimUltimoBloco)),

	((HInicioBloco == FimUltimoBloco1),!,(
		bloco_trabalho(B,_,HInicioBloco,HFimBloco),
		TempoConsecutivo1 is TempoConsecutivo+(HFimBloco-HInicioBloco),
		((TempoConsecutivo1 >= 14400),!,(
			diferenceEntrePrimeirosDoisBlocos([B|Blocos],Diferenca),
			((Diferenca < 3600),!,(
				atribuiBlocoAOutroMotorista(B,Resultado,IdTripulante),
				((Resultado == 1),!,(
					detetaRestricao3_1(Blocos,Auxiliar,NovosBlocos,FimUltimoBloco1,TempoConsecutivo1,IdTripulante)
				);(
					write('-> Nao foi possivel corrigir a hard constraint #3 (descanso 1h apos 4h trabalho) no servico tripulante '),write(IdTripulante),write('.'),nl,
					detetaRestricao3_1(Blocos,[B|Auxiliar],NovosBlocos,FimUltimoBloco1,TempoConsecutivo1,IdTripulante)
				))
			);(
				detetaRestricao3_1(Blocos,[B|Auxiliar],NovosBlocos,FimUltimoBloco1,TempoConsecutivo1,IdTripulante)
			))
		);(
			detetaRestricao3_1(Blocos,[B|Auxiliar],NovosBlocos,FimUltimoBloco1,TempoConsecutivo1,IdTripulante)
		))
	);(
		bloco_trabalho(B,_,HInicioBloco,HFimBloco),
		TempoConsecutivo1 is HFimBloco-HInicioBloco,
		detetaRestricao3_1(Blocos,[B|Auxiliar],NovosBlocos,FimUltimoBloco1,TempoConsecutivo1,IdTripulante)
	)).

atribuiBlocoAMotoristaLivre(Bloco,Resultado,MotoristaExplorado):-
	bloco_trabalho(Bloco,_,HInicioBloco,HFimBloco),
	findall(_,(
		tripulanteLivre(HInicio,HFim,Duracao,IdMotorista),
		(HInicio =< HInicioBloco),!,(
			(HFim >= HFimBloco),!,(
				assert(distribuicaoBlocosTrabalhoPorMotoristas(Bloco,IdMotorista)),
				Resultado is 1,
				retract(tripulanteLivre(HInicio,HFim,Duracao,IdMotorista)),
				Duracao1 is HInicioBloco-HInicio,
				Duracao2 is HFim-HFimBloco,
				assert(tripulanteLivre(HInicio,HInicioBloco,Duracao1,IdMotorista)),
				assert(tripulanteLivre(HFimBloco,HFim,Duracao2,IdMotorista))
			);(
				Resultado is 0
			)
		);(
			Resultado is 0
		)
	),_).

atribuiBlocoAOutroMotorista([]):-!.
atribuiBlocoAOutroMotorista(Bloco,Resultado,MotoristaExplorado):-
	bloco_trabalho(Bloco,_,HInicioBloco,HFimBloco),!,
	
	descobreProximoMotorista(Bloco,MotoristaExplorado,ProxMotorista),
	(var(ProxMotorista),!,(
		descobreMotoristaAnterior(Bloco,MotoristaExplorado,MotoristaAnterior),
		MotoristaTroca is MotoristaAnterior
	);(
		MotoristaTroca is ProxMotorista
	)),
	
	write('Bloco '),write(B),write('passou do motorista '),write(MotoristaExplorado),write(' para o motorista '),write(MotoristaTroca),nl,
	
	assert(distribuicaoBlocosTrabalhoPorMotoristas(Bloco,MotoristaTroca)),
	retract(servico_tripulante(MotoristaExplorado,Blocos)),
	removeBloco(Bloco,Blocos,[],NovoBlocos),
	length(NovoBlocos,B),
	((B>0),!,(
		assert(servico_tripulante(MotoristaExplorado,NovoBlocos)),
		Resultado is 1
	);(
		Resultado is 1
	)).

descobreProximoMotorista(Bloco,MotoristaExplorado,ProxMotorista):-
	findall(Proximo,(
		servico_viatura(IdServicoViatura,Blocos),
		member(Bloco,Blocos),
		nth1(Pos,Blocos,Bloco),
		getProximosBlocos(Pos,Blocos,ProxBlocos),
		descobreMotoristaDiferente(ProxBlocos,MotoristaExplorado,Proximo)
	),Proximos),
	length(Proximos, P),
	(P > 0),!,(
		nth1(1,Proximos,ProxMotorista)
	).

descobreMotoristaAnterior(Bloco,MotoristaExplorado,ProxMotorista):-
	findall(Proximo,(
		servico_viatura(IdServicoViatura,Blocos),!,
		member(Bloco,Blocos),!,
		nth1(Pos,Blocos,Bloco),!,
		getPrimeirosBlocos(Pos,Blocos,ProxBlocos),!,
		descobreMotoristaDiferente(ProxBlocos,MotoristaExplorado,Proximo)
	),Proximos),
	length(Proximos, P),
	(P > 0),!,(
		nth1(1,Proximos,ProxMotorista)
	).

getPrimeirosBlocos(Pos,ListaInicial,ListaResultado):-
	getPrimeirosBlocos1(1,Pos,ListaInicial,ListaAuxiliar,ListaResultado).

getPrimeirosBlocos1(_,_,[],ListaAuxiliar,ListaResultado):-reverse(ListaAuxiliar,ListaResultado),!.
getPrimeirosBlocos(PosAtual,Pos,[Bloco|ListaInicial],ListaAuxiliar,ListaResultado):-
	(PosAtual < Pos),!,(
		PosAtual1 is PosAtual+1,
		getProximosBlocos1(PosAtual1,Pos,ListaInicial,[Bloco|ListaAuxiliar],ListaResultado)
	);(
		PosAtual1 is PosAtual+1,
		getProximosBlocos1(PosAtual1,Pos,ListaInicial,ListaAuxiliar,ListaResultado)
	).

getProximosBlocos(Pos,ListaInicial,ListaResultado):-
	getProximosBlocos1(1,Pos,ListaInicial,ListaAuxiliar,ListaResultado).

getProximosBlocos1(_,_,[],ListaAuxiliar,ListaResultado):-reverse(ListaAuxiliar,ListaResultado),!.
getProximosBlocos1(PosAtual,Pos,[Bloco|ListaInicial],ListaAuxiliar,ListaResultado):-
	(PosAtual =< Pos),!,(
		PosAtual1 is PosAtual+1,
		getProximosBlocos1(PosAtual1,Pos,ListaInicial,ListaAuxiliar,ListaResultado)
	);(
		PosAtual1 is PosAtual+1,
		getProximosBlocos1(PosAtual1,Pos,ListaInicial,[Bloco|ListaAuxiliar],ListaResultado)
	).	

descobreMotoristaDiferente([],_,ProxMotorista):-!.
descobreMotoristaDiferente([B|Blocos],MotoristaExplorado,ProxMotorista):-
	findall(Proximo,(
		servico_tripulante(IdMotorista,BlocosServico),
		IdMotorista \== MotoristaExplorado,
		member(B,BlocosServico),!,
		Proximo is IdMotorista
	),Motoristas),
	length(Motoristas,L),
	
	(L > 0),!,(
		nth1(1,Motoristas,Motorista),
		ProxMotorista is Motorista,
		descobreMotoristaDiferente([],_,Motorista)
	);(
		descobreMotoristaDiferente(Blocos,MotoristaExplorado,ProxMotorista)
	).

removeBloco(_,[],NovoBlocos,Resultado):-reverse(NovoBlocos,Resultado),!.
removeBloco(Bloco,[Bloco|Blocos],NovoBlocos,Resultado):-!,
	removeBloco(Bloco,Blocos,NovoBlocos,Resultado).
removeBloco(Bloco,[BlocoDiferente|Blocos],NovoBlocos,Resultado):-!,
	removeBloco(Bloco,Blocos,[BlocoDiferente|NovoBlocos],Resultado).



heuristicaEarliestDueDate(InfoMotoristas,InfoAtualizadaAux,InfoAtualizada):-
	criaListaComHorasFim(InfoMotoristas,InfoAtualizadaAux,ListaComHorasFim),
	ordenaERemoveHoras(ListaComHorasFim, InfoAtualizada).	

heuristicaEarliestEntryTime(InfoMotoristas,InfoAtualizadaAux,InfoAtualizada):-
	criaListaComHorasInicio(InfoMotoristas,InfoAtualizadaAux,ListaComHorasInicio),
	ordenaERemoveHoras(ListaComHorasInicio, InfoAtualizada).

ordenaERemoveHoras(ListaComHorasFim, InfoAtualizada):-
	sort(ListaComHorasFim, ListaOrdenada),
	removeHoras(ListaOrdenada,[], InfoAtualizada).

removeHoras([],Lista,Lista).
removeHoras([(_,IdMotorista,NBlocos)|ListaComHorasFim], ListaAuxiliar,ListaSemHoras):-
	append([(IdMotorista,NBlocos)],ListaAuxiliar,ListaAppended),
	removeHoras(ListaComHorasFim,ListaAppended,ListaSemHoras).

criaListaComHorasFim([],ListaAuxiliar,ListaAuxiliar).
criaListaComHorasFim([(IdMotorista,NBlocos)|InfoMotoristas],ListaAuxiliar,ListaComHorasFim):-
	horariomotorista(IdMotorista,_,HFim,_,_),
	append([(HFim,IdMotorista,NBlocos)], ListaAuxiliar,ListaAppended),
	criaListaComHorasFim(InfoMotoristas,ListaAppended,ListaComHorasFim).

criaListaComHorasInicio([],ListaAuxiliar,ListaAuxiliar).
criaListaComHorasInicio([(IdMotorista,NBlocos)|InfoMotoristas],ListaAuxiliar,ListaComHorasInicio):-
	horariomotorista(IdMotorista,HInicio,_,_,_),
	append([(HInicio,IdMotorista,NBlocos)], ListaAuxiliar,ListaAppended),
	criaListaComHorasInicio(InfoMotoristas,ListaAppended,ListaComHorasInicio).

verificaCompatibilidadeEntreWorkblocksDoServicoEInformacaoDisponibilizada(IdServicoViatura,InfoMotoristasERespetivosNBlocos,Compativel):-
	servico_viatura(IdServicoViatura,LBlocos),
	length(LBlocos,L),
	somatorioBlocosTrabalho(InfoMotoristasERespetivosNBlocos,Somatorio),	
	((Somatorio \== L),!,(Compativel is 0);(Compativel is 1)).

gera(IdServicoViatura):-
	geracoes(NG), populacao(DP), menor_avaliacao(MenorAvaliacao), get_time(TempoInicial), tempo_limite(TempoLimite), estabilizacao(NumeroEstabilizacao), 
	
	lista_motoristas_nblocos(IdServicoViatura,InfoMotoristasERespetivosNBlocos),
	%heuristicaEarliestDueDate(InfoMotoristasERespetivosNBlocos,[],InfoAtualizada),
	heuristicaEarliestEntryTime(InfoMotoristasERespetivosNBlocos,[],InfoAtualizada),
	ordenacao_inicial_motoristas(IdServicoViatura, InfoAtualizada,_,OrdInicial, 0),!,

	length(InfoMotoristasERespetivosNBlocos,NMotoristas),
	((NMotoristas>1),!,(
		gera_populacao(OrdInicial,Populacao),
		servico_viatura(IdServicoViatura,ListaWorkblocks),
		avalia_populacao(Populacao,ListaWorkblocks,_,PopulacaoAvaliada),!,
		ordena_populacao(PopulacaoAvaliada,PopOrd),
		gera_geracao_mix_condicoes(0,NG,MenorAvaliacao,TempoInicial,TempoLimite,NumeroEstabilizacao,0,PopOrd,DP,IdServicoViatura,ListaWorkblocks)
	);(
		servico_viatura(IdServicoViatura,Blocos),
		associaBlocosAosMotoristasDinamicamente(Blocos,OrdInicial)
	)).

assertsTemporarios():-
	retractall(geracoes(_)),
	retractall(populacao(_)),
	retractall(menor_avaliacao(_)),
	retractall(tempo_limite(_)),
	retractall(estabilizacao(_)),
	retractall(prob_cruzamento(_)),
	retractall(prob_mutacao(_)),
	retractall(inicio_almoco(_)),
	retractall(fim_almoco(_)),
	retractall(inicio_jantar(_)),
	retractall(fim_jantar(_)),
	retractall(inicio_turnos(_)),
	retractall(fim_turnos(_)),

	assert(geracoes(10)),
	assert(populacao(8)),
	assert(menor_avaliacao(-99)),
	assert(tempo_limite(10)),
	assert(estabilizacao(2)),
	assert(prob_cruzamento(0.5)),
	assert(prob_mutacao(0.25)),
	assert(inicio_almoco(39600)),
	assert(fim_almoco(54000)),
	assert(inicio_jantar(64800)),
	assert(fim_jantar(79200)),
	assert(inicio_turnos(21600)),
	assert(fim_turnos(82800)).

associaBlocosAosMotoristasDinamicamente([],[]):-!.
associaBlocosAosMotoristasDinamicamente([Bloco|Blocos],[Motorista|Motoristas]):-
	assert(distribuicaoBlocosTrabalhoPorMotoristas(Bloco,Motorista)),
	associaBlocosAosMotoristasDinamicamente(Blocos,Motoristas).

imprimeGeracao(G,[Ind*Aval|Pop],IdServico):-
	format('~nGeracao '), write(G), write(':'), nl, write([Ind*Aval|Pop]), nl,
	write('Avaliacao do melhor individo: '),write(Aval),nl,
	mediaPopulacao([Ind*Aval|Pop],Media),
	write('Media da populacao: '),write(Media),nl,
	servico_viatura(IdServico,Blocos),
	associaBlocosAosMotoristasDinamicamente(Blocos,Ind).

gera_geracao_mix_condicoes(G,G,_,_,_,_,_,[Ind*Aval|Pop],_,IdServico,_) :- !,
	format('~nGeracao '), write(G), write(':'), nl, write([Ind*Aval|Pop]), nl,
	write('Avaliacao do melhor individo: '),write(Aval),nl,
	mediaPopulacao([Ind*Aval|Pop],Media),
	write('Media da populacao: '),write(Media),nl,
	servico_viatura(IdServico,Blocos),
	associaBlocosAosMotoristasDinamicamente(Blocos,Ind).

gera_geracao_mix_condicoes(N,G,C,TempoInicial,TempoLimite, NEstabilizacao, ContadorEstabilizacao, Pop,DP,IdServicoViatura,ListaWorkblocks) :-
	% Condicao termino: tempo limite
	get_time(TempoAtual),
	TempoRelativo is TempoAtual-TempoInicial,
	((TempoRelativo > TempoLimite),!,write('Tempo limite atingido.'),nl,!);

	gera_geracao(N,Pop,[Ind*Aval|PopulacaoFinal],DP,IdServicoViatura,ListaWorkblocks),

	% Condicao termino: Estabilizacao da populacao
	(((Pop==PopulacaoFinal),!,Contador is ContadorEstabilizacao+1);Contador is 0),
	N1 is N+1,
	(((Contador==NEstabilizacao),!,imprimeGeracao(N1,[Ind*Aval|PopulacaoFinal],IdServicoViatura),nl,write('Populacao estabilizada!'),nl,abort());

	% Condicao termino: Solucao com menor avaliacao
	((Aval =< C),!,imprimeGeracao(N1,[Ind*Aval|PopulacaoFinal],IdServicoViatura),nl,write('Solucao encontrada.'),nl,abort());

	N1 is N+1,
	gera_geracao_mix_condicoes(N1,G,C,TempoInicial,TempoLimite,NEstabilizacao, ContadorEstabilizacao,[Ind*Aval|PopulacaoFinal],DP,IdServicoViatura,ListaWorkblocks)).

gera_geracao(N,[Ind*Aval|Pop],PopFinal,DP,IdServicoViatura,ListaWorkblocks) :-
	format('~nGeracao '), write(N), write(':'), nl, write([Ind*Aval|Pop]), nl,
	write('Avaliacao do melhor individo: '),write(Aval),nl,
	mediaPopulacao([Ind*Aval|Pop],Media),
	write('Media da populacao: '),write(Media),nl,

	P is 2,
	separaPopulacao([Ind*Aval|Pop],Pop1,Pop2,P),

	random_permutation(Pop2,PopPermutado),

	cruzamento(PopPermutado,NPop1,IdServicoViatura),

	mutacao(NPop1,NPop),

	avalia_populacao(NPop,ListaWorkblocks,_,NPopAv),!,

	% Juntamos populacao anterior com a nova	
	append(Pop2,NPopAv,MixPop),

	elimina_repetidos(MixPop,_,PopOrdenadas),
	alteraAvaliacoes(PopOrdenadas,Pop2Avaliado),
	ordena_pior_populacao(Pop2Avaliado,NovoPop2ComDuplaAvaliacao),
	remove_segunda_avaliacao(NovoPop2ComDuplaAvaliacao, NovoPop2),
	ElementosEmFaltaNaGeracao is DP-P,
	separaPopulacao(NovoPop2,Pop3,_,ElementosEmFaltaNaGeracao),

	append(Pop1,Pop3,NovaPopulacao),

	ordena_populacao(NovaPopulacao,PopFinal).

mediaPopulacao(Pop,Media):-
	somaAvaliacoes(Pop,Soma),
	length(Pop,NIndividuos),
	Media is Soma/NIndividuos.

somaAvaliacoes(Pop,Soma):-
	somaAvaliacoes(Pop,0,Soma).

somaAvaliacoes([],Soma,Soma).

somaAvaliacoes([_*Aval|Pop],SomaAtual,Soma):-
	NovaSoma is Aval+SomaAtual,
	somaAvaliacoes(Pop,NovaSoma,Soma).

avalia_populacao([],_,PopulacaoAvaliadaAux,PopulacaoAvaliada):-!,
	reverse(PopulacaoAvaliadaAux,PopulacaoAvaliada1),
	reverse(PopulacaoAvaliada1,PopulacaoAvaliada).

avalia_populacao([Ind],ListaWorkblocks,PopulacaoAvaliadaAux,PopulacaoAvaliada):-!,
	cria_agenda(Ind,ListaWorkblocks,_,Agenda),
	condensar_agenda(1,Agenda,AgendaCondensada),
	avalia(AgendaCondensada,Aval,1),
	avalia_populacao([],ListaWorkblocks,[Ind*Aval|PopulacaoAvaliadaAux],PopulacaoAvaliada).

avalia_populacao([Ind|Populacao],ListaWorkblocks,PopulacaoAvaliadaAux,PopulacaoAvaliada):-
	cria_agenda(Ind,ListaWorkblocks,_,Agenda),
	condensar_agenda(1,Agenda,AgendaCondensada),
	retractall(motorista_verificado(_)),
	avalia(AgendaCondensada,Aval,1),
	avalia_populacao(Populacao,ListaWorkblocks,[Ind*Aval|PopulacaoAvaliadaAux],PopulacaoAvaliada).

avalia(Agenda,Aval,1):-
	retractall(avaliacao(_)),
	retractall(motorista_verificado(_)),
	assert(avaliacao(0)),!,	
	avalia(Agenda,Aval).

avalia([],Aval):-!,
	retract(avaliacao(Aval)).

avalia([t(HInicio,HFim,Motorista)|Agenda],Aval):-
	TempoConsecutivo is HFim-HInicio,
	getTripletosByMotorista(Motorista,[t(HInicio,HFim,Motorista)|Agenda],Tripletos),

	%Restricao 1
	restricaoUltrapassarQuatroHorasConsecutivas(TempoConsecutivo,Resultado1),

	%Restricao 2
	restricaoUltrapassarOitoHorasTotais(Motorista,Tripletos,Resultado2,1),

	%Restricao 3
	restricaoDescansoUmaHoraDepoisDeBlocoDeQuatroHoras(Tripletos,Resultado3,1),	

	%Restricao 4
	restricaoDeRefeicoes(Motorista,Tripletos,1,Resultado4),

	%Restricao 5
	restricaoHorariosTurnos(Tripletos,Resultado5),

	%Restricao 6
	restricaoPreferenciaHorarios(Motorista,Tripletos,Resultado6),

	%Indica que motorista ja foi verificado para nao repetir penalizacoes
	assert(motorista_verificado(Motorista)),

	%Atualiza avaliacao guardada dinamicamente
	retract(avaliacao(P)),

	PenalizacaoIndividual is ( (( Resultado1+Resultado2+Resultado3)*10 ) + ((Resultado4+Resultado5)*8) + Resultado6),
	Penalizacao is (P+PenalizacaoIndividual),
	assert(avaliacao(Penalizacao)),

	avalia(Agenda,Aval).

restricaoHorariosTurnos(Tripletos,Resultado5):-
	assert(excesso_trabalho(0)),
	restricaoHorariosTurnos(Tripletos),
	retract(excesso_trabalho(Resultado5)).

restricaoHorariosTurnos([]).
restricaoHorariosTurnos([t(HInicio,HFim,_)|Agenda]):-
	retract(excesso_trabalho(Excesso)),
	inicio_turnos(HInicioTurnos),
	fim_turnos(HFimTurnos),

	% Caso 1: O bloco de trabalho respeita o horario dos turnos
	((HInicio>=HInicioTurnos,HFim=<HFimTurnos),!,(NovoExcesso is Excesso);(
		((HInicio > HInicioTurnos),!,(
			% Caso 2: O bloco de trabalho comeca durante o intervalo mas termina depois do limite
			NovoExcesso is Excesso+(HFim-HFimTurnos)
		);(
			(HFim > HFimTurnos),!,(
				% Caso 3: O bloco de trabalho comeca durante o intervalo mas termina depois do limite
				NovoExcesso is Excesso+(HInicioTurnos-HInicio)+(HFim-HFimTurnos)

			);(
				% Caso 4: 
				NovoExcesso is Excesso+(HInicioTurnos-HInicio)
			)
		))
	)),
	assert(excesso_trabalho(NovoExcesso)),
	restricaoHorariosTurnos(Agenda).



restricaoPreferenciaHorarios(Motorista,Agenda,Resultado):-
	assert(pref_horarios(0)),
	restricaoPreferenciaHorarios(Motorista,Agenda),
	retract(pref_horarios(Resultado)).

restricaoPreferenciaHorarios(_,[]).
restricaoPreferenciaHorarios(Motorista,[t(HInicio,HFim,Motorista)|Tripletos]):-
	horariomotorista(Motorista,PrefInicio,PrefFim,_,_),

	((motorista_verificado(Motorista)),!,(Resultado is 0);(
		%Caso 1: O bloco de trabalho pertence ao intervalo desejado pelo motorista
		((HInicio >= PrefInicio,HFim =< PrefFim),!,(retract(pref_horarios(Excesso)),Resultado is 0);(
			
			(HInicio < PrefInicio),!,(

				% Caso 2: O bloco de trabalho comeca antes do intervalo desejado mas termina dentro do intervalo desejado
				(HFim =< PrefFim),!,(
					retract(pref_horarios(Excesso)),
					Resultado is Excesso+(PrefInicio-HInicio)
				);(
				% Caso 3: O bloco de trabalho comeca antes do intervalo desejado e termina depois do intervalo desejado
					retract(pref_horarios(Excesso)),
					Resultado is Excesso+((PrefInicio-HInicio)+(HFim-PrefFim))
				)
			);(
				% Caso 4: O bloco de trabalho comeca no intervalo desejado mas termina fora
				retract(pref_horarios(Excesso)),
				Resultado is Excesso+(HFim-PrefFim)
			)
		))
	)),
	assert(pref_horarios(Resultado)),
	restricaoPreferenciaHorarios(Motorista,Tripletos).


restricaoDeRefeicoes(Motorista,Agenda,1,Resultado):-
	retractall(intersecao_refeicao(_)),
	((motorista_verificado(Motorista)),!,(Resultado is 0);(
		assert(intersecao_refeicao(0)),
		restricaoDeRefeicoes1(Agenda),
		retract(intersecao_refeicao(Resultado))
	)).

restricaoDeRefeicoes1([]).

restricaoDeRefeicoes1([t(HInicio,HFim,_)|Agenda]):-
	intersetaHoraAlmoco(HInicio,HFim,IntersecaoAlmoco),
	((IntersecaoAlmoco>10800),!,(PenalizacaoAlmoco is IntersecaoAlmoco-10800);(PenalizacaoAlmoco is 0)),

	intersetaHoraJantar(HInicio,HFim, IntersecaoJantar),
	((IntersecaoJantar>10800),!,(PenalizacaoJantar is IntersecaoJantar-10800);(PenalizacaoJantar is 0)),

	retract(intersecao_refeicao(PenalizacaoAtual)),
	Penalizacao is PenalizacaoAtual+(PenalizacaoAlmoco+PenalizacaoJantar),
	assert(intersecao_refeicao(Penalizacao)),

	restricaoDeRefeicoes1(Agenda).

intersetaHoraJantar(HInicio,HFim,Intersecao):-
	inicio_jantar(InicioHoraJantar),
	fim_jantar(FimHoraJantar),

	% Verifica se o bloco de trabalho tem qualquer tipo de intersecao com o intervalo de almoco
	((HFim < InicioHoraJantar;HInicio > FimHoraJantar),!,(Intersecao is 0));(
		inicio_jantar(InicioHoraJantar),
		fim_jantar(FimHoraJantar),
		% Caso 1: Bloco de trabalho comeca antes do inicio da hora de almoco e...
		((HInicio < InicioHoraJantar),!,(
			(HFim < FimHoraJantar),!,(
				% Caso 1.1: Termina antes do fim da hora de almoco
				Intersecao is (HFim-InicioHoraJantar)
			);(	
				% Caso 1.2: Termina depois do fim da hora de almoco
				Intersecao is (FimHoraJantar-InicioHoraJantar)
			)
		);(
			% Caso 2: Bloco de trabalho comeca depois do inicio da hora de almoco e...
			(HFim < FimHoraJantar),!,(
				% Caso 2.1: Termina antes do fim da hora de almoco
				Intersecao is (HFim-HInicio)
			);(
				% Caso 2.2: Termina depois do fim da hora de almoco
				Intersecao is (FimHoraJantar-HInicio)
			)
		)
	)).


intersetaHoraAlmoco(HInicio,HFim,Intersecao):-
	inicio_almoco(InicioHoraAlmoco),
	fim_almoco(FimHoraAlmoco),

	% Verifica se o bloco de trabalho tem qualquer tipo de intersecao com o intervalo de almoco
	((HFim < InicioHoraAlmoco;HInicio > FimHoraAlmoco),!,(Intersecao is 0));(
		inicio_almoco(InicioHoraAlmoco),
		fim_almoco(FimHoraAlmoco),
		% Caso 1: Bloco de trabalho comeca antes do inicio da hora de almoco e...
		((HInicio < InicioHoraAlmoco),!,(
			(HFim < FimHoraAlmoco),!,(
				% Caso 1.1: Termina antes do fim da hora de almoco
				Intersecao is (HFim-InicioHoraAlmoco)
			);(
				% Caso 1.2: Termina depois do fim da hora de almoco
				Intersecao is (FimHoraAlmoco-InicioHoraAlmoco)
			)
		);(
			% Caso 2: Bloco comeca durante a hora de almoco mas termina depois do fim da hora de almoco
			(HFim < FimHoraAlmoco),!,(
				Intersecao is (HFim-HInicio)
			);(
				Intersecao is (FimHoraAlmoco-HInicio)
			)
		)
	)).


restricaoDescansoUmaHoraDepoisDeBlocoDeQuatroHoras(Tripletos,Resultado,1):-
	retractall(descansoAposQuatroHoras(_)),
	assert(descansoAposQuatroHoras(0)),
	restricaoDescansoUmaHoraDepoisDeBlocoDeQuatroHoras(Tripletos,Resultado).

restricaoDescansoUmaHoraDepoisDeBlocoDeQuatroHoras([t(_,_,_)],Resultado):-
	descansoAposQuatroHoras(Resultado).

restricaoDescansoUmaHoraDepoisDeBlocoDeQuatroHoras([t(HInicio1,HFim1,_),t(HInicio2,HFim2,_)|Tripletos],Resultado):-
	DuracaoBloco is HFim1-HInicio1,
	((DuracaoBloco >= 14400),!,
		(
			Descanso is HInicio2-HFim1,
			((Descanso < 3600),!,
				(Penalizacao is 3600-Descanso, retract(descansoAposQuatroHoras(Aval)), NovaAvaliacao is Aval+Penalizacao, assert(descansoAposQuatroHoras(NovaAvaliacao)));
				(write(''))
			)
		);(write(''))
	),
	restricaoDescansoUmaHoraDepoisDeBlocoDeQuatroHoras([t(HInicio2,HFim2,_)|Tripletos],Resultado).

getTripletosByMotorista(Motorista,Agenda,Tripletos):-
	findall(
		t(HInicio,HFim,Motorista),
			(
				member(t(HInicio,HFim,Motorista),Agenda)
			),
			Tripletos
		).

restricaoUltrapassarOitoHorasTotais(Motorista,Tripletos,Resultado,1):-
	assert(horas_trabalho_diarias(0)),
	motorista_verificado(Motorista),!,
	write(''),Resultado is 0;
	restricaoUltrapassarOitoHorasTotais(Motorista,Tripletos,Resultado).

restricaoUltrapassarOitoHorasTotais(_,[],Resultado):-
	retract(horas_trabalho_diarias(NSegundos)),
	((NSegundos > 28800),!,(Resultado is NSegundos-28800);(Resultado is 0)).

restricaoUltrapassarOitoHorasTotais(Motorista,[t(HInicio,HFim,_)|Tripletos],Resultado):-
	DuracaoBloco is HFim-HInicio,
	retract(horas_trabalho_diarias(NSegundos)),
	HorasTrabalhoDiarias is DuracaoBloco+NSegundos,
	assert(horas_trabalho_diarias(HorasTrabalhoDiarias)),
	restricaoUltrapassarOitoHorasTotais(Motorista,Tripletos,Resultado).

restricaoUltrapassarQuatroHorasConsecutivas(TempoConsecutivo,Resultado):-
	% 4 horas = 14400 segundos
	((TempoConsecutivo > 14400),!, Resultado is TempoConsecutivo-14400;
		Resultado is 0).

condensar_agenda(1,[t(NovoHInicio,NovoHFim,Motorista)|Agenda], AgendaCondensada):-
	retractall(ultimo_motorista(_,_,_)),
	assert(ultimo_motorista(NovoHInicio,NovoHFim,Motorista)),
	condensar_agenda(Agenda,_,AgendaCondensada).

condensar_agenda([],AgendaCondensadaAux,AgendaCondensada):- 
	reverse(AgendaCondensadaAux,AgendaCondensada),!.

condensar_agenda([t(NovoHInicio,NovoHFim,Motorista)|Agenda],AgendaCondensadaAux,AgendaCondensada):-
	retract(ultimo_motorista(HInicio,HFim,UltMotorista)),
	((Motorista == UltMotorista),!,
		(
		assert(ultimo_motorista(HInicio,NovoHFim,Motorista)),
		condensar_agenda(Agenda,AgendaCondensadaAux,AgendaCondensada));
	(
	assert(ultimo_motorista(NovoHInicio,NovoHFim,Motorista)),
	condensar_agenda(Agenda,[t(HInicio,HFim,UltMotorista)|AgendaCondensadaAux],AgendaCondensada))).


cria_agenda([],_,Agenda,Resultado):- reverse(Agenda,Resultado),!.
cria_agenda([H1|Ind],[H2|ListaWorkblocks],Agenda,Resultado):-
	bloco_trabalho(H2,_,HInicio,HFim),	
	cria_agenda(Ind,ListaWorkblocks,[t(HInicio,HFim,H1)|Agenda],Resultado).

ordenacao_inicial_motoristas(_,[],PopInicial,PopResultado,_):-
	reverse(PopInicial,PopResultado),!.

ordenacao_inicial_motoristas(IdServicoViatura, [(_,NBlocos)|InfoMotoristasERespetivosNBlocos],PopInicial,PopResultado, NBlocos):-!,
	ordenacao_inicial_motoristas(IdServicoViatura, InfoMotoristasERespetivosNBlocos,PopInicial,PopResultado, 0).

ordenacao_inicial_motoristas(IdServicoViatura, [(IdMotorista,NBlocos)|InfoMotoristasERespetivosNBlocos],PopInicial,PopResultado, Contador):-
	Contador1 is Contador+1,
	ordenacao_inicial_motoristas(IdServicoViatura, [(IdMotorista,NBlocos)|InfoMotoristasERespetivosNBlocos],[IdMotorista|PopInicial],PopResultado, Contador1).

gera_populacao(OrdInicial,Resultado):-
	populacao(TamPop),
	length(OrdInicial,L),
	gera_populacao(TamPop,OrdInicial,L,_,Resultado),!.

gera_populacao(0,_,_,Resto,Resultado):-reverse(Resto,Resultado),!.

gera_populacao(TamPop,OrdInicial,L,Resto,Resultado):-
	TamPop1 is TamPop-1,
	gera_individuo(OrdInicial,L,Ind),
	gera_populacao(TamPop1,OrdInicial,L,[Ind|Resto],Resultado),
	not(member(Ind,Resto)).

gera_individuo([G],1,[G]):-!.

gera_individuo(OrdInicial,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,OrdInicial,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.

bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).

btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).

% Ordenacao para a parte pior da populacao (N-P)
ordena_pior_populacao(PopAv,PopAvOrd):-
	bsort2(PopAv,PopAvOrd).

bsort2([X],[X]):-!.

bsort2([X|Xs],Ys):-
	bsort2(Xs,Zs),
	btroca2([X|Zs],Ys).

btroca2([X],[X]):-!.

btroca2([X*VX*VX2,Y*VY*VY2|L1],[Y*VY*VY2|L2]):-
	VX>VY,!,
	btroca2([X*VX*VX2|L1],L2).

btroca2([X|L1],[X|L2]):-btroca2(L1,L2).


% Elimina elementos repetidos de uma lista
elimina_repetidos([],ListaSemRepetidos,ListaF):- reverse(ListaSemRepetidos,ListaF),!.

elimina_repetidos([Cabeca|Cauda],ListaSemRepetidos,ListaF):-
	(member(Cabeca,Cauda),!,elimina_repetidos(Cauda, ListaSemRepetidos, ListaF);(elimina_repetidos(Cauda,[Cabeca|ListaSemRepetidos], ListaF))).


remove_segunda_avaliacao([],[]).
remove_segunda_avaliacao([L*_*Aval|Lista1], [L*Aval|Lista2]):-
	remove_segunda_avaliacao(Lista1,Lista2).

alteraAvaliacoes([],[]).
alteraAvaliacoes([Ind*Aval|Pop],[Ind*NovaAvaliacao*Aval|NovoPop]) :-
	random(0.0,1.0,Valor),
	NovaAvaliacao is Aval*Valor,
	alteraAvaliacoes(Pop,NovoPop).

separaPopulacao(Pop,Pop1,Pop2,PontoSeparacao):-
	length(Pop,TamanhoLista),
	separaPopulacaoAux(0,Pop,_,_,PontoSeparacao,TamanhoLista,Pop1,Pop2).

separaPopulacaoAux(Tamanho,_,Pop1,Pop2,_,Tamanho,Pop1Final,Pop2Final) :- 
	reverse(Pop1,Pop1Final),
	reverse(Pop2,Pop2Final),!.

separaPopulacaoAux(PosAtual,[Ind|Pop],Pop1,Pop2,P,Tamanho,Pop1Final,Pop2Final):-
	PosAtual1 is PosAtual+1,
	((PosAtual < P),!,separaPopulacaoAux(PosAtual1,Pop,[Ind|Pop1],Pop2,P,Tamanho,Pop1Final,Pop2Final);
			separaPopulacaoAux(PosAtual1,Pop,Pop1,[Ind|Pop2],P,Tamanho,Pop1Final,Pop2Final)).

gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	nblocos(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).

gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[],_).
cruzamento([Ind*_],[Ind],_).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1],IdServicoViatura):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1,IdServicoViatura),
	  cruzar(Ind2,Ind1,P1,P2,NInd2,IdServicoViatura))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1,IdServicoViatura).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).

% O predicado sublista tem como objetivo
% certificar-se que I1 < I2 para os enviar como
% parametros para sublista1
sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

% Se L=[1,2,3,4,5] e K=2, entao L1=[4,5,1,2,3]
rotate_right(L,K,L1):-
	nblocos(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L,_):-!.
insere([X|R],L,N,L2,IdServicoViatura):-
	nblocos(T),
	((N>T,!,N1 is N mod T);N1 = N),

	numberOfAppearances(1,X,L,NumberOfAppearances),
	lista_motoristas_nblocos(IdServicoViatura,InfoMotoristasERespetivosNBlocos),
	maxAppearances(InfoMotoristasERespetivosNBlocos,X,MaxAppearances),
	((NumberOfAppearances < MaxAppearances),!,( insere1(X,N1,L,L1),N2 is N+1,insere(R,L1,N2,L2,IdServicoViatura));(N2 is N,insere(R,L,N2,L2,IdServicoViatura))).

insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

numberOfAppearances(1,X,L,N):-!,
	numberOfAppearances(X,L,0,N).

numberOfAppearances(_,[],NAux,NAux):-!.

numberOfAppearances(X,[X|L],NAux,N):-!,
	N1 is NAux+1,
	numberOfAppearances(X,L,N1,N).

numberOfAppearances(X,[_|L],NAux,N):-!,
	numberOfAppearances(X,L,NAux,N).

maxAppearances([(_,_)|InfoMotoristasERespetivosNBlocos],X,Max):-
	maxAppearances(InfoMotoristasERespetivosNBlocos,X,Max),!.

maxAppearances([(X,Max)|_],X,Max):-!.


cruzar(Ind1,Ind2,P1,P2,NInd11,IdServicoViatura):-
	sublista(Ind1,P1,P2,Sub1),
	nblocos(NumT),
	
	% NumT = 13 ; P2 = 9 ; => R = 4
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	
	% P3 = 10
	P3 is P2 + 1,
	insere(Ind21,Sub1,P3,NInd1,IdServicoViatura),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut),!,(mutacao1(Ind,NInd));(NInd = Ind)),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).

gera_ranges_vd():-
	retractall(rangevd(_,_,_)),
	findall(_,(
		servico_viatura(IdServicoViatura,[PrimeiroBloco|BlocosTrabalho]),
		bloco_trabalho(PrimeiroBloco,_,LimInicial,_),
		getUltimoBlocoTrabalho(BlocosTrabalho,UltimoBloco),!,
		bloco_trabalho(UltimoBloco,_,_,LimFinal),
		assert(rangevd(IdServicoViatura,LimInicial,LimFinal))
	),_).

getUltimoBlocoTrabalho([Bloco],Bloco).
getUltimoBlocoTrabalho([_|BlocosTrabalho],UltimoBloco):-
	getUltimoBlocoTrabalho(BlocosTrabalho,UltimoBloco).
	
gera_tuplos():-
	retractall(t(_,_,_,_)),

	findall(_,(
		horariomotorista(IdMotorista,InicioLaboral,FimLaboral,TrabalhoDiario,BlocosTempo),
		TempoVago is (FimLaboral-InicioLaboral)-TrabalhoDiario,
		length(BlocosTempo,NBlocos),
		cria_tuplos(NBlocos,BlocosTempo,InicioLaboral,FimLaboral,IdMotorista,TempoVago,0))
	,_).

cria_tuplos(1,[Bloco],Inicio,Fim,IdMotorista,_,_):-!,
	%write('t('),write(Inicio),write(','),write(Fim),write(','),write(Bloco),write(','),write(IdMotorista),write(').'),nl.
	assert(t(Inicio,Fim,Bloco,IdMotorista)).
cria_tuplos(NBlocos,[Bloco|BlocosTempo],Inicio,Fim,IdMotorista,TempoExtra,1):-
	NBlocos1 is NBlocos-1,
	FimTuple is Inicio+Bloco+TempoExtra,
	%write('t('),write(Inicio),write(','),write(FimTuple),write(','),write(Bloco),write(','),write(IdMotorista),write(').'),nl,
	assert(t(Inicio,FimTuple,Bloco,IdMotorista)),
	cria_tuplos(NBlocos1,BlocosTempo,FimTuple,Fim,IdMotorista,TempoExtra,1).
cria_tuplos(NBlocos,[Bloco|BlocosTempo],Inicio,Fim,IdMotorista,TempoVago,0):-
	NBlocos1 is NBlocos-1,
	TempoExtra is ((TempoVago/(NBlocos-1))/2),
	FimTuple is Inicio+Bloco+TempoExtra,
	%write('t('),write(Inicio),write(','),write(FimTuple),write(','),write(Bloco),write(','),write(IdMotorista),write(').'),nl,
	assert(t(Inicio,FimTuple,Bloco,IdMotorista)),
	TempoExtra1 is TempoExtra*2,
	cria_tuplos(NBlocos1,BlocosTempo,FimTuple,Fim,IdMotorista,TempoExtra1,1).

calculoCarga(Carga):-
	gera_ranges_vd(),
	findall(Diferenca,(
		rangevd(_,Inicio,Fim),
		Diferenca is Fim-Inicio
	),Lista),
	somaElementosLista(Lista,Carga).

calculoCapacidadeSistema(Capacidade):-
	findall(Duracao,(
		horariomotorista(_,_,_,Duracao,_)
	),Lista),
	somaElementosLista(Lista,Capacidade).

calculaFolgaDisponibilidade(MargemPercentual,MargemSegundos):-
	calculoCarga(Carga),
	calculoCapacidadeSistema(Capacidade),
	write('Capacidade: '),write(Capacidade),write(' (s) | Carga: '),write(Carga),write(' (s)'),nl,
	((Capacidade>Carga),!,(
		MargemPercentual is (Capacidade/Carga)*100,
		MargemSegundos is Capacidade-Carga
	);(
		(Capacidade==Carga),!,(
			MargemPercentual is 0,
			MargemSegundos is 0
		);(
			MargemPercentual is (1-(Carga/Capacidade))*100,
			MargemSegundos is (0-(Carga-Capacidade))
		)
	)).

ordenaServicosViaturaPorHoraInicio(ListaServicos):-
	findall((servico_viatura(IdServico,[PrimeiroBloco|ListaBlocos])*HoraInicio),(
		servico_viatura(IdServico,[PrimeiroBloco|ListaBlocos]),		
		bloco_trabalho(PrimeiroBloco,_,HoraInicio,_)
	),ListaServicosComHorario),
	elimina_repetidos(ListaServicosComHorario,_,ListaServicosComHorarioNRepetido),
	ordena_populacao(ListaServicosComHorarioNRepetido,ListaServicosComHorarioMalOrdenada),
	reverse(ListaServicosComHorarioMalOrdenada,ListaServicosComHorarioOrdenada),
	remove_horario(ListaServicosComHorarioOrdenada,ListaServicos).

ordenaTuplesPorHoraInicio(ListaTuples):-
	findall(t(HoraInicio,HoraFim,Duracao,IdMotorista)*HoraInicio,(
		t(HoraInicio,HoraFim,Duracao,IdMotorista)
	),ListaTuplesComHoraInicio),
	ordena_populacao(ListaTuplesComHoraInicio,ListaTuplesComHoraInicioMalOrdenada),
	reverse(ListaTuplesComHoraInicioMalOrdenada,ListaTuplesComHoraInicioOrdenada),
	remove_horario(ListaTuplesComHoraInicioOrdenada,ListaTuples).

remove_horario(ListaComHorario,Lista):-
	remove_horario(ListaComHorario,[],Lista).

remove_horario([],Lista,Lista):-!.
remove_horario([Elemento*_|ListaComHorario],ListaAux,Lista):-
	remove_horario(ListaComHorario,[Elemento|ListaAux],Lista).

afetaMotoristas():-
	write('Afetacao de Motoristas...'),nl,
	retractall(afetacaoBlocoMotorista(_,_,_)),
	retractall(servico_tripulante(_,_)),
	retractall(lista_motoristas_nblocos(_,_)),

	calculaFolgaDisponibilidade(_,MargemSegundos),
	((MargemSegundos > 0),!,(
		ordenaServicosViaturaPorHoraInicio(ServicosViatura),
		gera_tuplos(),	
		afetacao(ServicosViatura),!,

		findall(lista_motoristas_nblocos(IdServico,ListaMotoristasNBlocos),(
			afetacaoBlocoMotorista(IdServico,_,_),

			findall((IdMotorista,NBlocosMotorista),(
				afetacaoBlocoMotorista(IdServico,IdMotorista,_),

				findall(NBlocos,(
					afetacaoBlocoMotorista(IdServico,IdMotorista,NBlocos)
				),BlocosDeUmMotoristaNumServico),
				somaElementosLista(BlocosDeUmMotoristaNumServico,NBlocosMotorista)

			),ListaMotoristasNBlocos),

			retractall(afetacaoBlocoMotorista(IdServico,_,_))
		),ListasMotoristasNBlocos),
		criaListasMotoristasNBlocos(ListasMotoristasNBlocos)
	)).

criaListasMotoristasNBlocos([]):-!.
criaListasMotoristasNBlocos([lista_motoristas_nblocos(IdServico,ListaMotoristasNBlocos)|ListasMotoristasNBlocos]):-
	length(ListaMotoristasNBlocos,L),
	((L>0),!,(
		assert(lista_motoristas_nblocos(IdServico,ListaMotoristasNBlocos)),
		criaListasMotoristasNBlocos(ListasMotoristasNBlocos)
	);(
		criaListasMotoristasNBlocos(ListasMotoristasNBlocos)
	)).

afetacao([]):-!.
afetacao([servico_viatura(IdServico,Blocos)|ServicosViatura]):-
	descobreDuracaoMaiorBloco(Blocos,Duracao),
	length(Blocos,NBlocos),
	write('-> servico_viatura('),write(IdServico),write(','),write(Blocos),write('). Dados afetacao... '),write(NBlocos),write(' blocos. '),write(Duracao),write(' maior duracao.'),nl,
	ordenaTuplesPorHoraInicio(Tuples),
	afetaServico(IdServico,Duracao,NBlocos,Tuples),
	afetacao(ServicosViatura).

afetaServico(_,_,0,Tuples):-criaTripulantesLivres(Tuples),!.
afetaServico(_,_,_,[]):-!.
afetaServico(IdServico,Duracao,NBlocos,[t(HInicio,HFim,DuracaoT,IdMotorista)|Tuples]):-
	NBlocosQueEsteTupleCobrePorArredondar is DuracaoT/Duracao,
	truncate(NBlocosQueEsteTupleCobrePorArredondar,0,NBlocosQueEsteTupleCobre),

	((NBlocosQueEsteTupleCobre > 0),!,(
		NBlocosRestantes is NBlocos-NBlocosQueEsteTupleCobre,
		retract(t(HInicio,HFim,DuracaoT,IdMotorista)),
		((NBlocosRestantes >= 0),!,(
			assert(afetacaoBlocoMotorista(IdServico,IdMotorista,NBlocosQueEsteTupleCobre)),
			afetaServico(IdServico,Duracao,NBlocosRestantes,Tuples)
		);(
			NBlocosQuePodemRealmenteSerAfetados is NBlocosQueEsteTupleCobre+NBlocosRestantes,
			assert(afetacaoBlocoMotorista(IdServico,IdMotorista,NBlocosQuePodemRealmenteSerAfetados)),
			afetaServico(IdServico,Duracao,0,Tuples)
	)));(
		afetaServico(IdServico,Duracao,NBlocos,Tuples)
	)).

criaTripulantesLivres([]):-!.
criaTripulantesLivres([t(HInicio,HFim,DuracaoT,IdMotorista)|Tuples]):-
	assert(tripulanteLivre(HInicio,HFim,DuracaoT,IdMotorista)),
	criaTripulantesLivres(Tuples).

truncate(X,N,Result):- X >= 0, Result is floor(10^N*X)/10^N, !.

descobreDuracaoMaiorBloco(Blocos,Duracao):-
	descobreDuracaoMaiorBloco(Blocos,0,Duracao).

descobreDuracaoMaiorBloco([],Duracao,Duracao):-!.
descobreDuracaoMaiorBloco([Bloco|Blocos],MaiorDuracao,Duracao):-
	bloco_trabalho(Bloco,_,Inicio,Fim),
	DuracaoBloco is Fim-Inicio,
	((DuracaoBloco>MaiorDuracao),!,(
		descobreDuracaoMaiorBloco(Blocos,DuracaoBloco,Duracao)
	);(
		descobreDuracaoMaiorBloco(Blocos,MaiorDuracao,Duracao)
	)).

somaElementosLista(Lista,Soma):-
	soma(Lista,0,Soma).

soma([],Soma,Soma):-!.
soma([A|Lista],SomaAux,Soma):-
	SomaAux1 is SomaAux + A,
	soma(Lista,SomaAux1,Soma).

getBloco(_,[],_).
getBloco(Posicao,Lista,Resultado):-
	getBloco(1,Posicao,Lista,Resultado).

getBloco(Posicao,Posicao,[Resultado|_],Resultado):-!.
getBloco(Contador,Posicao,[_|Lista],Resultado):-
	Contador1 is Contador+1,
	getBloco(Contador1,Posicao,Lista,Resultado).

gera_edges(HInicio):-retractall(edge(_,_,_,_,_)),
    findall(_,
            ((no(_,Act,@(true),@(false),_,_);no(_,Act,@(false),@(true),_,_)),
            linha(_,NLinha,LNos,_,_),  
            member(Act,LNos),
            nth1(Pos,LNos,Act),
            length(LNos,X),
            cria_edges(Act,Pos,LNos,X,NLinha,HInicio))
        ,_).

cria_edges(_,X,_,X,_,_):- !.

cria_edges(Act,Pos,LNos,X,NLinha,HInicio):-
    Pos1 is Pos+1,
    nth1(Pos1,LNos,Vizinho),
    ((no(_,Vizinho,@(true),@(false),_,_);no(_,Vizinho,@(false),@(true),_,_)),!,
        cria_edge(Act,Vizinho,LNos,NLinha,HInicio)),
    cria_edges(Act,Pos1,LNos,X,NLinha,HInicio).

cria_edge(Act,Vizinho,LNos,NLinha,HInicio):-
    % Vai buscar a posicao dos dois nos
    nth1(Pos1,LNos,Act),
    nth1(Pos2,LNos,Vizinho),

    % Vai buscar os horarios da linha NLinha
    horario(NLinha,LHorarios),
    !,
    % Descobre os horarios no qual passa nos dois nos e calcula o custo
    nth1(Pos1,LHorarios,HorarioNo1),
    nth1(Pos2,LHorarios,HorarioNo2),
    HorarioNo1>HInicio,

    % Cria edge
    assertz(edge(Act,Vizinho,NLinha,HorarioNo1,HorarioNo2)).

caminho2(No,No,Horario,Lusadas,Lfinal,Horario):-!,reverse(Lusadas,Lfinal).

caminho2(No1,Nof,Horario,Lusadas,Lfinal,HorarioChegada):-
    edge(No1,No2,N,H1,H2),
    H1 > Horario,
    \+member((_,_,N),Lusadas),
    \+member((No2,_,_),Lusadas),
    \+member((_,No2,_),Lusadas),
    caminho2(No2,Nof,H2,[(No1,No2,N)|Lusadas],Lfinal,HorarioChegada).

plan_mud_mot_horarios(Noi,Nof,HInicio,Caminho):-
    format('Content-type: text/plain~n'),
    get_time(Ti),
    %apagaBaseConhecimento(),
    %adicionaNos(),
    %adicionaLinhas(),
    gera_edges(HInicio),
    assert(nSol(0)),
    (melhor_caminho_horarios(Noi,Nof,HInicio);true),
    retract(melhor_sol_ntrocas(Caminho,C)),
    retract(nSol(NSolucoes)),
    get_time(Tf),
    TSol is Tf-Ti,
    write('Numero de solucoes: '),write(NSolucoes),nl,
    write('Tempo de geracao da solucao:'),write(TSol),nl,
    write('Hora Chegada: '),write(C),nl.

melhor_caminho_horarios(Noi,Nof,Horario):-
            asserta(melhor_sol_ntrocas(_,99999999)),
            caminho2(Noi,Nof,Horario,[],LCaminho,HorarioChegada),
            atualiza_melhor_horarios(LCaminho,HorarioChegada),
            fail.

atualiza_melhor_horarios(LCaminho,Custo):-
            retract(nSol(NSol)),
            NSol1 is NSol+1,
            assert(nSol(NSol1)),
            melhor_sol_ntrocas(_,MenorCusto),
            Custo<MenorCusto,retract(melhor_sol_ntrocas(_,_)),
            asserta(melhor_sol_ntrocas(LCaminho,Custo)).



adicionaNos():-
    % Envia pedido GET (nos)
    %http_get("https://pacific-retreat-73499.herokuapp.com/api/no",Json,[request_header('Accept'='application/json')]), 
	http_get("http://localhost:3000/api/no",Json,[request_header('Accept'='application/json')]), 
    format('Content-type: text/plain~n~n'),
    json_to_prolog(Json, Nos),    
    assertaNos(Nos).

assertaNos([]) :- !.

assertaNos([H|Nos]):-
    arg(1,H,No),
    asserta(no(No.nome,No.abreviatura,No.isPontoRendicao,No.isEstacaoRecolha,No.longitude,No.latitude)),
    assertaNos(Nos).

adicionaLinhas() :-
    retractall(doadorCodigoLinha(_)),
    assert(doadorCodigoLinha(0)),

    % Envia pedido GET (percursos)  
    %http_get("https://pacific-retreat-73499.herokuapp.com/api/percursos",Json1,[request_header('Accept'='application/json')]),
	http_get("http://localhost:3000/api/percursos",Json1,[request_header('Accept'='application/json')]),
    json_to_prolog(Json1, Percursos),    
	assertaPercursos(Percursos).

assertaPercursos([]):-!.
assertaPercursos([PropsJson]):-
	% Ignorar variaveis PropsX. Usadas para percorrer
    % o documento json e guardar as variaveis de forma independente
    arg(1,PropsJson,Props),
    arg(2,Props,Props1),
    arg(2,Props1,Props2),
    arg(2,Props2,Props3),
    arg(2,Props3,Props4),
    arg(2,Props4,Props5),
    arg(1,Props5,Props6),
    arg(2,Props6,NomeLinha),
    
    % Ignorar variaveis PropsX.
    arg(2,Props5,Props7),
    arg(2,Props7,Props8),
    arg(2,Props8,Props9),
    
    arg(1,Props9,Props10),
    % Lista segmentos Json
    arg(2,Props10,ListaSegmentosJson),
	
    % Lista de nos pronta a fazer assert
    processaSegmentos(ListaSegmentosJson,ListaNos, 0, ListaFinal, Tempo, Distancia),
	retract(doadorCodigoLinha(UltimoCodigo)),
    Codigo is UltimoCodigo+1,
    
    assert(linha(NomeLinha,Codigo,ListaFinal,Tempo,Distancia)),
    assert(doadorCodigoLinha(Codigo)),
    % Passa para o proximo percurso
	format('Content-type: text/plain~n'),
    write('Fiz assert das linhas.'),nl,!.

assertaPercursos([_|[PropsJson|Percursos]]):-
	% Ignorar variaveis PropsX. Usadas para percorrer
    % o documento json e guardar as variaveis de forma independente
    arg(1,PropsJson,Props),
    arg(2,Props,Props1),
    arg(2,Props1,Props2),
    arg(2,Props2,Props3),
    arg(2,Props3,Props4),
    arg(2,Props4,Props5),
    arg(1,Props5,Props6),
    arg(2,Props6,NomeLinha),
    
    % Ignorar variaveis PropsX.
    arg(2,Props5,Props7),
    arg(2,Props7,Props8),
    arg(2,Props8,Props9),
    
    arg(1,Props9,Props10),
    % Lista segmentos Json
    arg(2,Props10,ListaSegmentosJson),
	
    % Lista de nos pronta a fazer assert
    processaSegmentos(ListaSegmentosJson,ListaNos, 0, ListaFinal, Tempo, Distancia),
	retract(doadorCodigoLinha(UltimoCodigo)),
    Codigo is UltimoCodigo+1,
    
    assert(linha(NomeLinha,Codigo,ListaFinal,Tempo,Distancia)),
    assert(doadorCodigoLinha(Codigo)),
    % Passa para o proximo percurso
    assertaPercursos(Percursos).

processaSegmentos([],ListaNos,ListaFinal,_,_) :- reverse(ListaNos,ListaFinal),!. 

processaSegmentos(ListaSegmentos, ListaNos, ListaFinal, Tempo, Distancia):-
    arg(1,ListaSegmentos,SegmentoHeader),
    arg(2,ListaSegmentos,RestoLista),
    arg(1,SegmentoHeader,Props1),
    arg(2,Props1,Props2),
    arg(2,Props2,Props3),
    arg(1,Props3,Props4),
    arg(2,Props4,No2),
    
    processaSegmentos(RestoLista, [No2|ListaNos], ListaFinal, Tempo, Distancia).

processaSegmentos([],_,0,_,_,_) :- !.

processaSegmentos(ListaSegmentos, ListaNos, 0, ListaFinal, Tempo, Distancia):-
    arg(1,ListaSegmentos,SegmentoHeader),
    arg(2,ListaSegmentos,RestoLista),
    arg(1,SegmentoHeader,Props1),
    arg(2,Props1,Props2),
    arg(1,Props2,Props3),

    % Guarda no1 do segmento
    arg(2,Props3,No1),

    arg(2,Props2,Props4),
    arg(1,Props4,Props5),

    % Guarda No2 do segmento
    arg(2,Props5,No2),

    arg(2,Props4,Props6),
    arg(1,Props6,Props7),
    arg(2,Props6,Props8),
    arg(1,Props8,Props9),
    
    % Duracao do segmento
    arg(2,Props9,Tempo),

    % Distancia do segmento
    arg(2,Props7,Distancia),

    processaSegmentos(RestoLista, [No2,No1|ListaNos], ListaFinal, Tempo, Distancia).
    
plan_mud_mot_http(Request):-
	((
		option(method(options),Request),!,cors_enable(Request,[methods([get,post,delete])]),format('~n')
	));(
    % Interpreta parametros (embebidos no url) 
    http_parameters(Request,
                    [ no_inicio(NoI, []),
                      no_fim(NoF, []),
                      horario(HInicio, [between(0,100000)])
                    ]),
    apagaBaseConhecimento(),
    adicionaNos(),
    adicionaLinhas(),
    format('plan_mud_mot_horarios(~w,~w,~w,Caminho).~n',[NoI,NoF,HInicio]),
    plan_mud_mot_horarios(NoI,NoF,HInicio,Caminho),
	format('Caminho: ~w~n',[Caminho])).

algoritmo_genetico_http(Request):-
	format('Content-type: text/plain~n~n'),
    % Interpreta parametros embebidos no url
    http_parameters(Request,[
        dimensaoPop(DP_str,[]),
        numNovasGeracoes(NG_str,[]),
        probCruzamento(PC_str, []),
        probMutacao(PM_str, []),
        tempoLimite(TempoLimite_str, []),
        menorAvaliacao(MenorAvaliacao_str, []),
        fatorEstabilizacao(NumeroEstabilizacao_str, []),
		inicioAlmoco(InicioAlmoco_str, []),
		fimAlmoco(FimAlmoco_str, []),
		inicioJantar(InicioJantar_str, []),
		fimJantar(FimJantar_str, []),
		inicioTurnos(InicioTurnos_str, []),
		fimTurnos(FimTurnos_str, [])
    ]),
	
	apagaBaseConhecimento(),
	get_time(TempoInicial),
	atom_number(NG_str,NG),assert(geracoes(NG)),
	atom_number(DP_str,DP),assert(populacao(DP)),
	atom_number(MenorAvaliacao_str,MenorAvaliacao),assert(menor_avaliacao(MenorAvaliacao)),
	atom_number(TempoLimite_str,TempoLimite),assert(tempo_limite(TempoLimite)),
	atom_number(NumeroEstabilizacao_str,NumeroEstabilizacao),assert(estabilizacao(NumeroEstabilizacao)),
	atom_number(InicioAlmoco_str,InicioAlmoco),assert(inicio_almoco(InicioAlmoco)),
	atom_number(FimAlmoco_str,FimAlmoco),assert(fim_almoco(FimAlmoco)),
	atom_number(InicioJantar_str,InicioJantar),assert(inicio_jantar(InicioJantar)),
	atom_number(FimJantar_str,FimJantar),assert(fim_jantar(FimJantar)),
	atom_number(InicioTurnos_str,InicioTurnos),assert(inicio_turnos(InicioTurnos)),
	atom_number(FimTurnos_str,FimTurnos),assert(fim_turnos(FimTurnos)),
	atom_number(PC_str,PC_number),PC is PC_number/100,assert(prob_cruzamento(PC)),
	atom_number(PM_str,PM_number),PM is PM_number/100,assert(prob_mutacao(PM)),

	novo_gera().

aStar(Orig,Dest,HInicio,Cam,Tempo):-
    get_time(Ti),
    gera_edges(HInicio),
	write('Antes aStar2'),nl,
    aStar2(Dest,[(_,0,[Orig])],HInicio,Cam,Tempo),
	write('Depois aStar2. Cam: '),write(Cam),write(' | Tempo: '),write(Tempo),nl,
    get_time(Tf),
    TSol is Tf-Ti,
    write('Tempo de geracao da solucao:'),write(TSol),nl,!.

aStar2(Dest,[(Est,Ca,[Dest|T])|RestoLista],_,Cam,Ca):-
    length([(Est,Ca,[Dest|T])|RestoLista], L),
    write('Tamanho array c/ tentativas: '),write(L),nl,
    reverse([Dest|T],Cam).

aStar2(Dest,[(_,_,LA)|Outros],Horario,Cam,Tempo):-
    LA=[Act|_],
    findall((CEX,CaX,[X|LA]),
        (Dest\==Act,edge(Act,X,L,Horario1,Horario2),
        Horario1 > Horario, \+ member(X,LA),
        CaX is Horario2,estimativa(X,Dest,EstX,L),
        CEX is CaX+EstX),Novos),
    append(Outros,Novos,Todos),
    sort(Todos,TodosOrd),
    nth1(1,TodosOrd,(_,ProxHorario,_)),
    aStar2(Dest,TodosOrd,ProxHorario,Cam,Tempo).

estimativa(Nodo1,Nodo2,Estimativa,L):-
    linha(_,L,_,T,D),
    VMedia is D/(T*60),
    no(_,Nodo1,_,_,Lat1,Lon1),
    no(_,Nodo2,_,_,Lat2,Lon2),
    P is 0.017453292519943295,
    A is (0.5 - cos((Lat2 - Lat1) * P) / 2 + cos(Lat1 * P) * cos(Lat2 * P) * (1 - cos((Lon2 - Lon1) * P)) / 2),
    Dis is (12742 * asin(sqrt(A)))*1000,
    Estimativa is Dis/VMedia.