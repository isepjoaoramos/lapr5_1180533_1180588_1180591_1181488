:-dynamic geracoes/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.
:-dynamic tempo_limite/1.
:-dynamic menor_avaliacao/1.
:-dynamic estabilizacao/1.

% tarefa(Id,TempoProcessamento,TempConc,PesoPenalizacao).
tarefa(t1,2,5,1).
tarefa(t2,4,7,6).
tarefa(t3,1,11,2).
tarefa(t4,3,9,3).
tarefa(t5,3,8,2).

% tarefas(NTarefas).
tarefas(5).

% parameterizacao
escolhe_condicao_termino(CondicaoTermino) :-
	write('Condicoes de termino disponiveis: '),nl,
	write('1) Numero de geracoes'),nl,
	write('2) Tempo limite'),nl,
	write('3) Menor avaliacao'),nl,
	write('4) Estabilizacao da populacao'),nl,
	write('5) Combinacao de todas as condicoes'),nl,
	write('Escolha a condicao de termino: '),nl,read(CondicaoTermino),
	inicializa(CondicaoTermino),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)).

inicializa(1) :-
	write('Numero de novas Geracoes: '),read(NG), 			
	(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)).

inicializa(2):-
	write('Tempo Limite: '),read(TempoLimite),
	(retract(tempo_limite(_));true), asserta(tempo_limite(TempoLimite)).

inicializa(3) :-
	write('Menor avaliacao a ser procurada: '),read(MenorAvaliacao),
	(retract(menor_avaliacao(_));true), asserta(menor_avaliacao(MenorAvaliacao)),
	write('Tempo Limite: '),read(TempoLimite),
	(retract(tempo_limite(_));true), asserta(tempo_limite(TempoLimite)).

inicializa(4) :-
	write('Numero de estabilizacao: '),read(NumeroEstabilizacao),
	(retract(estabilizacao(_));true), asserta(estabilizacao(NumeroEstabilizacao)),
	write('Tempo Limite: '),read(TempoLimite),
	(retract(tempo_limite(_));true), asserta(tempo_limite(TempoLimite)).

inicializa(5) :-
	write('Numero de novas Geracoes: '),read(NG), 			
	(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	write('Tempo Limite: '),read(TempoLimite),
	(retract(tempo_limite(_));true), asserta(tempo_limite(TempoLimite)),
	write('Menor avaliacao a ser procurada: '),read(MenorAvaliacao),
	(retract(menor_avaliacao(_));true), asserta(menor_avaliacao(MenorAvaliacao)),
	write('Numero de estabilizacao: '),read(NumeroEstabilizacao),
	(retract(estabilizacao(_));true), asserta(estabilizacao(NumeroEstabilizacao)).

gera():-
	escolhe_condicao_termino(Condicao),
	gera_populacao(Pop),
	avalia_populacao(Pop,PopAv),
	ordena_populacao(PopAv,PopOrd),
	gera(Condicao,PopOrd).

gera(1,PopOrd):-
	geracoes(NG),
	populacao(DP),

	gera_geracao_numero_geracoes(0,NG,PopOrd,DP).

gera(2,PopOrd):-
	populacao(DP),
	get_time(TempoInicial),
	tempo_limite(TempoLimite),

	gera_geracao_tempo_limite(0,TempoInicial,TempoLimite,PopOrd,DP).

gera(3,PopOrd):-
	populacao(DP),
	menor_avaliacao(MenorAvaliacao),
	get_time(TempoInicial),
	tempo_limite(TempoLimite),

	gera_geracao_menor_avaliacao(0,MenorAvaliacao,TempoInicial,TempoLimite,PopOrd,DP).

gera(4,PopOrd):-
	populacao(DP),
	estabilizacao(NumeroEstabilizacao),
	get_time(TempoInicial),
	tempo_limite(TempoLimite),

	gera_geracao_estabilizar(0,NumeroEstabilizacao,TempoInicial,TempoLimite,PopOrd,DP,0).

gera(5,PopOrd):-
	geracoes(NG),
	populacao(DP),
	menor_avaliacao(MenorAvaliacao),
	get_time(TempoInicial),
	tempo_limite(TempoLimite),
	estabilizacao(NumeroEstabilizacao),

	gera_geracao_mix_condicoes(0,NG,MenorAvaliacao,TempoInicial,TempoLimite,NumeroEstabilizacao,0,PopOrd,DP).
	

gera_populacao(Pop):-
	populacao(TamPop),
	tarefas(NumT),
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	gera_populacao(TamPop,ListaTarefas,NumT,Pop).

gera_populacao(0,_,_,[]):-!.

gera_populacao(TamPop,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaTarefas,NumT,Resto),
	gera_individuo(ListaTarefas,NumT,Ind),
	not(member(Ind,Resto)).
gera_populacao(TamPop,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,ListaTarefas,NumT,L).

gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).

avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(T,Dur,Prazo,Pen),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	(
		(InstFim =< Prazo,!, VT is 0)
  ;
		(VT is (InstFim-Prazo)*Pen)
	),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.

bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).

btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).

% Ordenacao para a parte pior da populacao (N-P)
ordena_pior_populacao(PopAv,PopAvOrd):-
	bsort2(PopAv,PopAvOrd).

bsort2([X],[X]):-!.

bsort2([X|Xs],Ys):-
	bsort2(Xs,Zs),
	btroca2([X|Zs],Ys).

btroca2([X],[X]):-!.

btroca2([X*VX*VX2,Y*VY*VY2|L1],[Y*VY*VY2|L2]):-
	VX>VY,!,
	btroca2([X*VX*VX2|L1],L2).

btroca2([X|L1],[X|L2]):-btroca2(L1,L2).


% Elimina elementos repetidos de uma lista
elimina_repetidos([],ListaSemRepetidos,ListaF):- reverse(ListaSemRepetidos,ListaF),!.

elimina_repetidos([Cabeca|Cauda],ListaSemRepetidos,ListaF):-
	(member(Cabeca,Cauda),!,elimina_repetidos(Cauda, ListaSemRepetidos, ListaF);(elimina_repetidos(Cauda,[Cabeca|ListaSemRepetidos], ListaF))).


gera_geracao(N,Pop,PopFinal,DP) :-
	write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	random_permutation(Pop,PopPermutado),
	cruzamento(PopPermutado,NPop1),
	mutacao(NPop1,NPop),
	avalia_populacao(NPop,NPopAv),
	
	% Juntamos populacao anterior com a nova
	append(Pop,NPopAv,MixPop),

	ordena_populacao(MixPop,PopOrdenadaRepetidos),
	elimina_repetidos(PopOrdenadaRepetidos,ListaQualquer,PopOrdenadas),

	P is 2,
	separaPopulacao(PopOrdenadas,Pop1,Pop2,P),

	alteraAvaliacoes(Pop2,Pop2Avaliado),
	ordena_pior_populacao(Pop2Avaliado,NovoPop2ComDuplaAvaliacao),
	remove_segunda_avaliacao(NovoPop2ComDuplaAvaliacao, NovoPop2),
	ElementosEmFaltaNaGeracao is DP-P,
	separaPopulacao(NovoPop2,Pop3,Pop4,ElementosEmFaltaNaGeracao),
	append(Pop1,Pop3,NovaPopulacao),

	ordena_populacao(NovaPopulacao,PopFinal).

gera_geracao_estabilizar(N,NEstabilizacao,TempoInicial,TempoLimite,Pop,DP,ContadorEstabilizacao):-
	get_time(TempoAtual),
	TempoRelativo is TempoAtual-TempoInicial,
	((TempoRelativo > TempoLimite),!,nl,write('Tempo limite atingido.'),nl,!);

	gera_geracao(N,Pop,PopulacaoFinal,DP),
	(((Pop==PopulacaoFinal),!,Contador is ContadorEstabilizacao+1);Contador is 0),
	
	N1 is N+1,
	(((Contador==NEstabilizacao),!,write('Geracao '), write(N1), write(':'), nl, write(PopulacaoFinal),nl,write('Populacao estabilizada!'),nl);
	
	gera_geracao_estabilizar(N1,NEstabilizacao,TempoInicial,TempoLimite,PopulacaoFinal,DP,Contador)).

gera_geracao_menor_avaliacao(N,C,TempoInicial,TempoLimite,Pop,DP):-
	get_time(TempoAtual),
	TempoRelativo is TempoAtual-TempoInicial,
	((TempoRelativo > TempoLimite),!,write('Tempo limite atingido.'),nl,!);

	gera_geracao(N,Pop,[Ind*Aval|PopulacaoFinal],DP),
	N1 is N+1,
	(((Aval =< C),!,write('Geracao '), write(N1), write(':'), nl, write([Ind*Aval|PopulacaoFinal]),nl,write('Solucao encontrada.'),nl,abort());
	gera_geracao_menor_avaliacao(N1,C,TempoInicial,TempoLimite,[Ind*Aval|PopulacaoFinal],DP)).

gera_geracao_tempo_limite(N,TempoInicial,TempoLimite,Pop,DP):-
	get_time(TempoAtual),
	TempoRelativo is TempoAtual-TempoInicial,
	((TempoRelativo > TempoLimite),!,write('Tempo limite atingido.'),nl,!);
	gera_geracao(N,Pop,PopulacaoFinal,DP),
	N1 is N+1,
	gera_geracao_tempo_limite(N1,TempoInicial,TempoLimite,PopulacaoFinal,DP).

gera_geracao_numero_geracoes(G,G,Pop,_):-!,
	write('Geracao '), write(G), write(':'), nl, write(Pop), nl.

gera_geracao_numero_geracoes(N,G,Pop,DP):-
	gera_geracao(N,Pop,PopulacaoFinal,DP),
	N1 is N+1,
	gera_geracao_numero_geracoes(N1,G,PopulacaoFinal,DP).

gera_geracao_mix_condicoes(G,G,_,_,_,Pop,_) :- !,
	write('Geracao '), write(G), write(':'), nl, write(Pop), nl.

gera_geracao_mix_condicoes(N,G,C,TempoInicial,TempoLimite, NEstabilizacao, ContadorEstabilizacao, Pop,DP) :-
	
	% Condicao termino: tempo limite
	get_time(TempoAtual),
	TempoRelativo is TempoAtual-TempoInicial,
	((TempoRelativo > TempoLimite),!,write('Tempo limite atingido.'),nl,!);

	gera_geracao(N,Pop,[Ind*Aval|PopulacaoFinal],DP),

	% Condicao termino: Estabilizacao da populacao
	(((Pop==PopulacaoFinal),!,Contador is ContadorEstabilizacao+1);Contador is 0),
	(((Contador==NEstabilizacao),!,gera_geracao_mix_condicoes(G,G,_,_,_,[Ind*Aval|PopulacaoFinal],_),nl,write('Populacao estabilizada!'),nl);

	% Condicao termino: Solucao com menor avaliacao
	(((Aval =< C),!,gera_geracao_mix_condicoes(G,G,_,_,_,[Ind*Aval|PopulacaoFinal],_),nl,write('Solucao encontrada.'),nl,abort());

	N1 is N+1,
	gera_geracao_mix_condicoes(N1,G,C,TempoInicial,TempoLimite,NEstabilizacao, ContadorEstabilizacao,[Ind*Aval|PopulacaoFinal],DP))).

remove_segunda_avaliacao([],[]).
remove_segunda_avaliacao([L*_*Aval|Lista1], [L*Aval|Lista2]):-
	remove_segunda_avaliacao(Lista1,Lista2).

alteraAvaliacoes([],[]).
alteraAvaliacoes([Ind*Aval|Pop],[Ind*NovaAvaliacao*Aval|NovoPop]) :-
	random(0.0,1.0,Valor),
	NovaAvaliacao is Aval*Valor,
	alteraAvaliacoes(Pop,NovoPop).

separaPopulacao(Pop,Pop1,Pop2,PontoSeparacao):-
	length(Pop,TamanhoLista),
	%write('Lista a separar (Tamanho '),write(TamanhoLista),write('): '),write(Pop),nl,
	separaPopulacaoAux(0,Pop,Pop1Aux,Pop2Aux,PontoSeparacao,TamanhoLista,Pop1,Pop2).

separaPopulacaoAux(Tamanho,Pop,Pop1,Pop2,P,Tamanho,Pop1Final,Pop2Final) :- 
	reverse(Pop1,Pop3),reverse(Pop3,Pop1Final),
	reverse(Pop2,Pop4),reverse(Pop4,Pop2Final),!.

separaPopulacaoAux(PosAtual,[Ind|Pop],Pop1,Pop2,P,Tamanho,Pop1Final,Pop2Final):-
	%write('PosAtual: '),write(PosAtual),nl,
	%write('Estado atual de Pop1: '),write(Pop1),nl,
	%write('Estado atual de Pop2: '),write(Pop2),nl,
	PosAtual1 is PosAtual+1,
	((PosAtual < P),!,separaPopulacaoAux(PosAtual1,Pop,[Ind|Pop1],Pop2,P,Tamanho,Pop1Final,Pop2Final);
			separaPopulacaoAux(PosAtual1,Pop,Pop1,[Ind|Pop2],P,Tamanho,Pop1Final,Pop2Final)).

gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).

