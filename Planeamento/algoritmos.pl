% no(nome_paragem, abrev_paragem, flag_ponto_rendição, flag_estação_recolha,longitude, latitude)

no('Aguiar de Sousa', 'AGUIA', f, f, -8.4464785432391, 41.1293363229325).
no('Baltar', 'BALTR', t, f, -8.38716802227697, 41.1937898023744).
no('Besteiros', 'BESTR', f, f, -8.34043029659082, 41.217018845589).
no('Cete', 'CETE', t, f, -8.35164059584564, 41.183243425797).
no('Cristelo', 'CRIST', t, f, -8.34639896125324, 41.2207801252676).
no('Duas Igrejas', 'DIGRJ', f, f, -8.35481024956726, 41.2278665802794).
no('Estação (Lordelo)', 'ESTLO', f, t, -8.4227924957086, 41.2521157104055).
no('Estação (Paredes)', 'ESTPA', f, t, -8.33448520831829, 41.2082119860192).
no('Gandra', 'GAND', f, f, -8.43958765792976, 41.1956579348384).
no('Lordelo', 'LORDL', t, f, -8.42293614720057, 41.2452627470645).
no('Mouriz', 'MOURZ', t, f, -8.36577272258403, 41.1983610215263).
no('Parada de Todeia', 'PARAD', t, f, -8.37023578802149, 41.1765780321068).
no('Paredes', 'PARED', t, f, -8.33566951069481, 41.2062947118362).
no('Recarei', 'RECAR', f, f, -8.42215867462191, 41.1599363478137).
no('Sobrosa', 'SOBRO', t, f, -8.38118071581788, 41.2557331783506).
no('Vandoma', 'VANDO', f, f, -8.34160692293342, 41.2328015719913).
no('Vila Cova de Carros', 'VCCAR', t, f, -8.35109395257277, 41.2090666564063).

% linha(nome_linha, número_linha, lista_abrev_paragens, tempo_minutos, distância_metros)

linha('Paredes_Aguiar', 1, ['AGUIA','RECAR', 'PARAD', 'CETE', 'PARED'], 31, 15700).
linha('Paredes_Aguiar', 3, ['PARED', 'CETE','PARAD', 'RECAR', 'AGUIA'], 31, 15700).
linha('Paredes_Gandra', 5 , ['GAND', 'VANDO', 'BALTR', 'MOURZ', 'PARED'], 26, 13000).  
linha('Paredes_Gandra', 8, ['PARED', 'MOURZ', 'BALTR', 'VANDO', 'GAND'], 26, 13000).   
linha('Paredes_Lordelo', 9, ['LORDL','VANDO', 'BALTR', 'MOURZ', 'PARED'], 29, 14300).
linha('Paredes_Lordelo', 11, ['PARED','MOURZ', 'BALTR', 'VANDO', 'LORDL'], 29, 14300).
linha('Lordelo_Parada', 24, ['LORDL', 'DIGRJ', 'CRIST', 'VCCAR', 'BALTR', 'PARAD'], 22, 11000).
linha('Lordelo_Parada', 26, ['PARAD', 'BALTR', 'VCCAR', 'CRIST', 'DIGRJ', 'LORDL'], 22, 11000).
linha('Sobrosa_Cete', 22, ['SOBRO', 'CRIST', 'BESTR', 'VCCAR', 'MOURZ', 'CETE'], 23,
11500).
linha('Sobrosa_Cete', 20, ['CETE', 'MOURZ', 'VCCAR', 'BESTR', 'CRIST', 'SOBRO'], 23,
11500).
linha('Estação(Lordelo)_Lordelo',34,['ESTLO','LORDL'], 2,1500).
linha('Lordelo_Estação(Lordelo)',35,['LORDL','ESTLO'], 2,1500).
linha('Estação(Lordelo)_Sobrosa',36,['ESTLO','SOBRO'], 5,1500).
linha('Sobrosa_Estação(Lordelo)',37,['SOBRO','ESTLO'], 5,1800).
linha('Estação(Paredes)_Paredes',38,['ESTPA','PARED'], 2,1500).
linha('Paredes_Estação(Paredes)',39,['PARED','ESTPA'], 2,1500).

% viagem(id_viagem, número_linha, id_horario)


% horario(número_linha, lista_segundos).

horario(1, [36000,36540,36840,37140,37620]).
horario(1, [37800,38340,38640,38940,39420]).
horario(1, [39600,40140,40440,40740,41220]).
horario(1, [41400,41940,42240,42540,43020]).

horario(3, [73800,74280,74580,74880,75420]).
horario(3, [72900,73380,73680,73980,75420]).
horario(3, [72000,72480,72780,73080,73620]).
horario(3, [71100,71580,71880,72180,72720]).

horario(5, [30360,30960,31200,31440,31920]).
horario(5, [33960,34560,34800,35040,35520]).
horario(5, [37560,38160,38400,38640,39120]).
horario(5, [41160,41760,42000,42240,42720]).

horario(8, [28860,29340,29580,29820,30360]).
horario(8, [32400,32880,33120,33360,33960]).
horario(8, [36000,36480,36720,36960,37560]).
horario(8, [39600,40080,40320,40560,41160]).

horario(9, [30180,30480,30720,30960,31320,31560]).
horario(9, [31380,31680,31920,32160,32520,32760]).
horario(9, [32580,32880,33120,33360,33720,33960]).
horario(9, [33780,34080,34320,34560,34920,35160]).

horario(11, [36000,36240,36600,36840,37080,37380]).
horario(11, [37200,37440,37800,38040,38280,38580]).
horario(11, [38400,38640,39000,39240,39480,39780]).
horario(11, [39600,39840,40200,40440,40680,40980]).

horario(24, [71580,71880,72120,72360,72600,72900]).
horario(24, [70260,70560,70800,71040,71280,71580]).
horario(24, [68940,69240,69480,69720,69960,70260]).
horario(24, [67620,67920,68160,68400,68640,68940]).

horario(26, [33600,33900,34140,34380,34620,34920]).
horario(26, [32280,32580,32820,33060,33300,33600]).
horario(26, [30960,31260,31500,31740,31980,32280]).
horario(26, [29640,29940,30180,30420,30660,30960]).

horario(22, [28980,29280,29520,29760,30120,30360]).
horario(22, [30180,30480,30720,30960,31320,31560]).
horario(22, [31380,31680,31920,32160,32520,32760]).
horario(22, [32580,32880,33120,33360,33720,33960]).

horario(20, [34800,35040,35400,35640,35880,36180]).
horario(20, [33600,33840,34200,34440,34680,34980]).
horario(20, [32400,32640,33000,33240,33480,33780]).
horario(20, [31200,31440,31800,32040,32280,32580]).

horario(34, [26580,26700]).
horario(34, [27900,28020]).
horario(34, [29220,29340]).

horario(35, [73200,73320]).
horario(35, [74520,74640]).
horario(35, [75840,75960]).

horario(36, [27300,27600]).
horario(36, [28500,28800]).
horario(36, [29700,30000]).

horario(37, [77160,77460]).
horario(37, [74760,75060]).
horario(37, [75960,76260]).

horario(38, [27300,27420]).
horario(38, [28200,28320]).
horario(38, [28740,28860]).
horario(38, [29100,29220]).

horario(39, [76320,76440]).
horario(39, [89820,89940]).
horario(39, [82320,82440]).
horario(39, [75900,76020]).

% Bibliotecas
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_server)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_open)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)).
:- use_module(library(uri)).
:- use_module(library(http/http_cors)).
:- use_module(library(http/http_json)).
:- use_module(library(http/http_session)).
:- use_module(library(http/json_convert)).

% Cors
:- set_setting(http:cors, [*]).

% Predicado a ser executado quando e feito um pedido no url /mudanca_motoristas
:- http_handler(root(mudanca_motoristas), plan_mud_mot_http, []).

% Inicializacao do servidor
server(Port) :- http_server(http_dispatch, [port(Port)]).

% Base conhecimento dinâmica
:- dynamic liga/3.
:- dynamic melhor_sol_ntrocas/2.
:- dynamic edge/5.
:- dynamic no/6.
:- dynamic linhaAtlas/2.
:- dynamic percurso/4.
:- dynamic linha/5.

plan_mud_mot(Noi,Nof,LCaminho_menostrocas):-
    get_time(Ti),
    findall(LCaminho,caminho(Noi,Nof,LCaminho),LLCaminho),
    menor(LLCaminho,LCaminho_menostrocas),
    get_time(Tf),
    length(LLCaminho,NSol),
    TSol is Tf-Ti,
    write('Numero de Solucoes:'),write(NSol),nl,
    write('Tempo de geracao da solucao:'),write(TSol),nl.

menor([H],H):-!.
menor([H|T],Hmenor):-menor(T,L1),length(H,C),length(L1,C1),
((C<C1,!,Hmenor=H);Hmenor=L1).

caminho(Noi,Nof,LCaminho):-caminho(Noi,Nof,[],LCaminho).

caminho(No,No,Lusadas,Lfinal):-reverse(Lusadas,Lfinal).

caminho(No1,Nof,Lusadas,Lfinal):-
    liga(No1,No2,N),
    \+member((_,_,N),Lusadas),
    \+member((No2,_,_),Lusadas),
    \+member((_,No2,_),Lusadas),
    caminho(No2,Nof,[(No1,No2,N)|Lusadas],Lfinal).

gera_ligacoes:- retractall(liga(_,_,_)),
    findall(_,
        ((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),
        (no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
        No1\==No2,
        linha(_,N,LNos,_,_),
        ordem_membros(No1,No2,LNos),
        assertz(liga(No1,No2,N))
        ),_).

ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).

plan_mud_mot1(Noi,Nof,LCaminho_menostrocas):-
            get_time(Ti),
            (melhor_caminho(Noi,Nof);true),
            retract(melhor_sol_ntrocas(LCaminho_menostrocas,_)),
            get_time(Tf),
            TSol is Tf-Ti,
            write('Tempo de geracao da solucao:'),write(TSol),nl.


melhor_caminho(Noi,Nof):-
            asserta(melhor_sol_ntrocas(_,10000)),
            caminho(Noi,Nof,LCaminho),
            atualiza_melhor(LCaminho),
            fail.

atualiza_melhor(LCaminho):-
            melhor_sol_ntrocas(_,N),
            length(LCaminho,C),
            C<N,retract(melhor_sol_ntrocas(_,_)),
            asserta(melhor_sol_ntrocas(LCaminho,C)).

gera_edges(HInicio):-retractall(edge(_,_,_,_,_)),
    findall(_,
            ((no(_,Act,t,f,_,_);no(_,Act,f,t,_,_)),
            linha(_,NLinha,LNos,_,_),  
            member(Act,LNos),
            nth1(Pos,LNos,Act),
            length(LNos,X),
            cria_edges(Act,Pos,LNos,X,NLinha,HInicio))
        ,_).

cria_edges(_,X,_,X,_,_):-!.

cria_edges(Act,Pos,LNos,X,NLinha,HInicio):-
    Pos1 is Pos+1,
    nth1(Pos1,LNos,Vizinho),
    ((no(_,Vizinho,t,f,_,_);no(_,Vizinho,f,t,_,_)),!,
        cria_edge(Act,Vizinho,LNos,NLinha,HInicio)),
    cria_edges(Act,Pos1,LNos,X,NLinha,HInicio).

cria_edge(Act,Vizinho,LNos,NLinha,HInicio):-
    % Vai buscar a posicao dos dois nos
    nth1(Pos1,LNos,Act),
    nth1(Pos2,LNos,Vizinho),

    % Vai buscar os horarios da linha NLinha
    horario(NLinha,LHorarios),
    !,
    % Descobre os horarios no qual passa nos dois nos e calcula o custo
    nth1(Pos1,LHorarios,HorarioNo1),
    nth1(Pos2,LHorarios,HorarioNo2),
    HorarioNo1>HInicio,

    % Cria edge
    assertz(edge(Act,Vizinho,NLinha,HorarioNo1,HorarioNo2)).
    

aStar(Orig,Dest,HInicio,Cam,Tempo):-
    get_time(Ti),
    gera_edges(HInicio),
    aStar2(Dest,[(_,0,[Orig])],HInicio,Cam,Tempo),
    get_time(Tf),
    TSol is Tf-Ti,
    write('Tempo de geracao da solucao:'),write(TSol),nl,!.

aStar2(Dest,[(Est,Ca,[Dest|T])|RestoLista],_,Cam,Ca):-
    length([(Est,Ca,[Dest|T])|RestoLista], L),
    write("Lista aStar: "),write([(Est,Ca,[Dest|T])|RestoLista]),nl,
    write('Tamanho array c/ tentativas: '),write(L),nl,
    reverse([Dest|T],Cam).

aStar2(Dest,[(_,_,LA)|Outros],Horario,Cam,Tempo):-
    LA=[Act|_],
    findall((CEX,CaX,[X|LA]),
        (Dest\==Act,edge(Act,X,L,Horario1,Horario2),
        Horario1 > Horario, \+ member(X,LA),
        CaX is Horario2,estimativa(X,Dest,EstX,L),
        CEX is CaX+EstX),Novos),
    append(Outros,Novos,Todos),
    sort(Todos,TodosOrd),
    nth1(1,TodosOrd,(_,ProxHorario,_)),
    aStar2(Dest,TodosOrd,ProxHorario,Cam,Tempo).

estimativa(Nodo1,Nodo2,Estimativa,L):-
    linha(_,L,_,T,D),
    VMedia is D/(T*60),
    no(_,Nodo1,_,_,Lat1,Lon1),
    no(_,Nodo2,_,_,Lat2,Lon2),
    P is 0.017453292519943295,
    A is (0.5 - cos((Lat2 - Lat1) * P) / 2 + cos(Lat1 * P) * cos(Lat2 * P) * (1 - cos((Lon2 - Lon1) * P)) / 2),
    Dis is (12742 * asin(sqrt(A)))*1000,
    Estimativa is Dis/VMedia.

caminho2(No,No,Horario,Lusadas,Lfinal,Horario):-!,reverse(Lusadas,Lfinal).

caminho2(No1,Nof,Horario,Lusadas,Lfinal,HorarioChegada):-
    edge(No1,No2,N,H1,H2),
    H1 > Horario,
    \+member((_,_,N),Lusadas),
    \+member((No2,_,_),Lusadas),
    \+member((_,No2,_),Lusadas),
    caminho2(No2,Nof,H2,[(No1,No2,N)|Lusadas],Lfinal,HorarioChegada).

plan_mud_mot_horarios(Noi,Nof,HInicio,Caminho):-
            get_time(Ti),
            gera_edges(HInicio),
            assert(nSol(0)),
            (melhor_caminho_horarios(Noi,Nof,HInicio);true),
            retract(melhor_sol_ntrocas(Caminho,C)),
            retract(nSol(NSolucoes)),
            get_time(Tf),
            TSol is Tf-Ti,
            write('Numero de solucoes: '),write(NSolucoes),nl,
            write('Tempo de geracao da solucao:'),write(TSol),nl,
            write('Hora Chegada: '),write(C),nl.

melhor_caminho_horarios(Noi,Nof,Horario):-
            asserta(melhor_sol_ntrocas(_,9999999999)),
            caminho2(Noi,Nof,Horario,[],LCaminho,HorarioChegada),
            atualiza_melhor_horarios(LCaminho,HorarioChegada),
            fail.

atualiza_melhor_horarios(LCaminho,Custo):-
            retract(nSol(NSol)),
            NSol1 is NSol+1,
            assert(nSol(NSol1)),
            melhor_sol_ntrocas(_,MenorCusto),
            Custo<MenorCusto,retract(melhor_sol_ntrocas(_,_)),
            asserta(melhor_sol_ntrocas(LCaminho,Custo)).

bfs(Orig,Dest,Cam,Custo):-
            get_time(Ti),
            gera_ligacoes(),
            bfs2(Dest,(0,[Orig]),Cam,Custo),
            get_time(Tf),
            TSol is Tf-Ti,
            write('Tempo de geracao da solucao:'),write(TSol),nl,!.

bfs2(Dest,(Custo,[Dest|T]),Cam,Custo):-
                !,
                reverse([Dest|T],Cam).


bfs2(Dest,(Ca,LA),Cam,Custo):-
            LA=[Act|_],
            findall((EstX,CaX,[X|LA]),
            (liga(Act,X,CX),\+member(X,LA),estimativa_distancia(X,Dest,EstX),
            CaX is Ca+CX),Novos),
            sort(Novos,NovosOrd),
            NovosOrd = [(_,CM,Melhor)|_],
            bfs2(Dest,(CM,Melhor),Cam,Custo).


estimativa_distancia(Nodo1,Nodo2,Estimativa):-
                    no(_,Nodo1,_,_,X1,Y1),
                    no(_,Nodo2,_,_,X2,Y2),
                    Estimativa is sqrt((X1-X2)^2+(Y1-Y2)^2).

proximo([(_,CM,Melhor)|_],CM,Melhor).
proximo([_|L],CM,Melhor):-proximo(L,CM,Melhor).

bfs_horarios(Orig,Dest,HInicio,Cam,Custo):-
            get_time(Ti),
            gera_edges(HInicio),
            bfs2_horarios(Dest,(0,[Orig]),Cam,Custo),
            get_time(Tf),
            TSol is Tf-Ti,
            write('Tempo de geracao da solucao:'),write(TSol),nl,!.

bfs2_horarios(Dest,(Custo,[Dest|T]),Cam,Custo):-
                !,
                length([Dest|T], L),
                write('Numero de solucoes: '),write(L),nl,
                reverse([Dest|T],Cam).

bfs2_horarios(Dest,(Ca,LA),Cam,Custo):-
            LA=[Act|_],
            findall((EstX,CaX,[X|LA]),
                (edge(Act,X,L,Horario1,Horario2),
                Horario1 > Ca,
                \+member(X,LA),
                estimativa(X,Dest,EstX,L),
                CaX is Horario2)
            ,Novos),
            sort(Novos,NovosOrd),
            proximo(NovosOrd,CM,Melhor),
            bfs2_horarios(Dest,(CM,Melhor),Cam,Custo).

adicionaNos():-
    % Envia pedido GET (nos)
    http_get("http://localhost:3000/api/no",Json,[request_header('Accept'='application/json')]), 
    format('Content-type: text/plain~n~n'),
    json_to_prolog(Json, Nos),    
    assertaNos(Nos).

assertaNos([]).
assertaNos([H|Nos]):-
    arg(1,H,No),
    asserta(no(No.nome,No.abreviatura,No.isPontoRendicao,No.isEstacaoRecolha,No.longitude,No.latitude)),
    assertaNos(Nos).

plan_mud_mot_http(Request):-

    adicionaNos(),

    % Envia pedido GET (percursos)
    http_get("http://localhost:3000/api/percursos",Json1,[request_header('Accept'='application/json')]),
    json_to_prolog(Json1, Percursos),
    assertaPercursos(Percursos),

    % Envia pedido GET (linhas)
    http_get("http://localhost:3000/api/linha",Json2,[request_header('Accept'='application/json')]),
    json_to_prolog(Json2, Linhas),
    %assertaLinhas(Linhas),

    % Interpreta parametros (embebidos no url) 
    http_parameters(Request,
                    [ no_inicio(NoI, []),
                      no_fim(NoF, []),
                      horario(HInicio, [between(0,100000)])
                    ]),
    plan_mud_mot_horarios(NoI,NoF,HInicio,Caminho),
    format('Content-type: text/plain~n~n'),
	format('Caminho: ~w~n',[Caminho]).