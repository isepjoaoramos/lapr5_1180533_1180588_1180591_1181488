## Walking skeleton

#### Definição da arquitetura

Segundo a definição de uma arquitetura walking-skeleton, os vários componentes da aplicação estarão ligados desde uma fase inicial do projeto e desenvolvidos a partir daí, de uma forma paralela. No caso da nossa aplicação poderemos considerar como os componentes desenvolvidos o SPA (Single-Page Application), MDR (Master Data Rede), Planeamento e MDV (Master Data Viagens). 

#### Componentes e sua interação

Na aplicação desenvolvida, existem várias interações entre os diferentes componentes em diferentes fases.

###### Ponto de situação no sprint B

Até ao momento, as maiores interações existentes entre componentes são entre o SPA e o MDR. Desta forma, podemos ver interações nas várias funcionalidades implementadas, como por exemplo, ao criar um nó, percurso e linha. Nestas funcionalidades, o utilizador interage com o componente SPA ao criar estas entidades, que por sua vez interage com o MDR, de forma a estas serem persistidas na base de dados.

Para além destas interações, temos outra interação que é preciso mencionar. Na funcionalidade de visualizar a solução do caminho com menos trocas de motorista entre dois pontos, o SPA envia a informação para o componente de Planeamento, que processa os dados.



####  Implementação do Walking-Skeleton

Através de testes de integração entre componentes e testes End-to-End, é possível verificar e validar a integração entre os vários componentes da aplicação.