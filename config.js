import dotenv from 'dotenv';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (!envFound) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
  /**
   * Your favorite port
   */
  port: parseInt(process.env.PORT, 10),

  /**
   * That long string from mlab
   */
  databaseURL: process.env.MONGODB_URI,

  /**
   * Your secret sauce
   */
  jwtSecret: process.env.JWT_SECRET,

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'silly',
  },

  /**
   * API configs
   */
  api: {
    prefix: '/api',
  },

  controller: {
    importacaoGlx: {
      name: "ImportacaoGlxController",
      path: "../controllers/importacaoGlxController"
    },
    percurso: {
      name: "PercursoController",
      path: "../controllers/percursoController"
    },
    tipoTripulante: {
      name: "TipoTripulanteController",
      path: "../controllers/TipoTripulanteController"
    }, 
    tipoViatura: {
      name: "TipoViaturaController",
      path: "../controllers/TipoViaturaController"
    },
    no: {
      name: "NoController",
      path: "../controllers/NoController"
    },
    linha: {
      name: "LinhaController",
      path: "../controllers/LinhaController"
    },
    user: {
      name: "UserController",
      path: "../controllers/UserController"
    },
    login: {
      name: "LoginController",
      path:"../controllers/LoginController"
    }
    
  },

  repos: {
    role: {
      name: "RoleRepo",
      path: "../repos/roleRepo"
    },
    user: {
      name: "UserRepo",
      path: "../repos/UserRepo"
    },
    percurso: {
      name: "PercursoRepo",
      path: "../repos/percursoRepo"
    },
    tipoTripulante:{
      name: "TipoTripulanteRepo",
      path: "../repos/TipoTripulanteRepo"
    },
    tipoViatura:{
      name: "TipoViaturaRepo",
      path: "../repos/TipoViaturaRepo"
    },
    no:{
      name: "NoRepo",
      path: "../repos/NoRepo"
    },
    linha: {
      name: "LinhaRepo",
      path: "../repos/LinhaRepo"
    }

  },

  services: {
    role: {
      name: "RoleService",
      path: "../services/roleService"
    },
    percurso: {
      name: "PercursoService",
      path: "../services/percursoService"
    },
    tipoTripulante: {
      name: "TipoTripulanteService",
      path: "../services/TipoTripulanteService"
    },
    tipoViatura: {
      name: "TipoViaturaService",
      path: "../services/TipoViaturaService"
    },
    no: {
      name: "NoService",
      path: "../services/NoService"
    },
    linha: {
      name: "LinhaService",
      path: "../services/LinhaService"
    },
    user: {
      name: "UserService",
      path: "../services/UserService"
    },
    login: {
      name: "LoginService",
      path: "../services/LoginService"
    }
  }
};
