using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Dto;
using MDV_API.Domain;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{
    public class BlocoControllerTest
    {
        [Fact]
        public async Task Create()
        {
            // expected values to return from service 
            ActionResult<BlocoDTO> expected_bloco = new BlocoDTO
            {
                codigosViagem = new int[]{1},
                idServicoViatura = 1
            };

            // expected values to return from service 
            BlocoDTO bloco = new BlocoDTO
            {
                codigosViagem = new int[]{1},
                idServicoViatura = 1
            };

            ActionResult<InformacaoBlocoTrabalho> expected_infoBloco = new InformacaoBlocoTrabalho{
                idServicoViatura = 1,
                idBloco = 1,
                idViagem = 1
            };

            InformacaoBlocoTrabalho infoBloco = new InformacaoBlocoTrabalho{
                idServicoViatura = 1,
                idBloco = 1,
                idViagem = 1
            };
            


            // Testing setup
            var mockBlocoService = new Mock<IBlocoService>();
            mockBlocoService.Setup(service => service.AddAsync(bloco)).ReturnsAsync(expected_bloco.Value);

            var mockInfoBlocoService = new Mock<IInformacaoBlocoService>();
            mockInfoBlocoService.Setup(service => service.AddAsync(infoBloco)).ReturnsAsync(expected_infoBloco.Value);

            var bloco_controller = new BlocoController(mockBlocoService.Object, mockInfoBlocoService.Object);

            var result = await bloco_controller.Create(bloco);

            Assert.Equal(result.GetType(), expected_bloco.GetType());
            Assert.Equal(result.Value, expected_bloco.Value);

        }
    }
}
