using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Dto;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{

    public class ServicoViaturaControllerTest
    {

        [Fact]
        public async Task GetAllServicosViatura()
        {
            // expected values to return from service 
            List<ServicoViaturaDto> expected_servicos_viaturas = new List<ServicoViaturaDto>();
            ServicoViaturaDto servico_viatura1 = new ServicoViaturaDto
            {
                codServicoViatura = "SERV1",
                descricao = "Servico1",
            };
            ServicoViaturaDto servico_viatura2 = new ServicoViaturaDto
            {
                codServicoViatura = "SERV2",
                descricao = "Servico2",
            };
            ServicoViaturaDto servico_viatura3 = new ServicoViaturaDto
            {
                codServicoViatura = "SERV3",
                descricao = "Servico3",
            };
            expected_servicos_viaturas.Add(servico_viatura1);
            expected_servicos_viaturas.Add(servico_viatura2);
            expected_servicos_viaturas.Add(servico_viatura3);

            // Testing setup
            var mockService = new Mock<IServicoViaturaService>();
            mockService.Setup(service => service.GetAllAsync()).ReturnsAsync(expected_servicos_viaturas);

            var servicoViaturaController = new ServicoViaturaController(mockService.Object);


            var result = await servicoViaturaController.GetAllServicosViatura();

            var viewResult = Assert.IsType<List<ServicoViaturaDto>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<ServicoViaturaDto>>(
                viewResult);
            Assert.Equal(3, model.Count());
        }


         [Fact]
        public async Task GetByCodServicoViatura()
        {
            // expected values to return from service 
            ActionResult<ServicoViaturaDto> expected_servico_viatura = new ServicoViaturaDto
            {
                codServicoViatura = "SERV1",
                descricao = "Servico1",
            };

            string codServicoViatura = "SERV1";

            // Testing setup
            var mockService = new Mock<IServicoViaturaService>();
            mockService.Setup(service => service.GetByCodServicoViatura(codServicoViatura)).ReturnsAsync(expected_servico_viatura.Value);

            var servico_viatura_controller = new ServicoViaturaController(mockService.Object);

            var result = await servico_viatura_controller.GetByCodServicoViatura(codServicoViatura);

            Assert.Equal(result.GetType(), expected_servico_viatura.GetType());
            Assert.Equal(result.Value, expected_servico_viatura.Value);
        }

         [Fact]
        public async Task CreateServicoViatura()
        {
            // expected values to return from service 
            ActionResult<ServicoViaturaDto> expected_servico_viatura = new ServicoViaturaDto
            {
                codServicoViatura = "SERV1",
                descricao = "Servico1",
            };

            ServicoViaturaDto servicoViatura = new ServicoViaturaDto
            {
                codServicoViatura = "SERV1",
                descricao = "Servico1",
            };

            // Testing setup
            var mockService = new Mock<IServicoViaturaService>();
            mockService.Setup(service => service.AddAsync(servicoViatura)).ReturnsAsync(expected_servico_viatura.Value);

            var servico_viatura_controller = new ServicoViaturaController(mockService.Object);


            var result = await servico_viatura_controller.CreateViatura(servicoViatura);

            Assert.Equal(result.GetType(), expected_servico_viatura.GetType());
            Assert.Equal(result.Value, expected_servico_viatura.Value);

        }











    }

}
