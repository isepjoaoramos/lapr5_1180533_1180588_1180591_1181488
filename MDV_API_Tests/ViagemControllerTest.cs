using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Dto;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{
    public class ViagemControllerTest
    {
        [Fact]
        public async Task GetAllViagem()
        {
            // expected values to return from service 
            List<ViagemOutDto> expected_viagems = new List<ViagemOutDto>();
            ViagemOutDto viagem1 = new ViagemOutDto
            {
                Linha = "Paredes_Baltar",
                HoraSaida = 30000,
                Percurso = "PARED>VCCAR>BALTR",
            };
            ViagemOutDto viagem2 = new ViagemOutDto
            {
               Linha = "Paredes_Gandra",
                HoraSaida = 30000,
                Percurso = "PARED>VCCAR>GAND",
            };
            ViagemOutDto viagem3 = new ViagemOutDto
            {
                Linha = "Paredes_Lordelo",
                HoraSaida = 30000,
                Percurso = "PARED>VCCAR>LORDL",
            };
            expected_viagems.Add(viagem1);
            expected_viagems.Add(viagem2);
            expected_viagems.Add(viagem3);

            // Testing setup
            var mockService = new Mock<IViagemService>();
            mockService.Setup(service => service.GetAllAsync()).ReturnsAsync(expected_viagems);

            var viagem_controller = new ViagemController(mockService.Object);


            var result = await viagem_controller.GetAllViagem();

            var viewResult = Assert.IsType<List<ViagemOutDto>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<ViagemOutDto>>(
                viewResult);
            Assert.Equal(3, model.Count());
        }


       

          [Fact]
        public async Task Create()
        {
            // expected values to return from service 
            ActionResult<ViagemDto> expected_viagem = new ViagemDto
            {
                Linha = "Paredes_Baltar",
                HoraSaida = 30000,
                Percurso = "PARED>VCCAR>BALTR",
            };

            // expected values to return from service 
            ViagemDto viagem = new ViagemDto
            {
                Linha = "Paredes_Baltar",
                HoraSaida = 30000,
                Percurso = "PARED>VCCAR>BALTR",
            };

            // Testing setup
            var mockService = new Mock<IViagemService>();
            mockService.Setup(service => service.AddAsync(viagem)).ReturnsAsync(expected_viagem.Value);

            var viagem_controller = new ViagemController(mockService.Object);


            var result = await viagem_controller.Create(viagem);

            Assert.Equal(result.GetType(), expected_viagem.GetType());
            Assert.Equal(result.Value, expected_viagem.Value);

        }
    }
}
