using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Dto;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{
    public class ViaturaControllerTest
    {
        [Fact]
        public async Task GetAllViaturas()
        {
            // expected values to return from service 
            List<ViaturaDto> expected_viaturas = new List<ViaturaDto>();
            ViaturaDto viatura1 = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };
            ViaturaDto viatura2 = new ViaturaDto
            {
                matricula = "AX-76-PJ",
                vin = "WUASN8EN000694",
                cod_tipo_viatura = "123456789123456789JF",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };
            ViaturaDto viatura3 = new ViaturaDto
            {
                matricula = "AX-76-PH",
                vin = "WUASN8EN000696",
                cod_tipo_viatura = "123456789123456789JG",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };
            expected_viaturas.Add(viatura1);
            expected_viaturas.Add(viatura2);
            expected_viaturas.Add(viatura3);

            // Testing setup
            var mockService = new Mock<IViaturaService>();
            mockService.Setup(service => service.GetAllAsync()).ReturnsAsync(expected_viaturas);

            var viatura_controller = new ViaturaController(mockService.Object);


            var result = await viatura_controller.GetAllViatura();

            var viewResult = Assert.IsType<List<ViaturaDto>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<ViaturaDto>>(
                viewResult);
            Assert.Equal(3, model.Count());
        }


        [Fact]
        public async Task GetByMatricula()
        {
            // expected values to return from service 
            ActionResult<ViaturaDto> expected_viatura = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };

            string matricula = "AX-76-PL";

            // Testing setup
            var mockService = new Mock<IViaturaService>();
            mockService.Setup(service => service.GetByMatricula(matricula)).ReturnsAsync(expected_viatura.Value);

            var viatura_controller = new ViaturaController(mockService.Object);


            var result = await viatura_controller.GetByMatricula(matricula);

            Assert.Equal(result.GetType(), expected_viatura.GetType());
            Assert.Equal(result.Value, expected_viatura.Value);
        }


        [Fact]
        public async Task GetByVin()
        {
            // expected values to return from service 
            ActionResult<ViaturaDto> expected_viatura = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };

            string vin = "WUASN8EN000698";

            // Testing setup
            var mockService = new Mock<IViaturaService>();
            mockService.Setup(service => service.GetByVin(vin)).ReturnsAsync(expected_viatura.Value);

            var viatura_controller = new ViaturaController(mockService.Object);


            var result = await viatura_controller.GetByVin(vin);

            Assert.Equal(result.GetType(), expected_viatura.GetType());
            Assert.Equal(result.Value, expected_viatura.Value);

        }


          [Fact]
        public async Task CreateViatura()
        {
            // expected values to return from service 
            ActionResult<ViaturaDto> expected_viatura = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };

            // expected values to return from service 
            ViaturaDto viatura = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };

            // Testing setup
            var mockService = new Mock<IViaturaService>();
            mockService.Setup(service => service.AddAsync(viatura)).ReturnsAsync(expected_viatura.Value);

            var viatura_controller = new ViaturaController(mockService.Object);


            var result = await viatura_controller.CreateViatura(viatura);

            Assert.Equal(result.GetType(), expected_viatura.GetType());
            Assert.Equal(result.Value, expected_viatura.Value);

        }
    }
}
