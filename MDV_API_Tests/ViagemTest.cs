using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Domain;
using MDV_API.Services;
using MDV_API.Dto;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{
    public class ViagemTest
    {
        
       

          [Fact]
        public async Task Create()
        {
            NomeLinha nome = new NomeLinha("Paredes_Aguiar");
            NomeLinha nome2 = new NomeLinha("Paredes_Aguiar");
            HoraViagem hora = new HoraViagem(200);
            DescricaoPercurso percurso = new DescricaoPercurso("PARED>AGUIA");
            
            Assert.Throws<System.ArgumentException>(
                () => new HoraViagem(-1));
            Viagem viagem = new Viagem(1,nome,hora,percurso); 
            Viagem expected_viagem = new Viagem(2,nome2,hora,percurso); 

            Assert.Equal(viagem.GetType(),expected_viagem.GetType());
        }
    }
}
