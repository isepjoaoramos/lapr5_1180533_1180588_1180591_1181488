using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using MDV_API.Domain.Shared;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{

    public class ViaturaIntTest
    {

        [Fact]
        public async Task CreateViatura()
        {

            // expected values to return from service 
            ActionResult<ViaturaDto> expected_viatura = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };

            // expected values to return from service 
            ViaturaDto viatura = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };


            ActionResult<Viatura> expected_viaturas_model = new Viatura(expected_viatura.Value);

            Viatura viatura_model = new Viatura(expected_viatura.Value);


            //Testing setup mocks

            var mockRepo = new Mock<IViaturaRepository>();

            mockRepo.Setup(v => v.AddAsync(viatura_model)).ReturnsAsync(expected_viaturas_model.Value);

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(u => u.CommitAsync());

            var viaturaService = new ViaturaService(mockUnitOfWork.Object, mockRepo.Object);

            var viaturaController = new ViaturaController(viaturaService);


            var result = await viaturaController.CreateViatura(viatura);

            Assert.Equal(result.GetType(), expected_viatura.GetType());
            Assert.Equal(result.Value.matricula, expected_viatura.Value.matricula);
        }

        [Fact]
        public async Task GetAllViaturas()
        {
            ActionResult<List<Viatura>> list_expected_viaturas = new List<Viatura>();

            List<ViaturaDto> list_expected_viaturas_dto = new List<ViaturaDto>();


            ViaturaDto viatura1 = new ViaturaDto
            {
                matricula = "AX-76-PL",
                vin = "WUASN8EN000698",
                cod_tipo_viatura = "123456789123456789JH",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };
            ViaturaDto viatura2 = new ViaturaDto
            {
                matricula = "AX-76-PJ",
                vin = "WUASN8EN000694",
                cod_tipo_viatura = "123456789123456789JF",
                data_entrada_servico = new DateTime(2020, 11, 20)
            };

            list_expected_viaturas_dto.Add(viatura1);
            list_expected_viaturas_dto.Add(viatura2);



            // expected values to return from repo 
            Viatura expected_viatura = new Viatura(viatura1);

            // expected values to return from repo 
            Viatura expected_viatura_2 = new Viatura(viatura2);


            list_expected_viaturas.Value.Add(expected_viatura);

            list_expected_viaturas.Value.Add(expected_viatura_2);


            //Testing setup mocks

            var mockRepo = new Mock<IViaturaRepository>();

            mockRepo.Setup(v => v.GetAllAsync()).ReturnsAsync(list_expected_viaturas.Value);

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(u => u.CommitAsync());

            var viaturaService = new ViaturaService(mockUnitOfWork.Object, mockRepo.Object);

            var viaturaController = new ViaturaController(viaturaService);


            var result = await viaturaController.GetAllViatura();

            ViaturaDto[] result_array = result.ToArray();

            ViaturaDto[] list_expected_viaturas_dto_array = list_expected_viaturas_dto.ToArray();


            Assert.Equal(result_array.GetType(), list_expected_viaturas_dto_array.GetType());
            Assert.Equal(result_array[0].matricula, list_expected_viaturas_dto_array[0].matricula);
            Assert.Equal(result_array[1].matricula, list_expected_viaturas_dto_array[1].matricula);
        }
    }

}
