using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using MDV_API.Domain.Shared;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{

    public class ViagemIntTest
    {

        [Fact]
        public async Task CreateViagem()
        {

             ActionResult<List<Viagem>> list_expected_viagens = new List<Viagem>();
            // expected values to return from service 
            ActionResult<ViagemDto> expected_viagem = new ViagemDto
            {
                Linha = "Paredes_Gandra",
                HoraSaida = 1234,
                Percurso = "PARED>GAND"
            };

            // expected values to return from service 
            ViagemDto viagem = new ViagemDto
            {
                Linha = "Paredes_Gandra",
                HoraSaida = 1234,
                Percurso = "PARED>GAND"
            };


            ActionResult<Viagem> expected_viagens_model = new Viagem(expected_viagem.Value);

            Viagem viagem_model = new Viagem(expected_viagem.Value);


            //Testing setup mocks

            var mockRepo = new Mock<IViagemRepository>();

            mockRepo.Setup(v => v.AddAsync(viagem_model)).ReturnsAsync(expected_viagens_model.Value);

            mockRepo.Setup(v => v.GetAllAsync()).ReturnsAsync(list_expected_viagens.Value);

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(u => u.CommitAsync());

            var viagemService = new ViagemService(mockUnitOfWork.Object, mockRepo.Object);

            var viagemController = new ViagemController(viagemService);


            var result = await viagemController.Create(viagem);

            Assert.Equal(result.GetType(), expected_viagem.GetType());
            Assert.Equal(result.Value.Linha, expected_viagem.Value.Linha);
        }

        [Fact]
        public async Task GetAllViagem()
        {
            ActionResult<List<Viagem>> list_expected_viagens = new List<Viagem>();

            List<ViagemDto> list_expected_viagens_dto = new List<ViagemDto>();


            ViagemDto viagem1 = new ViagemDto
            {
                //viagemId = 1,
                 Linha = "Paredes_Gandra",
                HoraSaida = 1234,
                Percurso = "PARED>GAND"
            };
            ViagemDto viagem2 = new ViagemDto
            {
               // viagemId = 2,
                 Linha = "Paredes_Gandra",
                HoraSaida = 1234,
                Percurso = "PARED>VANDO>GAND"
            };

            list_expected_viagens_dto.Add(viagem1);
            list_expected_viagens_dto.Add(viagem2);



            // expected values to return from repo 
            Viagem expected_viagem = new Viagem(viagem1);

            // expected values to return from repo 
            Viagem expected_viagem_2 = new Viagem(viagem2);


            list_expected_viagens.Value.Add(expected_viagem);

            list_expected_viagens.Value.Add(expected_viagem_2);


            //Testing setup mocks

            var mockRepo = new Mock<IViagemRepository>();

            mockRepo.Setup(v => v.GetAllAsync()).ReturnsAsync(list_expected_viagens.Value);

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(u => u.CommitAsync());

            var viagemService = new ViagemService(mockUnitOfWork.Object, mockRepo.Object);

            var viagemController = new ViagemController(viagemService);


            var result = await viagemController.GetAllViagem();

            ViagemOutDto[] result_array = result.ToArray();

            ViagemDto[] list_expected_viagens_dto_array = list_expected_viagens_dto.ToArray();


           
            Assert.Equal(result_array[0].Linha, list_expected_viagens_dto_array[0].Linha);
            Assert.Equal(result_array[1].Linha, list_expected_viagens_dto_array[1].Linha);
        }
    }

}
