using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Infrastructure;
using MDV_API.Dto;
using MDV_API.Domain;
using MDV_API.Domain.Shared;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{

    public class TripulanteTest
    {

        [Fact]
        public async Task CreateTripulante()
        {

            TipoTripulanteDto tipo1 = new TipoTripulanteDto
            {
                tipoTripulanteId = 1,
                codigo = "codigoTeste1",
                descricao = "descricao1"
            };

            TipoTripulanteDto tipo2 = new TipoTripulanteDto
            {
                tipoTripulanteId = 2,
                codigo = "codigoTeste2",
                descricao = "descricao2"
            };

            List<TipoTripulanteDto> tiposTripulanteList = new List<TipoTripulanteDto>();
            tiposTripulanteList.Add(tipo1);
            tiposTripulanteList.Add(tipo2);

            TipoTripulanteDto[] tiposTripulante = tiposTripulanteList.ToArray();



            // expected values to return from service 
            ActionResult<TripulanteDto> expected_tripulante = new TripulanteDto
            {
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                tiposTripulante = tiposTripulante,
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };

            // expected values to return from service 
            TripulanteDto tripulante = new TripulanteDto
            {
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                tiposTripulante = tiposTripulante,
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };


            ActionResult<Tripulante> expected_tripulante_model = new Tripulante(expected_tripulante.Value);

            Tripulante tripulante_model = new Tripulante(expected_tripulante.Value);


            //Testing setup mocks

            var mockRepo = new Mock<ITripulanteRepository>();
            var mockRepo_2 = new Mock<ITiposTripulante_TripulanteRepository>();
            var mockRepo_3 = new Mock<ITipoTripulanteRepository>();

            mockRepo.Setup(r => r.AddAsync(tripulante_model)).ReturnsAsync(expected_tripulante_model.Value);

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(u => u.CommitAsync());

            var tripulanteService = new TripulanteService(mockUnitOfWork.Object, mockRepo.Object, mockRepo_2.Object, mockRepo_3.Object);

            var tripulanteController = new TripulanteController(tripulanteService);


            var result = await tripulanteController.createTripulante(tripulante);

            Assert.Equal(result.GetType(), expected_tripulante.GetType());
            Assert.Equal(result.Value.numMecanografico, expected_tripulante.Value.numMecanografico);
        }

       /*  [Fact]
        public async Task GetAllTripulantes()
        {
            TipoTripulanteDto tipo1 = new TipoTripulanteDto
            {
                tipoTripulanteId = 1,
                codigo = "codigoTeste1",
                descricao = "descricao1"
            };

            TipoTripulanteDto tipo2 = new TipoTripulanteDto
            {
                tipoTripulanteId = 2,
                codigo = "codigoTeste2",
                descricao = "descricao2"
            };

            List<TipoTripulanteDto> tiposTripulanteList = new List<TipoTripulanteDto>();
            tiposTripulanteList.Add(tipo1);
            tiposTripulanteList.Add(tipo2);

            TipoTripulanteDto[] tiposTripulante = tiposTripulanteList.ToArray();

            ActionResult<List<Tripulante>> list_expected_tripulantes = new List<Tripulante>();

            List<TripulanteDto> list_expected_tripulantes_dto = new List<TripulanteDto>();


            TripulanteDto tripulante1 = new TripulanteDto
            {
                tripulanteId = 0,
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                tiposTripulante = tiposTripulante,
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };
            TripulanteDto tripulante2 = new TripulanteDto
            {
                tripulanteId = 1,
                numMecanografico = "123456781",
                nome = "Jane Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234560,
                nif = 123456780,
                numCartaConducao = "1235467",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                tiposTripulante = tiposTripulante,
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };

            list_expected_tripulantes_dto.Add(tripulante1);
            list_expected_tripulantes_dto.Add(tripulante2);



            // expected values to return from repo 
            Tripulante expected_tripulante_1 = new Tripulante(tripulante1);

            // expected values to return from repo 
            Tripulante expected_tripulante_2 = new Tripulante(tripulante2);

            list_expected_tripulantes.Value.Add(expected_tripulante_1);

            list_expected_tripulantes.Value.Add(expected_tripulante_2);


            //Testing setup mocks

            var mockRepo = new Mock<ITripulanteRepository>();
            var mockRepo_2 = new Mock<ITiposTripulante_TripulanteRepository>();
            var mockRepo_3 = new Mock<ITipoTripulanteRepository>();

            
            mockRepo.Setup(v => v.GetAllAsync()).ReturnsAsync(list_expected_tripulantes.Value);

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(u => u.CommitAsync());

            var tripulanteService = new TripulanteService(mockUnitOfWork.Object, mockRepo.Object, mockRepo_2.Object, mockRepo_3.Object);

            var tripulanteController = new TripulanteController(tripulanteService);


            var result = await tripulanteController.getAllTripulante();

            TripulanteDto[] result_array = result.ToArray();

            TripulanteDto[] list_expected_tripulantes_dto_array = list_expected_tripulantes_dto.ToArray();


            Assert.Equal(result_array.GetType(), list_expected_tripulantes_dto_array.GetType());
            Assert.Equal(result_array[0].numMecanografico, list_expected_tripulantes_dto_array[0].numMecanografico);
            Assert.Equal(result_array[1].numMecanografico, list_expected_tripulantes_dto_array[1].numMecanografico);
        }

        [Fact]
        public async Task GetByNumMecanografico()
        {
            List<String> tiposTripulante = new List<string>(new string[] { "Camionista Junior", "Noturno", "Diário" });

            // expected values to return from service 
            ActionResult<TripulanteDto> expected_tripulante = new TripulanteDto
            {
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };

            string numMecanograficoToSearch = "123456780";

            // expected values to return from repo 
            Tripulante tripulante_created = new Tripulante(expected_tripulante.Value);


            //Testing setup mocks

            var mockRepo = new Mock<ITripulanteRepository>();
            var mockRepo_2 = new Mock<ITiposTripulante_TripulanteRepository>();
            var mockRepo_3 = new Mock<ITipoTripulanteRepository>();

            mockRepo.Setup(v => v.GetByNumMecanograficoAsync(numMecanograficoToSearch)).ReturnsAsync(tripulante_created);

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(u => u.CommitAsync());

            var tripulanteService = new TripulanteService(mockUnitOfWork.Object, mockRepo.Object, mockRepo_2.Object, mockRepo_3.Object);

            var tripulanteController = new TripulanteController(tripulanteService);


            var result = await tripulanteController.GetByNumMecanograficoAsync(numMecanograficoToSearch);

            Assert.Equal(result.GetType(), expected_tripulante.GetType());
            Assert.Equal(result.Value.numMecanografico, expected_tripulante.Value.numMecanografico);
        }*/

    } 

}
