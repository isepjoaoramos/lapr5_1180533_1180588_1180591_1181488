using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Dto;
using MDV_API.Domain;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{
    public class TripulanteControllerTest
    {
        [Fact]
        public async Task GetAllTripulantes()
        {
            // expected values to return from service 
            List<TripulanteDto> expected_tripulantes = new List<TripulanteDto>();
            TripulanteDto tripulante1 = new TripulanteDto
            {
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };
            TripulanteDto tripulante2 = new TripulanteDto
            {
                numMecanografico = "123456781",
                nome = "Jane Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234560,
                nif = 123456780,
                numCartaConducao = "1235467",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };
            TripulanteDto tripulante3 = new TripulanteDto
            {
                numMecanografico = "123456782",
                nome = "Dan Days",
                dataNascimento = new DateTime(1985, 08, 15),
                numCartaoCidadao = 1234561,
                nif = 123456781,
                numCartaConducao = "1235468",
                dataEmissaoCartaConducao = new DateTime(1996, 02, 10),
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2022, 11, 12)
            };
            expected_tripulantes.Add(tripulante1);
            expected_tripulantes.Add(tripulante2);
            expected_tripulantes.Add(tripulante3);

            // Testing setup
            var mockService = new Mock<ITripulanteService>();
            mockService.Setup(service => service.GetAllAsync()).ReturnsAsync(expected_tripulantes);

            var tripulante_controller = new TripulanteController(mockService.Object);


            var result = await tripulante_controller.getAllTripulante();

            var viewResult = Assert.IsType<List<TripulanteDto>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<TripulanteDto>>(
                viewResult);
            Assert.Equal(3, model.Count());
        }


        [Fact]
        public async Task GetByNumMecanografico()
        {

            TipoTripulanteDto tipo1 = new TipoTripulanteDto
            {
                tipoTripulanteId = 1,
                codigo = "codigoTeste1",
                descricao = "descricao1"
            };

            TipoTripulanteDto tipo2 = new TipoTripulanteDto
            {
                tipoTripulanteId = 2,
                codigo = "codigoTeste2",
                descricao = "descricao2"
            };

            List<TipoTripulanteDto> tiposTripulanteList = new List<TipoTripulanteDto>();
            tiposTripulanteList.Add(tipo1);
            tiposTripulanteList.Add(tipo2);
            
            TipoTripulanteDto[] tiposTripulante = tiposTripulanteList.ToArray();


            // expected values to return from service 
            ActionResult<TripulanteDto> expected_tripulante = new TripulanteDto
            {
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                tiposTripulante = tiposTripulante,
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };

            string numMecanografico = "123456780";

            NumMecanografico num = new NumMecanografico("123456780");

            // Testing setup
            var mockService = new Mock<ITripulanteService>();
            mockService.Setup(service => service.GetByNumMecanograficoAsync(numMecanografico)).ReturnsAsync(expected_tripulante.Value);

            var tripulante_controller = new TripulanteController(mockService.Object);


            var result = await tripulante_controller.GetByNumMecanograficoAsync(num.numMecanografico);
            Console.WriteLine(expected_tripulante.Value);
            Assert.Equal(result.GetType(), expected_tripulante.GetType());
            Assert.Equal(result.Value, expected_tripulante.Value);
        }


        [Fact]
        public async Task CreateTripulante()
        {
            List<String> tiposTripulante = new List<string>(new string[] { "Camionista Junior", "Noturno", "Diário" });

            // expected values to return from service 
            ActionResult<TripulanteDto> expected_tripulante = new TripulanteDto
            {
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };

            // expected values to return from service 
            TripulanteDto tripulante = new TripulanteDto
            {
                numMecanografico = "123456780",
                nome = "John Doe",
                dataNascimento = new DateTime(1990, 08, 15),
                numCartaoCidadao = 1234567,
                nif = 123456789,
                numCartaConducao = "123546",
                dataEmissaoCartaConducao = new DateTime(1995, 09, 10),
                dataEntradaEmpresa = new DateTime(1999, 10, 12),
                dataSaidaEmpresa = new DateTime(2021, 11, 12)
            };

            // Testing setup
            var mockService = new Mock<ITripulanteService>();
            mockService.Setup(service => service.AddAsync(tripulante)).ReturnsAsync(expected_tripulante.Value);

            var tripulante_controller = new TripulanteController(mockService.Object);


            var result = await tripulante_controller.createTripulante(tripulante);

            Assert.Equal(result.GetType(), expected_tripulante.GetType());
            Assert.Equal(result.Value, expected_tripulante.Value);

        }
    }
}
