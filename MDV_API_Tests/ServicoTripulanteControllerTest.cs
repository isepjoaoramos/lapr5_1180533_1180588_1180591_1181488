using System;
using Xunit;
using MDV_API.Controllers;
using MDV_API.Services;
using MDV_API.Dto;
using System.Collections.Generic;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MDV_API_Tests
{

    public class ServicoTripulnateControllerTest
    {

        [Fact]
        public async Task GetAllServicosTripulante()
        {
            // expected values to return from service 
            List<ServicoTripulanteDto> expected_servicos_tripulantes = new List<ServicoTripulanteDto>();
            ServicoTripulanteDto servico_tripulante1 = new ServicoTripulanteDto
            {
                codServicotripulante = "SERV1",
            };
            ServicoTripulanteDto servico_tripulante2 = new ServicoTripulanteDto
            {
                codServicotripulante = "SERV2",
            };
           ServicoTripulanteDto servico_tripulante3 = new ServicoTripulanteDto
            {
                codServicotripulante = "SERV3",
            };
            expected_servicos_tripulantes.Add(servico_tripulante1);
            expected_servicos_tripulantes.Add(servico_tripulante2);
            expected_servicos_tripulantes.Add(servico_tripulante3);

            // Testing setup
            var mockService = new Mock<IServicoTripulanteService>();
            mockService.Setup(service => service.GetAllAsync()).ReturnsAsync(expected_servicos_tripulantes);

            var servicoTripulanteController = new ServicoTripulanteController(mockService.Object);


            var result = await servicoTripulanteController.GetAllServicosTripulante();

            var viewResult = Assert.IsType<List<ServicoTripulanteDto>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<ServicoTripulanteDto>>(
                viewResult);
            Assert.Equal(3, model.Count());
        }


         [Fact]
        public async Task GetByCodServicoTripulante()
        {
            // expected values to return from service 
            ActionResult<ServicoTripulanteDto> expected_servico_tripulante = new ServicoTripulanteDto
            {
                codServicotripulante = "SERV1",
                };

            string codServicotripulante = "SERV1";

            // Testing setup
            var mockService = new Mock<IServicoTripulanteService>();
            mockService.Setup(service => service.GetByCodServicoTripulante(codServicotripulante)).ReturnsAsync(expected_servico_tripulante.Value);

            var servico_tripulante_controller = new ServicoTripulanteController(mockService.Object);

            var result = await servico_tripulante_controller.GetByCodServicoTripulante(codServicotripulante);

            Assert.Equal(result.GetType(), expected_servico_tripulante.GetType());
            Assert.Equal(result.Value, expected_servico_tripulante.Value);
        }

         [Fact]
        public async Task CreateServicoTripulante()
        {
            // expected values to return from service 
            ActionResult<ServicoTripulanteDto> expected_servico_tripulante = new ServicoTripulanteDto
            {
                codServicotripulante = "SERV1",
            };

            ServicoTripulanteDto servicotripulante = new ServicoTripulanteDto
            {
                codServicotripulante = "SERV1",
            };

            // Testing setup
            var mockService = new Mock<IServicoTripulanteService>();
            mockService.Setup(service => service.AddAsync(servicotripulante)).ReturnsAsync(expected_servico_tripulante.Value);

            var servico_tripulante_controller = new ServicoTripulanteController(mockService.Object);


            var result = await servico_tripulante_controller.CreateServicoTripulante(servicotripulante);

            Assert.Equal(result.GetType(), expected_servico_tripulante.GetType());
            Assert.Equal(result.Value, expected_servico_tripulante.Value);

        }











    }

}
