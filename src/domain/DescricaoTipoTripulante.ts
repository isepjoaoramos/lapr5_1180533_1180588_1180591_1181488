
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface DescricaoTipoTripulanteProps {
  value: string;
}

export class DescricaoTipoTripulante extends ValueObject<DescricaoTipoTripulanteProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: DescricaoTipoTripulanteProps) {
    super(props);
  }

  public static isAppropriateLength(value: string): boolean {
    if (value.length <= 250) {
      return true;
    }
    return false;
  }

  public static create(descricao: string): Result<DescricaoTipoTripulante> {
    const propsResult = Guard.againstNullOrUndefined(descricao, 'descricao');

    if (!propsResult.succeeded) {
      return Result.fail<DescricaoTipoTripulante>(propsResult.message);
    } else {

      if (!this.isAppropriateLength(descricao)
      ) {
        return Result.fail<DescricaoTipoTripulante>('A Descrição não vai de encontro com os parâmetros [max 250 caracteres]');
      }

      return Result.ok<DescricaoTipoTripulante>(new DescricaoTipoTripulante({
        value: descricao,
      }));
    }
  }
}