
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface TipoViaturaDescricaoProps {
  value: string;
}

export class TipoViaturaDescricao extends ValueObject<TipoViaturaDescricaoProps> {
  get value (): string {
    return this.props.value;
  }
  
  private constructor (props: TipoViaturaDescricaoProps) {
    super(props);
  }

  public static create (descricao: string): Result<TipoViaturaDescricao> {

    const guardResult = Guard.againstNullOrUndefined(descricao, 'descricao');

    if (!guardResult.succeeded || descricao.length > 250) {
      return Result.fail<TipoViaturaDescricao>(guardResult.message);
    } else {
      return Result.ok<TipoViaturaDescricao>(new TipoViaturaDescricao({ value: descricao }))
    }
  }
}