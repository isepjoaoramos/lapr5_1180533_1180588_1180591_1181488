
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface CorProps {
  R: number;
  G: number;
  B: number;
}

export class Cor extends ValueObject<CorProps> {
  get red(): number {
    return this.props.R;
  }

  get green(): number {
    return this.props.G;
  }

  get blue(): number {
    return this.props.B;
  }

  public constructor(props: CorProps) {
    super(props);
  }

  static create(props : CorProps): Cor {
    return new Cor(props);
  }
}