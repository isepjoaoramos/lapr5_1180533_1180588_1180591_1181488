
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface CorLinhaProps {
    vermelho: number;
    verde: number;
    azul: number;
}

export class CorLinha extends ValueObject<CorLinhaProps> {


    get vermelho (): number {
    return this.props.vermelho;
  }
  
    get verde (): number {
    return this.props.verde;
    }

    get azul (): number {
    return this.props.azul;
    }

  private constructor (props: CorLinhaProps) {
    super(props);
  }

  public static create (vermelho: number, verde: number, azul: number): Result<CorLinha> {

    const guardResultVermelho = Guard.againstNullOrUndefined(vermelho, 'vermelho');
    const guardResultVerde = Guard.againstNullOrUndefined(verde, 'verde');
    const guardResultAzul = Guard.againstNullOrUndefined(azul, 'azul');


    if (!guardResultVermelho.succeeded || !guardResultVerde.succeeded || !guardResultAzul.succeeded
        || (vermelho<0 || vermelho>255 ) || (verde<0 || verde>255) || (azul<0 || azul>255)) {
      return Result.fail<CorLinha>("ERRO: Criação da cor linha, parâmetros errados");
    } else {
      return Result.ok<CorLinha>(new CorLinha({ vermelho: vermelho, verde: verde, azul: azul }))
    }
  }
}