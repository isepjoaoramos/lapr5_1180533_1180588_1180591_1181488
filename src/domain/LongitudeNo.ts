
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface LongitudeNoProps {
  value: number;
}

export class LongitudeNo extends ValueObject<LongitudeNoProps> {
  get value(): number {
    return this.props.value;
  }

  private constructor(props: LongitudeNoProps) {
    super(props);
  }

  public static isAppropriateLength(value: number): boolean {
    if (value < 180 && value >-180) {
      return true;
    }
    return false;
  }

  static create(longitude: number): Result<LongitudeNo> {

    const propsResult = Guard.againstNullOrUndefined(longitude, 'longitude');

    if (!propsResult.succeeded) {
      return Result.fail<LongitudeNo>(propsResult.message);
    } else {

      if (!this.isAppropriateLength(longitude)
      ) {
        return Result.fail<LongitudeNo>('Longitude inválida');
      }

      return Result.ok<LongitudeNo>(new LongitudeNo({
        value: longitude,
      }));
    }
  }
}