
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";

interface RestricoesLinhaProps {
  value: string;
}

export class RestricoesLinha extends ValueObject<RestricoesLinhaProps> {
  get value (): string {
    return this.props.value;
  }
  
  private constructor (props: RestricoesLinhaProps) {
    super(props);
  }

  public static create (descricao: string): Result<RestricoesLinha> {

     if(descricao.length == 0){
        return Result.ok<RestricoesLinha>(new RestricoesLinha({ value: 'omissao' }))
     }else{
        return Result.ok<RestricoesLinha>(new RestricoesLinha({ value: descricao }))
     }
    }
  }