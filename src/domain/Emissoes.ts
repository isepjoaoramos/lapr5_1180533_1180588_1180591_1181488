import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface EmissoesProps {
  value: number;
}

export class Emissoes extends ValueObject<EmissoesProps> {
  get value (): number {
    return this.props.value;
  }
  
  private constructor (props: EmissoesProps) {
    super(props);
  }

  public static create (valor: number): Result<Emissoes> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || valor <= 0) {
      return Result.fail<Emissoes>(guardResult.message);
    } else {
      return Result.ok<Emissoes>(new Emissoes({ value: valor }))
    }
  }
}