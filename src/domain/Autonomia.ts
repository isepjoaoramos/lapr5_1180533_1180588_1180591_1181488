import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface AutonomiaProps {
  value: number;
}

export class Autonomia extends ValueObject<AutonomiaProps> {
  get value (): number {
    return this.props.value;
  }
  
  private constructor (props: AutonomiaProps) {
    super(props);
  }

  public static create (valor: number): Result<Autonomia> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || !Number.isInteger(valor)) {
      return Result.fail<Autonomia>(guardResult.message);
    } else {
      return Result.ok<Autonomia>(new Autonomia({ value: valor }))
    }
  }
}