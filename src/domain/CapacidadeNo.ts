
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface CapacidadeNoProps {
  value: number;
}

export class CapacidadeNo extends ValueObject<CapacidadeNoProps> {
  get value(): number {
    return this.props.value;
  }

  private constructor(props: CapacidadeNoProps) {
    super(props);
  }

  public static isValid(value: number): boolean {
    if (value >= 0) {
      return true;
    }
    return false;
  }

  static create(capacidade: number): Result<CapacidadeNo> {

    const propsResult = Guard.againstNullOrUndefined(capacidade, 'capacidade');

    if (!propsResult.succeeded) {
      return Result.fail<CapacidadeNo>(propsResult.message);
    } else {

      if (!this.isValid(capacidade)
      ) {
        return Result.fail<CapacidadeNo>('A capacidade inserida não é válida (<0)');
      }

      return Result.ok<CapacidadeNo>(new CapacidadeNo({
        value: capacidade,
      }));
    }
  }
}