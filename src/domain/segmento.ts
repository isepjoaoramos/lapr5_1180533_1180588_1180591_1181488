import { Entity } from "../core/domain/Entity";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Guard } from "../core/logic/Guard";
import { Result } from "../core/logic/Result";
import ISegmentoDTO from "../dto/ISegmentoDTO";
import { AbreviaturaNo } from "./AbreviaturaNo";
import { Distancia } from "./distancia";
import { Duracao } from "./duracao";
import { Ordem } from "./Ordem";


interface SegmentoProps {
    no1: string;
    no2: string;
    distancia: number;
    duracao: number;
    ordem: number
}

export class Segmento extends Entity<SegmentoProps>{

    get no1 (): string {
        return this.props.no1
      }

      get no2 (): string {
        return this.props.no2
      }  

      get distancia (): Number {
        return this.props.distancia
      }

      get duracao (): Number {
        return this.props.duracao
      }

      get ordem (): number {
        return this.props.ordem
      }

      public constructor(props: SegmentoProps, id?: UniqueEntityID) {
        super(props, id);
      }
    
    public static create (segmentoDTO: ISegmentoDTO, id?: UniqueEntityID): Result<Segmento> {
        const guardedProps = [
          {argument: segmentoDTO.no1, argumentName: 'no1'},
          {argument: segmentoDTO.no2, argumentName: 'no2'},
          {argument: segmentoDTO.distancia, argumentName: 'distancia'},
          {argument: segmentoDTO.duracao, argumentName: 'duracao'},
          {argument: segmentoDTO.ordem, argumentName: 'ordem'},
        ];

      const no1 = segmentoDTO.no1;
      const no2 = segmentoDTO.no2;
      const distancia = segmentoDTO.distancia;
      const duracao = segmentoDTO.duracao;
      const ordem = (segmentoDTO.ordem);
    
        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);
    
        //const name = percursoDTO.name;
    
        if (!guardResult.succeeded) {
          return Result.fail<Segmento>(guardResult.message)
        }     
        else {
          const props = {no1: no1, no2: no2, distancia: distancia, duracao: duracao, ordem: ordem };

          const segmento = new Segmento(
            props
          , id);
    
          return Result.ok<Segmento>(segmento);
        }
    
      }
}