import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface DistanciaProps {
  value: number;
}

export class Distancia extends ValueObject<DistanciaProps> {
  get valorDistancia (): number {
    return this.props.value;
  }
  
  private constructor (props: DistanciaProps) {
    super(props);
  }

  public static create (valor: number): Result<Distancia> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || valor <= 0) {
      return Result.fail<Distancia>(guardResult.message);
    } else {
      return Result.ok<Distancia>(new Distancia({ value: valor }))
    }
  }
}