
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface CodTipoViaturaProps {
  value: string;
}

export class CodTipoViatura extends ValueObject<CodTipoViaturaProps> {
  get value (): string {
    return this.props.value;
  }
  
  private constructor (props: CodTipoViaturaProps) {
    super(props);
  }

  public static create (codigo: string): Result<CodTipoViatura> {

    const guardResult = Guard.againstNullOrUndefined(codigo, 'codigo');

    if (!guardResult.succeeded || codigo.length != 20) {
      return Result.fail<CodTipoViatura>(''+guardResult.message);
    } else {
      return Result.ok<CodTipoViatura>(new CodTipoViatura({ value: codigo }));
    }
  }
}