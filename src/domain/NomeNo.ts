
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface NomeNoProps {
  value: string;
}

export class NomeNo extends ValueObject<NomeNoProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: NomeNoProps) {
    super(props);
  }

  public static isAppropriateLength(value: string): boolean {
    if (value.length <= 200) {
      return true;
    }
    return false;
  }

  static create(nome: string): Result<NomeNo> {

    const propsResult = Guard.againstNullOrUndefined(nome, 'nome');

    if (!propsResult.succeeded) {
      return Result.fail<NomeNo>(propsResult.message);
    } else {

      if (!this.isAppropriateLength(nome)
      ) {
        return Result.fail<NomeNo>('O Nome não vai de encontro com os parâmetros [max 200 caracteres]');
      }

      return Result.ok<NomeNo>(new NomeNo({
        value: nome,
      }));
    }
  }
}