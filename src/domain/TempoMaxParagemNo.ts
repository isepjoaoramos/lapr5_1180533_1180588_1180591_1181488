
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface TempoMaxParagemNoProps {
  value: number;
}

export class TempoMaxParagemNo extends ValueObject<TempoMaxParagemNoProps> {
  get value(): number {
    return this.props.value;
  }

  private constructor(props: TempoMaxParagemNoProps) {
    super(props);
  }

  public static isValid(value: number): boolean {
    if (value >= 0) {
      return true;
    }
    return false;
  }

  static create(tempo: number): Result<TempoMaxParagemNo> {

    const propsResult = Guard.againstNullOrUndefined(tempo, 'tempo');

    if (!propsResult.succeeded) {
      return Result.fail<TempoMaxParagemNo>(propsResult.message);
    } else {

      if (!this.isValid(tempo)
      ) {
        return Result.fail<TempoMaxParagemNo>('O tempo máximo de paragem inserido não é válido (<0)');
      }

      return Result.ok<TempoMaxParagemNo>(new TempoMaxParagemNo({
        value: tempo,
      }));
    }
  }
}