import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface CustoKmProps {
  value: number;
}

export class CustoKm extends ValueObject<CustoKmProps> {
  get value (): number {
    return this.props.value;
  }
  
  private constructor (props: CustoKmProps) {
    super(props);
  }

  public static create (valor: number): Result<CustoKm> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || valor <0) {
      return Result.fail<CustoKm>(guardResult.message);
    } else {
      return Result.ok<CustoKm>(new CustoKm({ value: valor }))
    }
  }
}