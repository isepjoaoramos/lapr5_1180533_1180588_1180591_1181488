import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";
import { PercursoId } from "./percursoId";
//import { No } from "./no";
import IPercursoDTO from "../dto/IPercursoDTO";
import { Guard } from "../core/logic/Guard";
import { orientacao } from "./Orientacao";
import { Segmento } from "./segmento";
import { AbreviaturaNo } from "./AbreviaturaNo";
import { No } from "./no";
import { Ordem } from "./Ordem";
import { Distancia } from "./distancia";
import { Duracao } from "./duracao";
import ISegmentoDTO from "../dto/ISegmentoDTO";


interface PercursoProps {
  orientacao: orientacao;
  distancia: number;
  descricao: string;
  duracao: number;
  linha: string;
  noInicio: string;
  noFim: string;
  segmentos: Array<ISegmentoDTO>
}

export class Percurso extends AggregateRoot<PercursoProps> {
  get id (): UniqueEntityID {
    return this._id;
  }

  get percursoId (): PercursoId {
    return PercursoId.caller(this.id);
  }

  get orientacao (): orientacao {
    return this.props.orientacao
  }

  get distancia (): number {
    return this.props.distancia
  }

  get descricao (): string {
    return this.props.descricao
  }

  get duracao (): number {
    return this.props.duracao
  }

  get linha (): string {
    return this.props.linha
  }

  get noInicio (): string {
    return this.props.noInicio
  }

  get noFim (): string {
    return this.props.noFim
  }

  get segmentos(): Array<ISegmentoDTO> {
    return this.props.segmentos
  }

  private constructor (props: PercursoProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (percursoDTO: IPercursoDTO, id?: UniqueEntityID): Result<Percurso> {

    const guardedProps = [
      {argument: percursoDTO.orientacao, argumentName: 'orientacao'},
      {argument: percursoDTO.distancia, argumentName: 'distancia'},
      {argument: percursoDTO.descricao, argumentName: 'descricao'},
      {argument: percursoDTO.duracao, argumentName: 'duracao'},
      {argument: percursoDTO.linha, argumentName: 'linha'},
      {argument: percursoDTO.noInicio, argumentName: 'noInicio'},
      {argument: percursoDTO.noFim, argumentName: 'noFim'},
      {argument: percursoDTO.segmentos, argumentName: 'segmentos'}
    ];

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

    const orientacao = percursoDTO.orientacao;
    const distancia = percursoDTO.distancia;
    const descricao = percursoDTO.descricao;
    const duracao = percursoDTO.duracao;
    const linha = percursoDTO.linha;
    const noInicio = percursoDTO.noInicio;
    const noFim = percursoDTO.noFim;
    const segmentos = percursoDTO.segmentos;
    
    var nosCertos = true;
    for (var i = 0; i < segmentos.length-1 ; i++){
      if(!(segmentos[i].no2 == segmentos[i+1].no1)){
        console.log("nosCertos");
        nosCertos = false;
      }
    }
    
    var ordemSegmentos = true;
    for (var j = 0; j < segmentos.length-1 ; j++){
      if((segmentos[j].ordem != segmentos[j+1].ordem -1)){
        console.log("ordemSegmentos");
        ordemSegmentos = false;
      } //console.log(segmentos[j].ordem);
    }
    
    var distanciasDuracoesNegativas = false;
    for (var k = 0; k < segmentos.length ; k++){
      //console.log(segmentos[k].distancia);
      if(segmentos[k].distancia <= 0 || segmentos[k].duracao <= 0){
        distanciasDuracoesNegativas = true;
      }
    }

    var orientacaoCorreta = true;
    if(orientacao.toString() !== "Return" && orientacao.toString() !== "Go"){
      orientacaoCorreta=false;
    }

    /*var segmentosEntidade = new Array<Segmento>();
    for(var i = 0; i < segmentos.length; i++) {
      var segmento = Segmento.create(segmentos[i]).getValue();
      segmentosEntidade[i] = segmento;
    }*/

    //console.log("Percurso");
    //console.log(segmentos[0].distancia);
    if (!guardResult.succeeded || nosCertos == false || ordemSegmentos == false || segmentos[0].ordem != 1 || distanciasDuracoesNegativas == true || !orientacaoCorreta) {
      console.log("Percurso falhou");
      return Result.fail<Percurso>('Os dados de percurso estão incorretos. Percurso não guardado.')
    } else {
      //const props = {orientacao: orientacao, distancia: Distancia.create(distancia).getValue(), descricao: descricao, duracao: Duracao.create(duracao).getValue(), 
      //linha: linha, noInicio: AbreviaturaNo.create(noInicio).getValue(), noFim: AbreviaturaNo.create(noFim).getValue(), segmentos: segmentosEntidade}

      const props = {orientacao: orientacao, distancia: distancia, descricao: descricao, duracao: duracao, 
        linha: linha, noInicio: noInicio, noFim: noFim, segmentos: segmentos}

      const percurso = new Percurso(
        props
      , id);

      return Result.ok<Percurso>(percurso);
    }

  
  }
}
