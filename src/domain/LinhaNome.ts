
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface LinhaNomeProps {
  value: string;
}

export class LinhaNome extends ValueObject<LinhaNomeProps> {
  get value (): string {
    return this.props.value;
  }
  
  private constructor (props: LinhaNomeProps) {
    super(props);
  }

  public static create (descricao: string): Result<LinhaNome> {

    const guardResult = Guard.againstNullOrUndefined(descricao, 'descricao');

    if (!guardResult.succeeded) {
      return Result.fail<LinhaNome>(guardResult.message);
    } else {
      return Result.ok<LinhaNome>(new LinhaNome({ value: descricao }))
    }
  }
}