import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";
import { CodTipoViatura } from "./CodTipoViatura";

import { TipoViaturaDescricao } from "./TipoViaturaDescricao";
// @ts-ignore
import { VelocidadeMedia } from "./velocidadeMedia";
import { ConsumoMedio } from "./ConsumoMedio";
import { CustoKm } from "./CustoKm";
import { Autonomia } from "./Autonomia";
import { Combustivel } from "./Combustivel";
import { Emissoes } from "./Emissoes";

import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";

interface TipoViaturaProps {
  codTipoViatura: CodTipoViatura;
  descricao: TipoViaturaDescricao;
  velocidadeMedia: VelocidadeMedia;
  tipoCombustivel: Combustivel;
  consumoMedio: ConsumoMedio;
  custoKm: CustoKm;
  autonomia: Autonomia;
  emissoes: Emissoes; 
}

export class TipoViatura extends AggregateRoot<TipoViaturaProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get codTipoViatura(): CodTipoViatura  {
    return this.props.codTipoViatura;
  }

  get descricao(): TipoViaturaDescricao {
    return this.props.descricao;
  }

  get velocidadeMedia(): VelocidadeMedia {
    return this.props.velocidadeMedia;
  }

  get tipoCombustivel(): Combustivel {
    return this.props.tipoCombustivel;
  }

  get consumoMedio(): ConsumoMedio {
    return this.props.consumoMedio;
  }

  get custoKm(): CustoKm {
    return this.props.custoKm;
  }

  get autonomia(): Autonomia {
    return this.props.emissoes;
  }

  get emissoes(): Emissoes {
    return this.props.autonomia;
  }

  set descricao(value: TipoViaturaDescricao) {
    this.props.descricao = value;
  }

  set velocidadeMedia(value: VelocidadeMedia) {
    this.props.velocidadeMedia = value;
  }


  set consumoMedio(value: ConsumoMedio) {
    this.props.consumoMedio = value;
  }

  set custoKm(value: CustoKm) {
    this.props.custoKm = value;
  }

  set autonomia(value: Autonomia) {
    this.props.autonomia = value;
  }

  set emissoes(value: Emissoes) {
    this.props.emissoes = value;
  }

  private constructor(props: TipoViaturaProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(tipoViaturaDTO: ITipoViaturaDTO, id?: UniqueEntityID): Result<TipoViatura> {

    const codTipoViatura = CodTipoViatura.create(tipoViaturaDTO.codTipoViatura);
    const descricao = TipoViaturaDescricao.create(tipoViaturaDTO.descricao);
    const velocidade_media = VelocidadeMedia.create(tipoViaturaDTO.velocidade_media);
    const combustivel = Combustivel.create(tipoViaturaDTO.tipocombustivel);
    const consumo_medio = ConsumoMedio.create(tipoViaturaDTO.consumo_medio);
    const custo_km = CustoKm.create(tipoViaturaDTO.custo_km);
    const autonomia = Autonomia.create(tipoViaturaDTO.autonomia);
    const emissoes = Emissoes.create(tipoViaturaDTO.emissoes);

    // if (descricao.isFailure || velocidade_media.isFailure || combustivel.isFailure || consumo_medio.isFailure ||
    //   custo_km.isFailure || autonomia.isFailure || codTipoViatura.isFailure || emissoes.isFailure) {
    //   return Result.fail<TipoViatura>('ERROR: Could not create TipoViatura. Please check variables');
    // } else {
      const props = {
        codTipoViatura: codTipoViatura.getValue(),  descricao: descricao.getValue(), velocidadeMedia: velocidade_media.getValue(), tipoCombustivel: combustivel.getValue(),
        consumoMedio: consumo_medio.getValue(), custoKm: custo_km.getValue(), autonomia: autonomia.getValue(), emissoes: emissoes.getValue()
      };
      const tipoViatura = new TipoViatura(props, id);
      return Result.ok<TipoViatura>(tipoViatura);
    }
  
}
