import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class NoId extends UniqueEntityID {
    id: any;
    
    static create(id: UniqueEntityID): NoId {
        return new NoId();
    }

}