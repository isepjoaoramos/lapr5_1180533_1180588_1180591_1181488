import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";
import { tipoCombustivelEnum } from "./TipoCombustivelEnum";

interface CombustivelProps {
  value: number;
}

export class Combustivel extends ValueObject<CombustivelProps> {
  get value (): number {
    return this.props.value;
  }
  
  private constructor (props: CombustivelProps) {
    super(props);
  }

  public static create (tipocombustivel: number): Result<Combustivel> {

    const guardResult = Guard.againstNullOrUndefined(tipocombustivel, 'tipocombustivel');

     if (!guardResult.succeeded || !((tipoCombustivelEnum[tipocombustivel] === 'Gasoleo') || !(tipoCombustivelEnum[tipocombustivel] === 'GPL') || 
     !(tipoCombustivelEnum[tipocombustivel] === 'Hidrogenio') || !(tipoCombustivelEnum[tipocombustivel] === 'Eletrico') || 
     !(tipoCombustivelEnum[tipocombustivel] === 'Gasolina'))){
      console.log("Test1")
      return Result.fail<Combustivel>(guardResult.message);
    } else {
      return Result.ok<Combustivel>(new Combustivel({ value: tipocombustivel }))
    }
  }
}