
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface CodLinhaProps {
  value: string;
}

export class CodLinha extends ValueObject<CodLinhaProps> {
  get value (): string {
    return this.props.value;
  }
  
  private constructor (props: CodLinhaProps) {
    super(props);
  }

  public static create (codigo: string): Result<CodLinha> {

    const guardResult = Guard.againstNullOrUndefined(codigo, 'codigo');

    if (!guardResult.succeeded || codigo.length < 1) {
      return Result.fail<CodLinha>("ERRO: Criação do Código Linha, verficar parâmetros!");
    } else {
      return Result.ok<CodLinha>(new CodLinha({ value: codigo }));
    }
  }
}