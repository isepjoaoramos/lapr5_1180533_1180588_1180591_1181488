import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";
import { CodTipoTripulante } from "./CodTipoTripulante";
import { DescricaoTipoTripulante } from "./DescricaoTipoTripulante";

import ITipoTripulanteDTO from "../dto/ITipoTripulanteDTO";
import { Guard } from "../core/logic/Guard";


interface TipoTripulanteProps {
  codigo: CodTipoTripulante;
  descricao: DescricaoTipoTripulante;
}

export class TipoTripulante extends AggregateRoot<TipoTripulanteProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get codigo(): CodTipoTripulante {
    return this.props.codigo;
  }

  get descricao(): DescricaoTipoTripulante {
    return this.props.descricao;
  }

  set descricao(value: DescricaoTipoTripulante) {
    this.props.descricao = value;
  }


  private constructor(props: TipoTripulanteProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(tipoTripulanteDTO: ITipoTripulanteDTO, id?: UniqueEntityID): Result<TipoTripulante> {

    const codigo = CodTipoTripulante.create(tipoTripulanteDTO.codigo);

    const descricao = DescricaoTipoTripulante.create(tipoTripulanteDTO.descricao);

    if (descricao.isFailure || codigo.isFailure) {
    

      return Result.fail<TipoTripulante>('Error.Check your variables!');
    } else {
      const props = {
        codigo: codigo.getValue(),  descricao: descricao.getValue()
      };

      const tipo = new TipoTripulante(props, id); 
      
      return Result.ok<TipoTripulante>(tipo);

    }
  }
}