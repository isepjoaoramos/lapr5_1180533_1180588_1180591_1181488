import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import * as bcrypt from "bcrypt-nodejs"
//import passwordHash from 'password-hash';
import { Result } from "../core/logic/Result";
import IUserDTO from "../dto/IUserDTO";

interface UserProps {
  email: string;
  username: string;
  password: string;
  cliente:boolean;
  data_admin:boolean;
  gestor:boolean;
  data_criacao: Date;
}

export class User extends AggregateRoot<UserProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get email(): string {
    return this.props.email;
  }

  get username(): string {
    return this.props.username;
  }

  get password(): string {
    return this.props.password;
  }

  get data_criacao(): Date {
    return this.props.data_criacao;
  }

  get cliente(): boolean {
    return this.props.cliente;
  }

  get data_admin(): boolean {
    return this.props.data_admin;
  }

  get gestor(): boolean {
    return this.props.gestor;
  }

  set email(value: string) {
    this.props.email = value;
  }

  set username(value: string) {
    this.props.username = value;
  }

  set password(value: string) {
    this.props.password = value;
  }

  set cliente(value: boolean ) {
    this.props.cliente = value;
  }

  set data_admin(value: boolean ) {
    this.props.data_admin = value;
  }

  set gestor(value: boolean ) {
    this.props.gestor = value;
  }

  set data_criacao(value: Date) {
    this.props.data_criacao = value;
  }

  private constructor(props: UserProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(userDTO: IUserDTO, id?: UniqueEntityID): Result<User> {
   
    const email = userDTO.email;
    const username = userDTO.username;
    const password = userDTO.password;
    const cliente = userDTO.cliente;
    const data_admin = userDTO.data_admin;
    const gestor = userDTO.gestor;

    const data_criacao = new Date(Date.now());

    if (email.length == 0 || username.length == 0 || password.length == 0) {
      return Result.fail<User>("Error.Check your variables!");
    } else {
      const props = {
        email: email,
        username: username,
        password: password,
        cliente: cliente,
        data_admin: data_admin,
        gestor: gestor,
        data_criacao: data_criacao
      };
      const user = new User(props, id);
      
      return Result.ok<User>(user);
    }
  }
}
