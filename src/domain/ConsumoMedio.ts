import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface ConsumoMedioProps {
  value: number;
}

export class ConsumoMedio extends ValueObject<ConsumoMedioProps> {
  get value (): number {
    return this.props.value;
  }
  
  private constructor (props: ConsumoMedioProps) {
    super(props);
  }

  public static create (valor: number): Result<ConsumoMedio> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || valor <= 0) {
      return Result.fail<ConsumoMedio>(guardResult.message);
    } else {
      return Result.ok<ConsumoMedio>(new ConsumoMedio({ value: valor }))
    }
  }
}