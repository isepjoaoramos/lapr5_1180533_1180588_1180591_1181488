
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface AbreviaturaNoProps {
  value: string;
}

export class AbreviaturaNo extends ValueObject<AbreviaturaNoProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: AbreviaturaNoProps) {
    super(props);
  }

  public static isAppropriateLength(value: string): boolean {
    if (value.length <= 20) {
      return true;
    }
    return false;
  }

  static create(abreviatura: string): Result<AbreviaturaNo> {

    const propsResult = Guard.againstNullOrUndefined(abreviatura, 'abreviatura');

    if (!propsResult.succeeded) {
      return Result.fail<AbreviaturaNo>(propsResult.message);
    } else {

      if (!this.isAppropriateLength(abreviatura)
      ) {
        return Result.fail<AbreviaturaNo>('A Abreviatura não vai de encontro com os parâmetros [max 20 caracteres]');
      }

      return Result.ok<AbreviaturaNo>(new AbreviaturaNo({
        value: abreviatura,
      }));
    }
  }
}