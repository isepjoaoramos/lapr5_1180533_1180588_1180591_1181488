
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface VelocidadeMediaProps {
  value: number;
}

export class VelocidadeMedia extends ValueObject<VelocidadeMediaProps> {
  get value (): number {
    return this.props.value;
  }
  
  private constructor (props: VelocidadeMediaProps) {
    super(props);
  }

  public static create (valor: number): Result<VelocidadeMedia> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || !Number.isInteger(valor)) {
      return Result.fail<VelocidadeMedia>(guardResult.message);
    } else {
      return Result.ok<VelocidadeMedia>(new VelocidadeMedia({ value: valor }))
    }
  }
}