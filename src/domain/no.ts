import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";
import { NoId } from "./NoId";
import { NomeNo } from "./NomeNo";
import { AbreviaturaNo } from "./AbreviaturaNo";
import { CapacidadeNo } from "./CapacidadeNo";
import { TempoMaxParagemNo } from "./TempoMaxParagemNo";

const capacidadeDefault = 25;
const tempoMaxParagemDefault = 10;

import INoDTO from "../dto/INoDTO";
import { LatitudeNo } from "./LatitudeNo";
import { LongitudeNo } from "./LongitudeNo";

interface NoProps {
  nome: NomeNo;
  latitude: LatitudeNo,
  longitude: LongitudeNo,
  abreviatura: AbreviaturaNo;
  isEstacaoRecolha: boolean;
  isPontoRendicao: boolean;
  capacidade: CapacidadeNo;
  tempoMaxParagem: TempoMaxParagemNo;
  modelo: string
}

export class No extends AggregateRoot<NoProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  /*get noId (): NoId {
    return NoId.create(this.id);
  }*/

  get nome(): NomeNo {
    return this.props.nome;
  }

  get latitude(): LatitudeNo {
    return this.props.latitude;
  }

  get longitude(): LongitudeNo {
    return this.props.longitude;
  }

  get abreviatura(): AbreviaturaNo {
    return this.props.abreviatura;
  }

  get isEstacaoRecolha(): boolean {
    return this.props.isEstacaoRecolha;
  }

  get isPontoRendicao(): boolean {
    return this.props.isPontoRendicao;
  }

  get capacidade(): CapacidadeNo {
    return this.props.capacidade;
  }

  get modelo(): string {
    return this.props.modelo;
  }

  set nome(value: NomeNo) {
    this.props.nome = value;
  }

  set latitude(value: LatitudeNo) {
    this.props.latitude = value;
  }

  set longitude(value: LongitudeNo) {
    this.props.longitude = value;
  }

  set abreviatura(value: AbreviaturaNo) {
    this.props.abreviatura = value;
  }

  set isEstacaoRecolha(value: boolean) {
    this.props.isEstacaoRecolha = value;
  }

  set isPontoRendicao(value: boolean) {
    this.props.isPontoRendicao = value;
  }

  set capacidade(value: CapacidadeNo) {
    this.props.capacidade = value;
  }

  set tempMaxParagem(value: TempoMaxParagemNo) {
    this.props.tempoMaxParagem = value;
  }

  set modelo(value: string) {
    this.props.modelo = value;
  }

  private constructor(props: NoProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(noDTO: INoDTO, id?: UniqueEntityID): Result<No> {

    const nome = NomeNo.create(noDTO.nome);
    const abreviatura = AbreviaturaNo.create(noDTO.abreviatura);
    const latitude = LatitudeNo.create(noDTO.latitude);
    const longitude = LongitudeNo.create(noDTO.longitude);
    const isEstacaoRecolha = noDTO.isEstacaoRecolha;
    const isPontoRendicao = noDTO.isPontoRendicao;
    const capacidade = CapacidadeNo.create(noDTO.capacidade);
    const tempo = TempoMaxParagemNo.create(noDTO.tempoMaxParagem);
    const modelo = noDTO.modelo;

    if (nome.isFailure || abreviatura.isFailure || latitude.isFailure || longitude.isFailure || capacidade.isFailure || tempo.isFailure) {
      return Result.fail<No>('Error.Check your variables!');
    } else {
      const props = {
        nome: nome.getValue(), latitude: latitude.getValue(), longitude: longitude.getValue(), abreviatura: abreviatura.getValue(),
        isEstacaoRecolha: isEstacaoRecolha, isPontoRendicao: isPontoRendicao, capacidade: capacidade.getValue(),
        tempoMaxParagem: tempo.getValue(), modelo: modelo
      };
      const no = new No(props, id);

      return Result.ok<No>(no);

    }
  }
}