import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface OrdemProps {
  value: number;
}

export class Ordem extends ValueObject<OrdemProps> {
  get valorOrdem (): number {
    return this.props.value;
  }
  
  private constructor (props: OrdemProps) {
    super(props);
  }

  public static create (valor: number): Result<Ordem> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || !Number.isInteger(valor)) {
        console.log("Ordem errada");
      return Result.fail<Ordem>(guardResult.message);
    } else {
      return Result.ok<Ordem>(new Ordem({ value: valor }))
    }
  }
}