
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";
import { ValueObject } from "../core/domain/ValueObject";


interface CodTipoTripulanteProps {
  value: string;
}

export class CodTipoTripulante extends ValueObject<CodTipoTripulanteProps>  {

  private constructor(props: CodTipoTripulanteProps) {
    super(props)
  }

  get value(): string {
    return this.props.value;
  }


  static create(codigo: string): Result<CodTipoTripulante> {

    const guardResult = Guard.againstNullOrUndefined(codigo, 'codigo');

    if (!guardResult.succeeded || codigo.length > 20) {
      return Result.fail<CodTipoTripulante>(guardResult.message);
    } else {
      return Result.ok<CodTipoTripulante>(new CodTipoTripulante({ value: codigo }));
    }
  }
}