import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";

import ILinhaDTO from "../dto/ILinhaDTO";
import { AbreviaturaNo } from "./AbreviaturaNo";
import { LinhaNome } from "./LinhaNome";
import { CorLinha } from "./CorLinha";
import { CodLinha } from "./CodLinha";
import { RestricoesLinha } from "./RestricoesLinha";

interface LinhaProps {
  codLinha: CodLinha
  nome: LinhaNome;
  cor: CorLinha;
  noInicio: AbreviaturaNo;
  noFim: AbreviaturaNo;
  restricoes: RestricoesLinha; 
}

export class Linha extends AggregateRoot<LinhaProps> {
  get id (): UniqueEntityID {
    return this._id;
  }

  get codLinha (): CodLinha {
    return this.props.codLinha;
  }

  get nome (): LinhaNome {
    return this.props.nome;
  }

  get cor (): CorLinha {
    return this.props.cor;
  }

  get NoInicio (): AbreviaturaNo {
    return this.props.noInicio;
  }

  get NoFim (): AbreviaturaNo {
    return this.props.noFim;
  }

  get restricoes(): RestricoesLinha{
    return this.props.restricoes;
  }

  set nome ( value: LinhaNome) {
    this.props.nome = value;
  }

  set idNoInicio ( value: AbreviaturaNo) {
    this.props.noInicio = value;
  }

  set idNoFim ( value: AbreviaturaNo) {
    this.props.noFim = value;
  }

  private constructor (props: LinhaProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (linhaDTO: ILinhaDTO, id?: UniqueEntityID): Result<Linha> {
    
    const codLinha = CodLinha.create(linhaDTO.codLinha);
    const nome = LinhaNome.create(linhaDTO.nome);
    let cor = CorLinha.create(linhaDTO.vermelho, linhaDTO.verde, linhaDTO.azul);
    const no_inicio = AbreviaturaNo.create(linhaDTO.noInicio);
    const no_fim = AbreviaturaNo.create(linhaDTO.noFim);
    const restricoes = RestricoesLinha.create(linhaDTO.restricoes)

 
    if (codLinha.isFailure || nome.isFailure || cor.isFailure || no_inicio.isFailure ||
      no_fim.isFailure || restricoes.isFailure) {
      return Result.fail<Linha>('ERROR: Could not create Linha. Please check variables');
    } else {
      const props = {
        codLinha: codLinha.getValue(),  nome: nome.getValue(), cor: cor.getValue(),
        noInicio: no_inicio.getValue(), noFim: no_fim.getValue(), restricoes: restricoes.getValue()
      };
      const linha = new Linha(props, id);
      return Result.ok<Linha>(linha);
  }
}
}