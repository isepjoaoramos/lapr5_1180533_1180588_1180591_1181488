
import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface LatitudeNoProps {
  value: number;
}

export class LatitudeNo extends ValueObject<LatitudeNoProps> {
  get value(): number {
    return this.props.value;
  }

  private constructor(props: LatitudeNoProps) {
    super(props);
  }

  public static isAppropriateLength(value: number): boolean {
    if (value < 90 && value >-90) {
      return true;
    }
    return false;
  }

  static create(latitude: number): Result<LatitudeNo> {

    const propsResult = Guard.againstNullOrUndefined(latitude, 'latitude');

    if (!propsResult.succeeded) {
      return Result.fail<LatitudeNo>(propsResult.message);
    } else {

      if (!this.isAppropriateLength(latitude)
      ) {
        return Result.fail<LatitudeNo>('Latitude inválida');
      }

      return Result.ok<LatitudeNo>(new LatitudeNo({
        value: latitude,
      }));
    }
  }
}