import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface DuracaoProps {
  value: number;
}

export class Duracao extends ValueObject<DuracaoProps> {
  get valor (): number {
    return this.props.value;
  }
  
  private constructor (props: DuracaoProps) {
    super(props);
  }

  public static create (valor: number): Result<Duracao> {

    const guardResult = Guard.againstNullOrUndefined(valor, 'valor');

    if (!guardResult.succeeded || valor <= 0) {
      return Result.fail<Duracao>(guardResult.message);
    } else {
      return Result.ok<Duracao>(new Duracao({ value: valor }))
    }
  }
}