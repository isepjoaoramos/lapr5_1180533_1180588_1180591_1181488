// @ts-ignore
const mongoose = require('mongoose');

const nomeSchema = mongoose.Schema({
    nome: {
        type: String,
        required: true,
        maxLength: 200
    }
})


const codTipoTripulanteSchema = mongoose.Schema({
    codTipoTripulante: {
        type: Number,
        unique: true,
        required: [true, 'O TipoTripulante precisa de um código !']
    }
})


const abreviaturaSchema = mongoose.Schema({
    abreviatura: {
        type: String,
        required: true,
        maxLength: 20
    }
})

const isEstacaoSchema = mongoose.Schema({  // is estaçao de recolha
    isEstacao: {
        type: Boolean,
        required: true
    }
})

const isPontoSchema = mongoose.Schema({   // is ponto de rendição
    isPonto: {
        type: Boolean,
        required: true
    }
})

const latitudeSchema = mongoose.Schema({
    latitude: {
        type: Number,
        required: true
        // LATITUDE tem de seguir o formato WGS84
    }
})

const longitudeSchema = mongoose.Schema({
    longitude: {
        type: Number,
        required: true
        // LONGITUDE tem de seguir o formato WGS84

    }
})

const capacidadeSchema = mongoose.Schema({
    capacidade: {
        type: Number,
        required: true
    }
})

const tempMaxParagemSchema = mongoose.Schema({
    tempMaxParagem: {
        type: Number,
        required: true
    }
})

const noSchema = new mongoose.Schema({
    nome: [nomeSchema],
    abreviatura: [abreviaturaSchema],
    isEstacao: [isEstacaoSchema],
    isPonto: [isPontoSchema],
    latitude: [latitudeSchema],
    longitude: [longitudeSchema],
    capacidade: [capacidadeSchema],
    tempMaxParagem: [tempMaxParagemSchema]
});

module.exports = mongoose.model('No', noSchema);