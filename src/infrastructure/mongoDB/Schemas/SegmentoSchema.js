const mongoose = require('mongoose');
const Percurso = require('./Percurso');
const No = require('./No.js');

const segmentoSchema = new mongoose.Schema({
    percurso: {
        type: Schema.Types.ObjectId, ref: 'Percurso',
        required: true
    },
    ordem: [ordemSchema],
    distancia: [distanciaSchema],
    tempo: [tempoSchema],
    noInicio: {
        type: Schema.Types.ObjectId, ref: 'No',
        required: true
    },
    noFim: {
        type: Schema.Types.ObjectId, ref: 'No',
        required: true
    }
});

const ordemSchema = {
    type: Number,
    required: true
};

const distanciaSchema = {
    type: Double,
    required: true
};

const tempoSchema = {
    type: Double,
    required: true
};


module.exports = mongoose.model('Segmento', segmentoSchema);