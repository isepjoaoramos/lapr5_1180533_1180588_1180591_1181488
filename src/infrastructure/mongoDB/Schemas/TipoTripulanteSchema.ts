// @ts-ignore
const mongoose = require('mongoose');


const idTipoTripulanteSchema = mongoose.Schema({
    idTipoTripulante: {
        type: Number,
        unique: true,
        required: [true, 'O TipoTripulante precisa de um id !']
    }
})


const descricaoTipoTripulanteSchema  = mongoose.Schema({
    descricaoTipoTripulante: {
        type: String,
        required: [true, 'O TipoTripulante precisa de uma descricao !']
    }
})


const tipoTripulanteschema = mongoose.Schema({
    idTipoTripulante: [idTipoTripulanteSchema],
    descricao: [descricaoTipoTripulanteSchema]
});


module.exports = mongoose.model('TipoTripulante', tipoTripulanteschema);
