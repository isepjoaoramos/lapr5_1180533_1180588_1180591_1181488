const { Double } = require('bson');
const mongoose = require('mongoose');
const No = require('./No.js');

const percursoSchema = new mongoose.Schema({
    distancia: [distanciaSchema],
    descricao: [descricaoSchema],
    duracao: [duracaoSchema],
    noInicio: {
        type: Schema.Types.ObjectId, ref: 'No',
        required: true
    },
    noFim: {
        type: Schema.Types.ObjectId, ref: 'No',
        required: true
    }
});

const distanciaSchema = {
    type: Double,
    required: true
};

const descricaoSchema = {
    type: String,
    required: true
};

const duracaoSchema = {
    type: Number,
    required: true
};

module.exports = mongoose.model('Percurso', percursoSchema);