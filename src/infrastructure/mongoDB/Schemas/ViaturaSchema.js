const mongoose = require('mongoose');
const TipoViatura = require('./TipoViatura');


const autonomiaSchema  = mongoose.Schema({
    autonomia: {
        type: Double,
        required: [true],
        minlength: 0
    }
})

const velocidadeMediaSchema  = mongoose.Schema({
    velocidadeMedia: {
        type: Double,
        required: [true],
        minlength: 0
    }
})

const tipoCombustivelSchema  = mongoose.Schema({
    tipoCombustivel: {
        type: String,
        required: true,
        enum: ['Diesel', 'Gasolina','Elétrico','GPL','Gás']
    },
})

const custoKmSchema  = mongoose.Schema({
    custoKm: {
        type: Number,
        required: true,
        min: 0,
    },
})

const custoKmSchema  = mongoose.Schema({
    custoKm: {
        type: Number,
        required: true,
        min: 0,
        // pode ser em euros mas deverá estar preparado para outra moeda
    },
})

const consumoMediochema  = mongoose.Schema({
    consumoMedio: {
        type: double,
        required: true,       
        minlength: 0,
        // com 3  casas decimais , ex: para gasóleo L/100km enquanto que um veiculo eletrico será Kw/10Km  
    },
})


const viaturaSchema = new mongoose.Schema({
    tipoViatura: {
        type: Schema.Types.ObjectId,
        ref:'TipoViatura',
        required: true
    },
    autonomia: {
        type: [autonomiaSchema]
    },
    velocidadeMedia: {
        type: [velocidadeMediaSchema]
    },
    tipoCombustivel: {
        type: [tipoCombustivelSchema]
    },
    custoKm:{
        type: [custoKmSchema]
    },
    consumoMedio:{
        type: [consumoMediochema]
    }
    
});

module.exports = mongoose.model('Viatura', noSchema);