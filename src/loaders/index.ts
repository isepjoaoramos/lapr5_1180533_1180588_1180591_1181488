import expressLoader from "./express";
import dependencyInjectorLoader from "./dependencyInjector";
import mongooseLoader from "./mongoose";
import Logger from "./logger";

import config from "../../config";
import PercursoController from "../controllers/percursoController";
import UserSchema from "../persistence/schema/UserSchema";
import UserController from "../controllers/UserController";

export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader();
  Logger.info("✌️ DB loaded and connected!");

  /*const userSchema = {
    // compare with the approach followed in repos and services
    name: 'userSchema',
    schema: '../persistence/schemas/userSchema',
  };
*/
  const percursoSchema = {
    // compare with the approach followed in repos and services
    name: "percursoSchema",
    schema: "../persistence/schema/percursoSchema",
  };

  const TipoViaturaSchema = {
    // compare with the approach followed in repos and services
    name: "TipoViaturaSchema",
    schema: "../persistence/schema/TipoViaturaSchema",
  };

  const LinhaSchema = {
    // compare with the approach followed in repos and services
    name: "LinhaSchema",
    schema: "../persistence/schema/LinhaSchema",
  };

  const NoSchema = {
    // compare with the approach followed in repos and services
    name: "NoSchema",
    schema: "../persistence/schema/NoSchema",
  };

  const TipoTripulanteSchema = {
    // compare with the approach followed in repos and services
    name: "TipoTripulanteSchema",
    schema: "../persistence/schema/TipoTripulanteSchema",
  };

  const UserSchema = {
    // compare with the approach followed in repos and services
    name: "UserSchema",
    schema: "../persistence/schema/UserSchema",
  };

  const TipoViaturaController = {
    name: config.controller.tipoViatura.name,
    path: config.controller.tipoViatura.path,
  };

  const TipoTripulanteController = {
    name: config.controller.tipoTripulante.name,
    path: config.controller.tipoTripulante.path,
  };

  const NoController = {
    name: config.controller.no.name,
    path: config.controller.no.path,
  };

  const LinhaController = {
    name: config.controller.linha.name,
    path: config.controller.linha.path,
  };

  const ImportacaoGlxController = {
    name: config.controller.importacaoGlx.name,
    path: config.controller.importacaoGlx.path,
  };

  const percursoController = {
    name: config.controller.percurso.name,
    path: config.controller.percurso.path,
  };

  const userController = {
    name: config.controller.user.name,
    path: config.controller.user.path,
  };

  const loginController = {
    name: config.controller.login.name,
    path: config.controller.login.path,
  };

  const percursoRepo = {
    name: config.repos.percurso.name,
    path: config.repos.percurso.path,
  };

  const TipoViaturaRepo = {
    name: config.repos.tipoViatura.name,
    path: config.repos.tipoViatura.path,
  };

  const TipoTripulanteRepo = {
    name: config.repos.tipoTripulante.name,
    path: config.repos.tipoTripulante.path,
  };

  const NoRepo = {
    name: config.repos.no.name,
    path: config.repos.no.path,
  };

  const LinhaRepo = {
    name: config.repos.linha.name,
    path: config.repos.linha.path,
  };

  const UserRepo = {
    name: config.repos.user.name,
    path: config.repos.user.path,
  };

  const percursoService = {
    name: config.services.percurso.name,
    path: config.services.percurso.path,
  };

  const TipoViaturaService = {
    name: config.services.tipoViatura.name,
    path: config.services.tipoViatura.path,
  };

  const TipoTripulanteService = {
    name: config.services.tipoTripulante.name,
    path: config.services.tipoTripulante.path,
  };

  const NoService = {
    name: config.services.no.name,
    path: config.services.no.path,
  };

  const LoginService = {
    name: config.services.login.name,
    path: config.services.login.path,
  };

  const LinhaService = {
    name: config.services.linha.name,
    path: config.services.linha.path,
  };

  const UserService = {
    name: config.services.user.name,
    path: config.services.user.path,
  };

  await dependencyInjectorLoader({
    mongoConnection,
    schemas: [
      //userSchema,
      //roleSchema
      percursoSchema,
      TipoViaturaSchema,
      LinhaSchema,
      NoSchema,
      TipoTripulanteSchema,
      UserSchema,
    ],
    controllers: [
      //roleController
      percursoController,
      TipoViaturaController,
      LinhaController,
      NoController,
      TipoTripulanteController,
      ImportacaoGlxController,
      userController,
      loginController,
    ],
    repos: [
      //roleRepo,
      //userRepo
      percursoRepo,
      TipoViaturaRepo,
      LinhaRepo,
      NoRepo,
      TipoTripulanteRepo,
      UserRepo
    ],
    services: [
      //roleService
      percursoService,
      TipoViaturaService,
      LinhaService,
      NoService,
      TipoTripulanteService,
      UserService,
      LoginService,
    ],
  });
  Logger.info("✌️ Schemas, Controllers, Repositories, Services, etc. loaded");

  await expressLoader({ app: expressApp });
  Logger.info("✌️ Express loaded");
};
