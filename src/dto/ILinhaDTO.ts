import { Cor } from "../domain/Cor";

export default interface ILinhaDTO {
  codLinha: string;
  nome: string;
  vermelho: number;
  verde: number; 
  azul: number;
  noInicio: string;
  noFim: string;
  restricoes: string;
}