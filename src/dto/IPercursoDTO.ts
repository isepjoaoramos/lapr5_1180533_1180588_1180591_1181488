import { AbreviaturaNo } from "../domain/AbreviaturaNo";
import { Distancia } from "../domain/distancia";
import { Duracao } from "../domain/duracao";
import { orientacao } from "../domain/Orientacao";
import { Segmento } from "../domain/segmento";
import ISegmentoDTO from "./ISegmentoDTO";

export default interface IPercursoDTO {
  orientacao: orientacao;
  distancia: number;
  descricao: string;
  duracao: number;
  linha: string;
  noInicio: string;
  noFim: string;
  segmentos: Array<ISegmentoDTO>
  }
  