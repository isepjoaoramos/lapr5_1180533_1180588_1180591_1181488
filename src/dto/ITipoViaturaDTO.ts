
export default interface ITipoViaturaDTO {
  codTipoViatura: string;
  descricao: string;
  velocidade_media: number;
  tipocombustivel: number;
  consumo_medio: number;
  custo_km: number;
  autonomia: number;
  emissoes: number;
}
