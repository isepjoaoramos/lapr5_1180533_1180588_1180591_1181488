import { AbreviaturaNo } from "../domain/AbreviaturaNo";
import { PercursoId } from "../domain/percursoId";

export default interface ISegmentoDTO {
    no1: string;
    no2: string;
    distancia: number;
    duracao: number;
    ordem: number
  }
  