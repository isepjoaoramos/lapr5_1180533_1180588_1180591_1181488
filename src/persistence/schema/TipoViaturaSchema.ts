import { ITipoViaturaPersistence } from '../../dataschema/ITipoViaturaPersistence';
import mongoose from 'mongoose';

const TipoViaturaSchema = new mongoose.Schema(
  {
    codTipoViatura: { type: String, unique: true},
    descricao: { type: String },
    velocidade_media: { type: Number},
    tipocombustivel: { type: String },
    consumo_medio: { type: Number},
    custo_km: {type: Number},
    autonomia: {type: Number},
    emissoes: {type: Number}
  },
  {
    timestamps: true
  }
);

export default mongoose.model<ITipoViaturaPersistence & mongoose.Document>('TipoViatura', TipoViaturaSchema);
