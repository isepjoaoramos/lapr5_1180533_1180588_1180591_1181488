import { ITipoTripulantePersistence } from '../../dataschema/ITipoTripulantePersistence';
import mongoose from 'mongoose';

const TipoTripulanteSchema = new mongoose.Schema(
  {
    codigo: { type: String, unique: true },
    descricao: { type: String, unique: true }
  },
  {
    timestamps: true
  }
);

export default mongoose.model<ITipoTripulantePersistence & mongoose.Document>('TipoTripulante', TipoTripulanteSchema);
