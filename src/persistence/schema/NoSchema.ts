import { INoPersistence } from '../../dataschema/INoPersistence';
import mongoose from 'mongoose';

const NoSchema = new mongoose.Schema(
  {
    nome: { type: String },
    latitude :{ type: Number },
    longitude :{ type: Number },
    abreviatura: { type: String, unique: true },
    isEstacaoRecolha: { type: Boolean},
    isPontoRendicao: { type: Boolean},
    capacidade: { type: Number },
    tempoMaxParagem: { type: Number},
    modelo: { type: String}
  },
  {
    timestamps: true
  }
);

export default mongoose.model<INoPersistence & mongoose.Document>('No', NoSchema);
