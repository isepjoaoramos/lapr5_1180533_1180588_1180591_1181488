import mongoose from 'mongoose';
import IUserPersistence from '../../dataschema/IUserPersistence';
import bcrypt from 'bcrypt';

const UserSchema = new mongoose.Schema(
  {
    email: { type: String, unique: true, require:true},
    username: { type: String, unique: true, require:true},
    password: { type: String, require:true, maxlength: 100},
    cliente: { type: Boolean},
    data_admin: { type: Boolean},
    gestor: { type: Boolean},
    data_criacao: { type: Date},
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IUserPersistence & mongoose.Document>('User', UserSchema);
