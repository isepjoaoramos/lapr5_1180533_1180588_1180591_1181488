
import mongoose from 'mongoose';
import ILinhaPersistence from '../../dataschema/ILinhaPersistence';

const LinhaSchema = new mongoose.Schema(
  {
    codLinha: { type: String, unique: true},
    nome: { type: String },
    vermelho: { type: Number},
    verde: { type: Number },
    azul: { type: Number},
    noInicio: {type: String},
    noFim: {type: String},
    restricoes: {type: String}
  },
  {
    timestamps: true
  }
);

export default mongoose.model<ILinhaPersistence & mongoose.Document>('Linha', LinhaSchema);
