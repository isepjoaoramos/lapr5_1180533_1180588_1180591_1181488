import { IPercursoPersistence } from '../../dataschema/IPercursoPersistence';
import mongoose from 'mongoose';

const PercursoSchema = new mongoose.Schema(
  {
    orientacao: { type: String },
    distancia: { type: Number },
    descricao: { type: String, unique: true},
    duracao: { type: Number },
    linha: { type: String },
    noInicio: { type: String },
    noFim: { type: String },
    segmentos: [{no1: String, no2: String, distancia: Number, duracao: Number, ordem: Number}]
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IPercursoPersistence & mongoose.Document>('Percurso', PercursoSchema);
