import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from 'mongoose';
import { ITipoViaturaPersistence } from '../dataschema/ITipoViaturaPersistence';

import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";
import { TipoViatura } from "../domain/TipoViatura";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { tipoCombustivelEnum } from "../domain/TipoCombustivelEnum";

export class TipoViaturaMap extends Mapper<TipoViatura> {
  
  public static toDTO( tipoViatura: TipoViatura): ITipoViaturaDTO {
    return {
      id: tipoViatura.id.toString,
      codTipoViatura: tipoViatura.codTipoViatura.value,
      descricao: tipoViatura.descricao.value,
      velocidade_media: tipoViatura.velocidadeMedia.value,
      tipocombustivel: tipoViatura.tipoCombustivel.value,
      autonomia: tipoViatura.autonomia.value,
      custo_km: tipoViatura.custoKm.value,
      consumo_medio: tipoViatura.consumoMedio.value,
      emissoes: tipoViatura.emissoes.value
    } as ITipoViaturaDTO;
  }

  public static toDomain (tipoViatura: any | Model<ITipoViaturaPersistence & Document> ): TipoViatura {
    const tipoViaturaOrError = TipoViatura.create(
      tipoViatura,
      new UniqueEntityID(tipoViatura.domainId)
    );

    tipoViaturaOrError.isFailure ? console.log(tipoViaturaOrError.error) : '';

    return tipoViaturaOrError.isSuccess ? tipoViaturaOrError.getValue() : null;
  }

  public static toPersistence (tipoViatura: TipoViatura): any {
    return {
      domainId: tipoViatura.id.toString(),
      codTipoViatura: tipoViatura.codTipoViatura.value,
      descricao: tipoViatura.descricao.value,
      velocidade_media: tipoViatura.velocidadeMedia.value,
      tipocombustivel: tipoCombustivelEnum[tipoViatura.tipoCombustivel.value],
      consumo_medio: tipoViatura.consumoMedio.value,
      custo_km: tipoViatura.custoKm.value,
      autonomia: tipoViatura.autonomia.value,    
      emissoes: tipoViatura.emissoes.value
    }
  }
}