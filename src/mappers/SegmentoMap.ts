import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from 'mongoose';
import { IPercursoPersistence } from '../dataschema/IPercursoPersistence';

import ISegmentoDTO from "../dto/ISegmentoDTO";
import { Segmento } from "../domain/segmento";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class SegmentoMap extends Mapper<Segmento> {
  
  public static toDTO( segmento: Segmento): ISegmentoDTO {
    return {
      no1: segmento.no1,
    no2: segmento.no2,
    distancia: segmento.distancia,
    duracao: segmento.duracao,
    ordem: segmento.ordem,
    } as ISegmentoDTO;
  }

  /*public static toDomain (segmento: any | Model<ISegmentoPersistence & Document> ): Segmento {
    const segmentoOrError = Segmento.create(
      segmento,
      new UniqueEntityID(segmento.domainId)
    );

    segmentoOrError.isFailure ? console.log(segmentoOrError.error) : '';

    return segmentoOrError.isSuccess ? segmentoOrError.getValue() : null;
  }

  public static toPersistence (segmento: Segmento): any {
    return {
      domainId: segmento.id.toString(),
      orientacao: segmento.orientacao,
      distancia: segmento.distancia,
      descricao: segmento.descricao,
      duracao: segmento.duracao,
      linha: segmento.linha,
      noInicio: segmento.noInicio,
      noFim: segmento.noFim
    }
  }*/
}