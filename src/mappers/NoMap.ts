import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from 'mongoose';
import { INoPersistence } from '../dataschema/INoPersistence';

import INoDTO from "../dto/INoDTO";
import { No } from "../domain/no";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class NoMap extends Mapper<No> {
  
  public static toDTO( no: No): INoDTO {
    return {
      nome: no.nome.value,
      latitude:no.latitude.value,
      longitude:no.longitude.value,
      isEstacaoRecolha:no.isEstacaoRecolha,
      isPontoRendicao:no.isPontoRendicao,
      abreviatura: no.abreviatura.value,
      capacidade: no.capacidade.value,
      tempoMaxParagem:no.capacidade.value,
      modelo:no.modelo
    } as INoDTO;
  }

  public static toDomain (no: any | Model<INoPersistence & Document> ): No {
    const noOrError = No.create(
        no,
      new UniqueEntityID(no.domainId)
    );

    noOrError.isFailure ? console.log(noOrError.error) : '';

    return noOrError.isSuccess ? noOrError.getValue() : null;
  }

  public static toPersistence (no: No): any {
    return {
      domainId: no.id.toString(),
      nome: no.nome.value,
      latitude:no.latitude.value,
      longitude:no.longitude.value,
      isEstacaoRecolha:no.isEstacaoRecolha,
      isPontoRendicao:no.isPontoRendicao,
      abreviatura: no.abreviatura.value,
      capacidade: no.capacidade.value,
      tempoMaxParagem:no.capacidade.value,
      modelo:no.modelo
    }
  }
}