import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from 'mongoose';
import { ITipoViaturaPersistence } from '../dataschema/ITipoViaturaPersistence';

import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";
import { TipoViatura } from "../domain/TipoViatura";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Linha } from "../domain/linha";
import ILinhaDTO from "../dto/ILinhaDTO";
import ILinhaPersistence from "../dataschema/ILinhaPersistence";

export class LinhaMap extends Mapper<Linha> {
  
  public static toDTO( linha: Linha): ILinhaDTO {
    return {
      id: linha.id.toString,
      codLinha: linha.codLinha.value,
      nome: linha.nome.value,
      vermelho: linha.cor.vermelho,
      verde: linha.cor.verde,
      azul: linha.cor.azul,
      noInicio: linha.NoInicio.value,
      noFim: linha.NoFim.value,
      restricoes: linha.restricoes.value      
    } as ILinhaDTO;
  }

  public static toDomain (linha: any | Model<ILinhaPersistence & Document> ): Linha {
    const linhaOrError = Linha.create(
      linha,
      new UniqueEntityID(linha.domainId)
    );

    linhaOrError.isFailure ? console.log(linhaOrError.error) : '';

    return linhaOrError.isSuccess ? linhaOrError.getValue() : null;
  }

  public static toPersistence (linha: Linha): any {
    return {
      domainId: linha.id.toString(),
      codLinha: linha.codLinha.value,
      nome: linha.nome.value,
      vermelho: linha.cor.vermelho,
      verde: linha.cor.verde,
      azul: linha.cor.azul,
      noInicio: linha.NoInicio.value,
      noFim: linha.NoFim.value,
      restricoes: linha.restricoes.value  
    }
  }
}