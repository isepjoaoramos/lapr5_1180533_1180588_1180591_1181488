import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from "mongoose";
import IUserPersistence from "../dataschema/IUserPersistence";

import IUserDTO from "../dto/IUserDTO";
import { User } from "../domain/User";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class UserMap extends Mapper<User> {
  public static toDTO(user: User): IUserDTO {
    return {
      email: user.email,
      username: user.username,
      password: user.password,
      cliente: user.cliente,
      data_admin: user.data_admin,
      gestor: user.gestor,
      data_criacao: user.data_criacao,
    } as IUserDTO;
  }

  public static toDomain(user: any | Model<IUserPersistence & Document>): User {
    const userOrError = User.create(user, new UniqueEntityID(user.domainId));

    userOrError.isFailure ? console.log(userOrError.error) : "";

    return userOrError.isSuccess ? userOrError.getValue() : null;
  }

  public static toPersistence(user: User): any {
    return {
      domainId: user.id.toString(),
      email: user.email,
      username: user.username,
      password: user.password,
      cliente: user.cliente,
      data_admin: user.data_admin,
      gestor: user.gestor,
      data_criacao: user.data_criacao,
    };
  }
}
