import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from 'mongoose';
import { IPercursoPersistence } from '../dataschema/IPercursoPersistence';

import IPercursoDTO from "../dto/IPercursoDTO";
import { Percurso } from "../domain/percurso";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class PercursoMap extends Mapper<Percurso> {
  
  public static toDTO( percurso: Percurso): IPercursoDTO {
    return {
      id: percurso.id.toString(),
      orientacao: percurso.orientacao,
      distancia: percurso.distancia,
      descricao: percurso.descricao,
      duracao: percurso.duracao,
      linha: percurso.linha,
      noInicio: percurso.noInicio,
      noFim: percurso.noFim,
      segmentos: percurso.segmentos
    } as IPercursoDTO;
  }

  public static toDomain (percurso: any | Model<IPercursoPersistence & Document> ): Percurso {
    const percursoOrError = Percurso.create(
      percurso,
      new UniqueEntityID(percurso.domainId)
    );

    percursoOrError.isFailure ? console.log(percursoOrError.error) : '';

    return percursoOrError.isSuccess ? percursoOrError.getValue() : null;
  }

  public static toPersistence (percurso: Percurso): any {
    return {
      domainId: percurso.id.toString(),
      orientacao: percurso.orientacao,
      distancia: percurso.distancia,
      descricao: percurso.descricao,
      duracao: percurso.duracao,
      linha: percurso.linha,
      noInicio: percurso.noInicio,
      noFim: percurso.noFim,
      segmentos: percurso.segmentos
    }
  }
}