import { Router } from "express";
import percursoRoute from "./routes/percursoRoute";
import importarGlxRoute from "./routes/ImportarGlxRoute";
import tipoViatura from "./routes/TipoViaturaRoute";
import linha from "./routes/LinhaRoute";
import tipoTripulante from "./routes/tipoTripulanteRoute";
import no from "./routes/NoRoute";
import user from "./routes/UserRoute";
import login from "./routes/LoginRoute";

export default () => {
  const app = Router();

  importarGlxRoute(app);
  tipoViatura(app);
  percursoRoute(app);
  linha(app);
  tipoTripulante(app);
  no(app);
  user(app);
  login(app);

  return app;
};
