import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import config from "../../../config";

// Solution based on https://medium.com/javascript-in-plain-english/creating-a-rest-api-with-jwt-authentication-and-role-based-authorization-using-typescript-fbfa3cab22a4

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {
  
    //Get the jwt token from the head
  const token = <string>req.headers["auth"];
  if( !token ) return res.status(401).send('Access Denied');
  let jwtPayload;
  
  //Try to validate the token and get data
  try {
    jwtPayload = <any>jwt.verify(token, config.jwtSecret);
    req.body.user = jwtPayload;
  } catch (error) {
    //If token is not valid, respond with 401 (unauthorized)
    res.status(401).send("Invalid token");
    return;
  }
  next();
};