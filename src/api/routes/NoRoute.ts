import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';

import { Container } from 'typedi';
import INoController from '../../controllers/IControllers/INoController';

import config from "../../../config";

import {checkJwt} from "../middlewares/checkJwt";

const route = Router();

export default (app: Router) => {
  app.use('/no', route);

  const ctrl = Container.get(config.controller.no.name) as INoController;

  route.post('', /*checkJwt,*/
    celebrate({
      body: Joi.object({
        nome: Joi.string().required().max(200),
        latitude: Joi.number().required().max(90).min(-90),
        longitude: Joi.number().required().max(180).min(-180),
        abreviatura: Joi.string().required().max(20),
        isEstacaoRecolha: Joi.boolean().required(),
        isPontoRendicao: Joi.boolean().required(),
        capacidade: Joi.number().required(),
        tempoMaxParagem: Joi.number().required().min(0),
        modelo: Joi.string().required()
      })
    }),
    (req, res, next) => ctrl.createNo(req, res, next));

  route.get('', /*checkJwt,*/ (req, res, next) => ctrl.getNos(req, res, next));

};