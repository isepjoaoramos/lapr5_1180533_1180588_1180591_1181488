import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';

import { Container } from 'typedi';

import config from "../../../config";

import ILinhaController from "../../controllers/IControllers/ILinhaController";

import {checkJwt} from "../middlewares/checkJwt";

const route = Router();

export default (app: Router) => {
  app.use('/linha', route);

  const ctrl = Container.get(config.controller.linha.name) as ILinhaController;

  route.post('', checkJwt,
    (req, res, next) => ctrl.createLinha(req, res, next));


    route.get('', checkJwt,
    (req, res, next) => ctrl.searchLinhas(req, res, next));

};