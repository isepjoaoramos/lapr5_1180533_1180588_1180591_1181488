import config from "../../../config";
import { Router } from "express";
import { Container } from "typedi";
import { celebrate, Joi } from "celebrate";

import IUserController from "../../controllers/IControllers/IUserController";

import {checkJwt} from "../middlewares/checkJwt";

var jwt = require("jsonwebtoken");

var bcrypt = require("bcrypt");

const route1 = Router();
const route2 = Router();
const route3 = Router();
const route4 = Router();

export default (app: Router) => {
  app.use("/register/cliente", route1);
  app.use("/register/data_admin", route2);
  app.use("/register/gestor", route3);
  app.use("/delete", route4);

  const ctrl = Container.get(config.controller.user.name) as IUserController;

  route1.post(
    "",
    celebrate({
      body: Joi.object({
        email: Joi.string()
          .required()
          .max(200),
        username: Joi.string().required(),
        password: Joi.string().required(),
      }),
    }),
    (req, res, next) => ctrl.registerCliente(req, res, next)
  );

  route1.get("", (req, res, next) => ctrl.getUser(req, res, next));

  route2.post(
    "",
    celebrate({
      body: Joi.object({
        email: Joi.string()
          .required()
          .max(200),
        username: Joi.string().required(),
        password: Joi.string().required(),
      }),
    }),
    (req, res, next) => ctrl.registerDataAdmin(req, res, next)
  );

  route2.get("", (req, res, next) => ctrl.getUser(req, res, next));

  route3.post(
    "",
    celebrate({
      body: Joi.object({
        email: Joi.string()
          .required()
          .max(200),
        username: Joi.string().required(),
        password: Joi.string().required(),
      }),
    }),
    (req, res, next) => ctrl.registerGestor(req, res, next)
  );

  route3.get("", (req, res, next) => ctrl.getUser(req, res, next));

  route4.delete("/:username", (req, res, next) => ctrl.deleteUser(req, res, next))
  console.log(route4+"/:username")
};
