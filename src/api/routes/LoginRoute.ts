import config from "../../../config";
import { Router } from "express";
import { Container } from "typedi";
import { celebrate, Joi } from "celebrate";

import ILoginController from "../../controllers/IControllers/ILoginController";


const route = Router();

export default (app: Router) => {
    app.use("/login", route);

    const ctrl = Container.get(config.controller.login.name) as ILoginController;

    route.post('',
        celebrate({
            body: Joi.object({
                username: Joi.string(),
                password: Joi.string(),
            }),

        }),
        (req, res, next) => ctrl.authenticateUser(req, res, next));
};