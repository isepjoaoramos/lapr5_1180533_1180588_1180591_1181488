import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';

import { Container } from 'typedi';

import config from "../../../config";

import ITipoViaturaController from "../../controllers/IControllers/ITipoViaturaController";

import {checkJwt} from "../middlewares/checkJwt";


const route = Router();

export default (app: Router) => {
  app.use('/tipoviatura', route);

  const ctrl = Container.get(config.controller.tipoViatura.name) as ITipoViaturaController;

  route.post('', checkJwt,
    (req, res, next) => ctrl.createTipoViatura(req, res, next) );



    route.get('', checkJwt, 
    (req, res, next) => ctrl.searchTipoViatura(req, res, next));
};