/*module.exports = function(app){
    var percursos = require('../../controllers/percursos.js');

    //criar percurso
    app.post('/api/percursos',percursos.createPercurso);
}
*/
import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';

import { Container } from 'typedi';
import IPercursoController from '../../controllers/IControllers/IPercursoController';

import {checkJwt} from "../middlewares/checkJwt";

import config from "../../../config";

const route = Router();

export default (app: Router) => {
  app.use('/percursos', route);

  const ctrl = Container.get(config.controller.percurso.name) as IPercursoController;
  
  route.post('', checkJwt,
    (req, res, next) => ctrl.createPercurso(req, res, next) );


    route.get('', /*checkJwt,*/ (req,res,next) => ctrl.getPercursos(req,res,next));
};