import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';

import { Container } from 'typedi';

import config from "../../../config";

import ITipoViaturaController from "../../controllers/IControllers/ITipoViaturaController";
import IImportacaoGlxController from '../../controllers/IControllers/IImportacaoGlxController';

import {checkJwt} from "../middlewares/checkJwt";


const route = Router();

const fileUpload = require('express-fileupload');
const morgan = require('morgan');

export default (app: Router) => {
  app.use(fileUpload({ createParentPath: true}));
  app.use(morgan('dev'));
  app.use('/importar_glx', route);

  const ctrl = Container.get(config.controller.importacaoGlx.name) as IImportacaoGlxController;

  route.post('',checkJwt, (req, res, next) => ctrl.importEntities(req, res, next));
};