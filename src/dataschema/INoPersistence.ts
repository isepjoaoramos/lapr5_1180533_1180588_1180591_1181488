export interface INoPersistence {
  _id: string;
  nome: string;
  latitude: number;
  longitude: number;
  abreviatura: string;
  isEstacaoRecolha: boolean;
  isPontoRendicao: boolean;
  capacidade: number,
  tempMaxParagem: number,
  modelo: string
}