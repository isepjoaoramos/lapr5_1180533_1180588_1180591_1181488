export default interface ILinhaPersistence {
    _id: string;
    codLinha: string;
    nome: string;
    vermelho: number; 
    verde: number;
    azul: number;
    noInicio: string;
    noFim: string;
    restricoes: string;
  }