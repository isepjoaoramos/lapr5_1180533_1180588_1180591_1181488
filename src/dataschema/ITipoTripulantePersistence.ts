import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { CodTipoTripulante } from "../domain/CodTipoTripulante";
import { DescricaoTipoTripulante } from "../domain/DescricaoTipoTripulante";

export interface ITipoTripulantePersistence {
  _id: string;
  codigo: string;
  descricao: string;
}