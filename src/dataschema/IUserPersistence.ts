export default interface IUserPersistence {
  
  email: string;
  username: string;
  password: string;
  cliente: boolean;
  data_admin: boolean;
  gestor: boolean;
  data_criacao: Date;
}
