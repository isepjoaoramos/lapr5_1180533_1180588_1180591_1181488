
export interface IPercursoPersistence {
    _id: string;
    orientacao: string;
    distancia: number;
    descricao: string;
    duracao: number;
    linha: string;
    noInicio: string;
    noFim: string;
    segmentos: [string,string,number,number,number]
  }