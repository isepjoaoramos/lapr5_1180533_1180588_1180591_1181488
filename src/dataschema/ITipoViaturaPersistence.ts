export interface ITipoViaturaPersistence {
    _id: string;
    codTipoViatura: string;
	descricao: string;
	velocidade_media: number;
	tipocombustivel: string;
	consumo_medio: number;
	custo_km: number;
	autonomia: number;
	emissoes: number;
  }