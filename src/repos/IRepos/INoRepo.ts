import { Repo } from "../../core/infra/Repo";
import { No } from "../../domain/no";
import { AbreviaturaNo } from "../../domain/AbreviaturaNo";

import { NomeNo } from "../../domain/NomeNo";

export default interface INoRepo extends Repo<No> {
  save(no: No): Promise<No>;
  findByDomainId (nomeNo: NomeNo | string): Promise<No>;
  exists(no: No | string): Promise<boolean>;

  findNosByAbreviat(abreviatura: AbreviaturaNo |string): Promise<Array<No>>;
  findNosByNome(nome: NomeNo | string): Promise<Array<No>>;

  findAllNos(): Promise<Array<No>>;
  
  //findByIds (rolesIds: RoleId[]): Promise<Role[]>;
  //saveCollection (roles: Role[]): Promise<Role[]>;
  //removeByRoleIds (roles: RoleId[]): Promise<any>
}
