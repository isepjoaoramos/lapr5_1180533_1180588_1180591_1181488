import { Repo } from "../../core/infra/Repo";
import { User } from "../../domain/User";

export default interface IUserRepo extends Repo<User> {
  save(user: User): Promise<User>;
  
  //findOne (email: string | string): Promise<No>;
  
  exists(user: User | string): Promise<boolean>;

  findUserByEmail(email: string | string): Promise<User>
  findUserByUsername(username: string | string): Promise<User>
  findUserByUsernameAndDelete(username: string | string): Promise<any>
  
  findAllUsers(): Promise<Array<User>>;

  //findByIds (rolesIds: RoleId[]): Promise<Role[]>;
  //saveCollection (roles: Role[]): Promise<Role[]>;
  //removeByRoleIds (roles: RoleId[]): Promise<any>
}
