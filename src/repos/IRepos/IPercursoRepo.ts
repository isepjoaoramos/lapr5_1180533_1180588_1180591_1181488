import { Repo } from "../../core/infra/Repo";
import { LinhaNome } from "../../domain/LinhaNome";
import { Percurso } from "../../domain/percurso";
import { PercursoId } from "../../domain/percursoId";
import { Segmento } from "../../domain/segmento";
import IPercursoDTO from "../../dto/IPercursoDTO";

export default interface IPercursoRepo extends Repo<Percurso> {
  save(percurso: Percurso): Promise<Percurso>;
  //save(percurso2: Segmento): Promise<Segmento>;
  findByDomainId (percursoId: PercursoId | string): Promise<Percurso>;
  exists(percurso: Percurso | string): Promise<boolean>;
  findByLinha (linha: LinhaNome | string): Promise<Array<Percurso>>;
  findByDescricao (descricao: string): Promise<Array<Percurso>>;
  findAllPercursos (): Promise<Array<Percurso>>;
  //findByIds (rolesIds: RoleId[]): Promise<Role[]>;
  //saveCollection (roles: Role[]): Promise<Role[]>;
  //removeByRoleIds (roles: RoleId[]): Promise<any>
}
