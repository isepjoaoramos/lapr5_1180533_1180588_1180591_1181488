import { Repo } from "../../core/infra/Repo";
import { CodLinha } from "../../domain/CodLinha";
import { Linha } from "../../domain/linha";


export default interface ILinhaRepo extends Repo<Linha> {
  save(linha: Linha): Promise<Linha>;
  findByCodLinha (codLinha: CodLinha | string): Promise<Linha>;
  exists(linha: Linha | string): Promise<boolean>;
  findLinhasByCod(string): Promise<Array<Linha>>;
  findLinhasByName(string): Promise<Array<Linha>>;
  findAllLinhas(): Promise<Array<Linha>>;
}
