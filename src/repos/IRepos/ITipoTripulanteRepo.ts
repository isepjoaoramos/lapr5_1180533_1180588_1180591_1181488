import { Repo } from "../../core/infra/Repo";
import { TipoTripulante } from "../../domain/TipoTripulante";
import { CodTipoTripulante } from "../../domain/CodTipoTripulante";
import { DescricaoTipoTripulante } from "../../domain/DescricaoTipoTripulante";

export default interface ITipoTripulanteRepo extends Repo<TipoTripulante> {
  save(tipoTripulante: TipoTripulante): Promise<TipoTripulante>;
  findByDomainId(tipoId: CodTipoTripulante | string): Promise<TipoTripulante>;
  exists(tipoTripulante: TipoTripulante | string): Promise<boolean>;

  findTiposByCod(codigo: CodTipoTripulante | string): Promise<Array<TipoTripulante>>
  findTiposByDescricao(descricao: DescricaoTipoTripulante | string): Promise<Array<TipoTripulante>>
  
  findAllTipos(): Promise<Array<TipoTripulante>>;



  //findByIds (rolesIds: RoleId[]): Promise<Role[]>;
  //saveCollection (roles: Role[]): Promise<Role[]>;
  //removeByRoleIds (roles: RoleId[]): Promise<any>
}
