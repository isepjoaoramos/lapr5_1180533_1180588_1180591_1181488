import { Repo } from "../../core/infra/Repo";
import { CodTipoViatura } from "../../domain/CodTipoViatura";
import { TipoViatura } from "../../domain/TipoViatura";


export default interface ITipoViaturaRepo extends Repo<TipoViatura> {
  save(tipoViatura: TipoViatura): Promise<TipoViatura>;
  findByCodViatura (tipoViaturaID: CodTipoViatura | string): Promise<TipoViatura>;
  exists(tipoViatura: TipoViatura | string): Promise<boolean>;

  findAllTiposViatura(): Promise<Array<TipoViatura>>;
}
