import { Service, Inject } from 'typedi';

import ITipoViaturaRepo from "./IRepos/ITipoViaturaRepo";
import { TipoViatura } from "../domain/TipoViatura";
import { CodTipoViatura } from "../domain/CodTipoViatura";
import { TipoViaturaMap } from "../mappers/TipoViaturaMap";

import { Document, Model } from 'mongoose';
import { ITipoViaturaPersistence } from '../dataschema/ITipoViaturaPersistence';
import ILinhaRepo from './IRepos/ILinhaRepo';
import ILinhaPersistence from '../dataschema/ILinhaPersistence';
import { Linha } from '../domain/linha';
import { LinhaMap } from '../mappers/LinhaMap';
import { CodLinha } from '../domain/CodLinha';

@Service()
export default class LinhaRepo implements ILinhaRepo {
  private models: any;

  constructor(
    @Inject('LinhaSchema') private linhaSchema: Model<ILinhaPersistence & Document>,
  ) { }

  private createBaseQuery(): any {
    return {
      where: {},
    }
  }

  public async exists(linha: Linha | string): Promise<boolean> {

    const idX = linha instanceof Linha ? (<Linha>linha).id.toValue : String;

    const query = { domainId: idX };
    const linhaDocument = await this.linhaSchema.findOne(query);

    return !!linhaDocument === true;
  }


  public async save(linha: Linha): Promise<Linha> {
    const query = { codLinha: linha.codLinha.value };

    const linhaDocument = await this.linhaSchema.findOne(query);

    try {
      if (linhaDocument === null) {
        const rawLinha: any = LinhaMap.toPersistence(linha);

        const linhaCreated = await this.linhaSchema.create(rawLinha);

        return LinhaMap.toDomain(linhaCreated);
      } else {
        linhaDocument.codLinha = linha.codLinha.value;

        await linhaDocument.save();

        return linha;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByCodLinha(codLinha: string): Promise<Linha> {

    const query = { codLinha: codLinha.toString() };

    const linhaRecord = await this.linhaSchema.findOne(query);

    if (linhaRecord != null) {
      return LinhaMap.toDomain(linhaRecord);
    }
    else
      return null;
  }

  public async findAllLinhas(): Promise<Array<Linha>> {

    const linhasRecords = await this.linhaSchema.find({});

    if (linhasRecords == null) {
      return null;
    }

    let linhasArray = new Array<Linha>();

    for (let i = 0; i < linhasRecords.length; i++) {
      linhasArray[i] = LinhaMap.toDomain(linhasRecords[i]);
    }
    
    return linhasArray;
  }

  public async findLinhasByCod(cod: string): Promise<Array<Linha>> {

    const query = { codLinha: new RegExp(cod, 'i') };

    console.log(query);

    const linhasRecords = await this.linhaSchema.find(query);

    if (linhasRecords == null) {
      return null;
    }

    let linhasArray = new Array<Linha>();
    for (let i = 0; i < linhasRecords.length; i++) {
      linhasArray[i] = LinhaMap.toDomain(linhasRecords[i]);
    }

    return linhasArray;
  }



  public async findLinhasByName(name: string): Promise<Array<Linha>> {

    const query = { nome: new RegExp(name, 'i') };

    const linhasRecords = await this.linhaSchema.find(query);

    if (linhasRecords == null) {
      return null;
    }

    let linhasArray = new Array<Linha>();
    for (let i = 0; i < linhasRecords.length; i++) {
      linhasArray[i] = LinhaMap.toDomain(linhasRecords[i]);
    }

    return linhasArray;
  }


}