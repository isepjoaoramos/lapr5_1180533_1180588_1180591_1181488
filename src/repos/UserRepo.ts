import { Service, Inject } from "typedi";

import IUserRepo from "./IRepos/IUserRepo";
import { User } from "../domain/User";
import { UserMap } from "../mappers/UserMap";

import { Document, Model } from "mongoose";
import IUserPersistence from "../dataschema/IUserPersistence";

@Service()
export default class UserRepo implements IUserRepo {
  private models: any;

  constructor(
    @Inject("UserSchema") private userSchema: Model<IUserPersistence & Document>
  ) {}

  public async save(user: User): Promise<User> {
    const query = { domainId: user.id.toString() };

    const userDocument = await this.userSchema.findOne(query);

    try {
      if (userDocument === null) {
        const rawUser: any = UserMap.toPersistence(user);

        
        const userCreated = await this.userSchema.create(rawUser);
        
        return UserMap.toDomain(userCreated);
      } else {
        userDocument.email = user.email;

        await userDocument.save();

        return user;
      }
    } catch (err) {
      throw err;
    }
  }

  public async exists(user: User | string): Promise<boolean> {

    const idX = user instanceof User ? (<User>user).id.toValue : user;

    const query = { domainId: idX };
    const userDocument = await this.userSchema.findOne(query);

    return !!userDocument === true;
  }

  public async findAllUsers(): Promise<Array<User>> {


    const usersRecords = await this.userSchema.find({});

    if (usersRecords == null || usersRecords.length==0) {
      return null;
    }

    let usersArray = new Array<User>();
    for (let i = 0; i < usersRecords.length; i++) {
      usersArray[i] = UserMap.toDomain(usersRecords[i]);
    }

    return usersArray;
  }

  public async findUserByEmail(email: string): Promise<User> {

    const query = { email: new RegExp(email, 'i') };

    console.log(query);

    const usersRecords = await this.userSchema.find(query);

    if (usersRecords == null || usersRecords.length==0) {
      return null;
    }
    console.log(usersRecords);

    let usersArray = new Array<User>();
    for (let i = 0; i < usersRecords.length; i++) {
      usersArray[i] = UserMap.toDomain(usersRecords[i]);
    }

    return usersArray[0];
  }

  public async findUserByUsername(username: string): Promise<User> {

    const query = { username: new RegExp(username, 'i') };

    console.log(query);
   
    const userRecord = await this.userSchema.findOne(query);

    if( userRecord != null) {
      return UserMap.toDomain(userRecord);
    }
    else
      return null;
  };

  public async findUserByUsernameAndDelete(username: string): Promise<boolean> {

    const query = { username: new RegExp(username, 'i') };

    console.log(query);
   
    const userRecord = await this.userSchema.deleteOne(query);

    if( userRecord != null) {
      return true
    }
    else
      return false;
  };


}
