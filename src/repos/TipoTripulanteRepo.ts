import { Service, Inject } from 'typedi';

import ITipoTripulanteRepo from "./IRepos/ITipoTripulanteRepo";
import { TipoTripulante } from "../domain/TipoTripulante";
import { CodTipoTripulante } from "../domain/CodTipoTripulante";
import { TipoTripulanteMap } from "../mappers/TipoTripulanteMap";

import { Document, Model } from 'mongoose';
import { ITipoTripulantePersistence } from '../dataschema/ITipoTripulantePersistence';

@Service()
export default class TipoTripulanteRepo implements ITipoTripulanteRepo {
  private models: any;

  constructor(
    @Inject('TipoTripulanteSchema') private tipoTripulanteSchema: Model<ITipoTripulantePersistence & Document>,
  ) { }

  private createBaseQuery(): any {
    return {
      where: {},
    }
  }

  /* public async exists (codTipoTripulante: CodTipoTripulante | string): Promise<boolean> {

    const idX = codTipoTripulante instanceof CodTipoTripulante ? (<CodTipoTripulante>codTipoTripulante).value : codTipoTripulante;

    const query = { domainId: idX}; 
    const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne( query );

    return !!tipoTripulanteDocument === true;
  } */

  public async exists(tipoTripulante: TipoTripulante | string): Promise<boolean> {

    const idX = tipoTripulante instanceof TipoTripulante ? (<TipoTripulante>tipoTripulante).id.toValue : tipoTripulante;

    const query = { domainId: idX };
    const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne(query);

    return !!tipoTripulanteDocument === true;
  }


  public async save(tipoTripulante: TipoTripulante): Promise<TipoTripulante> {
    const query = { domainId: tipoTripulante.id.toString() };

    const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne(query);

    try {
      if (tipoTripulanteDocument === null) {
        const rawTipoTripulante: any = TipoTripulanteMap.toPersistence(tipoTripulante);

        const tipoTripulanteCreated = await this.tipoTripulanteSchema.create(rawTipoTripulante);

        return TipoTripulanteMap.toDomain(tipoTripulanteCreated);
      } else {
        tipoTripulanteDocument.codigo = tipoTripulante.codigo.value;

        await tipoTripulanteDocument.save();

        return tipoTripulante;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId(tipoTripulanteId: CodTipoTripulante | string): Promise<TipoTripulante> {
    const query = { domainId: tipoTripulanteId };
    const tipoTripulanteRecord = await this.tipoTripulanteSchema.findOne(query);

    if (tipoTripulanteRecord != null) {
      return TipoTripulanteMap.toDomain(tipoTripulanteRecord);
    }
    else
      return null;
  }

  public async findTiposByCod(codigo: string): Promise<Array<TipoTripulante>> {

    const query = { codigo: new RegExp(codigo, 'i') };

    const tiposRecords = await this.tipoTripulanteSchema.find(query);

    if (tiposRecords == null || tiposRecords.length == 0) {
      return null;
    }

    let tiposArray = new Array<TipoTripulante>();
    for (let i = 0; i < tiposRecords.length; i++) {
      tiposArray[i] = TipoTripulanteMap.toDomain(tiposRecords[i]);
    }

    return tiposArray;
  }


  public async findTiposByDescricao(descricao: string): Promise<Array<TipoTripulante>> {

    const query = { descricao: new RegExp(descricao, 'i') };

    console.log(query);

    const tiposRecords = await this.tipoTripulanteSchema.find(query);

    if (tiposRecords == null || tiposRecords.length == 0) {
      return null;
    }

    let tiposArray = new Array<TipoTripulante>();
    for (let i = 0; i < tiposRecords.length; i++) {
      tiposArray[i] = TipoTripulanteMap.toDomain(tiposRecords[i]);
    }

    return tiposArray;
  }

  public async findAllTipos(): Promise<Array<TipoTripulante>> {


    const tiposRecords = await this.tipoTripulanteSchema.find({});

    if (tiposRecords == null || tiposRecords.length == 0) {
      return null;
    }

    let tiposArray = new Array<TipoTripulante>();
    for (let i = 0; i < tiposRecords.length; i++) {
      tiposArray[i] = TipoTripulanteMap.toDomain(tiposRecords[i]);
    }

    return tiposArray;
  }


}