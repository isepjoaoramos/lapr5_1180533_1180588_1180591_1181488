import { Service, Inject } from 'typedi';

import ITipoViaturaRepo from "./IRepos/ITipoViaturaRepo";
import { TipoViatura } from "../domain/TipoViatura";
import { CodTipoViatura } from "../domain/CodTipoViatura";
import { TipoViaturaMap } from "../mappers/TipoViaturaMap";

import { Document, Model } from 'mongoose';
import { ITipoViaturaPersistence } from '../dataschema/ITipoViaturaPersistence';

@Service()
export default class TipoViaturaRepo implements ITipoViaturaRepo {
  private models: any;

  constructor(
    @Inject('TipoViaturaSchema') private tipoViaturaSchema : Model<ITipoViaturaPersistence & Document>,
  ) {}

  private createBaseQuery (): any {
    return {
      where: {},
    }
  }

  public async exists (tipoViatura: TipoViatura | string): Promise<boolean> {

    const idX = tipoViatura instanceof TipoViatura ? (<TipoViatura>tipoViatura).id.toValue : String;

    const query = { domainId: idX}; 
    const tipoViaturaDocument = await this.tipoViaturaSchema.findOne( query );

    return !!tipoViaturaDocument === true;
  }


  public async save (tipoViatura: TipoViatura): Promise<TipoViatura> {
    const query = { codTipoViatura: tipoViatura.codTipoViatura.value}; 

    const tipoViaturaDocument = await this.tipoViaturaSchema.findOne( query );

    try {
      if (tipoViaturaDocument === null ) {
        const rawTipoViatura: any = TipoViaturaMap.toPersistence(tipoViatura);

        const tipoViaturaCreated = await this.tipoViaturaSchema.create(rawTipoViatura);

        return TipoViaturaMap.toDomain(tipoViaturaCreated);
      } else {
        tipoViaturaDocument.codTipoViatura = tipoViatura.codTipoViatura.value;

        await tipoViaturaDocument.save();

        return tipoViatura;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByCodViatura (codTipoViatura: CodTipoViatura | string): Promise<TipoViatura> {
    
    const query = { codTipoViatura: codTipoViatura.toString()};

    const tipoViaturaRecord = await this.tipoViaturaSchema.findOne( query );

    if( tipoViaturaRecord != null) {
      return TipoViaturaMap.toDomain(tipoViaturaRecord);
    }
    else
      return null;
  };




  public async findAllTiposViatura(): Promise<Array<TipoViatura>> {

    const tiposViaturaRecord = await this.tipoViaturaSchema.find({});

    if (tiposViaturaRecord == null) {
      return null;
    }

    let tiposViaturaArray = new Array<TipoViatura>();

    for (let i = 0; i < tiposViaturaRecord.length; i++) {
      tiposViaturaArray[i] = TipoViaturaMap.toDomain(tiposViaturaRecord[i]);
    }
    return tiposViaturaArray;
  }
}