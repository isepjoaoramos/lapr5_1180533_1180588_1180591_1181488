import { Service, Inject } from 'typedi';

import IPercursoRepo from "./IRepos/IPercursoRepo";
import { Percurso } from "../domain/percurso";
import { PercursoId } from "../domain/percursoId";
import { PercursoMap } from "../mappers/PercursoMap";

import { Document, Model } from 'mongoose';
import { IPercursoPersistence } from '../dataschema/IPercursoPersistence';
import { LinhaNome } from '../domain/LinhaNome';
import IPercursoDTO from '../dto/IPercursoDTO';

@Service()
export default class PercursoRepo implements IPercursoRepo {
  private models: any;

  constructor(
    @Inject('percursoSchema') private percursoSchema : Model<IPercursoPersistence & Document>,
  ) {}

  private createBaseQuery (): any {
    return {
      where: {},
    }
  }

  public async exists (percurso: Percurso | string): Promise<boolean> {
/*
    const idX = percursoId instanceof PercursoId ? (<PercursoId>percursoId).id.toValue() : percursoId;

    const query = { domainId: idX}; 
    const percursoDocument = await this.percursoSchema.findOne( query );

    return !!percursoDocument === true;*/
    return null;
  }

  public async save (percurso: Percurso): Promise<Percurso> {
    const query = { domainId: percurso.id.toString()}; 

    const percursoDocument = await this.percursoSchema.findOne( query );

    try {
      if (percursoDocument === null ) {
        const rawPercurso: any = PercursoMap.toPersistence(percurso);

        const percursoCreated = await this.percursoSchema.create(rawPercurso);

        return PercursoMap.toDomain(percursoCreated);
      } else {
        percursoDocument.descricao = percurso.descricao;
        await percursoDocument.save();

        return percurso;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId (percursoId: PercursoId | string): Promise<Percurso> {
    const query = { domainId: percursoId};
    const percursoRecord = await this.percursoSchema.findOne( query );

    if( percursoRecord != null) {
      return PercursoMap.toDomain(percursoRecord);
    }
    else
      return null;
  }


  public async findByLinha (linha: LinhaNome | string): Promise<Array<Percurso>> {
    const query = { linha: linha.toString() };

    const percursoRecords = await this.percursoSchema.find( query );

    if (percursoRecords==null){
      return null;
    }
    let percursosArray = new Array<Percurso>();
    for (let i = 0; i< percursoRecords.length; i++){
      percursosArray[i]=PercursoMap.toDomain(percursoRecords[i]);
    }


    if( percursoRecords != null) {
      return percursosArray;
    }
    else
      return null;
  }

  public async findByDescricao (descricao: string): Promise<Array<Percurso>> {
    const query = { descricao: descricao };

    const percursoRecords = await this.percursoSchema.find( query );

    if (percursoRecords==null){
      return null;
    }
    let percursosArray = new Array<Percurso>();
    for (let i = 0; i< percursoRecords.length; i++){
      percursosArray[i]=PercursoMap.toDomain(percursoRecords[i]);
    }


    if( percursoRecords != null) {
      return percursosArray;
    }
    else
      return null;
  }

  public async findAllPercursos (): Promise<Array<Percurso>> {
    
    const percursoRecords = await this.percursoSchema.find({});
    //console.log("Recordes: "+percursoRecords);
    //while(percursoRecord.)

    if (percursoRecords==null){
      return null;
    }
    let percursosArray = new Array<Percurso>();
    for (let i = 0; i< percursoRecords.length; i++){
      percursosArray[i]=PercursoMap.toDomain(percursoRecords[i]);
    }


    if( percursoRecords != null) {
      return percursosArray;
    }
    else
      return null;
  }
}