import { Service, Inject } from 'typedi';

import INoRepo from "./IRepos/INoRepo";
import { No } from "../domain/no";
import { NoMap } from "../mappers/NoMap";

import { Document, Model } from 'mongoose';
import { INoPersistence } from '../dataschema/INoPersistence';
import { NomeNo } from '../domain/NomeNo';
import { AbreviaturaNo } from '../domain/AbreviaturaNo';

@Service()
export default class NoRepo implements INoRepo {
  private models: any;

  constructor(
    @Inject('NoSchema') private noSchema: Model<INoPersistence & Document>,
  ) { }

  private createBaseQuery(): any {
    return {
      where: {},
    }
  }

  /* public async exists (codTipoTripulante: CodTipoTripulante | string): Promise<boolean> {

    const idX = codTipoTripulante instanceof CodTipoTripulante ? (<CodTipoTripulante>codTipoTripulante).value : codTipoTripulante;

    const query = { domainId: idX}; 
    const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne( query );

    return !!tipoTripulanteDocument === true;
  } */

  public async exists(no: No | string): Promise<boolean> {

    const idX = no instanceof No ? (<No>no).id.toValue : no;

    const query = { domainId: idX };
    const noDocument = await this.noSchema.findOne(query);

    return !!noDocument === true;
  }




  public async save(no: No): Promise<No> {
    const query = { domainId: no.id.toString() };

    const noDocument = await this.noSchema.findOne(query);

    try {
      if (noDocument === null) {
        const rawNo: any = NoMap.toPersistence(no);

        const noCreated = await this.noSchema.create(rawNo);

        return NoMap.toDomain(noCreated);
      } else {
        noDocument.nome = no.nome.value;

        await noDocument.save();

        return no;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId(noId: AbreviaturaNo | string): Promise<No> {
    const query = { abreviatura: noId.toString() };
    const noRecord = await this.noSchema.findOne(query);

    if (noRecord != null) {
      return NoMap.toDomain(noRecord);
    }
    else
      return null;
  }

  public async findNosByAbreviat(abreviatura: string): Promise<Array<No>> {

    const query = { abreviatura: new RegExp(abreviatura, 'i') };

    const nosRecords = await this.noSchema.find(query);

    if (nosRecords == null || nosRecords.length==0) {
      return null;
    }

    let nosArray = new Array<No>();
    for (let i = 0; i < nosRecords.length; i++) {
      nosArray[i] = NoMap.toDomain(nosRecords[i]);
    }

    return nosArray;
  }



  public async findNosByNome(nome: string): Promise<Array<No>> {

    const query = { nome: new RegExp(nome, 'i') };

    console.log(query);

    const nosRecords = await this.noSchema.find(query);

    if (nosRecords == null || nosRecords.length==0) {
      return null;
    }

    let nosArray = new Array<No>();
    for (let i = 0; i < nosRecords.length; i++) {
      nosArray[i] = NoMap.toDomain(nosRecords[i]);
    }

    return nosArray;
  }


  public async findAllNos(): Promise<Array<No>> {


    const nosRecords = await this.noSchema.find({});

    if (nosRecords == null || nosRecords.length==0) {
      return null;
    }

    let nosArray = new Array<No>();
    for (let i = 0; i < nosRecords.length; i++) {
      nosArray[i] = NoMap.toDomain(nosRecords[i]);
    }

    return nosArray;
  }


}