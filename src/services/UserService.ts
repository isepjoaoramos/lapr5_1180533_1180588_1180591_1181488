import { Service, Inject } from "typedi";
import config from "../../config";
import IUserDTO from "../dto/IUserDTO";
import { User } from "../domain/User";
import IUserRepo from "../repos/IRepos/IUserRepo";
import IUserService from "./IServices/IUserService";
import { Result } from "../core/logic/Result";
import { UserMap } from "../mappers/UserMap";
import { Console } from "console";

import * as bcrypt from "bcrypt-nodejs";

@Service()
export default class UserService implements IUserService {
  constructor(@Inject(config.repos.user.name) private userRepo: IUserRepo) {}

  public async registerCliente(userDTO: IUserDTO): Promise<Result<IUserDTO>> {
    // Generate hash to store password in database
    const password = bcrypt.hashSync(userDTO.password);
    const userOrError = await User.create({email: userDTO.email, username: userDTO.username, password: password, cliente:true,
      data_admin:false, gestor:false, data_criacao: userDTO.data_criacao});

    if (userOrError.isFailure) {
      return Result.fail<IUserDTO>(userOrError.errorValue());
    }

    const userResult = userOrError.getValue();
  

    await this.userRepo.save(userResult);

    
    const userDTOResult = UserMap.toDTO(userResult) as IUserDTO;
    
    return Result.ok<IUserDTO>(userDTOResult);
  }

  public async registerDataAdmin(userDTO: IUserDTO): Promise<Result<IUserDTO>> {
    // Generate hash to store password in database
    const password = bcrypt.hashSync(userDTO.password);
    const userOrError = await User.create({email: userDTO.email, username: userDTO.username, password: password, cliente:false,
      data_admin:true, gestor:false, data_criacao: userDTO.data_criacao});

    if (userOrError.isFailure) {
      return Result.fail<IUserDTO>(userOrError.errorValue());
    }

    const userResult = userOrError.getValue();
  

    await this.userRepo.save(userResult);

    
    const userDTOResult = UserMap.toDTO(userResult) as IUserDTO;
    
    return Result.ok<IUserDTO>(userDTOResult);
  }

  public async registerGestor(userDTO: IUserDTO): Promise<Result<IUserDTO>> {
    // Generate hash to store password in database
    const password = bcrypt.hashSync(userDTO.password);
    const userOrError = await User.create({email: userDTO.email, username: userDTO.username, password: password, cliente:false,
      data_admin:false, gestor:true, data_criacao: userDTO.data_criacao});

    if (userOrError.isFailure) {
      return Result.fail<IUserDTO>(userOrError.errorValue());
    }

    const userResult = userOrError.getValue();
  

    await this.userRepo.save(userResult);

    
    const userDTOResult = UserMap.toDTO(userResult) as IUserDTO;
    
    return Result.ok<IUserDTO>(userDTOResult);
  }

  public async getAllUsers(): Promise<Result<Array<IUserDTO>>> {
    try {
      const users = await this.userRepo.findAllUsers();
      const found = !!users;

      if (found) {
        let usersArrayDTO3 = new Array<IUserDTO>();
        //Convert from DomainModel to DTO
        for (let i = 0; i < users.length; i++) {
          usersArrayDTO3[i] = UserMap.toDTO(users[i]);
        }

        return Result.ok<Array<IUserDTO>>(usersArrayDTO3);
      } else {
        return Result.fail<Array<IUserDTO>>("Não foram encontrados users");
      }
    } catch (e) {
      throw e;
    }
  }

  public async getUserByEmail(email: string): Promise<Result<IUserDTO>> {
    try {
      const user_found = await this.userRepo.findUserByEmail(email);
      const found = !!user_found;

      if (found) {

        //Convert from DomainModel to DTO
        let user = UserMap.toDTO(user_found);

        return Result.ok<IUserDTO>(user);
      } else {
        return Result.fail<IUserDTO>("Não foram encontrados users");
      }
    } catch (e) {
      throw e;
    }
  }

  public async getUserByUsername(username: string): Promise<Result<IUserDTO>> {
    try {
      const user_found = await this.userRepo.findUserByUsername(username);
      const found = !!user_found;

      if (found) {
        
        //Convert from DomainModel to DTO
        let user = UserMap.toDTO(user_found);

        return Result.ok<IUserDTO>(user);
      } else {
        return Result.fail<IUserDTO>("Não foram encontrados users");
      }
    } catch (e) {
      throw e;
    }
  }

  public async getUserByUsernameAndDelete(username: string): Promise<Result<any>> {
    try {
      const user_found = await this.userRepo.findUserByUsernameAndDelete(username);
      const found = !!user_found;
      if (found) {
        
        
        //send message
        return Result.ok<any>("User apagado");
      } else {
        return Result.fail<any>("Não foram encontrados users");
      }
    } catch (e) {
      throw e;
    }
  }
}
