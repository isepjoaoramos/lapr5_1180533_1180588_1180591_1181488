import { Service, Inject } from "typedi";
import config from "../../config";
import ILoginDTO from "../dto/ILoginDTO";
import { User } from "../domain/User";
import IUserRepo from "../repos/IRepos/IUserRepo";
import ILoginService from "./IServices/ILoginService";
import { Result } from "../core/logic/Result";
import { UserMap } from "../mappers/UserMap";
import { Console } from "console";

import * as bcrypt from "bcrypt-nodejs";
import IUserDTO from "../dto/IUserDTO";

@Service()
export default class LoginService implements ILoginService {
    constructor(@Inject(config.repos.user.name) private userRepo: IUserRepo) { }

    public async authenticateUser(loginDTO: ILoginDTO): Promise<Result<IUserDTO>> {

        const user = await this.userRepo.findUserByUsername(loginDTO.username);
        const foundUser = !!user;
        
        if (foundUser) {
            console.log(loginDTO.username);
            console.log(loginDTO.password);
            let checkPassword: boolean = bcrypt.compareSync(loginDTO.password, user.password);

            /* if(checkPassword){
                return Result.ok<ILoginDTO>({ username: user.username, password: user.password } as ILoginDTO);
             */
            if(checkPassword){
                
                return Result.ok<User>(user);
             
            }else{
                return Result.fail<User>("Password Inválida");
            }
        } else {
            return Result.fail<User>("User não existe !!!")
        }
    }
}
