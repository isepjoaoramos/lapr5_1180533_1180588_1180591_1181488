import { Service, Inject } from 'typedi';
import config from "../../config";


import { Result } from "../core/logic/Result";
import ILinhaService from './IServices/ILinhaService';
import ILinhaDTO from '../dto/ILinhaDTO';
import { Linha } from '../domain/linha';
import INoRepo from '../repos/IRepos/INoRepo';
import ILinhaRepo from '../repos/IRepos/ILinhaRepo';
import { LinhaMap } from '../mappers/LinhaMap';


@Service()
export default class LinhaService implements ILinhaService {
  constructor(
    @Inject(config.repos.linha.name) private linhaRepo: ILinhaRepo,
    @Inject(config.repos.no.name) private noRepo: INoRepo
  ) { }

  public async createLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>> {


    const noInicio2 = await this.noRepo.findByDomainId(linhaDTO.noInicio);
    const noFim2 = await this.noRepo.findByDomainId(linhaDTO.noFim);

    if (noInicio2 == null || noFim2 == null) {
      return Result.fail<ILinhaDTO>("ERROR: Os nós ainda não foram criados");
    }

    try {
      const linhaOrError = await Linha.create(linhaDTO);

      if (linhaOrError.isFailure) {
        return Result.fail<ILinhaDTO>(linhaOrError.errorValue());
      }

      const linhaResult = linhaOrError.getValue();

      await this.linhaRepo.save(linhaResult);

      const linhaDTOResult = LinhaMap.toDTO(linhaResult) as ILinhaDTO;
      return Result.ok<ILinhaDTO>(linhaDTOResult)
    } catch (e) {
      throw e;
    }
  }


  public async getAllLinhas(): Promise<Result<Array<ILinhaDTO>>> {

    const linhas2 = await this.linhaRepo.findAllLinhas();
    const found2 = !!linhas2;

    if (found2) {

      let linhasArrayDTO2 = new Array<ILinhaDTO>();

      //Convert from DomainModel to DTO
      for (let i = 0; i < linhas2.length; i++) {
        linhasArrayDTO2[i] = LinhaMap.toDTO(linhas2[i]);
      }

      return Result.ok<Array<ILinhaDTO>>(linhasArrayDTO2);
    } else {
      return Result.fail<Array<ILinhaDTO>>("Não foram encontradas linhas com o código introduzido");
    }
  }

  public async getLinhaByCod(codLinha: string): Promise<Result<ILinhaDTO>> {

    try {
      const linha1 = await this.linhaRepo.findByCodLinha(codLinha);
      const found1 = !!linha1;

      if (found1) {
        const linhaDTOResult = LinhaMap.toDTO(linha1) as ILinhaDTO;
        return Result.ok<ILinhaDTO>(linhaDTOResult);
      } else {
        return Result.fail<ILinhaDTO>("Não foram encontradas Linhas com o código de linha pretendido");
      }

    } catch (e) {
      throw e;
    }
  }

}
