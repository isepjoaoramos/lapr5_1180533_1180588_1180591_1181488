import { Service, Inject } from 'typedi';
import config from "../../config";
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';
import { TipoViatura } from "../domain/TipoViatura";
import { Result } from "../core/logic/Result";
import { TipoViaturaMap } from "../mappers/TipoViaturaMap";
import ITipoViaturaService from './IServices/ITipoViaturaService';
import ITipoViaturaRepo from '../repos/IRepos/ITipoViaturaRepo';


@Service()
export default class TipoViaturaService implements ITipoViaturaService {
  constructor(
    @Inject(config.repos.tipoViatura.name) private tipoViaturaRepo: ITipoViaturaRepo
  ) { }

  public async createTipoViatura(tipoViaturaDTO: ITipoViaturaDTO): Promise<Result<ITipoViaturaDTO>> {
    try {

      console.log("Cheguei ao service");
      const tipoViaturaOrError = await TipoViatura.create(tipoViaturaDTO);

      if (tipoViaturaOrError.isFailure) {
        console.log("Erro na criação de instância de modelo de dominio")
        return Result.fail<ITipoViaturaDTO>(tipoViaturaOrError.errorValue());
      } else {
        console.log("Sucesso")
        const tipoViaturaResult = tipoViaturaOrError.getValue();
        await this.tipoViaturaRepo.save(tipoViaturaResult);

        const tipoViaturaDTOResult = TipoViaturaMap.toDTO(tipoViaturaResult) as ITipoViaturaDTO;
        return Result.ok<ITipoViaturaDTO>(tipoViaturaDTOResult)
      }

    } catch (e) {
      throw e;
    }
  };


  public async getAllTiposViatura(): Promise<Result<Array<ITipoViaturaDTO>>> {

    const tiposViaturaFound = await this.tipoViaturaRepo.findAllTiposViatura();
    const hasFound = tiposViaturaFound;

    if (hasFound) {

      let tiposViaturaArrayDTO = new Array<ITipoViaturaDTO>();

      //Convert from DomainModel to DTO
      for (let i = 0; i < tiposViaturaFound.length; i++) {
        tiposViaturaArrayDTO[i] = TipoViaturaMap.toDTO(tiposViaturaFound[i]);
      }

      return Result.ok<Array<ITipoViaturaDTO>>(tiposViaturaArrayDTO);
    } else {
      return Result.fail<Array<ITipoViaturaDTO>>("Não foram encontradas linhas com o código introduzido");
    }
  };





  public async getTipoViaturaByCod(codTipoViatura: string): Promise<Result<ITipoViaturaDTO>> {

    try {
      const tipoViaturaFound = await this.tipoViaturaRepo.findByCodViatura(codTipoViatura);
      const hasFound = !!tipoViaturaFound;

      if (hasFound) {
        const tipoViaturaDTO = TipoViaturaMap.toDTO(tipoViaturaFound) as ITipoViaturaDTO;
        return Result.ok<ITipoViaturaDTO>(tipoViaturaDTO);
      } else {
        return Result.fail<ITipoViaturaDTO>("Não foram encontradas tipos de viatura com o código de linha pretendido");
      }

    } catch (e) {
      throw e;
    }
  }

}





