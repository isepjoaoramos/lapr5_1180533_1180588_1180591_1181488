import Container, { Service, Inject } from 'typedi';
import config from "../../config";
import IPercursoDTO from '../dto/IPercursoDTO';
import { Percurso } from "../domain/percurso";
import IPercursoRepo from '../repos/IRepos/IPercursoRepo';
import PercursoRepo from '../repos/percursoRepo';
import IPercursoService from './IServices/IPercursoService';
import { Result } from "../core/logic/Result";
import { PercursoMap } from "../mappers/PercursoMap";
import INoRepo from '../repos/IRepos/INoRepo';
import { Segmento } from '../domain/segmento';
import ISegmentoDTO from '../dto/ISegmentoDTO';
import { AbreviaturaNo } from '../domain/AbreviaturaNo';

@Service()
export default class PercursoService implements IPercursoService {
  constructor(
      @Inject(config.repos.percurso.name) private percursoRepo : IPercursoRepo,
      @Inject(config.repos.no.name) private noRepo : INoRepo
  ) {}

  public async createPercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>> {
    if(this.noRepo.findByDomainId(percursoDTO.noInicio) == null ||this.noRepo.findByDomainId(percursoDTO.noFim) == null){
      return Result.fail<IPercursoDTO>("ERROR: Os nós ainda não foram criados");
    }
    
    try {
      percursoDTO.descricao = this.setDescricaoBody(percursoDTO.segmentos);
      percursoDTO.distancia = this.setDistanciaBody(percursoDTO.segmentos);
      percursoDTO.duracao = this.setDuracaoBody(percursoDTO.segmentos);
      percursoDTO.noInicio = percursoDTO.segmentos[0].no1;
      percursoDTO.noFim=percursoDTO.segmentos[percursoDTO.segmentos.length-1].no2;

      const percursoOrError = await Percurso.create( percursoDTO );

      if (percursoOrError.isFailure) {
        return Result.fail<IPercursoDTO>(percursoOrError.errorValue());
      }

      const percursoResult = percursoOrError.getValue();

      await this.percursoRepo.save(percursoResult);

      const percursoDTOResult = PercursoMap.toDTO( percursoResult ) as IPercursoDTO;
      return Result.ok<IPercursoDTO>( percursoDTOResult )
    } catch (e) {
      throw e;
    }
  }

    public async getPercursos (linha: string): Promise<Result<Array<IPercursoDTO>>> {
     //var percursoRepo = Container.get( PercursoRepo );
     //console.log(linha);
     const percursos = await this.percursoRepo.findByLinha( linha );
     const found = !!percursos;

     let percursosDTO = new Array<IPercursoDTO>();

      //Convert from DomainModel to DTO
      for (let i = 0; i < percursos.length; i++) {
        percursosDTO[i] = PercursoMap.toDTO(percursos[i]);
      }

     if (found) {
       return Result.ok<Array<IPercursoDTO>>(percursosDTO)
     } else {
       console.log("Percursos ñ encontrados");
       return Result.fail<Array<IPercursoDTO>>("Couldn't find path by id=" + percursos.toString());
     }
   }

   public async getPercursoByDescricao (descricao: string): Promise<Result<Array<IPercursoDTO>>> {
    //var percursoRepo = Container.get( PercursoRepo );
    //console.log(linha);
    const percursos = await this.percursoRepo.findByDescricao( descricao );
    const found = !!percursos;

    let percursosDTO = new Array<IPercursoDTO>();

     //Convert from DomainModel to DTO
     for (let i = 0; i < percursos.length; i++) {
       percursosDTO[i] = PercursoMap.toDTO(percursos[i]);
     }

    if (found) {
      return Result.ok<Array<IPercursoDTO>>(percursosDTO)
    } else {
      console.log("Percursos ñ encontrados");
      return Result.fail<Array<IPercursoDTO>>("Couldn't find path by id=" + percursos.toString());
    }
  }


   public async getAllPercursos(): Promise<Result<Array<IPercursoDTO>>> {
    //var percursoRepo = Container.get( PercursoRepo );
    //console.log(linha);
    const percursos = await this.percursoRepo.findAllPercursos();
    const found = !!percursos;


    let percursosDTO = new Array<IPercursoDTO>();

      //Convert from DomainModel to DTO
      for (let i = 0; i < percursos.length; i++) {
        percursosDTO[i] = PercursoMap.toDTO(percursos[i]);
      }
    if (found) {
      return Result.ok<Array<IPercursoDTO>>(percursosDTO)
    } else {
      console.log("Percursos ñ encontrados");
      return Result.fail<Array<IPercursoDTO>>("Couldn't find path by id=" + percursos.toString());
    }
  }

public setDescricaoBody(segmentos: ISegmentoDTO[]): string{
  var descricao="";
  for(var i = 0; i < segmentos.length; i++){
    descricao += segmentos[i].no1 +">";
    }
    descricao += segmentos[segmentos.length-1].no2;
    return descricao;
  }

  public setDistanciaBody(segmentos: ISegmentoDTO[]): number{
  var distanciaTotal=0;
      for(var j = 0; j < segmentos.length; j++){
      distanciaTotal += (segmentos[j].distancia);
      }
      return distanciaTotal;
    }

    public setDuracaoBody(segmentos: ISegmentoDTO[]): number{
    var duracaoTotal=0;
      var descricao="";
      for(var i = 0; i < segmentos.length; i++){
      duracaoTotal += (segmentos[i].duracao);
      }
      return duracaoTotal;
  }

}
