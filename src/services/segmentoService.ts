import { Service, Inject } from 'typedi';
import config from "../../config";
import IPercursoDTO from '../dto/IPercursoDTO';
import { Percurso } from "../domain/percurso";
import IPercursoRepo from '../repos/IRepos/IPercursoRepo';
import IPercursoService from './IServices/IPercursoService';
import { Result } from "../core/logic/Result";
import { PercursoMap } from "../mappers/PercursoMap";
import ISegmentoService from './IServices/ISegmentoService';
import ISegmentoDTO from '../dto/ISegmentoDTO';
import { Segmento } from '../domain/segmento';
import { SegmentoMap } from '../mappers/SegmentoMap';

@Service()
export default class SegmentoService implements ISegmentoService {
  constructor(
      @Inject(config.repos.percurso.name) private percursoRepo : IPercursoRepo
  ) {}

  public async createSegmento(segmentoDTO: ISegmentoDTO): Promise<Result<ISegmentoDTO>> {
    try {

      const segmentoOrError = await Segmento.create( segmentoDTO );

      if (segmentoOrError.isFailure) {
        return Result.fail<ISegmentoDTO>(segmentoOrError.errorValue());
      }

      const segmentoResult = segmentoOrError.getValue();

     // await this.percursoRepo.save(segmentoResult);

      const segmentoDTOResult = SegmentoMap.toDTO( segmentoResult ) as ISegmentoDTO;
      return Result.ok<ISegmentoDTO>( segmentoDTOResult )
    } catch (e) {
      throw e;
    }
  }

  /*public async createSegmento(segmentoDTO: ISegmentoDTO): Promise<Result<ISegmentoDTO>> {
    try {

      const segmentoOrError = await Segmento.create( segmentoDTO );

      if (segmentoOrError.isFailure) {
        return Result.fail<ISegmentoDTO>(segmentoOrError.errorValue());
      }

      const percursoResult = percursoOrError.getValue();

      await this.percursoRepo.save(percursoResult);

      const percursoDTOResult = PercursoMap.toDTO( percursoResult ) as IPercursoDTO;
      return Result.ok<IPercursoDTO>( percursoDTOResult )
    } catch (e) {
      throw e;
    }
  }*/

}
