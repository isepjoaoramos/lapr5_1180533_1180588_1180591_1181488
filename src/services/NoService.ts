import { Service, Inject } from 'typedi';
import config from "../../config";
import INoDTO from '../dto/INoDTO';
import { No } from "../domain/no";
import INoRepo from '../repos/IRepos/INoRepo';
import INoService from './IServices/INoService';
import { Result } from "../core/logic/Result";
import { NoMap } from "../mappers/NoMap";

@Service()
export default class NoService implements INoService {
  constructor(
    @Inject(config.repos.no.name) private noRepo: INoRepo
  ) { }

  public async createNo(noDTO: INoDTO): Promise<Result<INoDTO>> {
    const noOrError = await No.create(noDTO);

    if (noOrError.isFailure) {
      return Result.fail<INoDTO>(noOrError.errorValue());
    }

    const noResult = noOrError.getValue();
    await this.noRepo.save(noResult);
    const noDTOResult = NoMap.toDTO(noResult) as INoDTO;
    return Result.ok<INoDTO>(noDTOResult)
  }

  /* public async getNosAbrevNome(reqbody): Promise<Result<Array<INoDTO>>> {

    try {
      const opcao = reqbody.opcao;

      if (opcao === '1') {
        const nos1 = await this.noRepo.findNosByAbreviat(reqbody.abreviatura);
        const found1 = !!nos1;   // !! converts nos to boolean

        if (found1) {

          let nosArrayDTO1 = new Array<INoDTO>();

          //Convert from DomainModel to DTO
          for (let i = 0; i < nos1.length; i++) {
            nosArrayDTO1[i] = NoMap.toDTO(nos1[i]);
          }
          
          return Result.ok<Array<INoDTO>>(nosArrayDTO1);
        } else {
          return Result.fail<Array<INoDTO>>("Não foram encontrados nós com a abreviatura introduzida");
        }

      }

      if (opcao === '2') {

        const nos2 = await this.noRepo.findNosByNome(reqbody.nome);
        const found2 = !!nos2;

        if (found2) {

          let nosArrayDTO2 = new Array<INoDTO>();

          //Convert from DomainModel to DTO
          for (let i = 0; i < nos2.length; i++) {
            nosArrayDTO2[i] = NoMap.toDTO(nos2[i]);
          }

          return Result.ok<Array<INoDTO>>(nosArrayDTO2);
        } else {
          return Result.fail<Array<INoDTO>>("Não foram encontrados nós com o nome introduzido");
        }

      }
        const nos3 = await this.noRepo.findNos();
        const found3 = !!nos3;

        if (found3) {

          let nosArrayDTO3 = new Array<INoDTO>();

          //Convert from DomainModel to DTO
          for (let i = 0; i < nos3.length; i++) {
            nosArrayDTO3[i] = NoMap.toDTO(nos3[i]);
          }

          return Result.ok<Array<INoDTO>>(nosArrayDTO3);
        } else {
          return Result.fail<Array<INoDTO>>("Não foram encontrados nós");
        }

    } catch (e) {
      throw e;
    }
  } */

  public async getNosAbrev(abreviatura: string): Promise<Result<Array<INoDTO>>> {

    try {


      const nos = await this.noRepo.findNosByAbreviat(abreviatura);
      let found1;
      if(nos==null){
         found1=false;  
      }else{
      found1 = !!nos;   // !! converts nos to boolean
      }

      if (found1) {

        let nosArrayDTO = new Array<INoDTO>();

        //Convert from DomainModel to DTO
        for (let i = 0; i < nos.length; i++) {
          nosArrayDTO[i] = NoMap.toDTO(nos[i]);
        }

        return Result.ok<Array<INoDTO>>(nosArrayDTO);
      } else {
        return Result.fail<Array<INoDTO>>("Não foram encontrados nós com a abreviatura introduzida");
      }

    } catch (e) {
      throw e;
    }
  }

  public async getNosNome(nome: string): Promise<Result<Array<INoDTO>>> {

    try {


      const nos1 = await this.noRepo.findNosByNome(nome);
      const found1 = !!nos1;   // !! converts nos to boolean
      
      console.log(found1);
      if (found1) {

        let nosArrayDTO1 = new Array<INoDTO>();

        //Convert from DomainModel to DTO
        for (let i = 0; i < nos1.length; i++) {
          nosArrayDTO1[i] = NoMap.toDTO(nos1[i]);
        }

        return Result.ok<Array<INoDTO>>(nosArrayDTO1);
      } else {
        return Result.fail<Array<INoDTO>>("Não foram encontrados nós com o nome introduzida");
      }

    } catch (e) {
      throw e;
    }
  }

  public async getAllNos(): Promise<Result<Array<INoDTO>>> {

    const nos3 = await this.noRepo.findAllNos();
    const found3 = !!nos3;

    if (found3) {

      let nosArrayDTO3 = new Array<INoDTO>();

      //Convert from DomainModel to DTO
      for (let i = 0; i < nos3.length; i++) {
        nosArrayDTO3[i] = NoMap.toDTO(nos3[i]);
      }

      return Result.ok<Array<INoDTO>>(nosArrayDTO3);
    } else {
      return Result.fail<Array<INoDTO>>("Não foram encontrados nós");
    }

  } catch(e) {
    throw e;
  }

}
