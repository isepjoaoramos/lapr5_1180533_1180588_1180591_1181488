import { Service, Inject } from 'typedi';
import config from "../../config";
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';
import { TipoTripulante } from "../domain/TipoTripulante";
import ITipoTripulanteRepo from '../repos/IRepos/ITipoTripulanteRepo';
import ITipoTripulanteService from './IServices/ITipoTripulanteService';
import { Result } from "../core/logic/Result";
import { TipoTripulanteMap } from "../mappers/TipoTripulanteMap";

@Service()
export default class TipoTripulanteService implements ITipoTripulanteService {
  constructor(
    @Inject(config.repos.tipoTripulante.name) private tipoTripulanteRepo: ITipoTripulanteRepo
  ) { }

  public async createTipoTripulante(tipoTripulanteDTO: ITipoTripulanteDTO): Promise<Result<ITipoTripulanteDTO>> {
    try {

      const tipoTripulanteOrError = await TipoTripulante.create(tipoTripulanteDTO);

      if (tipoTripulanteOrError.isFailure) {
        return Result.fail<ITipoTripulanteDTO>(tipoTripulanteOrError.errorValue());
      }

      const tipoTripulanteResult = tipoTripulanteOrError.getValue();

      await this.tipoTripulanteRepo.save(tipoTripulanteResult);

      const tipoTripulanteDTOResult = TipoTripulanteMap.toDTO(tipoTripulanteResult) as ITipoTripulanteDTO;
      return Result.ok<ITipoTripulanteDTO>(tipoTripulanteDTOResult)
    } catch (e) {
      throw e;
    }
  }


  public async getTiposByCod(codigo: string): Promise<Result<Array<ITipoTripulanteDTO>>> {

    try {

      const tipos1 = await this.tipoTripulanteRepo.findTiposByCod(codigo);
      const found1 = !!tipos1;   // !! converts nos to boolean

      if (found1) {

        let tiposArrayDTO1 = new Array<ITipoTripulanteDTO>();

        //Convert from DomainModel to DTO
        for (let i = 0; i < tipos1.length; i++) {
          tiposArrayDTO1[i] = TipoTripulanteMap.toDTO(tipos1[i]);
        }

        return Result.ok<Array<ITipoTripulanteDTO>>(tiposArrayDTO1);
      } else {
        return Result.fail<Array<ITipoTripulanteDTO>>("Não foram encontrados tipos com o código introduzido");
      }

    } catch (e) {
      throw e;
    }
  }


  public async getTiposByDescricao(descricao: string): Promise<Result<Array<ITipoTripulanteDTO>>> {

    try {


      const tipos1 = await this.tipoTripulanteRepo.findTiposByDescricao(descricao);
      const found1 = !!tipos1;   // !! converts nos to boolean

      console.log(found1);
      if (found1) {

        let tiposArrayDTO1 = new Array<ITipoTripulanteDTO>();

        //Convert from DomainModel to DTO
        for (let i = 0; i < tipos1.length; i++) {
          tiposArrayDTO1[i] = TipoTripulanteMap.toDTO(tipos1[i]);
        }

        return Result.ok<Array<ITipoTripulanteDTO>>(tiposArrayDTO1);
      } else {
        return Result.fail<Array<ITipoTripulanteDTO>>("Não foram encontrados tipos com a descrição introduzida");
      }

    } catch (e) {
      throw e;
    }
  }

  public async getAllTipos(): Promise<Result<Array<ITipoTripulanteDTO>>> {

    const tipos3 = await this.tipoTripulanteRepo.findAllTipos();
    const found3 = !!tipos3;

    if (found3) {

      let tiposArrayDTO3 = new Array<ITipoTripulanteDTO>();

      //Convert from DomainModel to DTO
      for (let i = 0; i < tipos3.length; i++) {
        tiposArrayDTO3[i] = TipoTripulanteMap.toDTO(tipos3[i]);
      }

      return Result.ok<Array<ITipoTripulanteDTO>>(tiposArrayDTO3);
    } else {
      return Result.fail<Array<ITipoTripulanteDTO>>("Não foram encontrados tipos de tripulante");
    }

  } catch(e) {
    throw e;
  }




}


