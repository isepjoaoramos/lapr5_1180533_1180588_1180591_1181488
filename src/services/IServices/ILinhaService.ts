import { Result } from "../../core/logic/Result";
import { Linha } from "../../domain/linha";
import ILinhaDTO from "../../dto/ILinhaDTO";

export default interface ILinhaService  {
  createLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>>;
  getLinhaByCod(any): Promise<Result<ILinhaDTO>>;
  getAllLinhas(): Promise<Result<Array<ILinhaDTO>>>;
}
