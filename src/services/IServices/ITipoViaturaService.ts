import { Result } from "../../core/logic/Result";
import ITipoViaturaDTO from "../../dto/ITipoViaturaDTO";

export default interface ITipoViaturaService  {
  createTipoViatura(tipoViaturaDTO: ITipoViaturaDTO): Promise<Result<ITipoViaturaDTO>>;
  getAllTiposViatura(): Promise<Result<Array<ITipoViaturaDTO>>>;
  getTipoViaturaByCod(string): Promise<Result<ITipoViaturaDTO>>;
}
