import { Result } from "../../core/logic/Result";
import ILoginDTO from "../../dto/ILoginDTO";

export default interface ILoginService {
  authenticateUser(loginDTO: ILoginDTO): Promise<Result<ILoginDTO>>;
 // generateToken(loginDTO: ILoginDTO): Promise<Result<ILoginDTO>>;
}
