import { Result } from "../../core/logic/Result";
import INoDTO from "../../dto/INoDTO";

export default interface INoService  {
  createNo(noDTO: INoDTO): Promise<Result<INoDTO>>;
  //getNosAbrevNome(any): Promise<Result<Array<INoDTO>>>;
  getNosAbrev(string): Promise<Result<Array<INoDTO>>>;
  getNosNome(string): Promise<Result<Array<INoDTO>>>;
  getAllNos(): Promise<Result<Array<INoDTO>>>;

}
