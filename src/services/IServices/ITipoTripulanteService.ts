import { Result } from "../../core/logic/Result";
import ITipoTripulanteDTO from "../../dto/ITipoTripulanteDTO";

export default interface ITipoTripulanteService {
  createTipoTripulante(tipoTripulanteDTO: ITipoTripulanteDTO): Promise<Result<ITipoTripulanteDTO>>;
  getAllTipos(): Promise<Result<Array<ITipoTripulanteDTO>>>;
  getTiposByCod(string): Promise<Result<Array<ITipoTripulanteDTO>>>;
  getTiposByDescricao(string): Promise<Result<Array<ITipoTripulanteDTO>>>;
}
