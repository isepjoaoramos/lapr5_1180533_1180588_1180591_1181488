import { Result } from "../../core/logic/Result";
import ISegmentoDTO from "../../dto/ISegmentoDTO";

export default interface ISegmentoService  {
  createSegmento(segmentoDTO: ISegmentoDTO): Promise<Result<ISegmentoDTO>>;
}
