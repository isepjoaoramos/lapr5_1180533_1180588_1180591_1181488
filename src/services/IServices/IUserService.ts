import { Result } from "../../core/logic/Result";
import IUserDTO from "../../dto/IUserDTO";

export default interface IUserService {
  //registerUser(userDTO: IUserDTO): Promise<Result<IUserDTO>>;
  registerCliente(userDTO: IUserDTO): Promise<Result<IUserDTO>>;
  registerDataAdmin(userDTO: IUserDTO): Promise<Result<IUserDTO>>;
  registerGestor(userDTO: IUserDTO): Promise<Result<IUserDTO>>;
  getAllUsers(): Promise<Result<Array<IUserDTO>>>;
  getUserByEmail(string): Promise<Result<IUserDTO>>;
  getUserByUsername(string): Promise<Result<IUserDTO>>;
  getUserByUsernameAndDelete(string): Promise<Result<any>>;
}
