import { Result } from "../../core/logic/Result";
import { Percurso } from "../../domain/percurso";
import IPercursoDTO from "../../dto/IPercursoDTO";

export default interface IPercursoService  {
  createPercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>>;
  getPercursos(linha: string): Promise<Result<Array<IPercursoDTO>>>;
  getAllPercursos(): Promise<Result<Array<IPercursoDTO>>>
  getPercursoByDescricao(descricao: string): Promise<Result<Array<IPercursoDTO>>>
}
