import { Request, Response, NextFunction } from 'express';

export default interface ILinhaController  {
  createLinha(req: Request, res: Response, next: NextFunction);
  searchLinhas(req: Request, res: Response, next: NextFunction);
}