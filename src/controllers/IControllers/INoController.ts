import { Request, Response, NextFunction } from 'express';

export default interface INoController {
    createNo(req: Request,res: Response, next: NextFunction);
    getNos(req: Request, res: Response, next: NextFunction);
}