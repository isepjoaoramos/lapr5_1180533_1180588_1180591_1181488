import { Request, Response, NextFunction } from 'express';

export default interface ILoginController {
    authenticateUser(req: Request,res: Response, next: NextFunction);
}
