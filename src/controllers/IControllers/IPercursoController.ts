import { Request, Response, NextFunction } from 'express';

export default interface IPercursoController  {
  createPercurso(req: Request, res: Response, next: NextFunction);
  getPercursos(req: Request, res: Response, next: NextFunction);
}