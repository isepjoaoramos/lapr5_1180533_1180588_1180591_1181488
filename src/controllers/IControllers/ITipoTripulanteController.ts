import { Request, Response, NextFunction } from 'express';

export default interface ITipoTripulanteController {
    createTipoTripulante(req: Request,res: Response, next: NextFunction);
    getTipoTripulante(req: Request, res: Response, next: NextFunction);
}