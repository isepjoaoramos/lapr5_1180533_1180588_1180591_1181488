import { Request, Response, NextFunction } from 'express';

export default interface IImportacaoGlxController {
    importEntities(req: Request, res: Response, next:NextFunction);
}