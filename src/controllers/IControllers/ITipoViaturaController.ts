import { Request, Response, NextFunction } from 'express';

export default interface ITipoViaturaController  {
  createTipoViatura(req: Request, res: Response, next: NextFunction);
  searchTipoViatura(req: Request, res: Response, next: NextFunction);
}