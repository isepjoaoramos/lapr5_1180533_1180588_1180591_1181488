import { Request, Response, NextFunction } from 'express';

export default interface IUserController {
    registerCliente(req: Request,res: Response, next: NextFunction);
    registerDataAdmin(req: Request,res: Response, next: NextFunction);
    registerGestor(req: Request,res: Response, next: NextFunction);
    getUser(req: Request, res: Response, next: NextFunction);
    deleteUser(req: Request, res: Response, next: NextFunction);
} 