import { Request, Response, NextFunction } from "express";
import { Inject } from "typedi";
import config from "../../config";
import jwt from "jsonwebtoken";

import IUserController from "./IControllers/IUserController";

import IUserService from "../services/IServices/IUserService";
import IUserDTO from "../dto/IUserDTO";

import { Result } from "../core/logic/Result";

export default class UserController
  implements IUserController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.user.name) private userServiceInstance: IUserService
  ) {}

  public async registerCliente(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const userOrError = (await this.userServiceInstance.registerCliente(
        req.body as IUserDTO
      )) as Result<IUserDTO>;

      if (userOrError.isFailure) {
        return res.status(402).send(userOrError.error);
      }

      const userDTO = userOrError.getValue();
      return res.status(201).json(userDTO);
    } catch (e) {
      return next(e);
    }
  }

  public async registerDataAdmin(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const userOrError = (await this.userServiceInstance.registerDataAdmin(
        req.body as IUserDTO
      )) as Result<IUserDTO>;

      if (userOrError.isFailure) {
        return res.status(402).send(userOrError.error);
      }

      const userDTO = userOrError.getValue();
      return res.status(201).json(userDTO);
    } catch (e) {
      return next(e);
    }
  }

  public async registerGestor(req: Request, res: Response, next: NextFunction) {
    try {
      const userOrError = (await this.userServiceInstance.registerGestor(
        req.body as IUserDTO
      )) as Result<IUserDTO>;

      if (userOrError.isFailure) {
        return res.status(402).send(userOrError.error);
      }

      const userDTO = userOrError.getValue();
      return res.status(201).json(userDTO);
    } catch (e) {
      return next(e);
    }
  }

  public async getUser(req: Request, res: Response, next: NextFunction) {
    try {
      let email = req.query.email;
      let username = req.query.username;

      if (email == null && username != null) {
        let usersOrError = (await this.userServiceInstance.getUserByUsername(
          username
        )) as Result<IUserDTO>;
        if (usersOrError.isFailure) {
          return res.status(402).send(usersOrError.error);
        }

        let nosDTO = usersOrError.getValue();
        return res.status(201).json(nosDTO);
      }
      if (email != null && username == null) {
        let usersOrError = (await this.userServiceInstance.getUserByEmail(
          email
        )) as Result<IUserDTO>;
        if (usersOrError.isFailure) {
          return res.status(402).send(usersOrError.error);
        }

        let usersDTO = usersOrError.getValue();
        return res.status(201).json(usersDTO);
      } else {
        let usersOrError = (await this.userServiceInstance.getAllUsers()) as Result<
          Array<IUserDTO>
        >;
        if (usersOrError.isFailure) {
          return res.status(402).send(usersOrError.error);
        }
        let usersDTO = usersOrError.getValue();
        return res.status(201).json(usersDTO);
      }
    } catch (e) {
      return next(e);
    }
  }
  public async deleteUser(req: Request, res: Response, next: NextFunction) {
    try {
      let username = req.params.username;
      console.log("usernameeee:" + username);
      if (username != null) {
        let usersOrError = (await this.userServiceInstance.getUserByUsernameAndDelete(
          username
        )) as Result<any>;
        if (usersOrError.isFailure) {
          return res.status(402).send(usersOrError.error);
        }
        return res.status(201).json(usersOrError);
      }
    } catch (e) {
      return next(e);
    }
  }
}
