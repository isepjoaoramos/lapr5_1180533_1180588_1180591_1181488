import { Request, Response, NextFunction } from 'express';
import { Inject } from 'typedi';
import config from "../../config";

import ITipoViaturaController from "./IControllers/ITipoViaturaController";
import ITipoViaturaService from '../services/IServices/ITipoViaturaService';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';

import { Result } from "../core/logic/Result";

export default class TipoViaturaController implements ITipoViaturaController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.tipoViatura.name) private tipoViaturaServiceInstance: ITipoViaturaService
  ) { }

  public async createTipoViatura(req: Request, res: Response, next: NextFunction) {
    try {
      console.log(req.body as ITipoViaturaDTO);
      const tipoViaturaOrError = await this.tipoViaturaServiceInstance.createTipoViatura(req.body as ITipoViaturaDTO) as Result<ITipoViaturaDTO>;

      if (tipoViaturaOrError.isFailure) {
        return res.status(402).send();
      }

      const tipoViaturaDTO = tipoViaturaOrError.getValue();
      return res.status(201).json(tipoViaturaDTO);
    }
    catch (e) {
      return next(e);
    }
  };


  public async searchTipoViatura(req: Request, res: Response, next: NextFunction) {
    try {

      let codTipoViatura = req.query.codTipoViatura;

      if (codTipoViatura == null) {

        let tiposViaturaOrError = await this.tipoViaturaServiceInstance.getAllTiposViatura() as Result<Array<ITipoViaturaDTO>>;


        if (tiposViaturaOrError.isFailure) {
          return res.status(404).send(tiposViaturaOrError.error);
        }

        let tiposViaturaDTO = tiposViaturaOrError.getValue();
        return res.status(200).json(tiposViaturaDTO);

      }

      let tipoViaturaByCodResult = await this.tipoViaturaServiceInstance.getTipoViaturaByCod(codTipoViatura) as Result<ITipoViaturaDTO>;

      if (tipoViaturaByCodResult.isFailure) {
        return res.status(404).send(null);
      }

      let tipoViaturaByCod = tipoViaturaByCodResult.getValue();
      return res.status(200).json(tipoViaturaByCod);

    }
    catch (e) {
      return next(e);
    }
  };



}