/* const { userInfo } = require('os');
const TipoTripulanteModel = require('../model/TipoTripulante');

exports.createTipoTripulante = (req, res) => {
    var tipoTripulante = new TipoTripulanteModel();
    tipoTripulante.id = req.body.id;
    tipoTripulante.descricao = req.body.descricao;
    tipoTripulante.save(function(err){
        if(err)
            return res.status(500).send("Não foi possível criar o tipo de Tripulante")
        res.json({message:"Tipo de Tripulante criado"});
    })
  
}
 */


import { Request, Response, NextFunction, request } from 'express';
import { Inject } from 'typedi';
import config from "../../config";

import INoController from "./IControllers/INoController";
import INoService from '../services/IServices/INoService';
import INoDTO from '../dto/INoDTO';

import { Result } from "../core/logic/Result";

export default class NoController implements INoController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.no.name) private noServiceInstance: INoService
  ) { }

  public async createNo(req: Request, res: Response, next: NextFunction) {
    try {
      const NoOrError = await this.noServiceInstance.createNo(req.body as INoDTO) as Result<INoDTO>;

      if (NoOrError.isFailure) {
        return res.status(402).send(NoOrError.error);
      }

      const NoDTO = NoOrError.getValue();
      return res.status(201).json(NoDTO);
    }
    catch (e) {
      return next(e);
    }
  };

  public async getNos(req: Request, res: Response, next: NextFunction) {
    try {

      let nome = req.query.nome;
      let abreviatura = req.query.abreviatura;

      if (nome == null && abreviatura != null) {

        let nosOrError = await this.noServiceInstance.getNosAbrev(abreviatura) as Result<Array<INoDTO>>;
        if (nosOrError.isFailure) {
          return res.status(402).send(nosOrError.error);
        }

        let nosDTO = nosOrError.getValue();
        return res.status(201).json(nosDTO);

      }
      if (abreviatura == null && nome != null) {

        let nosOrError = await this.noServiceInstance.getNosNome(nome) as Result<Array<INoDTO>>;
        if (nosOrError.isFailure) {
          return res.status(402).send(nosOrError.error);
        }

        let nosDTO = nosOrError.getValue();
        return res.status(201).json(nosDTO);

      }
      else {

        let nosOrError = await this.noServiceInstance.getAllNos() as Result<Array<INoDTO>>;
        if (nosOrError.isFailure) {
          return res.status(402).send(nosOrError.error);
        }

        let nosDTO = nosOrError.getValue();
        return res.status(201).json(nosDTO);

      }


    }
    catch (e) {
      return next(e);
    }
  };

}