import { Request, Response, NextFunction } from 'express';
import { Inject } from 'typedi';
import config from "../../config";

import { Result } from "../core/logic/Result";
import ILinhaDTO from '../dto/ILinhaDTO';
import ILinhaController from './IControllers/ILinhaController';
import ILinhaService from '../services/IServices/ILinhaService';
import { Linha } from '../domain/linha';


export default class LinhaController implements ILinhaController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.linha.name) private linhaServiceInstance: ILinhaService
  ) { }

  public async createLinha(req: Request, res: Response, next: NextFunction) {
    try {
      const linhaOrError = await this.linhaServiceInstance.createLinha(req.body as ILinhaDTO) as Result<ILinhaDTO>;

      if (linhaOrError.isFailure) {
        return res.status(402).send(linhaOrError.error);
      }

      const linhaDTO = linhaOrError.getValue();
      return res.status(201).json(linhaDTO);
    }
    catch (e) {
      return next(e);
    }
  };


  public async searchLinhas(req: Request, res: Response, next: NextFunction) {
    try {

      let codLinha = req.query.codLinha;

      if (codLinha == null) {
        let linhasOrError = await this.linhaServiceInstance.getAllLinhas() as Result<Array<ILinhaDTO>>;


        if (linhasOrError.isFailure) {
          return res.status(404).send(linhasOrError.error);
        }

        let linhasDTO = linhasOrError.getValue();
        return res.status(200).json(linhasDTO);

      }

      let linhasOrError2 = await this.linhaServiceInstance.getLinhaByCod(codLinha) as Result<ILinhaDTO>;

      if (linhasOrError2.isFailure) {
        return res.status(404).send(null);
      }

      let linhasDTO2 = linhasOrError2.getValue();
      return res.status(200).json(linhasDTO2);

    }
    catch (e) {
      return next(e);
    }
  };
}