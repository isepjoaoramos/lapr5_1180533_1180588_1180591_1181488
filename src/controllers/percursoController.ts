/*const { userInfo } = require('os');
const PercursoModel = require('../model/Percurso');

exports.createPercurso = (req, res) => {
    var percurso = new PercursoModel();
    percurso.distancia = req.body.distancia;
    percurso.descricao = req.body.descricao;
    percurso.duracao = req.body.duracao;
    percurso.noInicio = req.body.noInicio;
    percurso.noFim = req.body.noFim;
    percurso.save(function(err){
        if(err)
            return res.status(500).send("Não foi possível criar o percurso")
        res.json({message:"Percurso criado"});
    })
  
}*/

import { Request, Response, NextFunction } from 'express';
import { Inject } from 'typedi';
import config from "../../config";

import IPercursoController from "./IControllers/IPercursoController";
import IPercursoService from '../services/IServices/IPercursoService';
import IPercursoDTO from '../dto/IPercursoDTO';

import { Result } from "../core/logic/Result";
import ISegmentoService from '../services/IServices/ISegmentoService';
import SegmentoSchema from '../infrastructure/mongoDB/Schemas/SegmentoSchema';
import { Percurso } from '../domain/percurso';

export default class PercursoController implements IPercursoController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
      @Inject(config.services.percurso.name) private percursoServiceInstance : IPercursoService
  ) {}

  public async createPercurso(req: Request, res: Response, next: NextFunction) {
    
    try {
      

       const percursoOrError = await this.percursoServiceInstance.createPercurso((req.body) as IPercursoDTO) as Result<IPercursoDTO>;
  
      if (percursoOrError.isFailure) {
        return res.status(402).send(percursoOrError.error);
      }

      const percursoDTO = percursoOrError.getValue();    
      return res.status(201).json( percursoDTO );
    }
    catch (e) {
      return next(e);
    }

  }


  public async getPercursos (req: Request, res: Response, next: NextFunction){
    try{

      let linha = req.query.linha;
      let descricao = req.query.descricao;
      console.log(descricao)
      if(linha!=null){
        let percursos = await this.percursoServiceInstance.getPercursos(linha as string) as Result<Array<IPercursoDTO>>;
        if (percursos!= null)
        return res.status(201).json(percursos.getValue());
        else return res.status(401).json("Percursos não encontrados");

      }else if (descricao != null){
        let percursos = await this.percursoServiceInstance.getPercursoByDescricao(descricao as string) as Result<Array<IPercursoDTO>>;
        console.log(percursos)
        if (percursos!= null)
        return res.status(201).json(percursos.getValue());
        else return res.status(401).json("Percursos não encontrados");

      }
      else {
        let percursos2 = await this.percursoServiceInstance.getAllPercursos() as Result<Array<IPercursoDTO>>;
        if (percursos2!= null)
      return res.status(201).json(percursos2.getValue());
      else return res.status(401).json("Percursos não encontrados");
      }
    }
    catch (e){
      return next(e);
    }
  }
}