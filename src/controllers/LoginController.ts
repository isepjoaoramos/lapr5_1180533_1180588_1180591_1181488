import { Request, Response, NextFunction } from "express";
import { Inject } from "typedi";
import config from "../../config";

import * as jwt from "jsonwebtoken";

import ILoginController from "./IControllers/ILoginController";

import ILoginService from "../services/IServices/ILoginService";
import ILoginDTO from "../dto/ILoginDTO";


import { Result } from "../core/logic/Result";
import { User } from "../domain/User";
import IUserDTO from "../dto/IUserDTO";



export default class LoginController
    implements ILoginController /* TODO: extends ../core/infra/BaseController */ {
    constructor(
        @Inject(config.services.login.name) private loginService: ILoginService
    ) { }

    public async authenticateUser(req: Request, res: Response, next: NextFunction) {
        try {

            const userOrError = (await this.loginService.authenticateUser(req.body as ILoginDTO)) as Result<IUserDTO>;

            if(userOrError.isFailure){
                return res.status(401).send(userOrError.error);
            }

            if(userOrError.isSuccess){
                
                 const payload = {
                    user:userOrError.getValue().username,
                    tokenExp: Math.floor(Date.now() / 1000) + (60 * 60),  //1 hora
                    cliente:userOrError.getValue().cliente,
                    data_admin:userOrError.getValue().data_admin,
                    gestor:userOrError.getValue().gestor
                }; 
                console.log("passou")
                console.log("tokenExp payload= "+payload.tokenExp)
                const token = jwt.sign(payload, config.jwtSecret);
                console.log("token= "+token)
                 

                // base
                // const token = jwt.sign(userOrError.getValue().username, config.jwtSecret);

                return res.status(200).send({token: token});
            }
        } catch (e) {
            return next(e);
        }
    }
}
