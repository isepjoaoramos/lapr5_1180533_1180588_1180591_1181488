import { Request, Response, NextFunction } from 'express';
import { Inject } from 'typedi';
import config from "../../config";

import ITipoTripulanteController from "./IControllers/ITipoTripulanteController";
import ITipoTripulanteService from '../services/IServices/ITipoTripulanteService';
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';

import { Result } from "../core/logic/Result";
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';

export default class TipoTripulanteController implements ITipoTripulanteController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.tipoTripulante.name) private tipoTripulanteServiceInstance: ITipoTripulanteService
  ) { }

  public async createTipoTripulante(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoTripulanteOrError = await this.tipoTripulanteServiceInstance.createTipoTripulante(req.body as ITipoTripulanteDTO) as Result<ITipoTripulanteDTO>;

      if (tipoTripulanteOrError.isFailure) {
        return res.status(402).send(tipoTripulanteOrError.error);

      }

      const tipoTripulanteDTO = tipoTripulanteOrError.getValue();
      return res.status(201).json(tipoTripulanteDTO);
    }
    catch (e) {
      return next(e);
    }
  };

  public async getTipoTripulante(req: Request, res: Response, next: NextFunction) {
    try {

      let codigo = req.query.codigo;
      let descricao = req.query.descricao;

      if (codigo == null && descricao != null) {

        let tiposOrError = await this.tipoTripulanteServiceInstance.getTiposByDescricao(descricao) as Result<Array<ITipoTripulanteDTO>>;
        if (tiposOrError.isFailure) {
          return res.status(402).send(tiposOrError.error);
        }

        let nosDTO = tiposOrError.getValue();
        return res.status(201).json(nosDTO);

      }
      if (codigo != null && descricao == null) {

        let tiposOrError = await this.tipoTripulanteServiceInstance.getTiposByCod(codigo) as Result<Array<ITipoTripulanteDTO>>;
        if (tiposOrError.isFailure) {
          return res.status(402).send(tiposOrError.error);
        }

        let tiposDTO = tiposOrError.getValue();
        return res.status(201).json(tiposDTO);

      }
      else {

        let tiposOrError = await this.tipoTripulanteServiceInstance.getAllTipos() as Result<Array<ITipoTripulanteDTO>>;
        if (tiposOrError.isFailure) {
          return res.status(402).send(tiposOrError.error);
        }
        let tiposDTO = tiposOrError.getValue();
        return res.status(201).json(tiposDTO);
      }
    }
    catch (e) {
      return next(e);
    }

  }

}