const { DOMParser } = require('xmldom');
const fs = require('fs');

import  { Inject } from 'typedi';
import config from "../../config";

import { Cor } from '../domain/Cor';
import { Segmento } from '../domain/segmento';

import ILinhaDTO from '../dto/ILinhaDTO';
import INoDTO from "../dto/INoDTO";
import IPercursoDTO from '../dto/IPercursoDTO';

import IPercursoService from '../services/IServices/IPercursoService';
import ITipoViaturaService from '../services/IServices/ITipoViaturaService';
import IImportacaoGlxController from "./IControllers/IImportacaoGlxController";
import INoService from '../services/IServices/INoService';
import ILinhaService from '../services/IServices/ILinhaService';
import { NomeNo } from '../domain/NomeNo';

import { AbreviaturaNo } from '../domain/AbreviaturaNo';
import { CapacidadeNo } from '../domain/CapacidadeNo';
import { TempoMaxParagemNo } from '../domain/TempoMaxParagemNo';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';
import { CodTipoTripulante } from '../domain/CodTipoTripulante';
import { DescricaoTipoTripulante } from '../domain/DescricaoTipoTripulante';
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';
import ITipoViaturaRepo from '../repos/IRepos/ITipoViaturaRepo';
import ITipoTripulanteService from '../services/IServices/ITipoTripulanteService';
import { StreamTransportOptions } from 'winston/lib/winston/transports';
import { Request, NextFunction, Response } from 'express';
import { Distancia } from '../domain/distancia';
import { Duracao } from '../domain/duracao';
import { watchFile } from 'fs';
import ISegmentoDTO from '../dto/ISegmentoDTO';
import { keys } from 'lodash';
import { Result } from '../core/logic/Result';

const capacidadeDefault = 25;
const tempoMaxParagemDefault = 10;
const descricaoPercursoDefault = "default";
const restricoesLinhaDefault = "";
const custoPorQuilometroLinhaDefault = 0.12;
const descricaoTipoViaturaDefault = "tipoDefault";
var keysPercurso = [];

export default class ImportacaoGlxController implements IImportacaoGlxController /* TODO: extends ../core/infra/BaseController */ {
    constructor(
        @Inject(config.services.no.name) private noServiceInstance : INoService,
        @Inject(config.services.percurso.name) private percursoServiceInstance : IPercursoService,
        @Inject(config.services.linha.name) private linhaServiceInstance : ILinhaService,
        @Inject(config.services.tipoViatura.name) private tipoViaturaServiceInstance : ITipoViaturaService,
        @Inject(config.services.tipoTripulante.name) private tipoTripulanteServiceInstance : ITipoTripulanteService
    ) {}

    public async processaNos(listaNos : HTMLCollectionOf<Element>) {
        var sucessoOperacao = true;
        var length = listaNos.length;
        console.log('O controller da US1 conseguiu processar '+length+' nos. Vai tentar passá-los ao Service da US2.');
        for (var i = 0; i < length; ++i) {
            var node = listaNos[i];

            var nomeNo;
            var latitude;
            var longitude;
            var abreviaturaNo;
            var isEstacaoRecolhaNo : boolean;
            var isPontoRendicaoNo: boolean;
            var capacidadeNo: CapacidadeNo;
            var tempoMaxParagemNo: TempoMaxParagemNo;
            var modelo: string

            abreviaturaNo = node.getAttribute('ShortName');
            var noPromise = this.noServiceInstance.getNosAbrev(abreviaturaNo);

            if ((await noPromise).getValue.length > 0) { continue; }

            var key = node.getAttribute('key');
            nomeNo = node.getAttribute('Name');
            latitude = parseFloat((node.getAttribute('Latitude')));
            longitude = parseFloat(node.getAttribute('Longitude'));
            /*const props = {latitude: latitude, longitude: longitude};
            coordenadasNo = Coordenadas.create(props).getValue();*/

            var isEstacaoRecolhaXml = node.getAttribute('IsDepot');
            if (isEstacaoRecolhaXml == 'True') {
                isEstacaoRecolhaNo = true; 
            } else {
                isEstacaoRecolhaNo = false;
            }
            var isPontoRendicaoXml = node.getAttribute('IsReliefPoint');
            if (isPontoRendicaoXml == 'True') {
                isPontoRendicaoNo = true; 
            } else {
                isPontoRendicaoNo = false;
            }

            modelo="default";

            const noDTO:INoDTO = {
                nome: nomeNo,
                latitude: latitude,
                longitude: longitude,
                abreviatura: abreviaturaNo,
                isEstacaoRecolha : isEstacaoRecolhaNo,
                isPontoRendicao : isPontoRendicaoNo,
                capacidade : capacidadeDefault,
                tempoMaxParagem: tempoMaxParagemDefault,
                modelo: modelo
            };

            console.log("Vai ser agora passado um noDTO para o service");
            const noOrError = this.noServiceInstance.createNo(noDTO);
            keysPercurso.push([key,abreviaturaNo]);
            if ((await noOrError).isFailure) {
                sucessoOperacao = false;
            }
        }
        return sucessoOperacao;
    }

    public async processaPercursos(listaPercursos, listaLinePaths : HTMLCollectionOf<Element>) {
        var sucessoOperacao = true;
        var length = listaPercursos.length;
        console.log('O controller da US1 conseguiu processar '+length+' percursos. Vai tentar passá-los ao Service da US4.');
        for (var i = 0; i < length; ++i) {
            var percurso = listaPercursos[i];

            var orientacaoPercurso;
            var distanciaPercurso = 0;
            var duracaoPercurso = 0;
            var linhaPercurso;
            var noInicio;
            var noFim;
            var segmentos = new Array<ISegmentoDTO>();

            var pathNodes = percurso.getElementsByTagName('PathNode');

            var numeroNos = pathNodes.length;
            
            //console.log('O percurso '+i+' e composto por '+(numeroNos-1)+' segmentos');
            for (var j = 0; j < numeroNos-1; ++j) {
                var pathNode = pathNodes.item(j);

                var firstKey = pathNode.getAttribute('Node');
                var firstAbrev;

                var nextPathNode = pathNodes.item(j+1);
                var secondKey = nextPathNode.getAttribute('Node');
                var secondAbrev;

                var segmento : Segmento;
                // Vai buscar abreviatura do Nó com a key = pathNode.getAttribute('Node')
                var numeroNosGuardados = keysPercurso.length;
                for (var k = 0; k < numeroNosGuardados; ++k) {
                    var mKey = keysPercurso[k][0];
                    if (mKey == firstKey) {
                        firstAbrev = keysPercurso[k][1];
                        break;
                    }
                }

                for (var k = 0; k < numeroNosGuardados; ++k) {
                    var mKey = keysPercurso[k][0];
                    if (mKey == secondKey) {
                        secondAbrev = keysPercurso[k][1];
                        break;
                    }
                }
                if (j==0) { 
                    noInicio = firstAbrev; 
                } else if (j == numeroNos-2) { 
                    noFim = secondAbrev; 
                }

                var distance = parseInt(nextPathNode.getAttribute('Distance'));
                var duration = parseInt(nextPathNode.getAttribute('Duration'));
                var ordem = j+1;

                const segmentoDTO : ISegmentoDTO = {
                    no1 : firstAbrev, no2 : secondAbrev, distancia : distance, duracao : duration, ordem : ordem
                };
                segmentos.push(segmentoDTO);
                distanciaPercurso = distanciaPercurso + distance;
                duracaoPercurso = duracaoPercurso + duration;
            }
            
            var numberOfLinePaths = listaLinePaths.length;
            for (var k = 0; k < numberOfLinePaths; ++k) {
                var linePath = listaLinePaths.item(k);
                var linePathPath = linePath.getAttribute('Path');
                if (linePathPath == percurso.getAttribute('key')) {
                    orientacaoPercurso = linePath.getAttribute('Orientation');
                    let parentNode  = linePath.parentNode.parentNode as Element;
                    linhaPercurso = parentNode.getAttribute('Name');
                    break;
                }
            }

            var distancia = Distancia.create(distanciaPercurso).getValue();
            var duracao = Duracao.create(duracaoPercurso).getValue();
    
            const percursoDTO:IPercursoDTO = {
                orientacao : orientacaoPercurso, distancia : distanciaPercurso, descricao : descricaoPercursoDefault, duracao : duracaoPercurso,
                linha : linhaPercurso, noInicio : noInicio, noFim : noFim, segmentos : segmentos
            }

            console.log("Vai ser agora passado um percursoDTO para o service.");
            const percursoOrError = this.percursoServiceInstance.createPercurso(percursoDTO);
            if ((await percursoOrError).isFailure) {
                sucessoOperacao = false;
            }
        }
        return sucessoOperacao;
    }

    // Nao temos o codigo da linha no XML. Vamos considerar que este está representado no atributo Code de Line
    // Verificar o tipo de Cor no ILinhaDTO
    public async processaLinhas(listaLinhas, listaPercursos, listaNos : HTMLCollectionOf<Element>) {
        var sucessoOperacao = true;
        var length = listaLinhas.length;
        console.log('O controller da US1 conseguiu processar '+length+' linhas. Vai tentar passá-las ao Service da US3.');
        for (var i = 0; i < length; ++i) {
            var linha = listaLinhas[i];

            var linhaAtualKey = linha.getAttribute('key');

            var codLinha = linha.getAttribute('Code');
            var valida = this.linhaServiceInstance.getLinhaByCod(codLinha);
            
            if ((await valida).isSuccess){ continue; }
            
             
            var nomeLinha = linha.getAttribute('Name');
            var corXml = linha.getAttribute('Color');
            
            var corSplit1 = corXml.split("(")[1];
            var corSplit2 = corSplit1.split(")")[0];
            var cores = corSplit2.split(",");
            var red = cores[0];//parseInt(corXml.substring(4,6));
            var green = cores[1];//parseInt(corXml.substring(7,9));
            var blue = cores[2];//parseInt(corXml.substring(10,12))
            var noInicioLinha;
            var noFimLinha;
            
            var listaLinePaths = linha.getElementsByTagName('LinePath');
            var primeiroLinePath = listaLinePaths.item(0);
            var linePathKey = primeiroLinePath.getAttribute('Path');

            var numPercursos = listaPercursos.length
            for (var j = 0; j < numPercursos; ++j) {
                var path = listaPercursos.item(j);
                var pathKey = path.getAttribute('key');
                if (linePathKey == pathKey) {
                    var pathNodesList = path.getElementsByTagName('PathNode');
                    var pathFirstNodeKey = pathNodesList.item(0).getAttribute('Node');
                    var pathLastNodeKey = pathNodesList.item(pathNodesList.length-1).getAttribute('Node');
                    break;
                }
            }

            var numeroNos = keysPercurso.length;
            for (var j = 0; j < numeroNos; ++j) {
                if (keysPercurso[j][0] == pathFirstNodeKey) {
                    noInicioLinha = keysPercurso[j][1];
                } else if (keysPercurso[j][0] == pathLastNodeKey) {
                    noFimLinha = keysPercurso[j][1];
                }
            }

            const linhaDTO:ILinhaDTO = {
                codLinha: codLinha, nome : nomeLinha, vermelho : red, verde : green, azul : blue, noInicio : noInicioLinha, noFim : noFimLinha, restricoes : restricoesLinhaDefault
            };

            console.log("Vai ser agora passado um linhaDTO para o service");
            const linhaOrError = this.linhaServiceInstance.createLinha(linhaDTO);
            if ((await linhaOrError).isFailure) {
                sucessoOperacao = false;
            }
        }
        return sucessoOperacao;
    }

    public async processaTiposViatura(listaTiposViatura:HTMLCollectionOf<Element>) {
        var sucessoOperacao = true;
        var length = listaTiposViatura.length;
        console.log('O controller da US1 conseguiu processar '+length+' tipos de viatura. Vai tentar passá-los ao Service da US6.');
        for (var i = 0; i < length; ++i) {
            var tipoViatura = listaTiposViatura.item(i);
            
            var codTipoViatura : string;
            var descricao : string;
            var velocidade_media : number;
            var tipocombustivel : number;
            var autonomia : number;
            var consumo_medio : number;
            var emissoes : number;

            codTipoViatura = tipoViatura.getAttribute('Code');
            var valida = this.tipoViaturaServiceInstance.getTipoViaturaByCod(codTipoViatura);

            if ((await valida).isSuccess) { continue; }

            velocidade_media = parseInt(tipoViatura.getAttribute('AverageSpeed'));
            tipocombustivel = parseInt(tipoViatura.getAttribute('EnergySource'));
            autonomia = parseInt(tipoViatura.getAttribute('Autonomy'));
            consumo_medio = parseInt(tipoViatura.getAttribute('Consumption'));
            emissoes = parseInt(tipoViatura.getAttribute('Emissions'));

            const tipoViaturaDTO:ITipoViaturaDTO = {
                codTipoViatura:codTipoViatura, descricao:descricaoTipoViaturaDefault, velocidade_media:velocidade_media, 
                tipocombustivel:tipocombustivel, consumo_medio:consumo_medio, 
                custo_km:custoPorQuilometroLinhaDefault, autonomia:autonomia, emissoes:emissoes
            }

            console.log("Vai ser agora passado um tipoViaturaDTO para o service");
            const tipoViaturaOrError = this.tipoViaturaServiceInstance.createTipoViatura(tipoViaturaDTO);
            if ((await tipoViaturaOrError).isFailure) {
                sucessoOperacao = false;
                console.log(tipoViaturaDTO);
            }
        }
        return sucessoOperacao;
    }

    public async processaTiposTripulante(listaTiposTripulante:HTMLCollectionOf<Element>) {
        var sucessoOperacao = true;
        var length = listaTiposTripulante.length;
        console.log('O controller da US1 conseguiu processar '+length+' tipos de tripulante. Vai tentar passá-los ao Service da US5.');
        for (var i = 0; i < length; ++i) {
            var tipoTripulante = listaTiposTripulante.item(i);
            
            var codTipoTripulante = tipoTripulante.getAttribute('Code');
            var valida = this.tipoTripulanteServiceInstance.getTiposByCod(codTipoTripulante);

            if ((await valida).isSuccess) { continue; }

            var descricaoTipoTripulante = tipoTripulante.getAttribute('name');

            const tipoTripulanteDTO:ITipoTripulanteDTO = {
                codigo:codTipoTripulante, descricao:descricaoTipoTripulante
            }

            console.log("Vai ser agora passado um tipoTripulanteDTO para o service");
            const tipoTripulanteOrError = this.tipoTripulanteServiceInstance.createTipoTripulante(tipoTripulanteDTO);
            if ((await tipoTripulanteOrError).isFailure) {
                sucessoOperacao = false;
            }
        }
        return sucessoOperacao;
    }

    public async importEntities(req: Request, res: Response, next:NextFunction) {
        console.log("Entrei no metodo importEntities() !!!");
        // @ts-ignore
        if (!req.files) {
            res.send({
                status: 402,
                message: 'Nao foi feito nenhum file upload no pedido.'
            })
        } else {
            // @ts-ignore
            let glx = req.files.glx;

            const conteudo = fs.readFileSync(glx.name, 'utf8');

            let parser : DOMParser = new DOMParser();
            let xmlDOM = parser.parseFromString(conteudo, 'application/xml');
            
            var listaNos = xmlDOM.getElementsByTagName('Node');
            let listaPercursos = xmlDOM.getElementsByTagName('Path');
            let listaLinhas = xmlDOM.getElementsByTagName('Line');
            let listaTiposViatura =  xmlDOM.getElementsByTagName('VehicleType');
            let listaTiposTripulante = xmlDOM.getElementsByTagName('DriverType');
            let listaLinePaths = xmlDOM.getElementsByTagName('LinePath');

            // Processar entidades: Nos
            // <Capacities/><InformationPoints/><CrewTravelTimes> ???
            var processamentoNos = this.processaNos(listaNos);
            console.log('nos: '+(await processamentoNos).valueOf());

            // Processar entidades: Tipos de Viatura
                // Notas: codigo do tipo de viatura foi acrescentado ao xml (Code)
            var processamentoTipoViaturas = this.processaTiposViatura(listaTiposViatura);
            console.log('tipos viatura: '+(await processamentoTipoViaturas).valueOf());

            // Processar entidades: Tipos de Tripulante
                // Notas: -codigo do tipo de tripulante foi acrescentado ao xml (Code)
                //        -descricao foi considerada o atributo 'name'
            var processamentoTipoTripulantes = this.processaTiposTripulante(listaTiposTripulante);
            console.log('tipos tripulante: '+(await processamentoTipoTripulantes).valueOf());

            // Processar entidades: Percursos
            var processamentoPercursos = this.processaPercursos(listaPercursos, listaLinePaths);
            console.log('percursos: '+(await processamentoPercursos).valueOf());

            // Processar entidades: Linhas
                // Notas: -codigo do tipo de viatura foi acrescentado ao xml (Code)
                //        -verificar tipo de dados da cor de ILinhaDTO (ver se está tudo ok) 
            var processamentoLinhas = this.processaLinhas(listaLinhas, listaPercursos, listaNos);
            console.log('linhas: '+(await processamentoLinhas).valueOf());

            if (processamentoNos && processamentoTipoViaturas && processamentoTipoTripulantes && processamentoPercursos && processamentoLinhas) {
                res.send({
                    status: true,
                    message: 'Em principio esta tudo ok.'
                })
            } else {
                res.send({
                    status: false,
                    message: 'Ocorreu um problema na importacao.'
                })
            }
        }

        
    }

    
}